<?php
	echo generate_script_link([
		// './js/jquery.min.js',
		BASE_URL.'./js/jquery.js',
		BASE_URL.'./js/popper.min.js',
		BASE_URL.'./js/bootstrap.min.js',
		BASE_URL.'./js/aos.js',
		BASE_URL.'./js/jquery.form-validator.min.js',
		BASE_URL.'./js/parallax.min.js',
		BASE_URL.'./js/custom.js',
		BASE_URL.'./js/owl.carousel.min.js',
	])
?>