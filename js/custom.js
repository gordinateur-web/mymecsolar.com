
window.onload = function() {
  AOS.refresh();
  AOS.init({
    disable:'mobile',
    easing: 'ease-in-out',
    duration: 700,
    once:false,
  });

  var width=$(window).width();
  if (width > 767) {
    $('.breadcum').parallax_content();
    $('.parallax').parallax(); 
  }
}

$(document).ready(function() {

//active menu navigation
var loc = window.location.pathname;
var page = location.pathname.split('/').pop();
$('#mobile-navbar ul.navbar-active li  a[href*="'+page+'"]').addClass('active');
$('#mobile-navbar ul.navbar-nav li  a.project-active[href*="'+page+'"]').addClass('active');

//active menu navigation IF HAVING DROPDOWN
var loc = window.location.pathname;
var page = location.pathname.split('/').pop();
if(page=='') {
  page = 'index.php';
}

if($('a[href*="'+page+'"]').parent().parent().hasClass('dropdown-menu')) {
  $('a[href*="'+page+'"]').parents('.dropdown').find('a:first').addClass('active');
}
else {
  $('a[href*="'+page+'"]').addClass('active');
}

// search box animatation js
$('.social-link .search-icon').click(function() {
	$('.navbar-div .box-style').focus();
});

// //cross icon header navigation 

$('#mobile-menu-action').click(function() {
  if($('.navbar-mob').hasClass('open')) {
    $('.navbar-mob').removeClass('open');
    $('#menu-open-overlay').remove();
    $(this).removeClass('active');
  }
  else {
    $('.navbar-mob').addClass('open');
    $('body').append('<div id="menu-open-overlay" style="z-index:1;position:fixed;width:100%;height:100%;top:0;right:0"></div>');
    $(this).addClass('active');
  }
});

/*mobile menu close on click outside*/
$(document).on('click','#menu-open-overlay',function() {
  $('#mobile-menu-action').trigger('click');
});

 //header shrink js

 $(document).on("scroll", function(){
   if($(document).scrollTop() > 50) {
     $("header").addClass("shrink");
   }
   else 
   {
     $("header").removeClass("shrink");
   }
// <--------------- top arrow show on some scrolltop --------------------->
   if ($(document).scrollTop() > 3800) {
     $(".top-arrow").show();
   } else {
     $(".top-arrow").hide();
   }
 });

// <--------------- top arrow --------------------->
$(document).on('click','.top-arrow', function() {
  $("html, body").animate({ scrollTop: 0 }, 1500);
});

});

$(window).on( 'hashchange', function(e) {
  var hash=window.location.hash;
  var ele=$('.btn.btn-link');
  var ele_top=$('#accordion ');
  if(ele.length > 0) {
    $(ele).trigger('click');
    $("html, body").animate({ scrollTop: $(ele_top).offset().top - ($('.breadcum').height()) }, 800);
  }
  if($(' #accordion').hasClass('show')) {
    $('#accordion').removeClass('show');
  }
  $('.btn.btn-link').attr('aria-expanded','false');
  $(hash+' .btn.btn-link').attr('aria-expanded','true');
  $(hash+'-tab').addClass('show');
});


// from one page to another page 
var hash = window.location.hash;
if(hash!='') {
  $('html, body').animate({
    scrollTop: $(hash).offset().top - ($('header').height() - 30)
  }, 1000);
}

// smart sticky header 
;
(function(window, document, undefined) {
    'use strict';
    // Extend function
    function extend(a, b) {
        for (var key in b) {
            if (b.hasOwnProperty(key)) {
                a[key] = b[key];
            }
        }
        return a;
    }
    // Throttle function (https://bit.ly/1eJxOqL)
    function throttle(fn, threshhold, scope) {
        threshhold || (threshhold = 250);
        var previous, deferTimer;
        return function() {
            var context = scope || this,
                current = Date.now(),
                args = arguments;
            if (previous && current < previous + threshhold) {
                clearTimeout(deferTimer);
                deferTimer = setTimeout(function() {
                    previous = current;
                    fn.apply(context, args);
                }, threshhold);
            } else {
                previous = current;
                fn.apply(context, args);
            }
        };
    }
    // Class management functions
    function classReg(className) {
        return new RegExp('(^|\\s+)' + className + '(\\s+|$)');
    }
    function hasClass(el, cl) {
        return classReg(cl).test(el.className);
    }
    function addClass(el, cl) {
        if (!hasClass(el, cl)) {
            el.className = el.className + ' ' + cl;
        }
    }
    function removeClass(el, cl) {
        el.className = el.className.replace(classReg(cl), ' ');
    }
    // Main function definition
    function headsUp(selector, options) {
        this.selector = document.querySelector(selector);
        this.options = extend(this.defaults, options);
        this.init();
    }
    // Overridable defaults
    headsUp.prototype = {
        defaults: {
            delay: 300,
            sensitivity: 20
        },
        // Init function
        init: function(selector) {
            var self = this,
                options = self.options,
                selector = self.selector,
                oldScrollY = 0,
                winHeight;
            // Resize handler function
            function resizeHandler() {
                winHeight = window.innerHeight;
                return winHeight;
            }
            // Scroll handler function
            function scrollHandler() {
                // Scoped variables
                var newScrollY = window.pageYOffset,
                    docHeight = document.body.scrollHeight,
                    pastDelay = newScrollY > options.delay,
                    goingDown = newScrollY > oldScrollY,
                    fastEnough = newScrollY < oldScrollY - options.sensitivity,
                    rockBottom = newScrollY < 0 || newScrollY + winHeight >= docHeight;
                // Where the magic happens
                if (pastDelay && goingDown) {
                    addClass(selector, 'heads-up');
                    removeClass(selector, 'shrink');
                } else if (!goingDown && fastEnough && !rockBottom || !pastDelay) {
                    removeClass(selector, 'heads-up');
                    addClass(selector, 'shrink');
                }
                if (newScrollY < 50) {
                    removeClass(selector, 'shrink');
                }
                // Keep on keeping on
                oldScrollY = newScrollY;
            }
            // Attach listeners
            if (selector) {
                // Trigger initial resize
                resizeHandler();
                // Resize function listener
                window.addEventListener('resize', throttle(resizeHandler), false);
                // Scroll function listener
                window.addEventListener('scroll', throttle(scrollHandler, 100), false);
            }
        }
    };
    window.headsUp = headsUp;
})(window, document);
$(document).ready(function() {
    // Instantiate HeadsUp
    new headsUp('header');
});

/*Intersection Observer*/
$(function(){
    getLazyImageLoaded();
});

function fetchImage(url) {
    return new Promise(function(resolve, reject){
        var image = new Image();
        image.src = url;
        image.onload = resolve;
        image.onerror = reject;
    });
}

function preloadImage(image) {
    var src = image.dataset.src;
    console.log(image);
    if (!src) {
        return;
    }

    return fetchImage(src).then(function(){ applyImage(image, src); });
}
function loadImagesImmediately(images) {

    for (var i = 0; i < images.length; i++) { 
        var image = images[i];
        preloadImage(image);
    }
}
function disconnect() {
    if (!observer) {
        return;
    }

    observer.disconnect();
}

function onIntersection(entries) {
    if (imageCount === 0) {
        observer.disconnect();
    }

    for (var i = 0; i < entries.length; i++) { 
        var entry = entries[i];
        if (entry.intersectionRatio > 0) {
          imageCount--;

          observer.unobserve(entry.target);
          preloadImage(entry.target);
      }
  }
}

function applyImage(img, src) {
    // Prevent this from being lazy loaded a second time.
    img.classList.add('js-lazy-image--handled');
    img.src = src;
    img.classList.add('fade-in');
}

function getLazyImageLoaded(){
  
    var images = document.querySelectorAll('.lazy-image');
    var config = {
        // If the image gets within 50px in the Y axis, start the download.
        rootMargin: '50px 0px',
        threshold: 0.01
    };

    imageCount = images.length;

    // If we don't have support for intersection observer, loads the images immediately
    if (!('IntersectionObserver' in window)) {
        loadImagesImmediately(images);
    } else {

        // It is supported, load the images

        observer = new IntersectionObserver(onIntersection, config);

        // foreach() is not supported in IE
        for (var i = 0; i < images.length; i++) {
            var image = images[i];
            if (image.classList.contains('js-lazy-image--handled')) {
                continue;
            }
            observer.observe(image);
        }
    }
}
$(document).on('keypress','.char-field',function(evt) {
    evt = (evt) ? evt : window.event;
    var charCode = (evt.which) ? evt.which : evt.keyCode;
    if(!((charCode == 8) || (charCode == 32) || (charCode == 46) || (charCode >= 35 && charCode <= 40) || (charCode >= 65 && charCode <= 90) || (charCode >= 97 && charCode <= 122))) {
        return false;
    }
    return true;
});
