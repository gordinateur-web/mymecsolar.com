       
<?php 
    require_once("config.php"); 
    
  $form=new Form();
  $form_model=array();

  $old_data=$CI->session->flashdata('old_data',$_POST);

  if(!empty($old_data)) {
    $form_model=$old_data;  
  }  
?>
<footer>
  <div class="footer">
    <div class="container">
      <div class="div-main">
        <div class="section section1">
          <h1>Quick Links</h1>
          <ul>
            <li><a href="<?php echo BASE_URL.'index.php';?>" title="Home">Home</a></li>
            <li><a href="<?php echo BASE_URL.'about.php';?>" title="About">About</a></li>
            <li><a href="<?php echo BASE_URL.'expertise.php';?>" title="Expertise">Expertise</a></li>
            <li><a href="<?php echo BASE_URL.'gallery.php';?>" title="Gallery">Gallery</a></li>
            <li><a href="<?php echo BASE_URL.'clients.php';?>" title="Clients">Clients</a></li>
          </ul>  
        </div>   
        <div class="section section2">
          <h1>Products</h1>
          <ul>
            <li><a href="<?php echo BASE_URL.'product/pump-motor';?>" title="Pump">Pump & Motor</a></li>
            <li><a href="<?php echo BASE_URL.'product/solar-pump-mppt-controller';?>" title="Controller">Controller</a></li>
            <li><a href="<?php echo BASE_URL.'product/remote-monitoring-system-rms';?>" title="Motor">Remote Monitoring System</a></li>
          </ul>
        </div>  

        <div class="section section3">
          <h1>About</h1> 
          <ul>

            <li><a href="<?php echo BASE_URL.'about.php#overview';?>" title="Overview" >Overview</a></li>
            <li><a href="<?php echo BASE_URL.'about.php#vision-mission';?>" title="Vision" >Vision</a></li>
            <li><a href="<?php echo BASE_URL.'about.php#vision-mission';?>" title="Mission" >Mission</a></li>
            <li><a href="<?php echo BASE_URL.'about.php#vision-mission';?>" title="Values" >Values</a></li>
            <li><a href="<?php echo BASE_URL.'about.php#leadership';?>" title="Leadership" >Leadership</a></li>
          </ul>        
        </div> 

        <div class="section section4">
          <h1>Contact</h1>
          <div class="address">
            <span class="icon-placeholder"></span>
            <p class="f_light"><b>Mercury Electronics Corporation , </br> Mecwin Technologies India Pvt.Ltd.</b>
              <br>Survey No.91/1,Kanakadasa Layout, Seegehalli, Magadi road, Bangalore 560091</p> 
          </div>
          <div class="address">
            <span class="icon-153857 call"></span>
            <p><a href="tel:+919741514646 " title="Call">+919741514646 </a></p> 
          </div><div class="address">
            <span class="icon-213381"></span>
            <p><a href="mailto:shiva@mymecindia.com" title="Mail">shiva@mymecindia.com</a></p> 
          </div>                 
        </div>

        <div class="section section5">
          <div class="social">
            <a href="https://www.facebook.com/mymecindia" target="_blank"><span class="icon-facebook-logo-outline"></span></a>
            <a href="https://www.linkedin.com/in/mercury-electronic-corporation-b76280176" target="_blank"><span class="icon-linkedin-social-outline-logotype"></span></a>
            
          </div>
          <img src="<?php echo BASE_URL.'images/white-logo.png';?>" class="img-fluid" alt="white-logo">
        </div>
        <div class="section section6">
          <a href="<?php echo BASE_URL.'blogs.php';?>" title="Blog">Blog</a>
          <a href="<?php echo BASE_URL.'career.php';?>" title="Career">Career</a>
         
          <a href="<?php echo BASE_URL.'team.php';?>" title="Team">Team</a>
          <a href="<?php echo BASE_URL.'news-&-media.php';?>" title="News & Media">News & Media</a>
          <a href="images/Company-Brochure.pdf"  title="Download" target="_blank">Download</a>
          <a href="<?php echo BASE_URL.'faqs.php';?>" title="FAQ’s">FAQ’s</a>
          <a href="<?php echo BASE_URL.'troubleshooting.php';?>" title="Trouble Shooting  & Service Center">Trouble Shooting  & Service Center</a>
        </div>
        <div class="clearfix"></div>
      </div>
    </div>

    <div class="footer-bottom">
      <div class="container">
        <div class="row">
          <div class="col-lg-4 col-md-12 col-12">
            <span class="foot-right dark-grey font_regular" > All Rights Reserved. © MEC 2020 </span>
            <span>|</span> 
            <a href="<?php echo BASE_URL.'sitemap.php';?>">Sitemap</a> 
          </div>
          <div class="col-lg-5 col-md-12 col-12">
            <a href="<?php echo BASE_URL.'quality-policy.php';?>" class="font_regular">Quality Policy </a>
            <span>|</span> 
            <a href="<?php echo BASE_URL.'terms-condition.php';?>" class="font_regular">Terms & Condition </a>
            <span>|</span> 
            <a href="<?php echo BASE_URL.'privacy-policy.php';?>" class="font_regular">Privacy Policy</a>
            <span>|</span>
            <a href="<?php echo BASE_URL.'disclaimer.php';?>" class="font_regular">Disclaimer</a>
          </div>
          <div class="col-lg-3 col-md 12 col-12">
            <span class="foot-right pull-right dark-grey font_regular right-section" > Design & Developed by<a class="dark-grey" href="https://www.gordinateur.com/" title="G-Ordinateur" target="_blank" style="margin-left: 5px;">G-Ordinateur</a></span> 
          </div>
        </div>
      </div>       
    </div>
  </div>
</footer>

 <!-- top arrow -->
<a href="#" class="top-arrow"><span class="icon-1256495"></span></a>
<?php include("script.php"); ?> 
<?php include("show_msg.php"); ?>
 <script type="text/javascript" src="js/jquery.form-validator.min.js"></script>
 
<script src="https://unpkg.com/aos@next/dist/aos.js"></script>
<link rel="stylesheet" href="https://unpkg.com/aos@next/dist/aos.css" />
<!-- Request a Quote -->
<div class="modal fade request-quote" id="request-quote" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
  <div class="modal-dialog modal-dialog-centered" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title text-center f_light gray" id="exampleModalLongTitle"><span class="icon-1183875"></span>Request a Quote </h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
       <!--  <form action="form_send.php"  class="contact-validation form_label_stly" method="post">
          <input type="hidden" name="token" value="<?php //echo $token; ?>"> -->
          <?php 
          echo $CI->form->form_model($form_model,BASE_URL_ADMIN.'custom_content/content/save_content_data',array('name'=>'quote_form_mymecsolar','class'=>'form-validation form_label_stly','method'=>'post','enctype'=>'multipart/form-data'));
          ?>
          <input type="hidden" name="content_type_section_id" value=""/>
          <input type="hidden" name="section_content_parent" value=""/>
          <input type="hidden" name="content_type_id" value="4"/>
          <div class="row">
            <div class="col-lg-6 col-md-6">
              <div class="form-group">
                <label class="hide_label gray f_book">Name</label>
               <input type="text" name="name_4[]" class="form-control char-field" data-validation="required">
              </div>
            </div>
            <div class="col-lg-6 col-md-6">
              <div class="form-group email">
                <label class="hide_label gray f_book">Email</label>
               <input type="email" name="email_4[]" class="form-control" data-validation="required">
              </div>
            </div>
            <div class="col-lg-6 col-md-6">
              <div class="form-group phone">
                <label class="hide_label gray f_book">Contact</label>
                  <input type="text" name="phone_4[]" class="form-control number-field"data-validation="required" maxlength="10">
              </div>
            </div>
            <div class="col-lg-6 col-md-6">
              <div class="form-group phone">
                <label class="hide_label gray f_book">Select Product</label><br>
                <select id="product" name="product_4[]" data-validation="required">
                  <option value="Complete System">Complete System</option>
                  <option value="Submersible Motor">Submersible Motor</option>
                  <option value="Surface Motor">Surface Motor</option>
                  <option value="Controller">Controller</option>
                </select><br>
              </div>
            </div>
            <div class="col-lg-12">
              <button type="submit" class="contact-btn common-btn f_medium">Submit <span class="icon-go-back-left-arrow"></span></button>
            </div>
          </div>
        </form> 
      </div>
    </div>
  </div>
</div>

<!-- search modal  -->
<div class="modal fade full-width search-modal" tabindex="-1" role="dialog" aria-hidden="true">
  <div class="modal-dialog full-width bg-transparent">
    <div class="modal-content full-width bg-transparent">
      <div class="search-wrapp container">
        <button type="button" class="close-btn" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">×</span>
        </button>
        <form>
          <div class="form-group">
            <input type="search" class="form-control input-box" placeholder="Search here" name="q">
            <button type="submit" class="submit-btn"><span class="icon-1379714"></span></button>
          </div>
        </form>
      </div>
    </div>
  </div>
</div>
<!-- search modal  -->
<script>
  $('#request,#request_header').click(function(){
    var option = $('.breadcum_container .product_heading').text();
    var check = $('#product').val();
    if(check == 'Select product'){
    }else if(check != option){
      if(option == ''){
        option = 'Select product';
      }
      if(option == 'Select product'){
        $('#product').prepend('<option value="" selected>'+option+'</option>');
      }else{
        $('#product').prepend('<option value="'+option+'">'+option+'</option>');
        $('#product').val(option);
      }
    }
  });

  // form validation
  $(document).ready(function(){
    $.validate({
      form: ".form-validation",
    });
  });
</script>