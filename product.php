<?php 
    require_once("config.php"); 

    $product_fields=$CI->config->item('product_fields');

    $url_title=$CI->uri->segment(2);

    if(!empty($url_title))
        $filter_product['content.url_title']=$url_title;

    $filter_product['content.content_type_id']=$product_fields['content_type_id'];
    $product_list=$CI->content_model->get_published_content($filter_product);
    if(empty($product_list)){
      redirect(BASE_URL.'404-error.php');
    }

    $filter_next_product['LIMIT']=3;
    $filter_next_product['WHERE_NOT_IN']= array('content.content_id'=>$product_list[0]['content']['content_id']);
    // $filter_next_product['content.content_id <']=$product_list[0]['content']['content_id'];
    $filter_next_product['content.content_type_id']=$product_fields['content_type_id'];
    $filter_next_product['content.content_published']=1;
    $filter_next_product['content.deleted_at']=NULL;
    $filter_next_product['WHERE'][]="(content.content_status is null or content.content_status='Approved')";
    $next_product_list=$CI->content_model->get_content_with_value($filter_next_product);
?>
<!DOCTYPE html>
<html lang="en">
    <head>
        <?php include("head.php"); ?>
        <title>Products | Mymecsolar</title>
        <meta  name="keywords" content="Pump & Motor, Controller , Remote Monitoring System">
  <meta  name="description" content="MEC has pioneered, innovated and excelled in the engineering and manufacturing of solar powered water pumping. We design, develop and manufacture the widest range of solar pumps for any company. Mechanical, electronic and software design is all in-house with a specialized team that have been working in solar pumping for 10 years.">
    </head>
    <body  data-spy="scroll" data-target="#list-example" data-offset="130">
        <?php include("header.php"); ?>
        <section class="breadcum">
            <img src="<?php echo BASE_URL.'images/product-breadcum.jpg';?>" class="img-fluid w-100" alt="about-breadcum">
            <?php foreach ($product_list as $key => $product_row) { ?>
            <div class="container breadcum_container">
                <h1 class="white f_light product_heading"><?php echo $product_row['content']['content_title']; ?></h1>
                <a href="<?php echo BASE_URL.'index.php';?>" class="white f_light" title="Home">Home</a>
                <span class="icon-1256495 white"></span>
                <span class="white f_medium">Products</span>
                <span class="icon-1256495 white"></span>
                <span class="white f_medium"><?php echo $product_row['content']['content_title']; ?></span>
            </div>
            <?php } ?>
        </section>
        <section class="product-main" data-toggle="affix">
            <div id="list-example" class="list-group">
                <a class="list-group-item list-group-item-action f_medium active" href="#overview">Overview</a>
            <?php if(!empty($product_row[$product_fields['features_description']][0]['content_value'])){ ?> 
                <a class="list-group-item list-group-item-action f_medium" href="#features">Features</a>
            <?php } ?>    
            <?php if(!empty($product_row[$product_fields['specifications_description']][0]['content_value'])){ ?> 
                <a class="list-group-item list-group-item-action f_medium" href="#specification"> Specification</a> 
            <?php } ?>
            </div>
            <div  class="scrollspy-example">
                <!-- overview -->
                <div class="overview overview-rms" id="overview">
                    <div class="container">
                        <div class="row">
                            <div class="col-lg-6 col-md-12">
                                <div class="img-wrapp">
                                    <img src="<?php echo base_url().$product_row[$product_fields['overview_image']][0]['media_path']; ?>" class="img-fluid" alt="product-overview">
                                </div>
                            </div>
                            <div class="col-lg-6 col-md-12">
                                <div class="overview-right">
                                    <h1 class="f_light heading" data-aos="fade-up" data-aos-delay="100">Overview</h1>
                                    <?php echo str_replace('<p>&nbsp;</p>','',$product_row[$product_fields['overview_description']][0]['content_value']); ?>
                                    <div class="btn-div">
                                        <a href="<?php echo BASE_URL.'contact.php';?>" class="common-btn"><span class="icon-phone-call"></span> Contact Us <span class="icon-go-back-left-arrow"></span></a>
                                        <a href="" class="common-btn" data-toggle="modal" data-target="#request-quote" id="request"><span class="icon-1183875"></span>Request a Quote <span class="icon-go-back-left-arrow"></span></a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <?php if(!empty($product_row[$product_fields['features_description']][0]['content_value'])){ ?>
                <!-- Features -->
                <div class="features features-rms" id="features">
                    <div class="container">
                        <h1 class="heading text-center f_light" data-aos="fade-up" data-aos-delay="100">Features</h1>
                        
                        <?php echo $product_row[$product_fields['features_description']][0]['content_value']; ?>

                        <?php if(!empty($product_row[$product_fields['download']][0]['media_path'])){ ?>
                        <!-- Download Manual -->
                        <a href="<?php echo base_url().$product_row[$product_fields['download']][0]['media_path'];?>" class="common-btn download-manual" target='_blank'><span class="icon-download-folder"></span><span> Download <i class="f_medium">Manual</i></span></a>
                        <?php } ?>
                    </div>
                </div>
                <?php } ?>

                <!-- Specification -->
                <?php if(!empty($product_row[$product_fields['specifications_description']][0]['content_value'])){ ?>                
                    <div class="specification" id="specification">
                    <h1 class="heading text-center f_light">Specifications</h1>
                    <div class="container">
                      <?php echo $product_row[$product_fields['specifications_description']][0]['content_value']; ?>
                    </div>
                    </div>
                <?php } ?>
            </div>
            <!--  </div> -->
            <!-- Related Products -->
            <div class="related-products" id="related-products">
                <div class="container">
                    <h2 class="f_medium text-center" data-aos="fade-up" data-aos-delay="100">Related Products</h2>
                    <div class="relatewrapp">
                        <?php foreach ($next_product_list as $key => $recent_product) { ?>
                        <div class="related-single" data-aos="fade-up" data-aos-delay="200">
                            <div class="img-wrapp">
                                <img src="<?php echo base_url().$recent_product[$product_fields['related_image']][0]['media_path']; ?>" alt="Related Products" class="img-fluid">
                            </div>
                            <h1 class="f_medium"><?php echo $recent_product['content']['content_title']; ?> </h1>
                            <a href="<?php echo BASE_URL."product/".$recent_product['content']['url_title']; ?>" class="common-btn f_medium">View <span class="icon-go-back-left-arrow"></span></a>
                        </div>
                        <?php } ?>
                    </div>
                </div>
            </div>
        </section>
        <?php include("footer.php"); ?>
        <script type="text/javascript">
            $(document).ready(function(){
             $(".list-group a").on('click', function(event) {
               if (this.hash !== "") {
                 event.preventDefault();
                 var hash = this.hash;
                 $('html, body').animate({ scrollTop: $(hash).offset().top - ($('header').height() + 30)
               }, 800); 
               }  
             });
             //scroll top with hash change on same page
             $( window ).on( 'hashchange', function( e ) { 
               var hash = window.location.hash;
               if(hash!='') {
                 $('html, body').animate({
                   scrollTop: $(hash).offset().top - ($('header').height() + 30)
                 }, 800);
               }
             });
            });
             $(document).on('scroll', function(){
            
              if($(document).scrollTop() > 350) {
                $('#list-example').addClass('fixed');
              }
              else 
              {
                $('#list-example').removeClass('fixed');
              }
            });
        </script>
    </body>
</html>