<?php require_once("config.php"); ?>
<!DOCTYPE html>
<html lang="en">
<head>
   <?php include("head.php"); ?>
 <title>Gallery | Mymecsolar</title>
  <meta  name="keywords" content="Pump & Motor, Controller , Remote Monitoring System">
  <meta  name="description" content="MEC has pioneered, innovated and excelled in the engineering and manufacturing of solar powered water pumping. We design, develop and manufacture the widest range of solar pumps for any company. Mechanical, electronic and software design is all in-house with a specialized team that have been working in solar pumping for 10 years.">
</head>
<body>
   <?php include("header.php"); ?>
   <section class="breadcum">
      <img src="images/gallery-breadcrum.jpg" class="img-fluid w-100" alt="about-breadcum">
      <div class="container breadcum_container">
         <h1 class="white f_light">Our Work Gallery</h1>
         <a href="index.php" class="white f_light" title="Home">Home</a>
       <span class="icon-1256495 white"></span>
        <span class="white f_medium">Gallery</span>
      </div>        
   </section>
 
<section class="gallery-main">
  

  <div class="container">
    <div class="gallery-wrapp lightgallery">
      <div class="gallery-single" data-aos="fade-up" data-aos-delay="100">
        <a href="images/gallery-12.jpg"  class="img-wrapp img-box">
          <img data-src="images/gallery-12.jpg" alt="Gallery" class="lazy-image">
          <h1 class="white">MEC Office<span class="icon-1256495"></span></h1>
        </a>
      </div>
       <div class="gallery-single" data-aos="fade-up" data-aos-delay="100">
        <a href="images/gallery-8.jpg"  class="img-wrapp img-box">
          <img data-src="images/gallery-8.jpg" alt="Gallery" class="lazy-image">
          <h1 class="white">Winding Shop Floor<span class="icon-1256495"></span></h1>
        </a>
      </div> 
       <div class="gallery-single" data-aos="fade-up" data-aos-delay="100">
        <a href="images/gallery-9.jpg"  class="img-wrapp img-box">
          <img data-src="images/gallery-9.jpg" alt="Gallery" class="lazy-image">
          <h1 class="white">Stator Assembly <span class="icon-1256495"></span></h1>
        </a>
      </div> 
       <div class="gallery-single" data-aos="fade-up" data-aos-delay="100">
        <a href="images/gallery-5.jpg"  class="img-wrapp img-box">
          <img data-src="images/gallery-5.jpg" alt="Gallery" class="lazy-image">
          <h1 class="white">MEC Office<span class="icon-1256495"></span></h1>
        </a>
      </div>  
       <div class="gallery-single" data-aos="fade-up" data-aos-delay="100">
        <a href="images/gallery-11.jpg"  class="img-wrapp img-box">
          <img data-src="images/gallery-11.jpg" alt="Gallery" class="lazy-image">
          <h1 class="white">Motor Assembly Bench<span class="icon-1256495"></span></h1>
        </a>
      </div>  
     
       <!-- <div class="gallery-single" data-aos="fade-up" data-aos-delay="100">
        <a href="images/gallery-13.jpg"  class="img-wrapp img-box">
          <img data-src="images/gallery-13.jpg" alt="Gallery" class="lazy-image">
          <h1 class="white">Motor Assembly <span class="icon-1256495"></span></h1>
        </a>
      </div> 
         -->
       <div class="gallery-single" data-aos="fade-up" data-aos-delay="100">
        <a href="images/gallery-7.jpg"  class="img-wrapp img-box">
          <img data-src="images/gallery-7.jpg" alt="Gallery" class="lazy-image">
          <h1 class="white">Identification of Product<span class="icon-1256495"></span></h1>
        </a>
      </div> 
      <div class="gallery-single" data-aos="fade-up" data-aos-delay="100">
        <a href="images/gallery-1.jpg"  class="img-wrapp img-box">
          <img data-src="images/gallery-1.jpg" alt="Gallery" class="lazy-image">
          <h1 class="white">Controller Assembly<span class="icon-1256495"></span></h1>
        </a>
      </div> 
      <div class="gallery-single" data-aos="fade-up" data-aos-delay="100">
        <a href="images/gallery-6.jpg"  class="img-wrapp img-box">
          <img data-src="images/gallery-6.jpg" alt="Gallery" class="lazy-image">
          <h1 class="white">Controller Assemblies<span class="icon-1256495"></span></h1>
        </a>
      </div>
      <div class="gallery-single" data-aos="fade-up" data-aos-delay="100">
        <a href="images/gallery-28.jpg"  class="img-wrapp img-box">
          <img data-src="images/gallery-28.jpg" alt="Gallery" class="lazy-image">
          <h1 class="white">Controller Box <span class="icon-1256495"></span></h1>
        </a>
      </div> 
       <div class="gallery-single" data-aos="fade-up" data-aos-delay="100">
        <a href="images/gallery-14.jpg"  class="img-wrapp img-box">
          <img data-src="images/gallery-14.jpg" alt="Gallery" class="lazy-image">
          <h1 class="white">Motor Assembly<span class="icon-1256495"></span></h1>
        </a>
      </div>
      <div class="gallery-single" data-aos="fade-up" data-aos-delay="100">
        <a href="images/gallery-13.jpg"  class="img-wrapp img-box">
          <img data-src="images/gallery-13.jpg" alt="Gallery" class="lazy-image">
          <h1 class="white">Motor Assembly<span class="icon-1256495"></span></h1>
        </a>
      </div>
      <div class="gallery-single" data-aos="fade-up" data-aos-delay="100">
        <a href="images/gallery-10.jpg"  class="img-wrapp img-box">
          <img data-src="images/gallery-10.jpg" alt="Gallery" class="lazy-image">
          <h1 class="white">Motor Ready for Testing<span class="icon-1256495"></span></h1>
        </a>
      </div>
      <div class="gallery-single" data-aos="fade-up" data-aos-delay="100">
        <a href="images/gallery-20.jpg"  class="img-wrapp img-box">
          <img data-src="images/gallery-20.jpg" alt="Gallery" class="lazy-image">
          <h1 class="white">Products Ready for packing<span class="icon-1256495"></span></h1>
        </a>
      </div>
      <div class="gallery-single" data-aos="fade-up" data-aos-delay="100">
        <a href="images/gallery-21.jpg"  class="img-wrapp img-box">
          <img data-src="images/gallery-21.jpg" alt="Gallery" class="lazy-image">
          <h1 class="white">Products Ready for packing<span class="icon-1256495"></span></h1>
        </a>
      </div>
       <div class="gallery-single" data-aos="fade-up" data-aos-delay="100">
        <a href="images/gallery-18.jpg"  class="img-wrapp img-box">
          <img data-src="images/gallery-18.jpg" alt="Gallery" class="lazy-image">
          <h1 class="white">Products Ready for packing<span class="icon-1256495"></span></h1>
        </a>
      </div>


       <div class="gallery-single" data-aos="fade-up" data-aos-delay="100">
        <a href="images/gallery-2.jpg"  class="img-wrapp img-box">
          <img data-src="images/gallery-2.jpg" alt="Gallery" class="lazy-image">
          <h1 class="white">Ready for Dispatch <span class="icon-1256495"></span></h1>
        </a>
      </div>
      <div class="gallery-single" data-aos="fade-up" data-aos-delay="100">
        <a href="images/gallery-3.jpg"  class="img-wrapp img-box">
          <img data-src="images/gallery-3.jpg" alt="Gallery" class="lazy-image">
          <h1 class="white">New Plant Blue Print<span class="icon-1256495"></span></h1>
        </a>
      </div>
      <div class="gallery-single" data-aos="fade-up" data-aos-delay="100">
        <a href="images/gallery-4.jpg"  class="img-wrapp img-box">
          <img data-src="images/gallery-4.jpg" alt="Gallery" class="lazy-image">
          <h1 class="white">New Plant Blue Print<span class="icon-1256495"></span></h1>
        </a>
      </div>
      <div class="gallery-single" data-aos="fade-up" data-aos-delay="100">
        <a href="images/gallery-27.jpg"  class="img-wrapp img-box">
          <img data-src="images/gallery-27.jpg" alt="Gallery" class="lazy-image">
          <h1 class="white">Products Installed in Sites from our Service Engineers
<span class="icon-1256495"></span></h1>
        </a>
      </div>
      <div class="gallery-single" data-aos="fade-up" data-aos-delay="100">
        <a href="images/gallery-26.jpg"  class="img-wrapp img-box">
          <img data-src="images/gallery-26.jpg" alt="Gallery" class="lazy-image">
          <h1 class="white">Products Installed in Sites from our Service Engineers<span class="icon-1256495"></span></h1>
        </a>
      </div>
       <div class="gallery-single" data-aos="fade-up" data-aos-delay="100">
        <a href="images/gallery-25.jpg"  class="img-wrapp img-box">
          <img data-src="images/gallery-25.jpg" alt="Gallery" class="lazy-image">
          <h1 class="white">Products Installed in Sites from our Service Engineers<span class="icon-1256495"></span></h1>
        </a>
      </div>
       <div class="gallery-single" data-aos="fade-up" data-aos-delay="100">
        <a href="images/gallery-24.jpg"  class="img-wrapp img-box">
          <img data-src="images/gallery-24.jpg" alt="Gallery" class="lazy-image">
          <h1 class="white">Customers with Our Product Onsite<span class="icon-1256495"></span></h1>
        </a>
      </div>
       <div class="gallery-single" data-aos="fade-up" data-aos-delay="100">
        <a href="images/gallery-23.jpg"  class="img-wrapp img-box">
          <img data-src="images/gallery-23.jpg" alt="Gallery" class="lazy-image">
          <h1 class="white">Customers with Our Product Onsite<span class="icon-1256495"></span></h1>
        </a>
      </div>
         <div class="gallery-single" data-aos="fade-up" data-aos-delay="100">
        <a href="images/gallery-22.jpg"  class="img-wrapp img-box">
          <img data-src="images/gallery-22.jpg" alt="Gallery" class="lazy-image">
          <h1 class="white">Customers with Our Product Onsite<span class="icon-1256495"></span></h1>
        </a>
      </div>
    </div>
    <!-- get a quote -->
    <!-- <a href="" class="common-btn f_medium">Con<span class="icon-go-back-left-arrow"></span></a> -->
  </div>
</section>


 <?php include("footer.php"); ?>
  <link rel="stylesheet" type="text/css" href="css/lightgallery.css">
  <script type="text/javascript" src="js/lightgallery.js"></script>

  <script type="text/javascript">
    $(document).ready(function(){
      $('.lightgallery').lightGallery({
        selector:".img-box",
        zoom:false,
        thumbnail:false,
        autoplay:false,
        fullScreen:false,
        autoplayFirstVideo:false,
        progressBar:false,
        share:false,
        download: false,
        autoplayControls:false,
        loop:false,
      });
    });
  </script>
  
</body>
</html>