<?php require_once("config.php"); ?>
<!DOCTYPE html>
<html lang="en">
<head>
   <?php include("head.php"); ?>
 <title>Disclaimer | Mymecsolar</title>
  <meta  name="keywords" content="Pump & Motor, Controller , Remote Monitoring System">
  <meta  name="description" content="MEC has pioneered, innovated and excelled in the engineering and manufacturing of solar powered water pumping. We design, develop and manufacture the widest range of solar pumps for any company. Mechanical, electronic and software design is all in-house with a specialized team that have been working in solar pumping for 10 years.">
</head>
<body>
   <?php include("header.php"); ?>
   <section class="breadcum">
      <img src="images/disclaimer.jpg" class="img-fluid w-100" alt="disclaimer-breadcum">
      <div class="container breadcum_container">
         <h1 class="white f_light">Disclaimer</h1>
         <a href="index.php" class="white f_light" title="Home">Home</a>
       <span class="icon-1256495 white"></span>
        <span class="white f_medium">Disclaimer</span>
      </div>        
   </section>
<section class="common-pages" data-aos="fade-up" data-aos-delay="100">
            <div class="container">
              
                <p class="f-light">This web site may include links to external web sites. When you follow such links the external web site may appear as a full screen (in which case you will need to use the back button on your browser to return to this web site) or in some cases it may appear within the frame of this web site (in which case you will be able to return to this web site by using the navigation buttons within the frame).</p>
                <p class="f-light">Where an external web site appears within the frame of this web site, this is purely for ease of navigation back to this web site and does not indicate any responsibility on our part for the external web site concerned. These links are provided in order to help you find relevant web sites, services and/or products which may be of interest to you quickly and easily. It is your responsibility to decide whether any services and/or products available through any of these web sites are suitable for your purposes.</p>
                
                <p class="f-light">All information or advice provided as part of this web site is intended to be general in nature and you should not rely on it in connection with the making of any decision. mymecsolar tries to ensure that all information provided as part of this web site is correct at the time of inclusion on the web site but does not guarantee the accuracy of such information.</p>
                <p>Mymecsolar is not liable for any action you may take as a result of relying on such information or advice or for any loss or damage suffered by you as a result of you taking this action. mymecsolar reserves the right to monitor any information transmitted or received through any forum provided.</p>
                <p>Mymecsolar, at its sole discretion and without prior notice, may at any time review, remove or otherwise block any material posted.</p>
            </div>
        </section>
 <?php include("footer.php"); ?>
</body>
</html>