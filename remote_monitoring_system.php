<?php 
    require_once("config.php"); 



?>
<!DOCTYPE html>
<html lang="en">
    <head>
        <?php include("head.php"); ?>
        <title>Products | Mymecsolar</title>
       <meta  name="keywords" content="Pump & Motor, Controller , Remote Monitoring System">
  <meta  name="description" content="MEC has pioneered, innovated and excelled in the engineering and manufacturing of solar powered water pumping. We design, develop and manufacture the widest range of solar pumps for any company. Mechanical, electronic and software design is all in-house with a specialized team that have been working in solar pumping for 10 years.">
    </head>
    <body  data-spy="scroll" data-target="#list-example" data-offset="130">
        <?php include("header.php"); ?>
        <section class="breadcum">
            <img src="images/product-breadcum.jpg" class="img-fluid w-100" alt="about-breadcum">
            <div class="container breadcum_container">
                <h1 class="white f_light">Remote Monitoring  System (RMS)</h1>
                <a href="index.php" class="white f_light" title="Home">Home</a>
                <span class="icon-1256495 white"></span>
                <span class="white f_medium">Products</span>
                <span class="icon-1256495 white"></span>
                <span class="white f_medium">Remote Monitoring  System (RMS)</span>
            </div>
        </section>
        <section class="product-main" data-toggle="affix">
            <div id="list-example" class="list-group">
                <a class="list-group-item list-group-item-action f_medium active" href="#overview">Overview</a>
                <a class="list-group-item list-group-item-action f_medium" href="#features">Features</a>
                <a class="list-group-item list-group-item-action f_medium" href="#specification"> Specification</a> 
            </div>
            <div  class="scrollspy-example">
                <!-- overview -->
                <div class="overview overview-rms" id="overview">
                    <div class="container">
                        <div class="row">
                            <div class="col-lg-6 col-md-12">
                                <div class="img-wrapp">
                                    <img src="images/overview-rms.png" class="img-fluid" alt="product-overview">
                                </div>
                            </div>
                            <div class="col-lg-6 col-md-12">
                                <div class="overview-right">
                                    <h1 class="f_light heading" data-aos="fade-up" data-aos-delay="100">Overview</h1>
                                    <p class="f_book gray" data-aos="fade-up" data-aos-delay="150">Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do 
                                        tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam,
                                        quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo
                                        consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse
                                        cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non
                                        proident, sunt in culpa qui officia deserunt mollit anim id est laborum.
                                    </p>
                                    <p class="f_book gray" data-aos="fade-up" data-aos-delay="200">Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do 
                                        tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam
                                    </p>
                                    <div class="btn-div">
                                        <a href="<?php echo BASE_URL.'contact.php';?>" class="common-btn"><span class="icon-phone-call"></span> Contact Us <span class="icon-go-back-left-arrow"></span></a>
                                        <a href="" class="common-btn" data-toggle="modal" data-target="#request-quote" id="request"><span class="icon-1183875"></span>Request a Quote <span class="icon-go-back-left-arrow"></span></a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- Features -->
                <div class="features features-rms" id="features">
                    <div class="container">
                        <h1 class="heading text-center f_light" data-aos="fade-up" data-aos-delay="100">Features</h1>

                        <div class="feature-wrapper">
                            <div class="feature-single">
                                <div class="feature-wrap">
                                    <ul>
                                        <li data-aos="fade-left" data-aos-delay="100"><span>GSM/GPRS based solar <br>drive monitoring unit.</span> <span>1</span></li>
                                        <li data-aos="fade-left" data-aos-delay="200"><span>Quad band support for GSM850MHz, <br>EGSM900MHz, DCS1800MHz & PCS1900MHz</span> <span>2</span></li>
                                        <li data-aos="fade-left" data-aos-delay="300"><span>Network location mapping using <br> network co-ordinates.</span> <span>3</span></li>
                                        <li data-aos="fade-left" data-aos-delay="400"><span>Nano sim support for ease of accessibility. <br> Sim ESD protection for failsafe operation.</span> <span>4</span></li>
                                        <li data-aos="fade-left" data-aos-delay="500"><span>Compatible with external Antenna for <br> more stabilized network signal reception.</span> <span>5</span></li>
                                        <li data-aos="fade-left" data-aos-delay="600"><span>Wide input voltage from 5V DC to 24V DC.</span> <span>6</span></li>
                                    </ul>
                                </div>
                            </div>
                            <div class="feature-single">
                                <div class="img-wrapp">
                                    <img src="images/features1.png" alt="Features" class="img-fluid product1" data-aos="zoom-in" data-aos-delay="400">
                                    <img src="images/product-circle.png" alt="circle" class="img-fluid circle" data-aos="zoom-in" data-aos-delay="700">
                                </div>
                            </div>
                            <div class="feature-single">
                                <div class="right-features">
                                    <ul>
                                        <li data-aos="fade-right" data-aos-delay="150"><span>7</span><span>Short circuit protection for unit <br>safety in accidental occurrences.</span> </li>
                                        <li data-aos="fade-right" data-aos-delay="250"><span>8</span><span>Reverse polarity protection implemented for unit.</span> </li>
                                        <li data-aos="fade-right" data-aos-delay="350"><span>9</span><span>Micro-SD Card support upto <br>64GB for Data logging only.</span> </li>
                                        <li data-aos="fade-right" data-aos-delay="450"><span>10</span><span>Robust Modbus transceiver with additional <br>ESD, EFT & Surge Immunity.</span> </li>
                                        <li data-aos="fade-right" data-aos-delay="550"><span>11</span><span>Modbus RTU protocol with multiple <br> address read and write options.</span> </li>
                                        <li data-aos="fade-right" data-aos-delay="650"><span>11</span><span>Historical data can be downloaded in <br>Microsoft excel sheet format.</span> </li>
                                    </ul>
                                </div>
                            </div>
                        </div>

                        <!-- Download Manual -->
                        <a href="" class="common-btn download-manual"><span class="icon-download-folder"></span><span> Download <i class="f_medium">Manual</i></span></a>
                    </div>
                </div>
            </div>
            <!--  </div> -->
            <!-- Related Products -->
            <div class="related-products" id="related-products">
                <div class="container">
                    <h2 class="f_medium text-center" data-aos="fade-up" data-aos-delay="100">Related Products</h2>
                    <div class="relatewrapp">
                        <div class="related-single" data-aos="fade-up" data-aos-delay="200">
                            <div class="img-wrapp">
                                <img src="images/related-product1.png" alt="Related Products" class="img-fluid">
                            </div>
                            <h1 class="f_medium">Solar Pump & motor </h1>
                            <a href="pump-&-motor.php" class="common-btn f_medium">View <span class="icon-go-back-left-arrow"></span></a>
                        </div>
                        <div class="related-single" data-aos="fade-up" data-aos-delay="300">
                            <div class="img-wrapp">
                                <img src="images/related-product3.png" alt="Related Products" class="img-fluid">
                            </div>
                            <h1 class="f_medium">Solar Pump MPPT Controller</h1>
                            <a href="solar_pump_mppt_controller.php" class="common-btn f_medium">View <span class="icon-go-back-left-arrow"></span></a>
                        </div>
                    </div>
                </div>
            </div>
        </section>
        <?php include("footer.php"); ?>
        <script type="text/javascript">
            $(document).ready(function(){
             $(".list-group a").on('click', function(event) {
               if (this.hash !== "") {
                 event.preventDefault();
                 var hash = this.hash;
                 $('html, body').animate({ scrollTop: $(hash).offset().top - ($('header').height() + 30)
               }, 800); 
               }  
             });
             //scroll top with hash change on same page
             $( window ).on( 'hashchange', function( e ) { 
               var hash = window.location.hash;
               if(hash!='') {
                 $('html, body').animate({
                   scrollTop: $(hash).offset().top - ($('header').height() + 30)
                 }, 800);
               }
             });
            });
             $(document).on('scroll', function(){
            
              if($(document).scrollTop() > 350) {
                $('#list-example').addClass('fixed');
              }
              else 
              {
                $('#list-example').removeClass('fixed');
              }
            });
        </script>
    </body>
</html>

