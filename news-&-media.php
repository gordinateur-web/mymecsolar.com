<?php 
  require_once("config.php"); 

  $news_media_fields=$CI->config->item('news_media_fields');
    
  $filter_news_media['content.content_type_id']=$news_media_fields['content_type_id'];
  $news_media_list=$CI->content_model->get_published_content($filter_news_media);
  // dsm($news_media_list);die;

?>
<!DOCTYPE html>
<html lang="en">
<head>
  <?php include("head.php"); ?>
  <title>News & Media | Mymecsolar</title>
  <meta  name="keywords" content="Pump & Motor, Controller , Remote Monitoring System">
  <meta  name="description" content="MEC has pioneered, innovated and excelled in the engineering and manufacturing of solar powered water pumping. We design, develop and manufacture the widest range of solar pumps for any company. Mechanical, electronic and software design is all in-house with a specialized team that have been working in solar pumping for 10 years.">
</head>
<body>
  <?php include("header.php"); ?>
  <section class="breadcum">
    <img src="images/blog-breadcum.jpg" class="img-fluid w-100 lazy-image" alt="about-breadcum">
    <div class="container breadcum_container">
      <h1 class="white f_light">News & Media</h1>
      <a href="index.php" class="white f_light" title="Home">Home</a>
      <span class="icon-1256495 white"></span>
      <span class="white f_medium"> News & Media</span>
    </div>        
  </section>

  <section class="blog-main">
    <div class="container">
      <div class="blog-wrapp news-wrapp">
<!--       <?php foreach ($news_media_list as $key => $news_media_row) { ?>
        <div class="blog-single" data-aos="fade-up" data-aos-delay="100">
          <div class="blog-inner">
            <a href="<?php echo BASE_URL."news-and-media/".$news_media_row['content']['url_title']; ?>">
              <div class="img-wrapp">
                <img src="<?php echo base_url().$news_media_row[$news_media_fields['image']][0]['media_path']; ?>" alt="Blog" class="img-fluid w-100">
              </div>
              <div class="blog-middle">
                <h1 class="f_book"><?php echo dateformat($news_media_row[$news_media_fields['date']][0]['content_value']); ?></h1>
                <h2 class="f_book"><?php echo strip_tags(word_limiter($news_media_row[$news_media_fields['description']][0]['content_value'],11)); ?></h2>
                <a href="<?php echo BASE_URL."news-and-media/".$news_media_row['content']['url_title']; ?>" class="common-btn f_book">Read More</a>
              </div>
            </a>
          </div>
        </div>
      <?php } ?> -->
      <h1 class="f_medium text-center">Comming Soon</h1>
     
        <!-- <div class="blog-single" data-aos="fade-up" data-aos-delay="200">
          <a href="">
            <div class="img-wrapp">
              <img src="images/news2.jpg" alt="Blog" class="img-fluid w-100">
            </div>
            <div class="blog-middle">
              <h1 class="f_book">15 Apr 2109</h1>
              <h2 class="f_book">Nulla aliquam dui in ghets congue fermentum fact of mades ipsum</h2>
              <a href="" class="common-btn f_book">Read More</a>
            </div>
          </a>
=======
        <div class="blog-single">
          <div class="blog-inner" data-aos="fade-up" data-aos-delay="100">
            <a href="news-&-media-detail.php">
              <div class="img-wrapp">
                <img src="images/news1.jpg" alt="Blog" class="img-fluid w-100 lazy-image">
              </div>
              <div class="blog-middle">
                <h1 class="f_book">15 Apr 2109</h1>
                <h2 class="f_book">Nulla aliquam dui in ghets congue fermentum fact of mades ipsum</h2>
                <a href="news-&-media-detail.php" class="common-btn f_book">Read More</a>
              </div>
            </a>
          </div>
        </div>

        <div class="blog-single" data-aos="fade-up" data-aos-delay="200">
          <div class="blog-inner" data-aos="fade-up" data-aos-delay="100">
            <a href="">
              <div class="img-wrapp">
                <img src="images/news2.jpg" alt="Blog" class="img-fluid w-100 lazy-image">
              </div>
              <div class="blog-middle">
                <h1 class="f_book">15 Apr 2109</h1>
                <h2 class="f_book">Nulla aliquam dui in ghets congue fermentum fact of mades ipsum</h2>
                <a href="" class="common-btn f_book">Read More</a>
              </div>
            </a>
          </div>
>>>>>>> cb416e141bc1fe4cffe0002999d317febcb6fd07
        </div>

        <div class="blog-single" data-aos="fade-up" data-aos-delay="300">
          <div class="blog-inner" data-aos="fade-up" data-aos-delay="100">
            <a href="">
              <div class="img-wrapp">
                <img src="images/news3.jpg" alt="Blog" class="img-fluid w-100 lazy-image">
              </div>
              <div class="blog-middle">
                <h1 class="f_book">15 Apr 2109</h1>
                <h2 class="f_book">Nulla aliquam dui in ghets congue fermentum fact of mades ipsum</h2>
                <a href="" class="common-btn f_book">Read More</a>
              </div>
            </a>
          </div>
        </div>

        <div class="blog-single" data-aos="fade-up" data-aos-delay="400">
          <div class="blog-inner" data-aos="fade-up" data-aos-delay="100">
            <a href="">
              <div class="img-wrapp">
                <img src="images/news4.jpg" alt="Blog" class="img-fluid w-100 lazy-image">
              </div>
              <div class="blog-middle">
                <h1 class="f_book">15 Apr 2109</h1>
                <h2 class="f_book">Nulla aliquam dui in ghets congue fermentum fact of mades ipsum</h2>
                <a href="" class="common-btn f_book">Read More</a>
              </div>
            </a>
          </div>
        </div>

        <div class="blog-single" data-aos="fade-up" data-aos-delay="500">
          <div class="blog-inner" data-aos="fade-up" data-aos-delay="100">
            <a href="">
              <div class="img-wrapp">
                <img src="images/news5.jpg" alt="Blog" class="img-fluid w-100 lazy-image">
              </div>
              <div class="blog-middle">
                <h1 class="f_book">15 Apr 2109</h1>
                <h2 class="f_book">Nulla aliquam dui in ghets congue fermentum fact of mades ipsum</h2>
                <a href="" class="common-btn f_book">Read More</a>
              </div>
            </a>
          </div>
        </div>

        <div class="blog-single" data-aos="fade-up" data-aos-delay="600">
<<<<<<< HEAD
          <a href="">
            <div class="img-wrapp">
              <img src="images/news6.jpg" alt="Blog" class="img-fluid w-100">
            </div>
            <div class="blog-middle">
              <h1 class="f_book">15 Apr 2109</h1>
              <h2 class="f_book">Nulla aliquam dui in ghets congue fermentum fact of mades ipsum</h2>
              <a href="" class="common-btn f_book">Read More</a>
            </div>
          </a>
        </div> -->
      </div>
    </div>
  </section>


  <?php include("footer.php"); ?>

</body>
</html>