<?php 
require_once("config.php"); 

$news_media_fields=$CI->config->item('news_media_fields');

$url_title=$CI->uri->segment(2);
    // dsm($url_title);die;

if(!empty($url_title))
  $filter_news_media['content.url_title']=$url_title;

$filter_news_media['content.content_type_id']=$news_media_fields['content_type_id'];
$news_media_list=$CI->content_model->get_published_content($filter_news_media);
    // dsm($news_media_list);die;

$filter_next_news['LIMIT']=3;
$filter_next_news['content.content_id <']=$news_media_list[0]['content']['content_id'];
$filter_next_news['content.content_type_id']=$news_media_fields['content_type_id'];
$filter_next_news['content.content_published']=1;
$filter_next_news['content.deleted_at']=NULL;
$filter_next_news['WHERE'][]="(content.content_status is null or content.content_status='Approved')";
$nextnews_list=$CI->content_model->get_content_with_value($filter_next_news);

?>
<!DOCTYPE html>
<html lang="en">
<head>
 <?php include("head.php"); ?>
 <title>News & Media | Mymecsolar</title>
 <meta  name="keywords" content="Pump & Motor, Controller , Remote Monitoring System">
  <meta  name="description" content="MEC has pioneered, innovated and excelled in the engineering and manufacturing of solar powered water pumping. We design, develop and manufacture the widest range of solar pumps for any company. Mechanical, electronic and software design is all in-house with a specialized team that have been working in solar pumping for 10 years.">
</head>
<body>
 <?php include("header.php"); ?>
 <section class="breadcum">
  <img src="<?php echo BASE_URL.'images/blog-breadcum.jpg';?>" class="img-fluid w-100" alt="about-breadcum">
  <div class="container breadcum_container">
   <h1 class="white f_light">News & Media</h1>
   <a href="<?php echo BASE_URL.'index.php';?>" class="white f_light" title="Home">Home</a>
   <span class="icon-1256495 white"></span>
   <span class="white f_medium"> News & Media</span>
 </div>        
</section>
<!-- blog detail -->
<section class="blog-detail">
 <div class="container">

  <?php foreach ($news_media_list as $key => $news_media_row) { ?>
   <div class="detail-wrap">
     <div class="img-wrapp">
       <img src="<?php echo base_url().$news_media_row[$news_media_fields['image']][0]['media_path']; ?>" alt="blog-detail" class="img-fluid">
     </div>
     <h1 class="f_medium blue"><?php echo $news_media_row['content']['content_title']; ?></h1>
     <h2 class="f_medium"><?php echo $news_media_row[$news_media_fields['date']][0]['content_value']; ?></h2>
     <?php echo $news_media_row[$news_media_fields['description']][0]['content_value']; ?>
   </div>
 <?php } ?>
</div>

</section>
<!-- Related Blogs -->
<section class="blog-main related-blog">
  <div class="container">
   <h1 class="heading text-center blue f_light" data-aos="fade-up" data-aos-delay="100">Related News</h1>
   
   <div class="blog-wrapp news-wrapp">
    <?php foreach($nextnews_list as $key => $news_recent) { ?>
      <div class="blog-single" data-aos="fade-up" data-aos-delay="100">
        <div class="blog-inner">
          <a href="<?php echo BASE_URL."news-and-media/".$news_recent['content']['url_title']; ?>">
            <div class="img-wrapp">
              <img src="<?php echo base_url().$news_recent[$news_media_fields['image']][0]['media_path']; ?>" alt="Blog" class="img-fluid w-100">
            </div>
            <div class="blog-middle">
              <h1 class="f_book"><?php echo dateformat($news_recent[$news_media_fields['date']][0]['content_value']); ?></h1>
              <h2 class="f_book"><?php echo strip_tags(word_limiter($news_recent[$news_media_fields['description']][0]['content_value'],11)); ?></h2>
              <a href="<?php echo BASE_URL."news-and-media/".$news_recent['content']['url_title']; ?>" class="common-btn f_book">Read More</a>
            </div>
          </a>
        </div>
      </div>
    <?php } ?>
  </div>

  <!-- View All -->
  <a href="<?php echo BASE_URL.'news-&-media.php';?>" class="common-btn f_medium">View All <span class="icon-go-back-left-arrow"></span></a>
</div>
</section>


<?php include("footer.php"); ?>
</body>
</html>