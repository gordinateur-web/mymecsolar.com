<?php require_once("config.php"); 
  $blog_fields=$CI->config->item('blog_fields');
  $filter_blog_fields['content.content_type_id']=$blog_fields['content_type_id'];
  $blog_list=$CI->content_model->get_published_content($filter_blog_fields);
?>
<!DOCTYPE html>
<html lang="en">
<head>
  <?php include("head.php"); ?>
  <title>Home | Mymecsolar</title>
  <meta  name="keywords" content="Pump & Motor, Controller , Remote Monitoring System">
  <meta  name="description" content="MEC has pioneered, innovated and excelled in the engineering and manufacturing of solar powered water pumping. We design, develop and manufacture the widest range of solar pumps for any company. Mechanical, electronic and software design is all in-house with a specialized team that have been working in solar pumping for 10 years.">
</head>
<body>
  <?php include("header.php"); ?>

  <section class="slider-section">
    <!-- slider div -->
    <div class="slider-div">
      <div class="owl-carousel owl-theme home-slider">
        <div class="item">
          <img src="images/Slide-1.jpg" alt="Slider-image" class="lazy-image">
          <div class="caption">
            <!-- <div class="container">
              <h1 class="white f_light">Join the solar <br> energy revolution</h1>
              <p class="white f_light">We're transforming access to renewable energy with simple, <br> powerful solar solutions.</p>
              <a href="" class="common-btn">Our Products<span class="icon-go-back-left-arrow"></span></a>
            </div> -->
          </div>
        </div>
        <div class="item">
          <img src="images/Slide-2.jpg" alt="Slider-image" class="lazy-image">
          <div class="caption">
            <!-- <div class="container">
              <h1 class="white f_light">Join the solar <br> energy revolution2</h1>
              <p class="white f_light">We're transforming access to renewable energy with simple, <br> powerful solar solutions.</p>
              <a href=""  class="common-btn">Our Products<span class="icon-go-back-left-arrow"></span></a>
              </div> -->
            </div>
          </div>
          <div class="item">
            <img src="images/Slide-3.jpg" alt="Slider-image" class="lazy-image">
            <div class="caption">
              <!-- <div class="container">
                <h1 class="white f_light">Join the solar <br> energy revolution3</h1>
                <p class="white f_light">We're transforming access to renewable energy with simple, <br> powerful solar solutions.</p>
                <a href="" class="common-btn">Our Products<span class="icon-go-back-left-arrow"></span></a>
              </div> -->
            </div>
          </div>
        </div> 
        <div class="text-box d-none d-md-block">
          <ul class="list-unstyled img-slide">
            <li><a href="about.php" class="step-0 white"><span class="icon-1002736"></span>Innovating <br>  Energy </a></li>
            <li><a href="<?php echo BASE_URL;?>product/pump-motor" class="step-1 white active" title="Express"><span class="icon-hggv76"></span>Solar Pumps <br> and Controllers</a></li>
            <li><a href="index.php#our-service" class="step-2 white" title="Lead"><span class="icon-sadvsdfvdf"></span>Manufacturing <br> Excellence </a></li>
          </ul>
        </div>
        <!-- animate arrow -->
        <div>
          <a href="#home-about" class="animate-arrow">
          <span class="icon-1256495"></span>
          <span class="icon-1256495"></span>
          </a>
        </div>
      </div>
  </section>
  <!-- about-us -->
  <section class="home-about" id="home-about">
    <div class="container">
      <div class="row">
        <div class=" col-lg-6 col-md-6">
          <div class="about-left">
            <h1 class="blue f_bold">About US</h1>
            <h2 class="blue f_light">Business Hand in <br>Hand with new Technology</h2>
            <p class="gray f_book">Founded in Bangalore, India during 2016. MEC has pioneered, innovated and excelled in the engineering and manufacturing of solar powered water pumping. We design, develop and manufacture the widest range of solar pumps for any company. Mechanical, electronic and software design is all in-house with a specialized team that have been working in solar pumping for 10 years.</p>
            <p class="gray f_book">Manufacturing is done in our own factory located in Bangalore ,India. Our production team are very loyal and professional who keep up our high quality standards and our unique, intensive 100% functional test schedule on every product we  manufacture .</p>
            <p class="gray f_book">MEC Leadership works with combination of superior systems engineering, technology, unwavering commitment and accelerated innovation with a strong value-based culture to enhance our customer success enabling them to
            shape the future.</p>
            <a href="about.php" class="white common-btn f_medium">About Us<span class="icon-go-back-left-arrow white"></span></a>
          </div>
        </div>
        <div class="col-lg-6 col-md-6">
          <div class="img-wrapp" data-aos="zoom-in" data-aos-delay="100">
            <div class="img-inner">
            <img src="images/home-circle.png" class="img-fluid">
            </div>
          </div>
        </div>
      </div>
    </div>
  </section>
  <!-- Our Products -->
  <section class="products-home">
      <div class="top-section">
            <h1 class="f_light heading text-center white" data-aos="fade-up" data-aos-delay="100">Our Products</h1>
        </div>
      </div>
      <!-- main product -->
      <div class="product-wrapper">
        <div class="product-single" data-aos="fade-up" data-aos-delay="100">
          <img src="images/product1.jpg" class="img-fluid w-100 lazy-image" alt="Motor">
          <div class="overlay"></div>
          <div class="img-wrapp">
            <img src="images/home-product1.png" alt="Motor png" class="img-fluid lazy-image">
          </div>
          <h1 class="f_bold white">Pump & Motor</h1>
          <a href="<?php echo BASE_URL;?>product/pump-motor" class="common-btn f_medium">View</a>
        </div>
           <div class="product-single" data-aos="fade-up" data-aos-delay="200">
          <img src="images/product1.jpg" class="img-fluid w-100 lazy-image" alt="Motor">
          <div class="overlay"></div>
          <div class="img-wrapp">
            <img src="images/home-product2.png" alt="pump png" class="img-fluid lazy-image">
          </div>
           <h1 class="f_bold white">Controller</h1>
           <a href="<?php echo BASE_URL;?>product/solar-pump-mppt-controller" class="common-btn f_medium">View</a>
        </div>
           <div class="product-single" data-aos="fade-up" data-aos-delay="300">
          <img src="images/product1.jpg" class="img-fluid w-100 lazy-image" alt="Motor">
          <div class="overlay"></div>
          <div class="img-wrapp">
            <img src="images/home-product3.png" alt="controller png" class="img-fluid lazy-image">
          </div>
           <h1 class="f_bold white">Remote Monitoring System</h1>
           <a href="<?php echo BASE_URL;?>product/remote-monitoring-system-rms" class="common-btn f_medium">View</a>
        </div>
      </div>   
  </section>

  <!-- Our impact -->
  <section class="impact">
    <div class="impact-left">
      <h1 class="f_bold heading">Our impact</h1>
      <h2 class="f_book heading">We're transforming access to renewable energy with simple, powerful solar solutions.</h2>
      <div class="impact-element counter">
        <ol>
          <li>
            <a href="#item1" class="active">
             <span class="icon-Icon22 new-icon-Icon2"><span class="path1"></span><span class="path2"></span><span class="path3"></span><span class="path4"></span><span class="path5"></span><span class="path6"></span><span class="path7"></span><span class="path8"></span><span class="path9"></span><span class="path10"></span><span class="path11"></span><span class="path12"></span><span class="path13"></span><span class="path14"></span><span class="path15"></span><span class="path16"></span><span class="path17"></span><span class="path18"></span><span class="path19"></span><span class="path20"></span><span class="path21"></span><span class="path22"></span><span class="path23"></span><span class="path24"></span><span class="path25"></span><span class="path26"></span><span class="path27"></span><span class="path28"></span><span class="path29"></span><span class="path30"></span><span class="path31"></span><span class="path32"></span><span class="path33"></span><span class="path34"></span><span class="path35"></span><span class="path36"></span><span class="path37"></span><span class="path38"></span><span class="path39"></span><span class="path40"></span><span class="path41"></span><span class="path42"></span><span class="path43"></span></span>
              <div class="text">
                <div class="counter-wrapp">
                  <h1 class="f_book counter-value" data-count="12000">0</h1>
                <span class="plus">+</span>
                 </div>
                <p class="f_book">sqft <br> Floor</p>
              </div>
            </a>
          </li>
          <li>
            <a href="#item2"> 
             <span class="new-icon-Icon1"></span>
              <div class="text">
                <div class="counter-wrapp">
                  <h1 class="f_book counter-value" data-count="24">0</h1>
                  <span class="plus">Hrs</span>
                </div>
                <p class="f_book">Power<br> Backup</p>
              </div>
            </a>
          </li>
          <li>
            <a href="#item3">
              <span class="new-icon-Icon5"></span>
              <div class="text">
                <div class="counter-wrapp">
                  <h1 class="f_book counter-value" data-count="100">0</h1>
                  <span class="plus">+</span>
                </div>
                <p class="f_book">Skilled <br> Technicians</p>
              </div>
            </a>
          </li>
        </ol>
      </div>
    </div>
    <!-- circle slider -->
    <div class="impact-right">
      <div class="impact-inner">
      <div class="owl-carousel impact-slider">
        <div class="item active" id="item1"><div class="img-wrapp"><img src="images/impact-circle.png"></div></div>
        <div class="item" id="item2"><div class="img-wrapp"><img src="images/impact-circle2.png"></div></div>
        <div class="item" id="item3"><div class="img-wrapp"><img src="images/home-circle.png"></div></div>
      </div>
      </div>
    </div>
  </section>
  <!-- our project -->
  <section class="projects">
    <div class="carusel-section">
      <!-- image slider -->
      <div class="image-outer carousel-wrapper">
        <div class="image-wrapper frame">
          <div id="carousel-a" class="carousel slide carousel-fade carousel-sync2 carousel-a" data-ride="carousel">
            <div class="carousel-inner">
              <div class="carousel-item active">
                <div class="img">
                <img src="images/Irrigation.jpg" alt="image2" class="w-100">
                </div>
              </div>
              <div class="carousel-item">
                <div class="img">
                <img src="images/drinking-water.jpg" alt="image2"  class="w-100">
              </div>
              </div>
              <div class="carousel-item">
                <div class="img">
                <img src="images/lift-irrigation.jpg" alt="image2" class="w-100">
              </div>
              </div>        
            </div> 
          </div>
        </div>
      </div>

      <div class="text-wrapper carousel-wrapper">
        <div id="carousel-a" class="carousel  carousel-fade carousel-sync2 carousel-a" data-ride="carousel">
          <div class="carousel-inner">
            <div class="carousel-item active">
              <div class="caption-box" >
              <div class="text-box">
              <h1 class="f_bold">Our Projects</h1>
              <h2 class="f_bold white">Irrigation</h2>
              <p class="f_light white">MEC solar pumping system can be used to transform unused land into productive farms or to improve the yields from existing crops. Bringing water to locations that do not have any existing infrastructure is improving food security and generating significant income for farmers and communities around the world. For this projects we have solar pumps ranging from 1Hp to 100Hp for both deep well (Bore well) and for open wells or rivers.</p>  
              <!-- <a href="" class="common-btn f_medium">Learn More<span class="icon-go-back-left-arrow white"></span></a> -->
              </div>
              <!-- next-info -->
              <!-- <div class="next-info">
                <span class="f_bold">01</span>
                <div class="text">
                  <p class="f_medium white">Pune (Motor Work)</p>
                  <p class="f_book">Eiusmod tempor incididunt ut labore et.....</p>
                </div>
                </div> -->
              </div>
            </div>
            <div class="carousel-item">
               <div class="caption-box" >
              <div class="text-box">
              <h1 class="f_bold">Our Projects</h1>
              <h2 class="f_bold white">Drinking water</h2>
              <p class="f_light white">MEC solar water pumping systems have a proven track record of delivering drinking water to homes, communities and livestock all around for over 4 years. As an application, there is nothing more critical than drinking water. Therefore, choosing a system that you know will deliver the water you need, reliably and predictably is of the upmost importance.</p>
              <p class="f_light white">To complement our solar water pumping systems MEC also solutions that allow for revenue collection and accountability of water use.</p>
              <!-- <a href="" class="common-btn f_medium">Learn More<span class="icon-go-back-left-arrow white"></span></a> -->
            </div>
              <!-- <div class="next-info">
                <span class="f_bold">02</span>
                <div class="text">
                  <p class="f_medium white">Nashik (Motor Work)</p>
                  <p class="f_book">Eiusmod tempor incididunt ut labore et.....</p>
                </div>
              </div> -->
            </div>
            </div> 
            <div class="carousel-item">
               <div class="caption-box">
              <div class="text-box">
              <h1 class="f_bold">Our Projects</h1>
              <h2 class="f_bold white">Lift Irrigation</h2>
              <p class="f_light white">MEC Solar Water pumps are capable to lift water from river or dams to longer distance with high discharge rates. MEC have done many projects where we lift water from Dam and Rivers to the village lakes for community irrigation. We have pumps from 25Hp to 300Hp for this application.</p>  
              <!-- <a href="" class="common-btn f_medium">Learn More<span class="icon-go-back-left-arrow white"></span></a> -->
            </div>
              <!-- <div class="next-info">
                <span class="f_bold">03</span>
                <div class="text">
                  <p class="f_medium white">Mumbai (Motor Work)</p>
                  <p class="f_book">Eiusmod tempor incididunt ut labore et.....</p>
                </div>
              </div> -->
            </div>
            </div> 
          </div> 
          <div class="arrow-div">
            <a class="left carousel-control control" href="#carousel-a" data-slide="prev">
              <span class="icon-go-back-left-arrow"></span>
            </a>
            <a class="right carousel-control control" href="#carousel-a" data-slide="next">
              <span class="icon-go-back-left-arrow"></span>
            </a>
          </div>
        </div>
      </div>    
    </div>
  </section>
    <!-- our project -->
    <!-- Our Services & Works -->
<section class="services-works" id="our-service">
  <h1 class="heading text-center f_light">Our Services & Works</h1>
  <div class="container">
    <!-- service slider -->
    <div class="carusel-section service-slider">
      <!-- image slider -->
      <div class="image-outer carousel-wrapper">
        <div class="image-wrapper frame">
          <div id="carousel-b" class="carousel slide carousel-fade carousel-sync carousel-a" data-ride="carousel1">
            <div class="carousel-inner">
              <div class="carousel-item active">
                <div class="img">
                <img src="images/service-design.jpg" alt="image2" class="w-100">
                </div>
              </div>
              <div class="carousel-item">
                <div class="img">
                <img src="images/manufacturing.jpg" alt="image2"  class="w-100">
              </div>
              </div>
              <div class="carousel-item">
                <div class="img">
                <img src="images/s-services.jpg" alt="image2" class="w-100">
              </div>
              </div>        
            </div>
            <div class="arrow-div">
              <a class="right carousel-control" href="#carousel-b" data-slide="next">
                <span class="icon-1256495 left-arrow"></span>
              </a>
              <a class="left carousel-control" href="#carousel-b" data-slide="prev">
                <span class="icon-1256495"></span>
              </a>
            </div> 
          </div>
        </div>
      </div>

      <div class="text-wrapper carousel-wrapper">
        <div id="carousel-b" class="carousel carousel-fade carousel-sync carousel-a" data-ride="carousel1">
          <div class="carousel-inner">
            <div class="carousel-item active">
              <div class="caption">
                <h1 class="f_book white">Design</h1>
                <p class="f_light white">MEC having a Dedicated Design team having more than
                10+ years Experience who design our products from Scratch to end including approvals from laboratories using required tools.</p>
                <!-- <a href="" class="common-btn f_medium">Learn More <span class="icon-go-back-left-arrow"></span></a> -->
              </div>
            </div>
            <div class="carousel-item">
              <div class="caption">
                <h1 class="f_book white">Manufacturing</h1>
                <p class="f_light white">MEC having skilled team of operators, Engineers, Quality team and Leadership personals to manufacture and supply to Customer needs with a built up area of 12000+ Sq. ft facility with Sophisticated Tools
                and Test set up with high Quality.</p>
               <!--  <a href="" class="common-btn f_medium">Learn More <span class="icon-go-back-left-arrow"></span></a> -->
              </div> 
            </div> 
               <div class="carousel-item">
              <div class="caption">
                <h1 class="f_book white">Services</h1>
                <p class="f_light white">MEC is having highly skilled Service Engineers who does the Installation and commissioning of our products and provide training to customers. When we receive any issue, Our Engineers will take care remotely or Onsite based on criticality.</p>
               <!--  <a href="" class="common-btn f_medium">Learn More <span class="icon-go-back-left-arrow"></span></a> -->
              </div> 
            </div> 
          </div> 

          <div class="arrow-div d-none d-md-block">
            <a class="right carousel-control" href="#carousel-b" data-slide="next">
              <span class="icon-1256495"></span>
            </a>
          </div>
        </div>
      </div>    
    </div>
  
  <!--work section -->
  <?php if(!empty($blog_list)){ ?>
  <div class="work-section">
    <div class="owl-carousel owl-theme work-slider">
    <?php foreach($blog_list as $key => $blog_row){ ?>
      <a href="<?php echo BASE_URL."blog-detail/".$blog_row['content']['url_title']; ?>" class="item">
        <img src="<?php echo base_url().$blog_row[$blog_fields['image']][0]['media_path']; ?>" alt="Slider-image">
        <div class="caption">
            <h1 class="white f_book"><?php echo dateformat($blog_row[$blog_fields['date']][0]['content_value']); ?></h1>
            <p class="white f_book"><?php echo strip_tags(word_limiter($blog_row[$blog_fields['description']][0]['content_value'],11)); ?></p>
        </div>
      </a>
    <?php } ?>
    </div>
  </div>
  <?php } ?>
  </div>
</section>
     <!-- Our Services & Works -->

  <!-- A Sustainable  Future Awaits You! -->
  <section class="sustainable">
    <img src="images/icon-bg.png" class=" icon-img" alt="Icon">
    <img src="images/sollar-bg.png" class=" solar-img" alt="Solar image">
    <div class="container">
      <div class="text">
        <h1 class="f_bold blue">Contact</h1>
        <h2 class="f_light blue">A Sustainable  Future Awaits You!</h2>
        <p class="f_light gray">So, let's do this. Let's talk solar.</p>
        <a href="" class="white common-btn f_medium" id="request" data-toggle="modal" data-target="#request-quote">Get A Quote<span class="icon-go-back-left-arrow white"></span></a>
      </div>
    </div>
  </section>

  <?php include("footer.php"); ?>
  <script type="text/javascript">
    $(document).ready(function(){

      // on click animate body
      $('.animate-arrow').on('click', function(event){
        var hash=this.hash;
        if(this.hash!=''){
          event.preventDefault();
          $('html, body').animate({
            scrollTop:$(hash).offset().top -60
          }, 800, function(){
            window.location.hash=hash;
          });
        }
      });
    
      $('.home-slider').owlCarousel({
        loop:false,
        margin:0,
        dots:false,
        nav:true,
        smartSpeed:800,
        navText: ['<span class="icon-1256495"></span>','<span class="icon-1256495"></span>'], 
        autoplay:false,
        responsive:{
          0:{
            items:1
          },
          600:{
            items:1
          },
          1000:{
            items:1
          }
        }
      });
      // work slider
       $('.work-slider').owlCarousel({
        loop:false,
        margin:0,
        dots:false,
        nav:false,
        smartSpeed:800,
        //navText: ['<span class="icon-go-back-left-arrow"></span>','<span class="icon-go-back-left-arrow"></span>'], 
        autoplay:false,
        autoplayTimeout:5000,
        responsive:{
          0:{
            items:1
          },
          600:{
            items:1
          },
          1000:{
            items:1
          }
        }
      });

 var home_carousel = $('.home-slider').owlCarousel();
// onclick element change item active element //

$(document).on('click','.img-slide li',function(){
  var index = $(this).index();
  $('.img-slide li a').removeClass('active');
  $('.img-slide li:nth-child('+(index+1)+') a').addClass('active');
  home_carousel.trigger('to.owl.carousel', [index]);
});

// on dragg change item active element //
home_carousel.on('changed.owl.carousel', function(event) {
  $('.img-slide li a').removeClass('active');

  if((event.item.index+1) == event.item.count) {
    setTimeout(function() {
      home_carousel.trigger('to.owl.carousel', [0]);         
    }, 8000);
  }

  $('.img-slide li:nth-child('+(event.item.index+1)+') a').addClass('active');
});

}); 

// syncronos slider

$('.services-works .carousel').carousel({
  interval:false,
});
$('.services-works .carousel-sync').carousel('cycle');
$('.services-works .carousel').on('click', '.carousel-control[data-slide]', function (ev) {
  ev.preventDefault();
  $('.services-works .carousel').carousel($(this).data('slide'));
});
// $('.services-works .carousel').on('mouseover', function(ev) {
//   ev.preventDefault();
//   $('.services-works .carousel').carousel('pause');
// });
// $('.services-works .carousel').on('mouseleave', function(ev) {
//   ev.preventDefault();
//   $('.services-works .carousel').carousel('cycle');
// });






$('.projects .carousel').carousel({
  interval:false,
});


$('.projects .carousel-sync2').carousel('cycle');
$('.projects .carousel').on('click', '.carousel-control.control[data-slide]', function (ev) {
  ev.preventDefault();
  $('.projects .carousel').carousel($(this).data('slide'));
});


// $('.projects .carousel').on('mouseover', function(ev) {
//   ev.preventDefault();
//   $('.projects .carousel').carousel('pause');
// });
// $('.projects .carousel').on('mouseleave', function(ev) {
//   ev.preventDefault();
//   $('.projects .carousel').carousel('cycle');
// });




// impact slider
var $owl = $('.impact-slider');
$owl.children().each( function( index ) {
$(this).attr( 'data-position', index ); 
});

$owl.owlCarousel({
  center: true,
  loop: true,
  items: 3,
});

$(document).on('click', '.owl-item>div', function() {
  var $speed = 700;  
  $owl.trigger('to.owl.carousel', [$(this).data( 'position' ), $speed] );
});

// impact-element trigger click slider active
$('.impact-element a').on('click',function(e){
  e.preventDefault();
  var show=$(this).attr('href');
  $('.impact-element a').removeClass('active');
  $(this).addClass('active');
  $('.impact-right .item').removeClass('active');
  $(show).trigger('click');
});
//slider item active trigger click impact-element add class active
var active_id=$('.impact-right .item').attr('id');
$('.impact-right .item').on('click',function(e){
  e.preventDefault();
  var active_id=$(this).attr('id');
  $('.impact-element li a').removeClass('active');
  $('.impact-element li a[href="#'+active_id+'"]').addClass('active');
});

// counter js animation script start 

function formatter (value) {
  return value.toFixed(0).replace(/\B(?=(?:\d{3})+(?!\d))/g, ',');
}

var a = 0;
$(window).scroll(function() {
  if($('.counter').length > 0) {
    var oTop = $('.counter').offset().top - window.innerHeight;
    if (a == 0 && $(window).scrollTop() > oTop) {
      $('.counter-value').each(function() {
        var $this = $(this),
        countTo = $this.attr('data-count');
        $({
          countNum: $this.text()
        }).animate({
          countNum: countTo
        },
        {
          duration: 2000,
          easing: 'swing',
          step: function() {
            $this.text(formatter(Math.floor(this.countNum)));
          },
          complete: function() {
            $this.text(formatter(this.countNum));
          }
        });
      });
      a = 1;
    }
  }
});


  </script>
  </body>
  </html> 