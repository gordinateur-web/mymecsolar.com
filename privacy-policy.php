<?php require_once("config.php"); ?>
<!DOCTYPE html>
<html lang="en">
<head>
   <?php include("head.php"); ?>
 <title>Privacy Policy | Mymecsolar</title>
  <meta  name="keywords" content="Pump & Motor, Controller , Remote Monitoring System">
  <meta  name="description" content="MEC has pioneered, innovated and excelled in the engineering and manufacturing of solar powered water pumping. We design, develop and manufacture the widest range of solar pumps for any company. Mechanical, electronic and software design is all in-house with a specialized team that have been working in solar pumping for 10 years.">
</head>
<body>
   <?php include("header.php"); ?>
   <section class="breadcum">
      <img src="images/privacy-policy.jpg" class="img-fluid w-100 lazy-image" alt="privacy-policy-breadcum">
      <div class="container breadcum_container">
         <h1 class="white f_light">Privacy Policy</h1>
         <a href="index.php" class="white f_light" title="Home">Home</a>
       <span class="icon-1256495 white"></span>
        <span class="white f_medium">Privacy Policy</span>
      </div>        
   </section>
  <section class="common-pages" data-aos="fade-up" data-aos-delay="100">
            <div class="container">
                
                <p class="f-light">We respect the privacy of all the individuals and companies working with us. We made a policy in this regard and recommend everyone doing business with us to read this and understand our approach towards use of personal data. By submitting personal data to us, you will be treated as having given your permission where necessary and appropriate, for disclosures referred to in this policy</p>
                 <h1 class="f-book">Purposes of the collection of your data :</h1>
                <p class="f-light">One of the purposes of our website is to inform you of who we are and what we do. We collect and use personal information (including name, address, telephone number and email) to better provide you with the required services, or information. We would therefore use your personal information in order to: respond to queries or requests submitted by you process orders or applications submitted by you</p>
                
                <p class="f-light">Administer or otherwise carry out our obligations in relation to any agreement you have with us anticipate and resolve problems with any goods or services supplied to you create products or services that may meet your needs. To optimise our services we may wish to use your personal data for direct marketing. As we respect your privacy we will only use your personal data for this purpose when you are aware thereof and if required we will request your consent prior to using your personal data for direct marketing.</p>
                <p class="f-light">Additionally, if at any time you wish us to stop using your information for any or all of the above purposes, please contact us as set out below. We will stop the use of your information for such purposes as soon as it is reasonably possible to do so. Except as set out in this privacy policy, we will not disclose any personally identifiable information without your permission unless we are legally entitled or required to do so (for example, if required to do so by legal process or for the purposes of prevention of fraud or other crime) or if we believe that such action is necessary to protect and/or defend our rights, property or personal safety and those of our users/customers or other individuals. Please be assured that we will not use your information for any of the purposes if you have indicated that you do not wish us to use your information in this way when submitting the information or at a later stage.</p>
                 <h1 class="f-book">Security of your personal data :</h1>
                <p class="f-light">We have implemented technology and policies with the objective of protecting your privacy from unauthorised access and improper use and will update these measures as new technology becomes available, as appropriate.</p>
                 <h1 class="f-book">Cookies policy</h1>
                 <p>What is a cookie?</p>
                <p class="f-light">Cookies are small data files that your browser places on your computer or device. Cookies help your browser navigate a website and the cookies themselves cannot collect any information stored on your computer or your files. When a server uses a web browser to read cookies they can help a website deliver a more user-friendly service. To protect your privacy, your browser only gives a website access to the cookies it has already sent to you.</p>

                 <p>Why do we use cookies?</p>
                <p class="f-light">We use cookies to learn more about the way you interact with our content and help us to improve your experience when visiting our website. Cookies remember the type of browser you use and which additional browser software you have installed. They also remember your preferences, such as language and region, which remain as your default settings when you revisit the website. Cookies also allow you to rate pages and fill in comment forms. Some of the cookies we use are session cookies and only last until you close your browser, others are persistent cookies which are stored on your computer for longer.</p>
                <p>How do I reject and delete cookies?</p>
                <p class="f-light">We will not use cookies to collect personally identifiable information about you. However, should you wish to do so, you can choose to reject or block the cookies set by mymecsolar by changing your browser settings – see the Help function within your browser for further details. Please note that most browsers automatically accept cookies so if you do not wish cookies to be used you may need to actively delete or block the cookies. Note, however, that if you reject the use of cookies you will still be able to visit our websites but some of the functions may not work correctly.</p>
                
            </div>
        </section>
 <?php include("footer.php"); ?>
</body>
</html>