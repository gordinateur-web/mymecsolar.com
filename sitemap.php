    <?php require_once("config.php"); ?>
<!DOCTYPE html>
<html lang="en">
<head>
   <?php include("head.php"); ?>
 <title>Sitemap | Mymecsolar</title>
  <meta  name="keywords" content="Pump & Motor, Controller , Remote Monitoring System">
  <meta  name="description" content="MEC has pioneered, innovated and excelled in the engineering and manufacturing of solar powered water pumping. We design, develop and manufacture the widest range of solar pumps for any company. Mechanical, electronic and software design is all in-house with a specialized team that have been working in solar pumping for 10 years.">
</head>
<body>
   <?php include("header.php"); ?>
   <section class="breadcum">
      <img src="images/about.jpg" class="img-fluid w-100" alt="about-breadcum">
      <div class="container breadcum_container">
         <h1 class="white f_light">Sitemap</h1>
         <a href="<?php echo BASE_URL.'index.php';?>" class="white f_light" title="Home">Home</a>
       <span class="icon-1256495 white"></span>
        <span class="white f_medium">Sitemap</span>
      </div>        
   </section>

   <section class="sitemap">
    <div class="container">
      <!-- <h1 class="title f-sm-bold black text-center">Sitemap</h1> -->
      <div class="row">
        <div class="col-7 col-md-2">
          <a href="<?php echo BASE_URL.'index.php';?>" class="m-menu" title="Home">Home</a>
            <a href="<?php echo BASE_URL.'expertise.php';?>" class="m-menu" title="Expertise">Expertise</a>
          <a href="<?php echo BASE_URL.'gallery.php';?>" class="m-menu" title="Gallery">Gallery</a>
          <a href="<?php echo BASE_URL.'clients.php';?>" class="m-menu" title="Clients">Clients</a>

          <a href="<?php echo BASE_URL.'blogs.php';?>" class="m-menu" title="Blog">Blog</a>
          <a href="<?php echo BASE_URL.'career.php';?>" class="m-menu" title="Career">Career</a>
          <a href="<?php echo BASE_URL.'troubleshooting.php';?>" class="m-menu" title="Trouble Shooting & Service Center">Trouble Shooting <br> & Service Center</a>
        </div>
        <div class="col-5 col-md-2">
          <a href="<?php echo BASE_URL.'team.php';?>" class="m-menu" title="Team">Team</a>
          <a href="<?php echo BASE_URL.'news-&-media.php';?>" class="m-menu" title="News & Media">News & Media</a>
          <a href="images/Company-Brochure.pdf" class="m-menu" title="Download" target="_blank">Download</a>
          <a href="<?php echo BASE_URL.'faqs.php';?>" class="m-menu" title="FAQ's">FAQ's</a>
          <a href="<?php echo BASE_URL.'contact.php';?>" class="m-menu" title="Contact">Contact</a>          
        </div>  
        <div class="col-6 col-md-2">
          <br class="d-block d-md-none">
          <a  class="m-menu">About</a>
          <a href="<?php echo BASE_URL.'about.php#overview';?>" class="s-menu f_light" title="Overview">Overview</a>
          <a href="<?php echo BASE_URL.'about.php#vision-mission';?>" class="s-menu f_light" title="Vision">Vision</a>
          <a href="<?php echo BASE_URL.'about.php#vision-mission';?>" class="s-menu f_light" title="Mission">Mission</a>
          <a href="<?php echo BASE_URL.'about.php#vision-mission';?>" class="s-menu f_light" title="Values">Values</a>
          <a href="<?php echo BASE_URL.'about.php#leadership';?>" class="s-menu f_light" title="Leadership">Leadership</a>
        </div>
        <div class="col-6 col-md-2">
          <br class="d-block d-md-none">
          <a  class="m-menu about">Products</a>
          <a href="<?php echo BASE_URL.'product/pump-motor';?>" class="s-menu f_light" title="Pump & Motor">Pump & Motor</a>
          <a href="<?php echo BASE_URL.'product/solar-pump-mppt-controller';?>" class="s-menu f_light" title="Controller">Controller</a>
          <a href="<?php echo BASE_URL.'product/remote-monitoring-system-rms';?>" class="s-menu f_light" title="Remote Monitoring System">Remote Monitoring System</a>
        </div>
         
         <div class="col-6 col-md-2">
           <br class="d-block d-md-none">
          <a class="m-menu about">Other Links</a>
          <a href="<?php echo BASE_URL.'quality-policy.php';?>" class="s-menu f_light" title="Quality Policy">Quality Policy</a>
          <a href="<?php echo BASE_URL.'terms-condition.php';?>" class="s-menu f_light" title="Terms & Condition">Terms & Condition</a>
          <a href="<?php echo BASE_URL.'privacy-policy.php';?>" class="s-menu f_light" title="Privacy Policy">Privacy Policy</a>
          <a href="<?php echo BASE_URL.'disclaimer.php';?>" class="s-menu f_light" title="Disclaimer">Disclaimer</a>

        </div>
      </div>
    </div>
  </section>

     <?php include("footer.php"); ?>

</body>
</html>