<?php require_once("config.php"); ?>
<!DOCTYPE html>
<html lang="en">
<head>
   <?php include("head.php"); ?>
 <title>404-Error | Mymecsolar</title>
  <meta  name="keywords" content="Pump & Motor, Controller , Remote Monitoring System">
  <meta  name="description" content="MEC has pioneered, innovated and excelled in the engineering and manufacturing of solar powered water pumping. We design, develop and manufacture the widest range of solar pumps for any company. Mechanical, electronic and software design is all in-house with a specialized team that have been working in solar pumping for 10 years.">
</head>
<body>
   <?php include("header.php"); ?>
   <section class="breadcum">
      <img src="<?php echo BASE_URL.'images/404.jpg';?>" class="img-fluid w-100" alt="404-breadcum">
   <!--    <div class="container breadcum_container">
         <h1 class="white f_light">404-Error</h1>
         <a href="index.php" class="white f_light" title="Home">Home</a>
       <span class="icon-1256495 white"></span>
        <span class="white f_medium">404-Error</span>
      </div> -->        
   </section>
   <section class="error-div">
    <div class="container text-center">
      <h1 class="blue f_bold">Oops!</h1>
      <h2 class="gray f_mdum">404 Not Found</h2>
      <p class="gray f_light">Sorry, an error has occured, Requested page not found!</p>

      <a href="<?php echo BASE_URL.'index.php';?>" class="common-btn">Home</a>
      <a href="<?php echo BASE_URL.'contact.php';?>" class="common-btn">Contact</a>
    </div>
  </section>
 <?php include("footer.php"); ?>
</body>
</html>