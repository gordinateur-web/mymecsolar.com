<?php require_once("config.php"); ?>
<!DOCTYPE html>
<html lang="en">
<head>
   <?php include("head.php"); ?>
 <title>Expertise | Mymecsolar</title>
 <meta  name="keywords" content="Pump & Motor, Controller , Remote Monitoring System">
  <meta  name="description" content="MEC has pioneered, innovated and excelled in the engineering and manufacturing of solar powered water pumping. We design, develop and manufacture the widest range of solar pumps for any company. Mechanical, electronic and software design is all in-house with a specialized team that have been working in solar pumping for 10 years.">
</head>
<body>
   <?php include("header.php"); ?>
   <section class="breadcum">
      <img src="images/about.jpg" class="img-fluid w-100 lazy-image" alt="about-breadcum">
      <div class="container breadcum_container">
         <h1 class="white f_light">Expertise</h1>
         <a href="index.php" class="white f_light" title="Home">Home</a>
       <span class="icon-1256495 white"></span>
        <span class="white f_medium">Expertise</span>
      </div>        
   </section>
   <section class="expertise-main">
    <div class="container">
      <h1 class="heading f_light text-center">MANUFACTURING FACILITY</h1>
      <div class="expertise-wrap">
          <div class="img-wrapp">
            <img src="images/area.jpg" alt="Expertise" class="lazy-image">
          </div>
          <div class="text-wrapp">
            <h2 class="f_medium">Area</h2>
            <p class="f-book">MEC is having about 12000+ Sq. ft area for Operations.</p>
          </div>
      </div>
       <div class="expertise-wrap">
          <div class="img-wrapp">
            <img src="images/skilled-employees.jpg" alt="Expertise" class="lazy-image">
          </div>
          <div class="text-wrapp">
             <h2 class="f_medium">Skilled Employees</h2>
            <p class="f-book">MEC is having 100+ Employees in different departments like
            Design, Manufacturing, Quality, Supply Chain, Stores and Inventory
            and Services.</p>
          </div>
      </div>
       <div class="expertise-wrap">
          <div class="img-wrapp">
            <img src="images/realistic-method-testing.jpg" alt="Expertise" class="lazy-image">
          </div>
           <div class="text-wrapp">
           <h2 class="f_medium">Realistic Method Testing</h2>
            <p class="f-book">MEC has designed and established a Testing facility
            realistic to field condition.</p>
          </div>
      </div>
       <div class="expertise-wrap">
          <div class="img-wrapp">
            <img src="images/ac-dc.jpg" alt="Expertise" class="lazy-image">
          </div>
          <div class="text-wrapp">
            <h2 class="f_medium">AC,DC Simulated Power</h2>
            <p class="f-book">Source facility up to 1000V</p>
          </div>
      </div>
       <div class="expertise-wrap">
          <div class="img-wrapp">
            <img src="images/ware-house.jpg" alt="Expertise" class="">
          </div>
          <div class="text-wrapp">
            <h2 class="f_medium"> Well Established Ware House</h2>
            <p class="f-book">MEC having well established wear house.
          Material receipts, Issuance, Inventory, Production orders
          and Quality clearance are monitored by “ERP”</p>
          </div>
      </div>
     <!--  <div class="row">
        <div class="col-md-6">
          <div class="img-wrapp">
            <div class="img-inner">
            <img src="images/expertise.jpg"  alt="Expertise" class="lazy-image">
            </div>
          </div>
        </div>
        <div class="col-md-6">
          
            <div class="facility-wrapp">
              <div class="facility-single">
                <div class="icon-wrapp">
                  <span class="new-icon-Icon2"><span class="path1"></span><span class="path2"></span><span class="path3"></span><span class="path4"></span><span class="path5"></span><span class="path6"></span><span class="path7"></span><span class="path8"></span><span class="path9"></span><span class="path10"></span><span class="path11"></span><span class="path12"></span><span class="path13"></span><span class="path14"></span><span class="path15"></span><span class="path16"></span><span class="path17"></span><span class="path18"></span><span class="path19"></span><span class="path20"></span><span class="path21"></span><span class="path22"></span><span class="path23"></span><span class="path24"></span><span class="path25"></span><span class="path26"></span><span class="path27"></span><span class="path28"></span><span class="path29"></span><span class="path30"></span><span class="path31"></span><span class="path32"></span><span class="path33"></span><span class="path34"></span><span class="path35"></span><span class="path36"></span><span class="path37"></span><span class="path38"></span><span class="path39"></span><span class="path40"></span><span class="path41"></span><span class="path42"></span></span>
                </div>
                <p class="f_book"> 4000 sqft <br> Floor</p>
              </div>
              <div class="facility-single">
                <div class="icon-wrapp">
                 <span class="new-icon-Icon1"></span>
                </div>
                <p class="f_book"> 24 hours Power<br> Backup</p>
              </div>
              <div class="facility-single">
                <div class="icon-wrapp">
                   <span class="new-icon-Icon5"></span>
                </div>
                <p class="f_book"> 150+ Skilled <br> Technicians</p>
              </div>
              <div class="facility-single">
                <div class="icon-wrapp">
                  <span class="new-icon-Icon3"></span>
                </div>
                <p class="f_book">Realistic method testing <br>facility with PV panel.  </p>
              </div>
              <div class="facility-single">
                <div class="icon-wrapp">
                  <span class="new-icon-Icon4"></span>
                </div>
                <p class="f_book">AC, DC simulated power <br> source facility up to 1000V Well <br> established ware house.  </p>
              </div>
              <div class="facility-single">
                <div class="icon-wrapp">
                  <span class="new-icon-Icon6"></span>
                </div>
                <p class="f_book">Field Relibility 
                <br> Testing</p>
              </div>           
            </div>
          </div>
        </div> -->
        <div class="expertise-btn">
        <!-- <a href="" class="common-btn download-manual" target="_blank"><span class="icon-download-folder"></span><span> Download User Manual</span></a> -->
        <a href="images/Surface Pump Manual_Final.pdf" class="common-btn download-manual" target="_blank"><span class="icon-download-folder"></span><span> Download User Manual</span></a>
      </div>
    </div>
  </section>




 <?php include("footer.php"); ?>
 <script type="text/javascript" src="js/slimscroll.min.js"></script>
<script type="text/javascript">
   $(function(){
      var width=$(window).width();
      if (width > 767) {
        $('.nav-scroll').slimScroll({
          railVisible: true,
         alwaysVisible: true
        });
      }
    });
</script>
</body>
</html>