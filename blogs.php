<?php 
    require_once("config.php"); 
    
    $blog_fields=$CI->config->item('blog_fields');
    
    $filter_blog_fields['content.content_type_id']=$blog_fields['content_type_id'];
    $blog_list=$CI->content_model->get_published_content($filter_blog_fields);
    // dsm($blog_list);die;
    
    ?>
<!DOCTYPE html>
<html lang="en">
    <head>
        <?php include("head.php"); ?>
        <title>Our Blogs | Mymecsolar</title>
        <meta  name="keywords" content="Pump & Motor, Controller , Remote Monitoring System">
  <meta  name="description" content="MEC has pioneered, innovated and excelled in the engineering and manufacturing of solar powered water pumping. We design, develop and manufacture the widest range of solar pumps for any company. Mechanical, electronic and software design is all in-house with a specialized team that have been working in solar pumping for 10 years.">
    </head>
    <body>
        <?php include("header.php"); ?>
        <section class="breadcum">
            <img src="images/blog-breadcum.jpg" class="img-fluid w-100" alt="about-breadcum">
            <div class="container breadcum_container">
                <h1 class="white f_light">Our Blogs</h1>
                <a href="<?php BASE_URL.'index.php';?>" class="white f_light" title="Home">Home</a>
                <span class="icon-1256495 white"></span>
                <span class="white f_medium"> Blogs</span>
            </div>
        </section>
        <section class="blog-main">
            <div class="container">
                <div class="blog-wrapp">
                    <?php foreach ($blog_list as $key => $blog_row) { ?>
                    <div class="blog-single" data-aos="fade-up" data-aos-delay="100">
                        <div class="blog-inner">
                            <a href="<?php echo BASE_URL."blog-detail/".$blog_row['content']['url_title']; ?>">
                                <div class="img-wrapp">
                                    <img src="<?php echo base_url().$blog_row[$blog_fields['image']][0]['media_path']; ?>" alt="Blog" class="img-fluid w-100">
                                </div>
                                <div class="blog-middle">
                                    <h1 class="f_book"><?php echo dateformat($blog_row[$blog_fields['date']][0]['content_value']); ?></h1>
                                    <h2 class="f_book"><?php echo strip_tags(word_limiter($blog_row[$blog_fields['description']][0]['content_value'],11)); ?></h2>
                                     <span class="f_book "> <a  href="<?php echo BASE_URL."blog-detail/".$blog_row['content']['url_title'];  ?> " class="read-more">Read More</a></span>
                                </div>
                            </a>
                           
                        </div>
                    </div>
                    <?php } ?>
                </div>
            </div>
        </section>
        <?php include("footer.php"); ?>
    </body>
</html>