<?php 
    require_once("config.php");

    $form=new Form();
    $form_model=array();

    $old_data=$CI->session->flashdata('old_data',$_POST);

    if(!empty($old_data)) {
        $form_model=$old_data;  
    }  
?>
<!DOCTYPE html>
<html lang="en">
<head>
   <?php include("head.php"); ?>
 <title>Troubleshooting | Mymecsolar</title>
  <meta  name="keywords" content="Pump & Motor, Controller , Remote Monitoring System">
  <meta  name="description" content="MEC has pioneered, innovated and excelled in the engineering and manufacturing of solar powered water pumping. We design, develop and manufacture the widest range of solar pumps for any company. Mechanical, electronic and software design is all in-house with a specialized team that have been working in solar pumping for 10 years.">
</head>
<body>
   <?php include("header.php"); ?>
   <section class="breadcum">
      <img src="images/troubleshooting.jpg" class="img-fluid w-100" alt="about-breadcum">
      <div class="container breadcum_container">
         <h1 class="white f_light">Troubleshooting</h1>
         <a href="index.php" class="white f_light" title="Home">Home</a>
       <span class="icon-1256495 white"></span>
        <span class="white f_medium">Troubleshooting</span>
      </div>        
   </section>
   <section class="troubleshooting">
    <div class="container">
      <div class="row">
        <div class="col-lg-6 col-md-12">
          <div class="img-wrap" data-aos="zoom-in" data-aos-delay="100">
            <div class="img-inner">
              <img src="images/troubleshooting-circle.png" alt="troubleshooting-circle" class="img-fluid">
            </div>
          </div>
        </div>
        <div class="col-lg-6 col-md-12">
          <div class="trouble-right">
            <h1 class="heading blue f_light" data-aos="fade-up" data-aos-delay="100">Troubleshooting</h1>
            <!-- <p class="f_light gray" data-aos="fade-up" data-aos-delay="200">Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod
            tempor incididunt ut labore et dolore magna aliqua.</p> -->
           <!--  <form action="form_send.php"  class="contact-validation form_label_stly" method="post" data-aos="fade-up" data-aos-delay="300"> -->
              <!-- <input type="hidden" name="token" value="<?php //echo $token; ?>"> --><br>
              <?php 
              echo $CI->form->form_model($form_model,BASE_URL_ADMIN.'custom_content/content/save_content_data',array('name'=>'troubleshooting','class'=>'form-validation contact-validation form_label_stly', 'method'=>'post','enctype'=>'multipart/form-data'));
              ?>
              <input type="hidden" name="content_type_section_id" value=""/>
              <input type="hidden" name="section_content_parent" value=""/>
              <input type="hidden" name="content_type_id" value="8"/>
              <div class="row">
                <div class="col-lg-12 col-md-6">
                  <div class="form-group">
                    <label class="hide_label f_book gray">Brand Name </label>
                    <input type="text" name="brand_name_8[]" class="form-control" data-validation="required">
                  </div>
                </div>
                <div class="col-lg-12 col-md-6">
                  <div class="form-group email">
                    <label class="hide_label f_book gray">Product Capacity</label>
                    <input type="text" name="product_capacity_8[]"  class="form-control" data-validation="required">
                  </div>
                </div>
                <div class="col-lg-12 col-md-12">
                  <div class="form-group phone">
                    <label class="hide_label f_book gray">Error Code</label>
                    <input type="text" name="error_code_8[]"  class="form-control" data-validation="required">
                  </div>
                </div>
                <div class="col-lg-12 col-md-12">
                  <button type="submit" class="contact-btn common-btn">Submit <span class="icon-go-back-left-arrow"></span></button>
                </div>
              </div>
            </form> 
          </div>
        </div>
      </div>
    </div>
  </section>
<!-- map section -->
<section class="map-section">
  <div class="container">
    <div class="row">
      <div class="col-md-6">
        <div class="map-left">
          <h1 class="heading blue f_light" data-aos="fade-up" data-aos-delay="100"> We extend Service Support, in terms of design, install & repair, of the highest order</h1>
          <p class="f_light gray" data-aos="fade-up" data-aos-delay="200">A good spread of Service Centres & Service Engineers</p>
          <div class="table-wrapp table-wrapper-scroll-y" data-aos="fade-up" data-aos-delay="300">
            <table class="table table-bordered ">
              <thead class="head">
                <tr>
                  <th>State</th>
                  <th>No of Service <br> Partners </th>
                  <th>No of Service <br> Engineers </th>
                </tr>
              </thead>
              <tbody>
                <tr>
                  <td>Jammu & Kashmir</td>
                  <td>1</td>
                  <td>1</td>
                </tr>
                <tr>
                  <td>Haryana</td>
                  <td>1</td>
                  <td>2</td>
                </tr>
                <tr>
                  <td>New Delhi</td>
                  <td>1</td>
                  <td>2</td>
                </tr>
                <tr>
                  <td>Rajasthan</td>
                  <td>1</td>
                  <td>1</td>
                </tr>
                <tr>
                  <td>Gujarat</td>
                  <td>1</td>
                  <td>2</td>
                </tr>
                <tr>
                  <td>Maharashtra</td>
                  <td>4</td>
                  <td>10</td>
                </tr>
                <tr>
                  <td>Telangana</td>
                  <td>1</td>
                  <td>1</td>
                </tr>
                <tr>
                  <td>Andhra Pradesh</td>
                  <td>2</td>
                  <td>4</td>
                </tr>
                <tr>
                  <td>Chattisgarh</td>
                  <td>1</td>
                  <td>2</td>
                </tr>
                <tr>
                  <td>Odisha</td>
                  <td>1</td>
                  <td>2</td>
                </tr>
                <tr>
                  <td>Jharkhand</td>
                  <td>1</td>
                  <td>4</td>
                </tr>
                <tr>
                  <td>West Bengal</td>
                  <td>1</td>
                  <td>1</td>
                </tr>
                <tr>
                  <td>Karnatak</td>
                  <td>6</td>
                  <td>10</td>
                </tr>
                <tr>
                  <td>Kerala</td>
                  <td>1</td>
                  <td>2</td>
                </tr>
                <tr>
                  <td>Tamilnadu</td>
                  <td>4</td>
                  <td>6</td>
                </tr>
              </tbody>
              <thead class="footer">
                <tr>
                  <th>Total</th>
                  <th>27</th>
                  <th>50 </th>
                </tr>
              </thead>
            </table>
          </div>
        </div>
      </div>
      <div class="col-md-6">
        <div class="map-left" data-aos="zoom-in" data-aos-delay="100">
          <img src="images/location-map.png" alt="Location map" class="img-fluid">
          <div class="mark-location jammu-kashmir" data-toggle="tooltip" data-placement="top" title="Jammu & Kashmir"><span class="icon-pin"></span></div>
          <div class="mark-location himachal-pradesh" data-toggle="tooltip" data-placement="top" title="Himachal Pradeshr"><span class="icon-pin"></span></div>
          <div class="mark-location punjab" data-toggle="tooltip" data-placement="top" title="Punjab"><span class="icon-pin"></span></div>
          <div class="mark-location uttarakhand" data-toggle="tooltip" data-placement="top" title="Uttarakhand"><span class="icon-pin"></span></div>
          <div class="mark-location haryana" data-toggle="tooltip" data-placement="top" title="Haryana"><span class="icon-pin"></span></div>
          <div class="mark-location rajasthan" data-toggle="tooltip" data-placement="top" title="Rajasthan"><span class="icon-pin"></span></div>
          <div class="mark-location uttar-pradesh" data-toggle="tooltip" data-placement="top" title="Uttar Pradesh"><span class="icon-pin"></span></div>
          <div class="mark-location bihar" data-toggle="tooltip" data-placement="top" title="Bihar"><span class="icon-pin"></span></div>
          <div class="mark-location gujrat" data-toggle="tooltip" data-placement="top" title="Gujrat"><span class="icon-pin"></span></div>
          <div class="mark-location madhya-pradesh" data-toggle="tooltip" data-placement="top" title="Madhya Pradesh"><span class="icon-pin"></span></div>
          <div class="mark-location maharashtra" data-toggle="tooltip" data-placement="top" title="Maharashtra"><span class="icon-pin"></span></div>
          <div class="mark-location karnataka" data-toggle="tooltip" data-placement="top" title="Karnataka"><span class="icon-pin"></span></div>
          <div class="mark-location kerala" data-toggle="tooltip" data-placement="top" title="Kerala"><span class="icon-pin"></span></div>
          <div class="mark-location tamil-nadu" data-toggle="tooltip" data-placement="top" title="Tamil Nadu"><span class="icon-pin"></span></div>
          <div class="mark-location andhra-pradesh" data-toggle="tooltip" data-placement="top" title="Andhra Pradesh"><span class="icon-pin"></span></div>
          <div class="mark-location odisha" data-toggle="tooltip" data-placement="top" title="Odisha"><span class="icon-pin"></span></div>
          <div class="mark-location chhattisgarh" data-toggle="tooltip" data-placement="top" title="Chhattisgarh"><span class="icon-pin"></span></div>
          <div class="mark-location jharkhand" data-toggle="tooltip" data-placement="top" title="Jharkhand"><span class="icon-pin"></span></div>
          <div class="mark-location west-bengal" data-toggle="tooltip" data-placement="top" title="West Bengal"><span class="icon-pin"></span></div>
          <div class="mark-location arunachal-pradesh" data-toggle="tooltip" data-placement="top" title="Arunachal Pradesh"><span class="icon-pin"></span></div>
          <div class="mark-location assam" data-toggle="tooltip" data-placement="top" title="Assam"><span class="icon-pin"></span></div>
          <div class="mark-location meghalaya" data-toggle="tooltip" data-placement="top" title="Meghalaya"><span class="icon-pin"></span></div>
          <div class="mark-location sikkim" data-toggle="tooltip" data-placement="top" title="Sikkim"><span class="icon-pin"></span></div>
        </div>
      </div>
    </div>
  </div>
</section>
<?php include("footer.php"); ?>
  <?php include("show_msg.php"); ?>
 <script type="text/javascript">
        $(document).ready(function(){

          $('[data-toggle="tooltip"]').tooltip()

            $.validate({
                form: ".contact-validation",
            });

            $(document).on("focus",".form_label_stly .form-control", function(){
                $(this).parent().find('.hide_label').css("top", "-12px");
                $(this).parent().find('.hide_label').css("font-size", "13px");
            });

            $(document).on("blur",".form_label_stly .form-control",function(){
                $(this).parent().find('.hide_label').css("top", "-12px");
                $(this).parent().find('.hide_label').css("font-size", "13px");
            });

            $(".form_label_stly .form-control").blur(function() {
                if($(this).val() =='' && !$(this).is("select")) {
                    $(this).parent().find('.hide_label').css("top", "5px");
                    $(this).parent().find('.hide_label').css("font-size", "16px");
                }
            });   
        });

    </script>
     
</body>
</html>