<?php 
    require_once("config.php"); 

    $blog_fields=$CI->config->item('blog_fields');

    $url_title=$CI->uri->segment(2);

    if(!empty($url_title))
          $filter_blog['content.url_title']=$url_title;

    $filter_blog['content.content_type_id']=$blog_fields['content_type_id'];
    $blogData=$CI->content_model->get_published_content($filter_blog);
    if(empty($blogData)){
      redirect(BASE_URL.'404-error.php');
    }
    $filter_next_blog['LIMIT']=3;
    $filter_next_blog['content.content_id <']=$blogData[0]['content']['content_id'];
    $filter_next_blog['content.content_type_id']=$blog_fields['content_type_id'];
    $filter_next_blog['content.content_published']=1;
    $filter_next_blog['content.deleted_at']=NULL;
    $filter_next_blog['WHERE'][]="(content.content_status is null or content.content_status='Approved')";
    $next_blog_list=$CI->content_model->get_content_with_value($filter_next_blog);
    // dsm($next_blog_list);die;

?>
<!DOCTYPE html>
<html lang="en">
<head>
  <?php include("head.php"); ?>
  <title><?php echo $blogData[0]['content']['content_title'];?> | Mymecsolar</title>
   <meta property="og:url" content="<?php echo BASE_URL."blog-detail/".$blogData[0]['content']['url_title']; ?>"/>
  <meta property="og:image" content="<?php echo base_url().$blogData[0][$blog_fields['image']][0]['media_path']; ?>"/>
  <meta property="og:description" name="description" content="<?php echo word_limiter(strip_tags($blogData[0][$blog_fields['description']][0]['content_value']),30); ?>">
  <meta property="twitter:card" content="summary_large_image">
  <meta property="twitter:title" content="<?php echo $blogData[0]['content']['content_title']; ?>">
  <meta property="twitter:description" content="<?php echo word_limiter(strip_tags($blogData[0][$blog_fields['description']][0]['content_value']),30); ?>">
  <meta property="twitter:image" content="<?php echo base_url().$blogData[0][$blog_fields['image']][0]['media_path']; ?>">
  <meta property="twitter:site" content="@Mymecsolar">
  <meta property="twitter:creator" content="@Mymecsolar">
  <meta  name="keywords" content="Pump & Motor, Controller , Remote Monitoring System">
  <meta  name="description" content="MEC has pioneered, innovated and excelled in the engineering and manufacturing of solar powered water pumping. We design, develop and manufacture the widest range of solar pumps for any company. Mechanical, electronic and software design is all in-house with a specialized team that have been working in solar pumping for 10 years.">
</head>
<body>
   <?php include("header.php"); ?>
   <section class="breadcum">
      <img src="<?php echo BASE_URL.'images/blog-breadcum.jpg'; ?>" class="img-fluid w-100" alt="about-breadcum">
      <div class="container breadcum_container">
         <h1 class="white f_light">Our Blogs</h1>
         <a href="<?php echo BASE_URL.'index.php';?>" class="white f_light" title="Home">Home</a>
       <span class="icon-1256495 white"></span>
        <span class="white f_medium"> Blogs</span>
      </div>        
   </section>
   <!-- blog detail -->
   <section class="blog-detail">
     <div class="container">
       <div class="detail-wrap">
         <div class="img-wrapp">
           <img src="<?php echo base_url().$blogData[0][$blog_fields['image']][0]['media_path']; ?>" alt="blog-detail" class="img-fluid">
         </div>
         <h1 class="f_medium blue"><?php echo $blogData[0]['content']['content_title'];?></h1>
         <h2 class="f_medium"><?php echo dateformat($blogData[0][$blog_fields['date']][0]['content_value']); ?></h2>
         <?php echo $blogData[0][$blog_fields['description']][0]['content_value']; ?>

         <!-- share blog -->
         <div class="social-share">
          <span class="f_medium blue">Share On:</span>
          <input type="hidden" id='share_title_<?php echo $blogData[0]['content']['content_id']; ?>' value="<?php echo $blogData[0]['content']['content_title']; ?>">
          <input type="hidden" id='share_description_<?php echo $blogData[0]['content']['content_id']; ?>' value="<?php echo word_limiter(strip_tags($blogData[0][$blog_fields['description']][0]['content_value']),30); ?>">
          <input type="hidden" id='share_img_url_<?php echo $blogData[0]['content']['content_id']; ?>' value="<?php echo base_url().$blogData[0][$blog_fields['image']][0]['media_path']; ?>">
          <input type="hidden" id='share_url_<?php echo $blogData[0]['content']['content_id']; ?>' value="<?php echo BASE_URL."blog-detail/".$blogData[0]['content']['url_title']; ?>">

          <!-- FaceBook-->
          <a href="javascript:void(0)" onclick="socialShare('facebook','<?php echo $blogData[0]['content']['content_id']; ?>')"><i class="fab fa-facebook-f"></i></a>
          <button style="display:none;" id="social_share_facebook_<?php echo $blogData[0]['content']['content_id']; ?>" data-title="" data-description="" data-sharer="" data-url=""></button>
           <!-- Twitter-->
           <a href="javascript:void(0)" onclick="socialShare('twitter','<?php echo $blogData[0]['content']['content_id']; ?>')"><i class="fab fa-twitter"></i></a>
           <button style="display:none;" id="social_share_twitter_<?php echo $blogData[0]['content']['content_id']; ?>" data-title="" data-description="" data-sharer="" data-url=""></button>

           <!-- Linkedin-->
           <a href="javascript:void(0)" onclick="socialShare('linkedin','<?php echo $blogData[0]['content']['content_id']; ?>')"><i class="fab fa-linkedin-in"></i></a>
           <button style="display:none;" id="social_share_linkedin_<?php echo $blogData[0]['content']['content_id']; ?>" data-title="" data-description="" data-sharer="" data-url=""></button>

        </div>
       </div>
     </div>
   </section>
 <!-- Related Blogs -->
  <?php if(!empty($next_blog_list)){ ?>
    
   <section class="blog-main related-blog">
    <div class="container">
      <h1 class="heading text-center blue f_light">Related Blogs</h1>
      <div class="blog-wrapp">

      <?php foreach ($next_blog_list as $key => $next_blog_row) { ?>
        <div class="blog-single">
          <div class="blog-inner" data-aos="fade-up" data-aos-delay="100">
          <a href="<?php echo BASE_URL."blog-detail/".$next_blog_row['content']['url_title']; ?>">
          <div class="img-wrapp">
            <img src="<?php echo base_url().$next_blog_row[$blog_fields['image']][0]['media_path']; ?>" alt="Blog" class="img-fluid w-100">
          </div>
          <div class="blog-middle">
            <h1 class="f_book"><?php echo dateformat($next_blog_row[$blog_fields['date']][0]['content_value']); ?></h1>
            <h2 class="f_book"><?php echo strip_tags(word_limiter($next_blog_row[$blog_fields['description']][0]['content_value'],11)); ?></h2>
          </div>
          </a>
          <div class="blog-bottom">
            <span class="f_book"> <a href="<?php echo BASE_URL."blog-detail/".$next_blog_row['content']['url_title']; ?>">Read More</a></span>
            <span class="f_book">Share</span>
            <div class="share-icon">
              <span class="icon-share share">
                <div class="social">
                  <a href="<?php echo $next_blog_row[$blog_fields['facebook_link']][0]['content_value']; ?>"><i class="fab fa-facebook-f"></i></a>
                  <a href="<?php echo $next_blog_row[$blog_fields['linkedin_link']][0]['content_value']; ?>"><i class="fab fa-linkedin-in"></i></a>
                  <a href="<?php echo $next_blog_row[$blog_fields['twitter_link']][0]['content_value']; ?>"><i class="fab fa-twitter"></i></a>
                </div>
              </span>
            </div>
          </div>
        </div>
      </div>
    <?php } ?>
  </div>
       <!-- View All -->
    <a href="<?php echo BASE_URL.'blogs.php';?>" class="common-btn f_medium">View All <span class="icon-go-back-left-arrow"></span></a>
    </div>
  </section>
  <?php } ?>

 <?php include("footer.php"); ?>
 </body>
 <script type="text/javascript" src="<?php echo BASE_URL;?>js/socialShare.js"></script>
</html>