<?php require_once("config.php"); ?>
<!DOCTYPE html>
<html lang="en">
<head>
   <?php include("head.php"); ?>
 <title>Solar Pump  MPPT Controller | Mymecsolar</title>
  <meta  name="keywords" content="Pump & Motor, Controller , Remote Monitoring System">
  <meta  name="description" content="MEC has pioneered, innovated and excelled in the engineering and manufacturing of solar powered water pumping. We design, develop and manufacture the widest range of solar pumps for any company. Mechanical, electronic and software design is all in-house with a specialized team that have been working in solar pumping for 10 years.">
</head>
<body  data-spy="scroll" data-target="#list-example" data-offset="130">
   <?php include("header.php"); ?>
   <section class="breadcum">
      <img src="images/product-breadcum.jpg" class="img-fluid w-100" alt="about-breadcum">
      <div class="container breadcum_container">
         <h1 class="white f_light">Solar Pump  MPPT Controller</h1>
         <a href="index.php" class="white f_light" title="Home">Home</a>
       <span class="icon-1256495 white"></span>
        <span class="white f_medium">Products</span>
        <span class="icon-1256495 white"></span>
        <span class="white f_medium">Solar Pump  MPPT Controller</span>
      </div>        
   </section>
   <section class="product-main" data-toggle="affix">

    <div id="list-example" class="list-group">
      <a class="list-group-item list-group-item-action f_medium active" href="#overview">Overview</a>
      <a class="list-group-item list-group-item-action f_medium" href="#features">Features</a>
      <a class="list-group-item list-group-item-action f_medium" href="#specification"> Specification</a> 
    </div>
      <div  class="scrollspy-example">
        <!-- overview -->
        <div class="overview" id="overview">
          <div class="container">
          <div class="row">
            <div class="col-lg-6 col-md-12">
              <div class="img-wrapp">
                <img src="images/product-overview.png" class="img-fluid" alt="product-overview">
              </div>
            </div>
            <div class="col-lg-6 col-md-12">
              <div class="overview-right">
                <h1 class="f_light heading" data-aos="fade-up" data-aos-delay="100">Overview</h1>
                <ul>
                  <span class="icon-go-back-left-arrow"></span>
                  <li class="f_book gray" data-aos="zoom-in" data-aos-delay="200">MEC Solar pump MPPT Drive is a high efficiency controller, Designed & Made in India.</li>
                  <span class="icon-go-back-left-arrow"></span>
                  <li class="f_book gray" data-aos="zoom-in" data-aos-delay="250">The Drive is available in the following models*</li>
                  <span class="icon-go-back-left-arrow"></span>
                  <li class="f_book gray" data-aos="zoom-in" data-aos-delay="300">1HP, 3HP, 5HP drives for pumps using PMSM(Permanent Magnet Synchronous Motor). (DC Motor) designed for Solar application.</li>
                  <span class="icon-go-back-left-arrow"></span>
                  <li class="f_book gray" data-aos="zoom-in" data-aos-delay="350">1HP, 3HP, 5HP drives also available for for pumps using Induction motors (AC Motor)designed for Solar application. </li>
                  <div class="clearfix"></div>
                </ul>
                <div class="btn-div">
                  <a href="contact.php" class="common-btn"><span class="icon-phone-call"></span> Contact Us <span class="icon-go-back-left-arrow"></span></a>
                  <a href="" class="common-btn" data-toggle="modal" data-target="#request-quote" id="request"><span class="icon-1183875"></span>Request a Quote <span class="icon-go-back-left-arrow"></span></a>
                </div>
              </div>
            </div>
          </div>
        </div>
        </div>
 
        <!-- Features -->
        <div class="features" id="features">
          <div class="container">
            <h1 class="heading text-center f_light" data-aos="fade-up" data-aos-delay="200">Features</h1>

            <div class="feature-wrapper">
              <div class="feature-single">
                <div class="feature-wrap">
                  <ul>
                    <li data-aos="fade-left" data-aos-delay="100"><span>Easy configuration for a <br> class of motor types.</span> <span>1</span></li>
                    <li data-aos="fade-left" data-aos-delay="200"><span>Reducing on field <br> installation complexity.</span> <span>2</span></li>
                    <li data-aos="fade-left" data-aos-delay="300"><span>Better Pump Output Per <br> Available Solar Watts.</span> <span>3</span></li>
                    <li data-aos="fade-left" data-aos-delay="400"><span>Pump starts earlier in the day and <br> can run even in low Solar power</span> <span>4</span></li>
                    <li data-aos="fade-left" data-aos-delay="500"><span>Motor drive limits output <br> safely to rating of motor</span> <span>5</span></li>
                    <li data-aos="fade-left" data-aos-delay="600"><span>Ability to add more <br> Panels if required.</span> <span>6</span></li>
                  </ul>
                </div>
              </div>
              <div class="feature-single">
                <div class="img-wrapp">
                  <img src="images/product1.png" alt="Features" class="img-fluid product1" data-aos="zoom-in" data-aos-delay="400">
                  <img src="images/product-circle.png" alt="circle" class="img-fluid circle" data-aos="zoom-in" data-aos-delay="700">
                </div>
              </div>
              <div class="feature-single">
                <div class="right-features">
                  <ul>
                    <li data-aos="fade-right" data-aos-delay="150"><span>7</span><span>Integrated communication : <br> Modbus over RS485.</span> </li>
                    <li data-aos="fade-right" data-aos-delay="250"><span>8</span><span>Supports remote monitoring,<br> remote access and control and <br>over-the-air firmware upgrade.</span> </li>
                    <li data-aos="fade-right" data-aos-delay="350"><span>9</span><span>Extensive protection and monitoring <br>of variousparameters of operation.</span> </li>
                    <li data-aos="fade-right" data-aos-delay="450"><span>10</span><span>Increased reliability, protects motor and <br>drive from catastrophic failures.</span> </li>
                    <li data-aos="fade-right" data-aos-delay="550"><span>11</span><span>IP65 enclosure.</span> </li>
                  </ul>
                </div>
              </div>
            </div>

          </div>
        </div>

        <!-- Specification -->
        <div class="specification" id="specification">
        <h1 class="heading text-center f_light" data-aos="fade-up" data-aos-delay="100">Specifications</h1>
        <div class="container">

          <div class="top table-responsive-md">
            <table class="table table-bordered" data-aos="fade-up" data-aos-delay="200">
              <thead>
                <tr>
                  <th></th>
                  <th>Motor <br> Power</th>
                  <th>Recommended <br> Solar Array </th>
                  <th>Minimum <br> Input Voltage (DC) </th>
                  <th>Maximum Input <br> breadcum Voltage (DC) </th>
                  <th>Maximum Output <br> Current Per Phase </th>
                  <th>Output <br>  Frequency  </th>
                </tr>
              </thead>
              <thead>
                <tr>
                  <th></th>
                  <th>HP</th>
                  <th>KWP </th>
                  <th>V </th>
                  <th>V</th>
                  <th>A(rms) </th>
                  <th>HZ </th>
                </tr>
              </thead>
              <tbody>
                <tr>
                  <td>A</td>
                  <td>1</td>
                  <td>0.8 to 1.2</td>
                  <td>60</td>
                  <td>200</td>
                  <td>7.5</td>
                  <td>Up to 110</td>
                </tr>
                <tr>
                  <td>B</td>
                  <td>3</td>
                  <td>1.8 to 3.5</td>
                  <td>130</td>
                  <td>475</td>
                  <td>7.5</td>
                  <td>Up to 110</td>
                </tr>
                <tr>
                  <td>C</td>
                  <td>5</td>
                  <td>4.5 to 5.5</td>
                  <td>130</td>
                  <td>800</td>
                  <td>7.5</td>
                  <td>Up to 110</td>
                </tr>
              </tbody>
            </table>
          </div>
          
           <!-- Download Manual -->
            <a href="" class="common-btn download-manual"><span class="icon-download-folder"></span><span> Download <i class="f_medium">Manual</i></span></a>
        </div>
        </div>
      </div>
   <!--  </div> -->
                 <!-- Related Products -->
     <div class="related-products" id="related-products">
      <div class="container">
        <h2 class="f_medium text-center" data-aos="fade-up" data-aos-delay="100">Related Products</h2>
        <div class="relatewrapp">
          <div class="related-single" data-aos="fade-up" data-aos-delay="200">
            <div class="img-wrapp">
              <img src="images/related-product1.png" alt="Related Products" class="img-fluid">
            </div>
            <h1 class="f_medium">Solar Pump & motor </h1>
            <a href="" class="common-btn f_medium">View <span class="icon-go-back-left-arrow"></span></a>
          </div>
          <div class="related-single" data-aos="fade-up" data-aos-delay="300">
            <div class="img-wrapp">
              <img src="images/related-product2.png" alt="Related Products" class="img-fluid">
            </div>
            <h1 class="f_medium">Remote Monitoring System</h1>
            <a href="" class="common-btn f_medium">View <span class="icon-go-back-left-arrow"></span></a>
          </div>
        </div>
      </div>
    </div>
  </section>
 <?php include("footer.php"); ?>
 <script type="text/javascript">
   $(document).ready(function(){
    $(".list-group a").on('click', function(event) {
      if (this.hash !== "") {
        event.preventDefault();
        var hash = this.hash;
        $('html, body').animate({ scrollTop: $(hash).offset().top - ($('header').height() + 30)
      }, 800); 
      }  
    });
    //scroll top with hash change on same page
    $( window ).on( 'hashchange', function( e ) { 
      var hash = window.location.hash;
      if(hash!='') {
        $('html, body').animate({
          scrollTop: $(hash).offset().top - ($('header').height() + 30)
        }, 800);
      }
    });
  });
    $(document).on('scroll', function(){

     if($(document).scrollTop() > 350) {
       $('#list-example').addClass('fixed');
     }
     else 
     {
       $('#list-example').removeClass('fixed');
     }
});
 </script>
</body>
</html>

  