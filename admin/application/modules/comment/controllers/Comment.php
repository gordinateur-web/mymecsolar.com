<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Comment extends CI_Controller {

	public function __construct() {
		parent::__construct();
		/* Permission for adminpanel access, Comment module access */
		permission_check(array(1,6));
		
		$this->load->model('comment_model');
		$this->load->model('user/user_model');
		$user_id=$this->session->userdata('user_id');
	}

	public function index() {
		$data['title']="Comment List";

		/* get user name list */
		$filter_user['users.enable']='1';
		$data['user_list']=$this->user_model->get_user($filter_user);

		$this->config->load('custom_content');
		$data['content_type_list']=$this->config->item('content_type_id_map');

		/* search comment */
		$filter=array();
		$user_id=$this->input->get('user_id');
		$content_id=$this->input->get('content_id');
		$comment_status=$this->input->get('comment_status');
		$keyword=$this->input->get('keyword');
		$from_date=$this->input->get('from_date');
		$to_date=$this->input->get('to_date');
		$data['old_data']=$this->input->get();

		if(!empty($user_id)) {
			$filter['users.user_id']=$user_id;
			$data['clearfilter']=true;
		}
		if(!empty($comment_status)) {
			$filter['comment.comment_status']=$comment_status;
			$data['clearfilter']=true;
		}
		if(!empty($content_id)) {
			$filter['content_comment.content_id']=$content_id;
			$data['clearfilter']=true;
		}

		if(!empty($keyword)) {
			$filter['WHERE']="comment.comment_body LIKE '%".$keyword."%' ";
			$data['clearfilter']=true;
		}
		if(!empty($from_date)) {
			$filter['DATE(comment.updated_at) >=']=mysql_dateformat($from_date);
			$data['clearfilter']=true;
			
		}
		if(!empty($to_date)) {
			$filter['DATE(comment.updated_at) <=']=mysql_dateformat($to_date);
			$data['clearfilter']=true;
		}
		/* comment count */
		$count_filter=$filter;
		$count_filter['SELECT']='count(*) as cnt';
		$count_filter['comment.deleted_at']=null;
		$comment_cnt=$this->comment_model->get_comment_content($count_filter);

		/* list pagination config  */
		$config['per_page'] = '100';
		$get_data=$data['old_data'];
		unset($get_data['per_page']);
		$config['base_url'] = base_url().uri_string()."?".http_build_query($get_data);
		$data['pagination_links']=get_pagination_links($comment_cnt[0]['cnt'], $config, false);
		$page=$this->input->get('per_page');
		if(!$page) {
			$page=0;
		}
		$filter_comment=$filter;
		$filter_comment['LIMIT']=array($config['per_page'],$page);
		$filter_comment['comment.deleted_at']=null;
		$data['comment_list']=$this->comment_model->get_comment_content($filter_comment);
		//dsm($data['comment_list']);die;

		view_with_master('comment/comment_list',$data);
	}

	public function comment_edit($comment_id=false) {
		$data=array();
		if(!empty($comment_id)) {
			$filter_comment['comment.comment_id']=$comment_id;
			$edit_data=$this->comment_model->get_comment($filter_comment);
			$data['edit_data']=$edit_data[0];
		}
		$this->load->view('comment/comment_edit',$data);
	}

	public function comment_edit_save($comment_id=false) {
		/* post array */
		$post_array=$this->input->post();
		$edit_id=$this->input->post('comment_id');

		$comment_data=array(
			'comment_status'=>$post_array['comment_status'],
		);

		if(!empty($edit_id)) {
			$this->comment_model->update_comment($comment_data, $edit_id);
		}
		set_message("Comment status changed successfully","success");
		redirect_back();
	}

	public function comment_save() {
		$this->load->library('form_validation');
		$validate_rules=array(
			array(
				'field'=>'comment_body',
				'label'=>'Comment',
				'rules'=>'required',
			)
		);

		$this->form_validation->set_rules($validate_rules);
		
		if ($this->form_validation->run() == FALSE) {
			if($this->input->is_ajax_request()) {
				echo json_encode(array(
					'status'=>'0',
					'message'=>validation_errors()
				));
				die;
			}
			else {
				set_message(validation_errors());
				$this->session->set_flashdata('old_data',$this->input->post());
				redirect_back();
				return 0;
			}
		}

		$user_id=$this->session->userdata('user_id');
		$user_name=$this->session->userdata('name');

		/* post array */
		$post_array=$this->input->post();
		$comment_id=$this->input->post('comment_id');
		$content_id=$this->input->post('content_id');

		$comment_data=array(
			'comment_body'=>$post_array['comment_body'],
			//'comment_status'=>'active',
		);

		/* Get content type config */
		if(!empty($content_id)) {
			$filter_content['content.content_id']=$content_id;
			$content_data=$this->content_model->get_content($filter_content);
			$comment_permission=$this->content_type_model->get_content_type(array('content_type_id'=>$content_data[0]['content_type_id']));
		}

		/* Edit comment */
		if(!empty($comment_id)) {
			$comment_data['updated_at']=date('Y-m-d H:i:s');
			$comment_data['updated_by']=$user_id;
			$this->comment_model->update_comment($comment_data, $comment_id);

			/* get comment data */
			$update_comment=$this->comment_model->get_comment(array('comment.comment_id'=>$comment_id));

			

			if(!empty($update_comment)) {
				$return=array(
					'status'=>'1',
					'comment_id'=>$comment_id,
					'comment_body'=>$update_comment[0]['comment_body'],
					'updated_at'=>dateformat($update_comment[0]['updated_at'], 'F d, Y'),
					
				);
				echo json_encode($return);
				die;
			}
		}
		/* Add new comment */
		else {
			$comment_data['user_id']=$user_id;
			$comment_data['updated_at']=date('Y-m-d H:i:s');
			$comment_data['created_at']=date('Y-m-d H:i:s');
			$comment_data['created_by']=$user_id;

			/* check comment approval for comment status  */
			if(!empty($comment_permission) && $comment_permission[0]['comment_approval']==1) {
				$comment_data['comment_status']='pending';
			}
			else {
				$comment_data['comment_status']='active';
			}

			$comment_id=$this->comment_model->add_comment($comment_data);

			$content_comment_data=array(
				'content_id'=>$post_array['content_id'],
				'comment_id'=>$comment_id,
			);
			$this->comment_model->add_content_comment($content_comment_data, $comment_data['comment_status']);
			$comment=$this->comment_model->get_comment(array('comment.comment_id'=>$comment_id));
			
		}

		/* get comment count */
			$filter_comment['SELECT']='count(*) as cnt_cmnt';
			$filter_comment['comment.deleted_at']=null;
			$filter_comment['content_comment.content_id']=$content_id;
			$filter_comment['WHERE']=array("(comment.comment_status='active' or comment.user_id=".$user_id.')');
			$comment_count=$this->comment_model->get_comment($filter_comment);

		if(!empty($comment)) {
			$return=array(
				'status'=>'1',
				'comment_id'=>$comment[0]['comment_id'],
				'name'=>$comment[0]['name'],
				'designation'=>$comment[0]['designation'],
				'comment_body'=>$comment[0]['comment_body'],
				'updated_at'=>dateformat($comment[0]['updated_at'], 'F d, Y'),
				'profile_pic'=>$comment[0]['profile_pic'],
				'comment_count'=>$comment_count[0]['cnt_cmnt'],
			);
			echo json_encode($return);
			die;
		}
		else {
			$return=array(
				'status'=>'0',
				'message'=>'Error while saving comment, Please try again',
			);
			echo json_encode($return);
			die;
		}
	}

	public function comment_delete($comment_id=false) {
		
		if(!empty($comment_id)) {
			$comment_data['deleted_at']=date('Y-m-d H:i:s');
			$comment_data['deleted_by']=$user_id;
			$this->comment_model->delete_comment($comment_data, $comment_id);
			set_message("Comment deleted successfully","success");
			redirect_back();
		}
		else {
			set_message("Please select valid comment");
			redirect_back();
		}
	}

	public function forum_comment() {
		$data['title']="Comment List";
		$this->load->model('front/forum_model');

		/* get user name list */
		$filter_user['users.enable']='1';
		$data['user_list']=$this->user_model->get_user($filter_user);

		/* get forum comment */
		$data['forum_commnet_list']=$this->forum_model->get_forum_comment_with_user();
		//dsm($data['forum_commnet_list']);die;

		view_with_master('comment/forum_comment_list',$data);
	}
}