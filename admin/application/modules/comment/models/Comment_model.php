<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Comment_model extends CI_model {
	public $comment_type_map;

	public function __construct() {
		$this->load->model('custom_content/content_type_model');

		/* Used for save, update comment */
		$this->comment_type_map=array(
			'content'=>array(
				'table'=>'content_comment',
				'table_refefence_column'=>'content_comment.content_id',
				/* this should be available in comment_model */
				'get_comment_function'=>'get_comment',
				'get_active_comment_function'=>'get_active_comment',
				'add_comment_function'=>'add_content_comment'
			),
			'forum'=>array(
				'table'=>'forum_comment',
				'table_refefence_column'=>'forum_comment.forum_question_id',
				/* this should be available in comment_model */
				'get_comment_function'=>'get_forum_comment',
				'get_active_comment_function'=>'get_active_forum_comment',
				'add_comment_function'=>'add_forum_comment'
			)
		);
	}

	public function add_comment($data) {
		$this->db->insert('comment',$data);
		return $this->db->insert_id();
	}

	public function add_content_comment($data, $comment_status) {
		//dsm($data);die;
		$this->db->insert('content_comment',$data);
		$insert_id = $this->db->insert_id();

		/* Increase comment count in content column */
		if($comment_status == 'active') {
			$this->increase_content_comment($data['content_comment.content_id']);
		}

		return $insert_id;
	}

	/*
		Increase comment count in content table
	*/
	public function increase_content_comment($content_id) {
		$query="update content set comment_count = comment_count+1 where content_id=?";
		return $this->db->query($query, $content_id);
	}
	
	/*
		Decrease comment count in content table
	*/
	public function decrease_content_comment($content_id) {
		$query="update content set comment_count = comment_count-1 where content_id=?";
		return $this->db->query($query, $content_id);
	}

	public function add_forum_comment($data) {
		$this->db->insert('forum_comment',$data);
		return $this->db->insert_id();
	}
	
	public function update_comment($data, $edit_id) {
		$content_comment=$this->get_comment(array('content_comment.comment_id'=>$edit_id));

		/* Increase comment count in content column */
		if($data['comment_status'] =='active' && $content_comment[0]['comment_status']!='active') {
			$this->increase_content_comment($content_comment[0]['content_id']);
		}
		else if($data['comment_status'] !='active' && $content_comment[0]['comment_status'] =='active'  ) {
			$this->decrease_content_comment($content_comment[0]['content_id']);
		}

		$this->db->where('comment_id',$edit_id);
		return $this->db->update('comment',$data);
	} 

	/* 
		Return comment of content
	*/
	public function get_comment($filter=false) {
		if($filter){
			apply_filter($filter);	
		}

		if(empty($filter['SELECT'])) {
			$this->db->select('content_comment.*, users.*, comment.*');
		}
		/* Ignore deleted content types and content */
		$this->db->where('comment.deleted_at is null',null,false);
		$this->db->join('users','users.user_id=comment.user_id','left');
		$this->db->join('content_comment','content_comment.comment_id=comment.comment_id','left');
		//$this->db->order_by('comment.created_at', 'DESC');
		$rs=$this->db->get('comment');
		return $rs->result_array();
	}

	/* get comment with status active and all comment of current users */
	public function get_active_comment($filter=false) {

		$user_id=$this->session->userdata('user_id');

		/* show only active comment and current user's pending comment */
		$filter['WHERE']="((comment.comment_status='active') or (comment.user_id=".$user_id." and comment.comment_status='pending'))";
		return $this->get_comment($filter);
	}

	/* 
		Return comment of content
	*/
	public function get_forum_comment($filter=false) {
		if($filter){
			apply_filter($filter);	
		}

		if(empty($filter['SELECT'])) {
			$this->db->select('forum_comment.*, users.*, comment.*');
		}
		/* Ignore deleted content types and content */
		$this->db->where('comment.deleted_at is null',null,false);
		$this->db->join('users','users.user_id=comment.user_id','left');
		$this->db->join('forum_comment','forum_comment.comment_id=comment.comment_id','left');
		//$this->db->order_by('comment.created_at', 'DESC');
		$rs=$this->db->get('comment');
		return $rs->result_array();
	}

	/* get comment with status active and all comment of current users */
	public function get_active_forum_comment($filter=false) {

		$user_id=$this->session->userdata('user_id');

		/* show only active comment and current user's pending comment */
		$filter['WHERE']="((comment.comment_status='active') or (comment.user_id=".$user_id." and comment.comment_status='pending'))";
		return $this->get_forum_comment($filter);
	}

	/* get content with their comment */
	public function get_comment_content($filter=false) {
		if($filter){
			apply_filter($filter);	
		}

		if(empty($filter['SELECT'])) {
			$this->db->select('content.*, content_comment.*, users.*, spam_by.name as user_spam, comment.*');
		}
		/* Ignore deleted content types and content */
		$this->db->where('comment.deleted_at is null',null,false);
		$this->db->where('content.deleted_at is null',null,false);
		$this->db->join('users','users.user_id=comment.user_id','left');
		$this->db->join('users as spam_by','spam_by.user_id=comment.spam_by','left');
		$this->db->join('content_comment','content_comment.comment_id=comment.comment_id');
		$this->db->join('content','content.content_id=content_comment.content_id','left');
		$this->db->order_by('comment.created_at', 'DESC');
		$rs=$this->db->get('comment');
		return $rs->result_array();
	}

	public function delete_comment($data, $edit_id) {
		$content_comment=$this->get_comment(array('content_comment.comment_id'=>$edit_id));
		$this->decrease_content_comment($content_comment[0]['content_id']);
		

		$this->db->where('comment_id',$edit_id);
		return $this->db->update('comment',$data);
	} 

	/* Render content comments 
		@content_id  - Forum or content id
		@comment_type - content or forum
	*/
	public function render_front_comment($content_id, $comment_type='content') {

		/* Get content type config */
		if(empty($content_id)) {
			return "";
		}

		$user_id=$this->session->userdata('user_id');
		$filter_comment=array();

		/* Flag to show load next comment in comment list */
		$data['show_next_load']=false;
		$data['comment_type']=$comment_type;
		$data['like_count']=0;

		/* User list for tagging in comment */
		$this->load->model('user/user_model');
		$user_data = $this->user_model->get_user(array(
			'SELECT'=>'users.user_id,users.employee_id,users.email,users.name,users.designation,users.employee_grade',
			'ORDER_BY'=>array('users.name'=>'asc')
		));
		$data['username_list']=json_encode($user_data);;

		/* show comment based on get parameter */
		$comment_from=$this->input->get('comment_from');
		if(!empty($comment_from)) {
			$filter_comment['comment.comment_id <=']=($comment_from+3);
			$data['show_next_load']=true;
		}

		/* get comment filter */
		$filter_comment['LIMIT']=COMMENT_PER_PAGE;
		$filter_comment['ORDER_BY']=array('comment.comment_id'=>'desc');


		/*
			If comment type of content check permission from content type
		*/
		if($comment_type == 'content') {
			$filter_content['content.content_id']=$content_id;
			$content_data=$this->content_model->get_content($filter_content);

			/* Check content type config for comment */
			$comment_permission=$this->content_type_model->get_content_type(array(
				'content_type_id'=>$content_data[0]['content_type_id'],
			));
			
			/* Return  blank data if comment not allowed */
			if($comment_permission[0]['allow_comment']!=1) {
				return "";
			}

			/* Content comment filter parameter */
			$filter_comment['content_comment.content_id']=$content_id;
			$filter_count['content_comment.content_id']=$content_id;

			$data['comment_list']=$this->get_active_comment($filter_comment);
			$data['content_id']=$content_id;

			/* get comment count */
			$filter_count['SELECT']='count(*) as cnt_cmnt';
			$data['comment_count']=$this->get_active_comment($filter_count);

			/* get like count */
			$filter_like['SELECT']='count(*) as cnt_like';
			$filter_like['likes.content_id']=$content_id;
			$data['like_count']=$this->like_model->get_like($filter_like);

			/* check user like or dislike */
			$check_like['user_id']=$user_id;
			$check_like['content_id']=$content_id;
			$data['check_like']=$this->like_model->get_comment_like($check_like);
		}
		elseif($comment_type == 'forum') {
			/* Content comment filter parameter */
			$filter_comment['forum_comment.forum_question_id']=$content_id;
			$filter_count['forum_comment.forum_question_id']=$content_id;

			$data['comment_list']=$this->get_active_forum_comment($filter_comment);
			$data['content_id']=$content_id;

			/* get comment count */
			$filter_count['SELECT']='count(*) as cnt_cmnt';
			$data['comment_count']=$this->get_active_forum_comment($filter_count);
		}

		$this->load->view('comment/front_comment_list',$data);
	}

	public function add_comment_spam($data) {
		$this->db->insert('comment_spam',$data);
		return $this->db->insert_id();
	}

	public function delete_comment_spam($filter=false) {
		if($filter){
			apply_filter($filter);
		}
		$this->db->delete('comment_spam');
	} 

	/*
		Return user_id array which is mentioned in comment with attribute
		data_userid in strong tag
	*/
	public function get_comment_tagged_users($comment_body) {
		$this->load->library('Simple_html_dom');
		$simple_html=str_get_html($comment_body);
		$users = $simple_html->find('strong[data_userid]');
		$users_array=array();
		if(!empty($users)) {
			foreach ($users as $user) {
				$users_array[]=$user->data_userid;
			}
		}
		return $users_array;
	}


	/* Send comment tagged, owner based on content type setting  notification to users */
	public function send_comment_notification($comment_id, $content_id, $comment_type,  $old_comment_data, $new_comment_data) {
		$this->load->model('custom_content/content_model');
		$this->load->model('notification/notification_model');
		$this->config->load('custom_content');
		$current_user=$this->session->userdata('user_id');
		$current_username=$this->session->userdata('name');

		/* Send notification to content owner */
		if($comment_type == 'content') {
			/* Send notification to content owner based on content type settings */
			$filter_content['content.content_id']=$content_id;
			$content_data=$this->content_model->get_content($filter_content);
			

			/* get content type url for notification */
			$content_type_id_map=$this->config->item('content_type_id_map');
			$content_type_fields=$this->config->item($content_type_id_map[$content_data[0]['content_type_id']]);
			$notification_url=$content_type_fields['url'].'/'.$content_data[0]['url_title'].'?comment_from='.$comment_id;;


			/*
				content setting check for comment and also check 
				if content owner add comment than ignore it  and also ignore if comment is update
			*/
			if(empty($old_comment_data)) {
				if($content_data[0]['comment_notification'] == '1' && $new_comment_data['created_by']!=$content_data[0]['created_by']) {
					$notification_title=$current_username.' has commented on your '.$content_data[0]['content_type_title'].'" '.$content_data[0]['content_title'].' "';

					$notification_array = array(
						'user_id'=>$content_data[0]['created_by'],
						'content_id'=>$content_id,
						'notification_title'=>$notification_title,
						'notification_url'=>$notification_url,
						'created_at'=>date('Y-m-d H:i:s'),
					);
					$this->notification_model->add_notification($notification_array);
				}
			}
		}
		/* Send notification to post owner of forum */
		elseif($comment_type == 'forum') {
			/* Get commented forum data */
			$this->load->model('front/Forum_model');
			$forum_data = $this->Forum_model->get_forum_question(array(
				'forum_question.forum_question_id'=>$content_id
			));
			$notification_url='forum/forum_detail/'.$forum_data[0]['forum_url'].'?comment_from='.$comment_id;;

			/* Ignore comment edit notification */
			if(empty($old_comment_data)) {
				/* Ignore owner of the post's comment */
				if($new_comment_data['created_by']!=$forum_data[0]['created_by']) {
					$notification_title=$current_username.' has commented on your forum post "'.$forum_data[0]['forum_title'].' "';
					$notification_array = array(
						'user_id'=>$forum_data[0]['created_by'],
						'notification_title'=>$notification_title,
						'notification_url'=>$notification_url,
						'created_at'=>date('Y-m-d H:i:s'),
					);
					$this->notification_model->add_notification($notification_array);
				}
			}
		}

		/* Send notification to tagged users */
		$this->send_tagged_user_notification($old_comment_data, $new_comment_data,$notification_url);
	}

	/* Send notification to comment tagged users */
	public function send_tagged_user_notification($old_comment_data,$new_comment_data, $notification_url) {
		$old_tagged_users=array();
		/* Get tagged users from old comment */
		if(!empty($old_comment_data)) {
			$old_tagged_users=$this->get_comment_tagged_users($old_comment_data['comment_body']);	
		}

		/* Get tagged users ids */
		$new_tagged_users=$this->get_comment_tagged_users($new_comment_data['comment_body']);	

		$current_username=$this->session->userdata('name');
		$notification_title=$current_username.' has tagged you in comment.';

		/* find newly added users only */
		$tagged_users=array_diff($new_tagged_users, $old_tagged_users);
		
		/* Send notification to tagged users */
		if(empty($tagged_users)) {
			return false;
		}

		$notification_array=array();
		foreach ($tagged_users as $key=>$user) {
			$notification_array[$key] = array(
				'user_id'=>$user,
				'notification_title'=>$notification_title,
				'notification_url'=>$notification_url,
				'created_at'=>date('Y-m-d H:i:s'),
			);
		}
		return $this->db->insert_batch('notification',$notification_array);
	}
}