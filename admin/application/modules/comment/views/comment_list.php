<div class="row">
	<?php $base_url=base_url();?>
		<div class="col-md-12">
			<div class="box box-primary">
				<div class="box-header with-border">
					<div class="box-title">
						<a data-toggle="modal" data-target="#search_content" class="btn btn-primary btn-sm"><i class="fa fa-search"></i> Search</a>
						<?php if(!empty($clearfilter)) { ?>
							<a href="<?php echo $base_url.'comment/'; ?>" class="btn btn-primary btn-sm">
								<i class="fa fa-search-minus"></i> Clear Search
							</a>
						<?php } ?>
					</div>
					<div class="pull-right">
						<?php if (isset($pagination_links)){ echo $pagination_links; } ?>
					</div>
					
				</div>

				<div class="panel-body">
					<div class="box-body table-responsive">
						<table class='table table-bordered table-striped' id="full-height-datatable">
							<thead>
								<tr>
									<th width="10%">Name</th>
									<th width="40%">Comment</th>
									<th width="25%">Content Title</th>
									<th width="15%">Date</th>
									<th width="5%">Status</th>
									<th width="5%">Action</th>
								</tr>
							</thead>
                    
							<tbody> 
							  	<?php foreach ($comment_list as $key => $comment_row) { 
									$fields=$this->config->item($content_type_list[$comment_row['content_type_id']]);
							  	?>
							  	
								<tr id="materialtr<?php echo $comment_row['comment_id']; ?>">
									
									<td><?php echo $comment_row['name']; ?></td>
									<td width="10%"><?php echo strip_tags($comment_row['comment_body'], '<img>'); ?></td>
									<td>
										<a href="<?php echo base_url().$fields['url'].'/'.$comment_row['url_title'].'?comment_from='.$comment_row['comment_id']; ?>" target="_blank">
											<?php echo word_limiter($comment_row['content_title'], 8); ?>
										</a>
									</td>
									<td><?php echo dateformat($comment_row['updated_at'],0,1); ?></td>
									<td>
									<?php
									if($comment_row['comment_status']=='active') {
										echo '<label class="label label-success">Active</label>';
									}
									if($comment_row['comment_status']=='pending') {
										echo '<label class="label label-warning">Pending</label>';
									}
									if($comment_row['comment_status']=='marked_spam') {
										echo '<label class="label label-info">Spam</label>';
										echo '<br><span class="text-danger">by '.$comment_row['user_spam'].'</span>';

									}
									if($comment_row['comment_status']=='blocked') {
										echo '<label class="label label-danger">Blocked</label>';
									}
									?>	
									</td>
									<td>
										<div class="btn-group options">
											<button type="button" class="btn btn-default btn-sm" dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
												<i class="fa fa-cog"></i> Options
											</button>
											 
											<ul class="dropdown-menu">
											<?php if(permission_check(array(1,6),'and',0)) { ?>
												<li>
													<a class="pointer table-list-edit" data-editurl="comment/comment_edit" data-editid="<?php echo $comment_row['comment_id']; ?>" data-modeltitle="Edit Comment">
														<i class="fa fa-edit fa-margin"></i> change Status
													</a>
												</li>
											<?php } ?>
												<li>
													<a data-id="<?php echo $comment_row['comment_id']; ?>" class="pointer delete-comment">
														<i class="fa fa-trash-o fa-margin"></i> Delete
													</a>
												</li>

											</ul>
										</div>
									</td>
								</tr>
							<?php } ?>

							</tbody>
						</table>
					</div>
				</div>
			</div>
	</div>
</div>


<!-- Modal -->
<div class="modal fade" id="search_content" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" data-backdrop="static">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
				<h4 class="modal-title" id="myModalLabel">Search Comment</h4>
			</div>
			<div class="modal-body">
				<?php 
					$form_model=array();
					/* Fill old data */
					if(!empty($old_data)) {
						$form_model=$old_data;  
					}
					echo $this->form->form_model($form_model, $base_url.'comment/index/', array('name'=>'search_content','id'=>'search_comment_form', 'class'=>'validate-form' , 'method'=>'get')); 
				?>
					<div class="row">
						<div class="col-md-12">
							<div id="error_div"></div>
						</div>
						<div class="col-md-12">
							<div class="col-md-6">
								<label>User Name</label>
								<?php 
								$other_option=array(
									'class'=>'form-control',
									'placeholder'=>'User Name',
								);
								echo $this->form->form_dropdown_fromdatabase('user_id',$user_list,'user_id','name','', $other_option); 
								?>
							</div>
							<div class="col-md-6 col-sm-6">
								<label>Status </label>
								<?php 
								$other_option=array(
									'class'=>'form-control',
									);
								$option=array('active'=>'Active', 'pending'=>'Pending', 'marked_spam'=>'Marked Spam', 'blocked'=>'Blocked');
								echo $this->form->form_dropdown('comment_status', $option,'','', $other_option); 
								?>
							</div>
							<div class="clearfix"></div>

							<div class="col-md-12">
								<label>Keyword</label>
								<?php 
								$other_option=array(
									'class'=>'form-control',
									'placeholder'=>'Keyword',
									);
								echo $this->form->form_input('keyword',$other_option); 
								?>
							</div>
							<div class="clearfix"></div>

							<div class="col-md-6">
								<label>From Date</label>
								<?php 
								$other_option=array(
									'class'=>'form-control datepicker',
									'placeholder'=>'Date',
								);
								echo $this->form->form_input('from_date',$other_option); 
								?>
							</div>

							<div class="col-md-6">
								<label>To Date</label>
								<?php 
								$other_option=array(
									'class'=>'form-control datepicker',
									'placeholder'=>'Date',
								);
								echo $this->form->form_input('to_date',$other_option); 
								?>
							</div>			
							
							

						</div>
					</div>
					<div class="row"><br/>
						<div class="modal-footer">
							<input type="submit" name="submit"  class="btn btn-primary" value="Search">
							<input type="reset" name="reset"  class="btn btn-default" value="Reset">
							<button type="button" class="btn btn-danger" data-dismiss="modal">Close</button>
						</div>							      			
					</div>							      			
				</form>
			</div>
		</div>
	</div>
</div>	

<script type="text/javascript">
	$(document).ready(function() {
		$('.delete-comment').click(function() {
			var id=$(this).attr('data-id');
			swal({
				title: "Are you sure?",
				text: "Comment to delete?",
				type: "warning",
				showCancelButton: true,
				confirmButtonColor: "#DD6B55",
				confirmButtonText: "Yes, delete it!",
				closeOnConfirm: false
			},
			function(){
				window.location = base_url+'comment/comment_delete/'+id;
			});
		});
	})

</script>

