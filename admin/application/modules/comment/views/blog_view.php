<?php
$base_url = base_url();
$form_model=array();
if(!empty($edit_data)) {
	$form_model=$edit_data;
}
/* Fill old data */
$old_data=$this->session->flashdata('old_data');
if(!empty($old_data)) {
	$form_model=$old_data;  
}
echo $this->form->form_model($form_model, $base_url.'comment/comment_save/',array('name'=>'save_comment','id'=>'comment_form'));
if(!empty($edit_data)) {
	echo $this->form->form_hidden('comment_id');
}
?>

<div class="row">
	<?php $base_url=base_url();?>
	<div class="col-md-12">
		<div class="box box-primary">

			<div class="box-header with-border">
			</div>

				<div class="box-body">
					<div id="model_errors"></div>
					
					<p>The government's sudden decision to demonetise ₹500 and ₹1,000 notes without proper infrastructure being put in place has hit hard the common man.</p>

					<p>Though the main idea behind demonetisation was to curb black money, it has resulted in a revolution of another kind — the growth of the digital (or cashless) economy. But with key challenges such as non-availability of strong Internet connectivity and cheap smartphones, can this revolution percolate down to the last Indian?</p>

					<p>The government’s sudden decision to demonetise ₹500 and ₹1,000 notes without proper infrastructure being put in place has hit hard the common man. Though the main idea behind demonetisation was to curb black money, it has resulted in a revolution of another kind - the growth of the digital (or cashless) economy. But with key challenges such as non-availability of strong Internet connectivity and cheap smartphones, can this revolution percolate down to the last Indian?</p>

					<div class="col-md-12 col-sm-6">
						<div class="form-group">
							<label>Comment </label>
							<?php 
							$other_option=array(
								'class'=>'form-control',
								'placeholder'=>'Description',
								'row'=>'2',
							);
							echo $this->form->form_textarea('comment_body', $other_option); 
							?>
						</div>
					</div>

					

				</div>

			<div class="box-footer with-border">
				<div class="box-tools pull-right">
					<input type="reset" class="btn btn-primary" value="Reset">
					<input type="submit" class="btn btn-danger" value="Submit">
				</div>
			</div>

		</div>
	</div>
</div>

<?php echo $this->form->form_close(); ?>

<script src="<?php echo base_url().'public/plugins/ckeditor/ckeditor.js';?>" type="text/javascript"></script>
<script type="text/javascript">

	$(document).ready(function() {	
		CKEDITOR.replace('block_body');
		CKEDITOR.config.disableNativeSpellChecker = false;
	});

</script>