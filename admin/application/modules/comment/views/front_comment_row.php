<?php
    $marked_spam=false;
    if($comment_status=='marked_spam') {
        $marked_spam=true;
    }
    
    if(!isset($new_comment_class)) {
        $new_comment_class='';
    }

?>
<?php $current_user=$this->session->userdata('user_id'); ?>
<div class="row cmnt_box <?php echo $new_comment_class.' comment'.$comment_id; if($marked_spam==true){ echo ' mark-as-spam'; }  ?>" id="<?php echo 'comment'.$comment_id ?>" data-id="<?php echo $comment_id; ?>">
    <div class="width55 pull-left">
        <?php $user_profile_pic=$profile_pic;
        if(empty($profile_pic)) {
            $user_profile_pic=USER_PROFILE_PIC;
        } ?>
        <img src="<?php echo base_url().$user_profile_pic; ?>" class="img-responsive" alt="">
    </div>
    <div class="remaining-width">
        <h5><strong>
            <?php echo $name; ?>, <?php echo $designation; ?>
            </strong>
        </h5>
        <div class="comment-content"><?php echo $comment_body; ?> </div>
        <span class="comment-date">
        <?php echo dateformat($updated_at, 'F d, Y h:i A'); ?>
        </span>
        <ul class="list-inline bullets-grey pull-right">
            <!-- <li><a href="#" title="Like">Likes</a></li> -->
            <?php if($user_id==$current_user) { ?>
                <li>
                    <a title="Edit" data-id="<?php echo $comment_id; ?>" class="edit_comment pointer" >Edit
                    </a>

                </li>
                <li>
                    <a title="Delete" data-id="<?php echo $comment_id; ?>" class="delete_comment pointer" >Delete
                    </a>
                </li>
            <?php } 
            else if($marked_spam==false) { ?>
                <li>
                    <a title="Mark as spam" data-id="<?php echo $comment_id; ?>" class="pointer spam_comment"> Mark as spam </a>
                </li>
            <?php } ?>
        </ul>
    </div>
</div>