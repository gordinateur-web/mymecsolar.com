<?php 
    $base_url=base_url();
?>
<!-- comment section start -->
<div class="comment-partial-width">
    <div class="comment-container">
        <ul class="comment-like-inline">
            <li><a title="Comments"><i class="glyphicon glyphicon-comment" aria-hidden="true"></i>&nbsp;<span class="comment_count">
                <?php echo $comment_count[0]['cnt_cmnt']; ?>
                </span> Comments</a>
            </li>
            <?php
            /* check permission content type allowed like */
            if(!empty($content_id) && $comment_type == 'content') {
                $filter_content['content.content_id']=$content_id;
                $content_data=$this->content_model->get_content($filter_content);
            
            $comment_permission=$this->content_type_model->get_content_type(array('content_type_id'=>$content_data[0]['content_type_id']));

            if($comment_permission[0]['allow_like']==1) {
            ?>
            <li>
                <?php if(!empty($check_like)) { ?>
                    <a title="Like" class="pointer comment_like" data-id="<?php echo $content_id; ?>" ><i class="fa fa-thumbs-up" aria-hidden="true"></i>&nbsp;
                    <span class="like_count"><?php echo $like_count[0]['cnt_like']; ?></span>
                        <?php
                            if($like_count[0]['cnt_like']>1) {
                               echo 'Likes';
                            } 
                            else {
                                echo 'Like';
                            }
                         ?>
                     </a>
                <?php } else { ?>
                    <a title="Like" class="pointer comment_like" data-id="<?php echo $content_id; ?>" ><i class="fa fa-thumbs-o-up" aria-hidden="true"></i>&nbsp;
                    <span class="like_count"><?php echo $like_count[0]['cnt_like']; ?></span>
                        <?php
                            if($like_count[0]['cnt_like']>1) {
                                echo 'Likes';
                            } 
                            else {
                                echo 'Like';
                            }
                        ?>
                     </a>
                <?php } ?>
            </li>
            <?php } } ?>
        </ul>

        <div class="clearfix"></div>
        
        <?php if($comment_count[0]['cnt_cmnt'] > 3) { ?>
        <a  class="btn-load btn-load-prev get_more_comment pointer">Load older comments &nbsp;<i class="glyphicon glyphicon-menu-up"></i></a>
        <?php } ?>

        <div class="clearfix"></div>
        <div class="comment-list">
            <?php
                $comment_list=array_reverse($comment_list);
                foreach ($comment_list as $key => $comment_row) { 
                    $this->load->view('comment/front_comment_row', $comment_row);
                }
            ?>
        </div>

        <?php if($show_next_load) { ?>
        <a  class="btn-load btn-load-next get_more_comment pointer">Load next comments &nbsp;<i class="glyphicon glyphicon-menu-down"></i></a>
        <?php } ?>

        <div class="comment-enter-box">
            <?php $form_model=array();
                echo $this->form->form_model($form_model,'',array('name'=>'','id'=>'comment_form')); ?>
            <!-- <input type="hidden" name="comment_id" value=""> -->
            <input type="hidden" name="content_id" value="<?php echo $content_id; ?>">
            <div class="width55 pull-left">
                <?php $profile_pic=$this->session->userdata('profile_pic'); 
                if(empty($profile_pic)) {
                    $profile_pic=USER_PROFILE_PIC;
                }
                ?>
                <img src="<?php echo $base_url.$profile_pic; ?>" class="img-responsive" alt="">
            </div>
            <div class="remaining-width">
                <div class="form-horizontal">
                    <!-- Text input-->
                    <div class="form-group">
                        <div class="col-md-12">
                            <textarea name="comment_body" id="add_comment" type="text" placeholder="Leave a comment" class="form-control input-md comment-editor"></textarea>
                            <div class="pull-right btn-comment btn-input-submit-ico">
                                <span><input class="btn btn-input-submit comment_submit" type="button" value="Submit"></span>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <?php echo $this->form->form_close(); ?>
        </div>

    </div>
</div>
<!-- comment section end -->


<!--Blog detailing section end -->
<div class="comment-enter-box" id="edit-comment" style="display: none;">
    <form class="edit-comment-form">
        <input type="hidden" name="comment_id">
        <input type="hidden" name="content_id" value="<?php echo $content_id; ?>">
        <div class="width55 pull-left">
            <!-- <img src="<?php echo $base_url.'public/front_assets/images/comment-thumb.jpg'; ?>" class="img-responsive" alt="">  -->
        </div>
        <div class="remaining-width pull-right">
            <div class="form-horizontal">
                <!-- Text input-->
                <div class="form-group">
                    <div class="col-md-12">
                        <textarea name="edit_comment_body" id="edit_comment" type="text" placeholder="Leave a comment" class="form-control input-md "></textarea>
                        <div class="pull-right btn-comment btn-input-submit-ico">
                            <span>
                                <input class="btn btn-input-submit update_comment" type="button" value="Submit">
                            </span>
                        </div>
                        <div class="pull-right btn-comment">
                            <span>
                                <input class="btn btn-input-submit cancel_comment" type="button" value="Cancel">
                            </span>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </form>
</div>
<link rel="stylesheet" type="text/css" href="http://ichord.github.io/At.js/dist/css/jquery.atwho.css"/>
<script src="<?php echo base_url().'public/plugins/ckeditor/ckeditor.js';?>"></script>
<script type="text/javascript" src="http://ichord.github.io/Caret.js/src/jquery.caret.js"></script>
<script type="text/javascript" src="http://ichord.github.io/At.js/dist/js/jquery.atwho.js"></script>
<script type="text/javascript">
    var base_url='<?php echo base_url(); ?>';
    var load_cnt=1;
    var comment_type='<?php echo $comment_type ?>';
    
    $(document).ready(function() {

        /* Scroll down to comment id */
        var comment_id=getParameterByName('comment_from');
        if(comment_id && $('#comment'+comment_id).length > 0) {
            $('html, body').animate({
                scrollTop: $('#comment'+comment_id).offset().top - $('.main-header').height()
            }, 800);
        }

        /* Like/Dislike current content */
        $('.comment_like').on('click', function() {
            var content_id=$(this).data('id');
            var like_data={
                "content_id":content_id,
            };

            var that=this;
            /* ajax request */
            sendXhr("front/comment_like/",like_data,function (response) {
                remove_loading();
                response=$.parseJSON(response);
                $('.like_count').html(response.cnt_like);
                if(response.like_type==1) {
                    $('i',that).removeClass('fa-thumbs-o-up');
                    $('i',that).addClass('fa-thumbs-up');
                }
                else {
                    $('i',that).removeClass('fa-thumbs-up');
                    $('i',that).addClass('fa-thumbs-o-up');
                }
            });
        });

        /* Mark comment as a spam */
        $(document).on('click','.spam_comment', function() {
            var comment_id=$(this).data('id');
            var comment_data={
                "comment_id":comment_id,
            };
            var that=this;

            /* confirm comment mark as spam */
            swal({
                title: "Are you sure?",
                text: "Comment mark as spam?",
                type: "warning",
                showCancelButton: true,
                confirmButtonColor: "#DD6B55",
                confirmButtonText: "Yes, Mark as spam!",
                closeOnConfirm: false,
            },
            function(isConfirm){
                if (isConfirm) {
                    /* ajax request */
                    sendXhr("front/comment_spam_save/",comment_data,function (response) {
                        remove_loading();
                        response=$.parseJSON(response);
                        
                        if(response.status == '1') {
                            $('.comment'+response.comment_id).css({
                                'background':'#ffebeb',
                                'padding':'10px',
                            });
                            swal("Mark as spam!", "Comment has been mark as spam.", "success");
                        }
                    });
                }
            });
        });

        /* Delete comment */
        $(document).on('click','.delete_comment', function() {
            var comment_id=$(this).data('id');
            var comment_data={
                "comment_id":comment_id,
            };
            var that=this;

            /* confirm comment mark as spam */
            swal({
                title: "Are you sure?",
                text: "Comment Delete?",
                type: "warning",
                showCancelButton: true,
                confirmButtonColor: "#DD6B55",
                confirmButtonText: "Yes, delete it!",
                closeOnConfirm: false,
            },
            function(isConfirm){
                if (isConfirm) {
                    /* ajax request */
                    sendXhr("front/comment_delete/",comment_data,function (response) {
                        remove_loading();
                        response=$.parseJSON(response);
                        
                        if(response.status == '1') {
                            $('.comment'+response.comment_id).css('display','none');
                            var comment_count= parseInt($('.comment_count').text());
                            comment_count--;
                            $('.comment_count').html(comment_count);
                            swal("Deleted!", "Comment deleted successfully.", "success");
                        }
                    });
                }
            });
        });
    
        /* new comment */
        $('.comment_submit').on('click', function() {
            var comment_data = $('#comment_form').serializeArray();
           // var comment=$('[name="comment_body"]','#comment_form').val();
           var content_id=$('[name="content_id"]','#comment_form').val();

            var comment = CKEDITOR.instances.add_comment.getData();
            comment_data={
                "content_id":content_id,
                "comment_body":comment,
                "comment_type":comment_type,
            };

            if(comment!='') {
                sendXhr("front/comment_save",comment_data,function (response) {
                    remove_loading();
                    response=$.parseJSON(response);
                     $('.comment_count').html(response.comment_count);
                    if(response.status == '1') {
                        $('#comment_form')[0].reset();
                        var cmnt_list=$('.comment-list');
                        $(cmnt_list).append(response.data);
                        CKEDITOR.instances.add_comment.setData('');
                        setTimeout(function() {
                            apply_whoat();    
                        },200);
                    }
                });
            }
            return false;
        });
    
        /* update comment */
        $(document).on('click','.update_comment', function() {
            var comment_data = $('.edit-comment-form','.comment-list').serializeArray();

            var content_id=$('[name="content_id"]','.comment-list').val();
            var comment_id=$('[name="comment_id"]','.comment-list').val();

            var comment = CKEDITOR.instances.edit_comment.getData();
            comment_data={
                "content_id":content_id,
                "comment_id":comment_id,
                "comment_body":comment,
                "comment_type":comment_type,
            };

            //console.log(comment_data);
    
            if(comment_data!='') {
                sendXhr("front/comment_save",comment_data,function (response) {
                    remove_loading();
                    response=$.parseJSON(response);
                    $('.comment-list .edit-comment-form').remove();
                    $('#comment'+comment_id).replaceWith(response.data);
                });
            }
            return false;
        });

        $(document).on('click','.cancel_comment', function() {
            $('.comment-list .edit-comment-form').remove();
        });

    
        /* load more comment */
        $('.btn-load-prev').on('click', function() {
            load_more_comments('prev');
        });

        /* load more comment */
        $('.btn-load-next').on('click', function() {
            load_more_comments('next');
        });
    
        /* edit commenting box */
        $(document).on('click','.edit_comment', function() {

            $('.comment-list .edit-comment-form').remove();
            var edit_id=$(this).data('id');
            var cmnt_box=$('.comment'+edit_id);
            var comment_text=$('.comment-content',cmnt_box).html();
    
            /* Append html content */
            $(cmnt_box).append($('#edit-comment').html());
    
            $('.edit-comment-form [name="edit_comment_body"]',cmnt_box).val(comment_text);
            $('.edit-comment-form [name="comment_id"]',cmnt_box).val(edit_id);

            
            CKEDITOR.config.extraPlugins = 'smiley';
            CKEDITOR.config.toolbar = [
                    ['Bold','Italic','Underline'],
                    ['Smiley']
            ];
            CKEDITOR.config.height = 80;  
            CKEDITOR.config.removePlugins = 'elementspath';
            CKEDITOR.config.resize_enabled = false;
            CKEDITOR.config.extraAllowedContent = 'strong span[*]';
            CKEDITOR.config.disableNativeSpellChecker = false;
            CKEDITOR.replace('edit_comment_body',{
                on : {
                    instanceReady : function( evt ) {
                        apply_whoat('.comment-list .edit-comment-form');
                    }
                }
            });
        });
        
        CKEDITOR.replace('comment_body',{
            on : {
                instanceReady : function( evt ) {
                    apply_whoat();
                }
            }
        });
        CKEDITOR.config.extraPlugins = 'smiley';
        CKEDITOR.config.toolbar = [
                ['Bold','Italic','Underline'],
                ['Smiley']
        ];
        CKEDITOR.config.height = 80;  
        CKEDITOR.config.removePlugins = 'elementspath';
        CKEDITOR.config.resize_enabled = false;
        CKEDITOR.config.extraAllowedContent = 'div(*)';
        CKEDITOR.config.disableNativeSpellChecker = false;
    });
    
    function load_more_comments(type) {
        var content_id=$('input[name="content_id"]').val();
        var start=$('.cmnt_box:first').attr('data-id');
        var end=$('.cmnt_box:last').attr('data-id');
            
        var post_data={
            "content_id":content_id,
            "start":start,
            "end":end,
            "type":type,
            "comment_type":comment_type,
        };

        sendXhr("front/get_more_comment",post_data,function (response) {
            load_cnt++;
            var cmnt_list=$('.comment-list');

            if(response=='0') {
                if(type=='prev') {
                    $('.btn-load-prev').hide();
                }
                else {
                    $('.btn-load-next').hide();   
                }
                return 0;
            }
            if(type=='prev') {
                $(cmnt_list).prepend(response);
            }
            else {
                $(cmnt_list).append(response);   
            }

            /* Remove background */
            setTimeout(function() {
                $('.new_comment').removeClass('new_comment');
            },2000)
        });
    }

    function apply_whoat(parent) {
        if(parent) {
            parent='body';
        }

        /* Config for who at */
        var at_config = {
          at: "@",
          data: <?php echo $username_list; ?>,
          headerTpl: '<div class="atwho-header">User List</div>',
          insertTpl: '<strong data_userid="${user_id}">@${name}</strong>',
          displayTpl: "<li>${name} <small>${designation}</small></li>",
          limit: 200
        };

        ifr = $('.cke_wysiwyg_frame',parent)[0];
        doc = ifr.contentWindow.document || ifr.contentDocument;
       
        ifrBody = doc.body
        ifrBody.id = 'ifrBody'
        $(ifrBody).atwho('setIframe', ifr).atwho(at_config)
    }


</script>