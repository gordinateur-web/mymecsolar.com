<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Quiz_model extends CI_model {

	public function __construct() {
		
	}

	public function add_quiz_master($data) {
		$this->db->insert('quiz_master',$data);
		return $this->db->insert_id();
	}

	public function update_quiz_master($data, $edit_id) {
		$this->db->where('quiz_master_id',$edit_id);
		return $this->db->update('quiz_master',$data);
	} 

	public function get_quiz_master($filter=false) {
		if($filter){
			apply_filter($filter);	
		}
		/* Ignore deleted content types and content */
		$this->db->where('deleted_at is null',null,false);
		//$this->db->join('quiz_visibility','quiz_visibility.quiz_master_id=quiz_master.quiz_master_id');
		$this->db->order_by('quiz_master_id','DESC');
		$rs=$this->db->get('quiz_master');
		return $rs->result_array();
	}

	public function add_quiz_visibility($data) {
		$this->db->insert('quiz_visibility',$data);
		return $this->db->insert_id();
	}

	public function update_quiz_visibility($data, $edit_id) {
		$this->db->where('quiz_master_id',$edit_id);
		return $this->db->update('quiz_visibility',$data);
	}

	public function delete_quiz_visibility($filter=false) {
		if($filter){
			apply_filter($filter);
		}
		$this->db->delete('quiz_visibility');
	}

	public function get_quiz_visibility($filter=false) {
		if($filter){
			apply_filter($filter);	
		}
		$rs=$this->db->get('quiz_visibility');
		return $rs->result_array();
	}

	public function add_quiz_question($data) {
		$this->db->insert('quiz_question',$data);
		return $this->db->insert_id();
	}

	public function update_quiz_question($data, $edit_id) {
		$this->db->where_in('question_id',$edit_id);
		return $this->db->update('quiz_question',$data);
	} 

	public function delete_quiz_question($filter=false) {
		if($filter){
			apply_filter($filter);
		}
		$this->db->delete('quiz_question');
	}

	public function add_question_option($data) {
		$this->db->insert('quiz_question_option',$data);
		return $this->db->insert_id();
	}

	public function update_question_option($data, $edit_id) {
		$this->db->where_in('option_id',$edit_id);
		return $this->db->update('quiz_question_option',$data);
	} 

	public function delete_question_option($filter=false) {
		if($filter){
			apply_filter($filter);
		}
		$this->db->delete('quiz_question_option');
	}

	public function upload_image($quiz_master_id, $user_id, $file_array, $key) {
		$this->load->model('custom_content/media_model');
		/* image upload folder path  */
		$folder_path = UPLOAD_DIR.'quiz/';

		/* Upload file */
		$upload_config['upload_path']= $folder_path;
		$upload_config['allowed_types']= 'gif|jpg|png';
		$config['max_size'] = '10M';
		$upload_config['file_ext_tolower']= true;

		/* Check if upload folder available */
		if(!file_exists($upload_config['upload_path'])) {
			mkdir($upload_config['upload_path']);
		}
		/* Upload image */
		$this->load->library('upload', $upload_config);

		if(empty($file_array['file']['name'][$key])) {
			return false;
		}

		//$files = $_FILES;
		$_FILES['option_file']['name'] = $file_array['file']['name'][$key];
		$_FILES['option_file']['type']= $file_array['file']['type'][$key];
		$_FILES['option_file']['tmp_name']= $file_array['file']['tmp_name'][$key];
		$_FILES['option_file']['error']= $file_array['file']['error'][$key];
		$_FILES['option_file']['size']= $file_array['file']['size'][$key];    
		$this->upload->initialize($upload_config);

		if (!$this->upload->do_upload('option_file')) {
			set_message($this->upload->display_errors());
			redirect(base_url()."quiz/quiz_question_list/".$quiz_master_id);
		}
		else {
			$upload_data=$this->upload->data();
			$file_data=array(
				'media_title'=>$upload_data['file_name'],
				'media_alt'=>$upload_data['file_name'],
				'media_path'=>$upload_config['upload_path'].$upload_data['orig_name'],
				'media_type'=>$upload_data['file_type'],
				'media_size'=>$upload_data['file_size'],
				'created_at'=>date('Y-m-d H:i:s'),
				'created_by'=>$user_id,
			);	
			//$file_id=$this->quiz_model->add_file($file_data);
			$file_id=$this->media_model->add_media($file_data);
			return $file_id;
		}
	}

	public function add_file($data) {
		$this->db->insert('file',$data);
		return $this->db->insert_id();
	}

	public function get_quiz_question($filter=false) {

		if($filter){
			apply_filter($filter);	
		}
		/* Ignore deleted content types and content */
		$this->db->where('deleted_at is null',null,false);
		$this->db->order_by('question_weight','asc');
		$rs=$this->db->get('quiz_question');
		return $rs->result_array();
	}

	public function get_question_option($filter=false) {

		if($filter){
			apply_filter($filter);	
		}
		/* Ignore deleted content types and content */
		$this->db->where('quiz_question.deleted_at is null',null,false);
		$this->db->join('quiz_question','quiz_question.question_id=quiz_question_option.question_id','left');
		$this->db->join('media','media.media_id=quiz_question_option.file_id','left');
		$rs=$this->db->get('quiz_question_option');
		return $rs->result_array();
	}

	public function add_quiz($data) {
		$this->db->insert('quiz',$data);
		return $this->db->insert_id();
	}

	public function update_quiz($data, $edit_id) {
		$this->db->where('quiz_id',$edit_id);
		return $this->db->update('quiz',$data);
	} 

	public function get_quiz($filter=false) {

		if($filter){
			apply_filter($filter);	
		}
		$this->db->select('quiz.*, quiz_master.quiz_title, users.name');
		/* Ignore deleted content types and content */
		$this->db->where('quiz.deleted_at is null',null,false);
		$this->db->join('users','users.user_id=quiz.user_id','left');
		$this->db->join('quiz_master','quiz_master.quiz_master_id=quiz.quiz_master_id','left');
		$rs=$this->db->get('quiz');
		return $rs->result_array();
	}

	public function add_quiz_answer($data) {
		$this->db->insert('quiz_answer',$data);
		return $this->db->insert_id();
	}

	public function update_quiz_answer($data, $edit_id) {
		$this->db->where('quiz_answer_id',$edit_id);
		return $this->db->update('quiz_answer',$data);
	} 

	public function get_quiz_answer($filter=false) {

		if($filter){
			apply_filter($filter);	
		}
		/* Ignore deleted content types and content */
		$this->db->where('deleted_at is null',null,false);
		$rs=$this->db->get('quiz_answer');
		return $rs->result_array();
	}

	public function get_answer_with_points($filter=false) {
		if($filter){
			apply_filter($filter);	
		}
		/* Ignore deleted content types and content */
		$this->db->where('quiz_answer.deleted_at is null',null,false);
		$this->db->join('quiz_question','quiz_question.question_id=quiz_answer.quiz_question_id','left');
		$rs=$this->db->get('quiz_answer');
		return $rs->result_array();
	}

	/* quiz question render view */
	public function quiz_question_render($quiz_master, $quiz_question, $quiz_question_option ,$edit_data=false, $submit=false, $all_answered=false) {
		$data['quiz_master']=$quiz_master;
		$data['quiz_question']=$quiz_question;
		$data['quiz_question_option']=$quiz_question_option;
		$data['edit_data']=array();
		if(!empty($edit_data)) {
			$data['edit_data']=$edit_data;
		}

		if($submit) {
			$data['submit']=true;
		}
		
		$this->load->view('quiz/quiz_question_render',$data);
	}

	public function quiz_question_option_render($quiz_master, $question, $quiz_question_option ,$edit_data=false, $all_answered=false) {

		$data['quiz_master']=$quiz_master;
		$data['question']=$question;
		$data['quiz_question_option']=$quiz_question_option;
		$data['edit_data']=array();
		if(!empty($edit_data)) {
			$data['edit_data']=$edit_data;
		}
		$this->load->view('quiz/quiz_question_option_render',$data);
	}

	/*
		Return true and false based on quiz setting
	*/
	public function quiz_current_user_visibility($quiz_id) {
		$branch_id=$this->session->userdata('branch_id');

		/* Get quiz visibility to check access */
		$filter_quiz['quiz_master_id']=$quiz_id;
		$quiz_visibility=$this->quiz_model->get_quiz_visibility($filter_quiz);
		$quiz_master=$this->quiz_model->get_quiz_master($filter_quiz);
		$quiz_master=$quiz_master[0];
		/*dsm($quiz_master);
		dsm($quiz_visibility);die;*/
		

		/* If not found any record in table meaning all is selected */
		if(empty($quiz_visibility)) {
			return true;
		}

		/* check for branch user */
		if($quiz_master['quiz_visibility']=='Branch') {
			$quiz_visibility=array_column($quiz_visibility, 'branch_id');
			if(!in_array($branch_id,$quiz_visibility)) {
				return false;
			}
		}

		/* check for employee grade */
		else if($quiz_master['quiz_visibility']=='Employee grade') {
			$quiz_visibility=array_column($quiz_visibility, 'grade_master_id');
			$employee_grade=$this->session->userdata('employee_grade');
			if(!in_array($employee_grade,$quiz_visibility)) {
				return false;
			}
		}

		/* check for region */
		else if($quiz_master['quiz_visibility']=='Region') {
			$branch=$this->user_model->get_branch(array('branch_id'=>$branch_id));
			$region=$this->user_model->get_branch_region(array('branch_region_id'=>$branch[0]['branch_region_id']));
			$quiz_visibility=array_column($quiz_visibility, 'region_id');
			if(!in_array($region[0]['branch_region_id'],$quiz_visibility)) {
				return false;
			}
		}
		return true;
	}

}