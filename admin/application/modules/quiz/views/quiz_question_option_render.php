<div class="form-group">
	<label class="control-label" for="radios"><?php echo $question['question_title']; ?></label>
	<?php 
	if(!empty($all_answered)) { ?> 
		<div class="alert alert-info alert-dismissible alert-margin poll_msg">
			<i class="icon fa fa-ban"></i> You have already participated in this poll
		</div>
	<?php 
	}
	else if($question['question_type']=='textarea') {
		$question_id=$question['question_id'];
		$other_option=array(
			'class'=>'form-control answer_textarea',
			'data_question_id'=>$question_id,
		);
		$answer_textarea='';
		if(!empty($edit_data[$question_id][0]['textarea_ans'])) {
			$answer_textarea=$edit_data[$question_id][0]['textarea_ans'];
		}
		echo $this->form->form_textarea('textarea_ans['.$question_id.']','',$answer_textarea,$other_option); 
	?>
	<?php } else { ?>
	<?php foreach ($quiz_question_option[$question['question_id']] as $key => $option) { ?>
		<div class="radio">
			<label for="quize-op-<?php echo $option['option_id']; ?>">

				<?php 
				$question_id=$question['question_id'];
				$other_option=array(
					'class'=>'answer_option',
					'id'=>'quize-op-'.$option['option_id'],
					'data_question_id'=>$question_id,
					'data_is_true'=>$option['is_true'],
					);
				$answer_option='';
				if(!empty($edit_data[$question_id][0]['option_id'])) {
					if($edit_data[$question_id][0]['option_id']==$option['option_id']) {
						$answer_option=true;
					}
				}
				echo $this->form->form_radio('option_id['.$question_id.']',$option['option_id'],'',$answer_option,$other_option); 

				$other_option=array(
					'data_question_id'=>$question_id,
					);
				echo $this->form->form_hidden('is_true['.$question_id.']',$option['is_true']); 
				?>

				<?php if(!empty($option['option_title'])) {  
					echo $option['option_title']; 
				} ?>

				<?php if(!empty($option['file_id'])) { ?>
				<img src="<?php echo base_url().$option['media_path']; ?>">
				<?php } ?>
			</label>
		</div>
		<?php  } /* OPtion loop end */ ?>
		<?php }/* If textarea condition end */ ?>
	</div>