<div class="row">
	<?php $base_url=base_url();?>
		<div class="col-md-12">
			<div class="box box-primary">
				<div class="box-header with-border">
					<div class="box-title">
						<!-- <a href="<?php echo base_url().'quiz/quiz_master_add' ?>" class="btn btn-sm btn-primary" title="Add New"><i class="fa fa-plus"></i> Add Question</a> -->
						
						<a href="#" class="btn btn-sm btn-primary" onclick="get_modaldata('Add Question',base_url+'quiz/quiz_question_add/<?php echo $quiz_master_id; ?>')" title="Add Question"><i class="fa fa-plus"></i> Add Question</a>
						<a class="btn btn-sm btn-primary" id="save_order" title="Save Order" ><i class="fa fa-sort"></i> Save Order</a>
					</div>
					<div class="pull-right">
						<?php if (isset($pagination_links)){ echo $pagination_links; } ?>
					</div>
				</div>

				<div class="panel-body">
					<div id="order-warning" class="alert alert-warning alert-dismissable" style="display:none;">
						<span>Please click on Save order to change the order of Fields</span>
					</div> 

					<div id="save-msg" class="alert alert-success alert-dismissable" style="display:none;">
						<span></span>
					</div> 
					<div class="box-body table-responsive">
						<table class='table table-bordered table-striped dataTable' id="full-height-datatable">
							<thead>
								<tr>
									<th width="5">#</th>
									<th>Title</th>
									<th>Type</th>
									<th>Point</th>
									<th>Status</th>
									<th width="5%">Option</th>
								</tr>
							</thead>
							<tbody id="sortable">
							  <?php foreach($quiz_question_list as $quiz_question) { 
							  	$encrypt_id=encrypt_id($quiz_question['question_id']);
							  	?>
								<tr id=<?php echo $encrypt_id; ?>>
									<td ><i class="cursor-move fa fa-fw fa-arrows"></i></td>
									<td><?php echo $quiz_question['question_title']; ?></td>
									<td><?php echo $quiz_question['question_type']; ?></td>
									<td><?php echo $quiz_question['question_point']; ?></td>
									<td>
										<?php
										if($quiz_question['enable']=='1') {
											echo '<label class="label label-success">Enable</label>';
										}
										else {
											echo '<label class="label label-danger">Disable</label>';
										}
										?>

									</td>
									<td>
										<div class="btn-group options">
											<button type="button" class="btn btn-default btn-sm" dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
												<i class="fa fa-cog"></i> Options
											</button>
											<ul class="dropdown-menu">
												<li>
													<a href="#" onclick="get_modaldata('Add Question',base_url+'quiz/quiz_question_add/<?php echo $quiz_master_id.'/'.$quiz_question['question_id']; ?>')"> 
														<i class="fa fa-edit fa-margin"></i> Edit
													</a>
												</li>
												<li>
													<a data-id="<?php echo $quiz_question['question_id']; ?>" class="pointer delete-menu-master">
														<i class="fa fa-trash-o fa-margin"></i> Delete
													</a>
												</li>
											</ul>
										</div>
									</td>
								</tr>
							<?php }?>  
							</tbody>
						</table>
					</div>
				</div>
			</div>
	</div>
</div>

<link href="<?php echo base_url().'public/plugins/jquery-ui/jquery-ui.min.css';?>" rel="stylesheet" />
<script src="<?php echo base_url().'public/plugins/jquery-ui/jquery-ui.min.js';?>"></script>

<script type="text/javascript">
	$(document).ready(function() {
		$("#sortable" ).sortable({
            handle: ".cursor-move"
        });
        $("#sortable" ).disableSelection();
        $("#sortable" ).on("sortstop", function( event, ui ) {
            $('#order-warning').show();
            $('#save-msg').hide();
        });
        $('#save_order').click(function() {
            sortable_list();
        });

		$('.delete-menu-master').click(function() {
			var id=$(this).attr('data-id');
			swal({
				title: "Are you sure?",
				text: "quiz question to delete?",
				type: "warning",
				showCancelButton: true,
				confirmButtonColor: "#DD6B55",
				confirmButtonText: "Yes, delete it!",
				closeOnConfirm: false
			},
			function(){
				window.location = base_url+'quiz/quiz_question_delete/'+id;
			});
		});
		
	});

	function sortable_list() {

        var sortedIDs =$("#sortable").sortable("toArray");
        $.ajax({
            url     : base_url+"quiz/quiz_question_save_order",
            type    : 'POST',
            data    : {'ids':sortedIDs},
            success : function(data){
                data=$.parseJSON(data);
                remove_loading();
                if(data.status == '1') {
                    $('#order-warning').hide();
                    $('#save-msg').show();
                    $('#save-msg').find('span').html('');
                    $('#save-msg').find('span').html(data.message);
                }
                else {
                    swal("Oops...", data.message, "error");
                }
            }
        });
    }

</script>


