<div class="row">
	<?php $base_url=base_url();?>
	<div class="col-md-12">
		<div class="box box-primary">
			<div class="box-header with-border">
				<div class="box-title">
					<h3 class="box-title"><?php echo $quiz_master[0]['quiz_title']; ?></h3>
				</div>
			</div>

			<div class="panel-body">

				<div class="quiz-container">
					<!-- <h1 class="red-text">Quiz: <span class="text-capitalize"><?php echo $quiz_master[0]['quiz_title']; ?></span></h1> -->
					<p><?php echo $quiz_master[0]['quiz_description']; ?></p>
					<p><?php echo $quiz_master[0]['quiz_instruction']; ?></p>
					
					<h4 class="start_quiz_msg text-success"></h4>

					<!-- Quiz question and option render  -->
					<?php $this->quiz_model->quiz_question_render($quiz_master, $quiz_question, $quiz_question_option, $edit_data); ?>

				</div>
			</div>


		</div>
	</div>
</div>



<!-- <script type="text/javascript">
	$('#start_quiz').click(function() {
		var quiz_master_id=$('input[name="quiz_master_id"]').val();

		var post_data={
			"quiz_master_id":quiz_master_id,
		};

		sendXhr("front/quiz/quiz_start",post_data,function (response) {
			response=$.parseJSON(response);
			//alert(response.quiz_id);

			if(response.status=='1') {
				$('.quiz-section').show();
				$('.start_quiz').hide();
				$('input[name="quiz_id"]').val(response.quiz_id);
				$('.start_quiz_msg').html(response.message);
			}
			else {
				$('start_quiz').show();
				$('.quiz-section').hide();   
			}

		});
	});

	
	$(".answer_option").change(function () {
		var post_data={
			"quiz_id":$('input[name="quiz_id"]').val(),
			//"textarea_ans":$('textarea.answer_option').val(),
			"quiz_question_id":$(this).attr('data_question_id'),
			"is_true":$(this).attr('data_is_true'),
			"option_id":$(this).attr('value'),
		};

		save_single_question(post_data, this);
	});

	$(".answer_textarea").change(function () {
		var post_data={
			"quiz_id":$('input[name="quiz_id"]').val(),
			"textarea_ans":$('textarea.answer_textarea').val(),
			"quiz_question_id":$(this).attr('data_question_id'),
			//"option_id":$(this).attr('value'),
		};

		save_single_question(post_data, this);
	});

	/*
		Call on change and blur of question
	*/
	function save_single_question(post_data, ele) {
		sendXhr("front/quiz/question_answer_single",post_data,function (response) {
			response=$.parseJSON(response);
			
		});
	}
</script> -->