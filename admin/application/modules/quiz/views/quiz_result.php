<div class="row">
	<?php $base_url=base_url();?>
		<div class="col-md-12">
			<div class="box box-primary">
				<div class="box-header with-border">
					<div class="box-title">
						
					</div>
					<div class="pull-right">
						<?php if (isset($pagination_links)){ echo $pagination_links; } ?>
					</div>
				</div>

				<div class="panel-body">
					<div class="box-body table-responsive">
						<table class='table table-bordered table-striped dataTable' id="full-height-datatable">
							<thead>
								<tr>
									<th>User</th>
									<th>title</th>
									<th>start</th>
									<th>End</th>
									<th width="15%">Total Marks</th>
								</tr>
							</thead>
							<tbody>
							<?php foreach($user_list as $user) { 
							  	$encrypt_id=encrypt_id($user['quiz_id']);
							  	$pass=false;
							  	if($user['total_marks']>=$quiz_master[0]['passing_marks']) {
							  		$pass=true;
							  	}
							?>
								<tr class="<?php if($pass==true) { echo 'success'; } ?>" >
									<td><a href="<?php echo base_url().'quiz/user_quiz_review/'.$user['quiz_master_id'].'/'.$user['quiz_id']; ?>"><?php echo $user['name']; ?></a></td>
									<td><?php echo $user['quiz_title']; ?></td>
									<td><?php echo dateformat($user['started_at'],'d F Y h:i A'); ?></td>
									<td><?php echo dateformat($user['finish_at'], 'd F Y h:i A'); ?></td>
									<td><?php echo $user['total_marks']; ?></td>
								</tr>
							<?php }?>  
							</tbody>
						</table>
					</div>
				</div>
			</div>
	</div>
</div>

<script type="text/javascript">
	$(document).ready(function() {
	
	})

</script>


