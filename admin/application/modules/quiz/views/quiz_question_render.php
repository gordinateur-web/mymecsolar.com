<section class="quiz-section">
	<?php 
	$form_model=array();
	if(!empty($edit_data)) {
		$form_model=$edit_data;
	}
	echo $this->form->form_model($form_model, base_url().'front/quiz/question_answer_save/',array('name'=>'save_quiz_question_ans','id'=>'quiz_question_ans_form', 'class'=>'validate-form')); 
	echo $this->form->form_hidden('quiz_id'); 
	?>
	<!-- Multiple Radios -->
	<?php if(!empty($quiz_question)) { 
		foreach ($quiz_question as $key => $question) { 
			/* Ignore question which don't have option and not textarea type */
			if(empty($quiz_question_option[$question['question_id']]) && $question['question_type']!='textarea') {
				continue;
			}
			?>
			<!-- Quiz option render  -->
			<?php 
			$this->quiz_model->quiz_question_option_render($quiz_master, $question, $quiz_question_option, $edit_data, 0); 
			?>

			<?php } /* Question loop end */  
		} /* Question empty check */
		?>
		
		<?php if($submit==true) { ?>
		<div class="form-group">
			<button type="button" class="btn btn-default submit-btn" id="quiz-submit">Submit <i class="glyphicon glyphicon-menu-right" aria-hidden="true"></i></button>
		</div>
		<?php } ?>
		<?php echo $this->form->form_close(); ?>
</section>
<!-- quiz Section end -->

<script type="text/javascript">
	$('#quiz-submit').click(function() {

		swal({
			title: "Are you sure?",
			text: "Do you want a submit quiz?",
			type: "warning",
			showCancelButton: true,
			confirmButtonColor: "#DD6B55",
			confirmButtonText: "Submit quiz!",
			//closeOnConfirm: false
		},
		function(){
			$('#quiz_question_ans_form').submit();
		});
	});
</script>