<?php
$base_url = base_url();
$form_model=array();
if(!empty($edit_data)) {
	$form_model=$edit_data;
}
/* Fill old data */
$old_data=$this->session->flashdata('old_data');
if(!empty($old_data)) {
	$form_model=$old_data;  
}
echo $this->form->form_model($form_model, $base_url.'quiz/quiz_master_save/',array('name'=>'save_quiz_master','id'=>'quiz_master_form', 'class'=>'validate-form'));
if(!empty($edit_data)) {
	echo $this->form->form_hidden('quiz_master_id');
}
if(!empty($edit_data['question_id'])) {
	echo $this->form->form_hidden('question_id');
}
?>

	<div class="box box-primary">
		<div class="panel-body">
			<div class="row">
				<div class="box-body">
					<div id="model_errors"></div>
					
						<div class="col-md-6 col-sm-6">
							<div class="form-group">
								<label>Title <span class="text-danger">*</span></label>
								<?php 
								$other_option=array(
									'class'=>'form-control',
									'placeholder'=>'Title',
									'data-validation'=>'required',
									'title'=>'Title',
									);
								echo $this->form->form_input('quiz_title', $other_option); 
								?>
							</div>
						</div>

						<div class="col-md-2 col-sm-2">
							<div class="form-group">
								<label>Type <span class="text-danger">*</span></label>
								<?php 
								$other_option=array(
									'class'=>'form-control',
									'placeholder'=>'Type',
									'data-validation'=>'required',
									'title'=>'Type',
								);
								if(!empty($edit_data)) {
									$other_option['disabled']='disabled';
								}
								$option= array(
									''=>'Select Type',
									'poll'=>'Poll',
									'quiz'=>'Quiz',
								);
								echo $this->form->form_dropdown('quiz_type',$option,'','', $other_option); 
								?>
							</div>
						</div>
						
						<div class="col-md-3 col-sm-3 quiz_mode">
							<div class="form-group">
								<label>Mode <span class="text-danger">*</span>
								<span data-toggle="tooltip" data-placement="top" title="Use these settings to customize how your quiz will behave. Empower respondents to view correct answers and display their final results at the end, so they immediately know how they did"><i class="fa fa-question-circle" aria-hidden="true" style="padding-right: 10px;"></i></span>
								</label><br>
								<?php 
								$option= array(
									''=>'Select Type',
									'1'=>'Quiz Mode',
									'0'=>'Servey Mode',
								);
								echo $this->form->form_dropdown('quiz_mode',$option,'','', $other_option); 
								?>
								

								<!-- <label class="pointer quiz_result">
								<?php 
								$other_option=array(
									'title'=>'Type',
								);
								$option= array(
									''=>'Select Type',
									'survey'=>'Survey',
									'competition'=>'Competition',
								);
								echo $this->form->form_checkbox('quiz_result','1',$other_option,false); 
								?>
								Quiz Result <span data-toggle="tooltip" data-placement="top" title="Show correct answers to incorrect responses"><i class="fa fa-question-circle" aria-hidden="true"></i></span></label> -->
							</div>
						</div>
						<div class="clearfix"></div>

						<div class="col-md-4 col-sm-4">
							<div class="form-group">
								<label>Visibility <span class="text-danger">*</span></label>
								<?php 
								$other_option=array(
									'class'=>'form-control',
									'placeholder'=>'Status',
									'data-validation'=>'required',
									'title'=>'Visibility',
								);
								$option= array('All'=>'All','Branch'=>'Branch','Employee grade'=>'Employee grade','Region'=>'Region');
								echo $this->form->form_dropdown('quiz_visibility',$option,'','', $other_option); 
								?>
							</div>
						</div>

						<div class="col-md-4 col-sm-4 branch" style="display: none;">
							<div class="form-group">
								<label>Branch <span class="text-danger">*</span></label><br>
								<?php 
								$other_option=array(
									'class'=>'form-control select2',
									'title'=>'Branch',
									'multiple'=>'multiple',
									'style'=>'width:100%'
								);
								echo $this->form->form_dropdown_fromdatabase('branch_id[]',$branch,'branch_id','branch_title','', $other_option); 
								?>
							</div>
						</div>

						<div class="col-md-4 col-sm-4 grade" style="display: none;">
							<div class="form-group">
								<label>Employee Grade <span class="text-danger">*</span></label>
								<?php 
								$other_option=array(
									'class'=>'form-control select2',
									'title'=>'Employee Grade',
									'multiple'=>'multiple',
									'style'=>'width:100%'
								);
								echo $this->form->form_dropdown_fromdatabase('grade_master_id[]',$grade,'grade','grade','', $other_option); 
								?>
							</div>
						</div>

						<div class="col-md-4 col-sm-4 region" style="display: none;">
							<div class="form-group">
								<label>Region <span class="text-danger">*</span></label>
								<?php 
								$other_option=array(
									'class'=>'form-control select2',
									'title'=>'Region',
									'multiple'=>'multiple',
									'style'=>'width:100%'
								);
								echo $this->form->form_dropdown_fromdatabase('region_id[]',$region,'branch_region_id','branch_region_title','', $other_option); 
								?>
							</div>
						</div>

						<div class="col-md-4 col-sm-4 passing_marks">
							<div class="form-group">
								<label>Passing Mark </label>
								<?php 
								$other_option=array(
									'class'=>'form-control number-field',
									'placeholder'=>'Passing Mark',
								);
								echo $this->form->form_input('passing_marks', $other_option); 
								?>
							</div>
						</div>
						<div class="clearfix"></div>
						
						

						<div class="col-md-6 col-sm-6">
							<div class="form-group">
								<label>Start Date <span class="text-danger">*</span></label>
								<?php 
								$other_option=array(
									'class'=>'form-control datepicker',
									'data-validation'=>'required',
									'placeholder'=>'Start Date',
								);
								echo $this->form->form_input('started_on', $other_option); 
								?>
							</div>
						</div>

						<div class="col-md-6 col-sm-6">
							<div class="form-group">
								<label>End Date</label>
								<?php 
								$other_option=array(
									'class'=>'form-control datepicker',
									'placeholder'=>'End Date',
								);
								echo $this->form->form_input('end_on', $other_option); 
								?>
							</div>
						</div>

						
						<div class="clearfix"></div>

						<div class="col-md-12 col-sm-12 ">
							<div class="form-group">
								<label>Instruction </label>
								<?php 
								$other_option=array(
									'class'=>'form-control',
									'placeholder'=>'Instruction',
								);
								echo $this->form->form_textarea('quiz_instruction', $other_option); 
								?>
							</div>
						</div>


						<div class="col-md-12 col-sm-12 ">
							<div class="form-group">
								<label>Description </label>
								<?php 
								$other_option=array(
									'class'=>'form-control',
									'id'=>'ckeditor',
									'placeholder'=>'Description',
									'title'=>'Description',
								);
								echo $this->form->form_textarea('quiz_description', $other_option); 
								?>
							</div>
						</div>
						<div class="clearfix"></div>

					</div>
				</div>


			<div class="clearfix"></div>

			<div class="poll_question" style="display: none;">
				<div class="col-sm-12">
					<h4>Poll Question</h4>
				</div>
				<div class="col-sm-12">
					<div class="form-group">
						<label>Question Title <span class="text-danger">*</span></label>
						<?php 
						$other_option=array(
							'class'=>'form-control',
							'placeholder'=>'Question Title',
							'data-validation'=>'required',
							'Title'=>'Question Title',
							);
						echo $this->form->form_input('question_title', $other_option); 
						?>
					</div>
				</div>

				<div class="col-md-3 col-sm-3">
					<div class="form-group">
						<label>Question Type <span class="text-danger">*</span></label>
						<?php 
						$other_option=array(
							'class'=>'form-control',
							'placeholder'=>'Question Type',
							'data-validation'=>'required',
							'Title'=>'Question Type',
						);
						if(!empty($edit_data['question_type'])) {
							$other_option['disabled']='disabled';
						}
						$option=array();
						$option['']='SELECT';
						$option['Option']=array(
							'image'=>'Option Image',
							'option'=>'Option text',
						);
						echo $this->form->form_dropdown('question_type',$option,'','', $other_option); 
						?>
					</div>
				</div>

				<div class="col-md-3 col-sm-3">
					<div class="form-group">
						<label>Question Status <span class="text-danger">*</span></label>
						<?php 
						$other_option=array(
							'class'=>'form-control',
							'placeholder'=>'Question Status',
							'data-validation'=>'required',
							'Title'=>'Question Status',
							);
						$option= array(
							'1'=>'Enable',
							'0'=>'Disable',
							);
						echo $this->form->form_dropdown('enable',$option,'','', $other_option); 
						?>
					</div>
				</div>
				<div class="clearfix"></div>
				<div class="question_option col-md-12">
					<h4 class="pull-left">Options </h4>

					<a class="pull-right btn btn-sm btn-primary question_point_add">
						<i class="fa fa-plus"></i> Add Option
					</a>
					<div class="clearfix"></div>
					<span class="true_error text-danger" align="center" style="display: none;">Please select true option</span>
					<div class="clearfix"></div>
					<div class="table-responsive">
						<table class='table table-bordered table-striped dataTable add_option' >
							<thead>
								<tr>
									<th>Title</th>
									<th>Option</th>
								</tr>
							</thead>
							<tbody class="">
								<tr class="not_add_option">
									<td colspan="2"><p class="text-danger"> Please select question type</p></td>
								</tr>
								<?php if(!empty($question_option_list)) {
									foreach ($question_option_list as $key => $question_option) { ?>
									<tr>
										<td>
											<div class="image_type">
												<!-- <input type="file" name="file[]" > -->
												<?php
												echo $this->form->form_upload('file['.$question_option['option_id'].']','','',''); 
												?>

												<?php if(!empty($question_option['media_path'])) { ?>
												<div class="thumbnail" style="width: 100px;">
													<img src="<?php echo $base_url.$question_option['media_path']; ?>" height="50">
												</div>
												<?php } ?>
											</div>
											<div class="option_type">
												<?php 
												$other_option=array(
													'class'=>'form-control',
													'placeholder'=>'Title'
													);
												echo $this->form->form_input('option_title['.$question_option['option_id'].']','',$question_option['option_title'],$other_option); 
												?>
											</div>
										</td>
											<td>
												<a class="pointer btn btn-sm btn-danger" onclick="delete_question_option(this, <?php echo $question_option['option_id']; ?>);">
													<i class="fa fa-trash-o fa-margin"></i> Delete
												</a>
											</td>
										</tr>
										<?php } } ?>
							</tbody>
						</table>
					</div>
				</div>
			</div>
			<div class="clearfix"></div>

			<div class="box-footer with-border">
				<div class="box-tools pull-right">
					<input type="reset" class="btn btn-default" value="Reset">
					<input type="submit" class="btn btn-primary" value="Submit">
				</div>
			</div>
			<?php echo $this->form->form_close(); ?>

		</div>
	</div>

<div class="question_option_row" style="display: none;">
	<table>
		<tbody>
			<td>
				<div class="image_type">
					<input type="file" name="file[]" data-validation="required" title="Option image" class="form-control" >
				</div>
				<div class="option_type">
					<input type="text" name="option_title[]" data-validation="required" placeholder="Title" title="Option title" class="form-control" >
				</div>
				<!-- <div class="html_type">
					<textarea rows="2" name="option_title[]" class="form-control"></textarea>
				</div> -->
			</td>
			<td>
				<a class="pointer btn btn-sm btn-danger" onclick="delete_question_option(this);">
					<i class="fa fa-trash-o fa-margin"></i> Delete
				</a>
			</td>
		</tbody>
	</table>
</div>

<script src="<?php echo base_url().'public/plugins/ckeditor/ckeditor.js';?>" type="text/javascript"></script>
<script type="text/javascript">
	function delete_icon(ele) {
		var id=$(ele);
		$('#menu_link_form').append('<input type="hidden" name="deleted_icon" value="1"/>');
		$(id).parent().remove();
	}

	$(document).ready(function() {	

		$('select[name="quiz_type"]').on('change',function() {
			var quiz_type=$('select[name="quiz_type"]').val();
			if(quiz_type=='quiz') {
				$('.quiz_mode').show();
				$('input[name="quiz_mode"]').removeAttr('disabled','disabled');
				$('input[name="quiz_result"]').removeAttr('disabled','disabled');
			}
			else {
				$('.quiz_mode').hide();
				$('.passing_marks').hide();
				$('input[name="passing_marks"]').attr('disabled','disabled');
				$('input[name="quiz_mode"]').attr('disabled','disabled');
				$('input[name="quiz_result"]').attr('disabled','disabled');
			}
		});
		$('select[name="quiz_type"]').trigger('change');

		$('input[name="quiz_mode"]').on('change',function(){
			var quiz_mode=$(this).is('checked');
			if(this.checked){
				$('.passing_marks').show();
				$('.quiz_result').show();
				$('input[name="passing_marks"]').removeAttr('disabled','disabled');
			}
			else {
				$('.passing_marks').hide();
				$('.quiz_result').hide();
				$('input[name="passing_marks"]').attr('disabled','disabled');
			}
		});
		$('input[name="quiz_mode"]').trigger('change');

		$('select[name="quiz_visibility"]').on('change',function() {
			var quiz_visibility=$('select[name="quiz_visibility"]').val();
			if(quiz_visibility=='All') {
				$('.branch').hide();
				$('.grade').hide();
				$('.region').hide();

			}
			if(quiz_visibility=='Branch') {
				$('.branch').show();
				$('.grade').hide();
				$('.region').hide();
				$('select[name="branch_id[]"]').attr('data-validation','required');
				$('select[name="grade_master_id[]"]').removeAttr('data-validation','required');
				$('select[name="region_id[]"]').removeAttr('data-validation','required');
			}
			if(quiz_visibility=='Employee grade') {
				$('.branch').hide();
				$('.grade').show();
				$('.region').hide();
				$('select[name="branch_id[]"]').removeAttr('data-validation','required');
				$('select[name="grade_master_id[]"]').attr('data-validation','required');
				$('select[name="region_id[]"]').removeAttr('data-validation','required');
			}
			if(quiz_visibility=='Region') {
				$('.branch').hide();
				$('.grade').hide();
				$('.region').show();
				$('select[name="branch_id[]"]').removeAttr('data-validation','required');
				$('select[name="grade_master_id[]"]').removeAttr('data-validation','required');
				$('select[name="region_id[]"]').attr('data-validation','required');
			}
		});
		$('select[name="quiz_visibility"]').trigger('change');

		$('input[name="started_on"]').datepicker({
			format: 'yyyy-mm-dd',
			autoclose: true,
		}).on('changeDate', function (selected) {
			var startDate = new Date(selected.date.valueOf());
			$('input[name="end_on"]').datepicker('setStartDate', startDate);
		}).on('clearDate', function (selected) {
			$('input[name="end_on"]').datepicker('setStartDate', null);
		});

		$('input[name="end_on"]').datepicker({
			format: 'yyyy-mm-dd',
			autoclose: true,
		}).on('changeDate', function (selected) {
			var endDate = new Date(selected.date.valueOf());
			$('input[name="started_on"]').datepicker('setEndDate', endDate);
		}).on('clearDate', function (selected) {
			$('input[name="started_on"]').datepicker('setEndDate', null);
		});

		CKEDITOR.replace('ckeditor');
		CKEDITOR.config.disableNativeSpellChecker = false;

		/* poll question script */
		$('select[name="quiz_type"]').on('change',function() {
			var quiz_type=$('select[name="quiz_type"]').val();
			if(quiz_type=="poll") {
				$('.poll_question').show();
			}
			else {
				$('.poll_question').hide();
			}
		});
		$('select[name="quiz_type"]').trigger('change');

		var i=0;
		$('.question_point_add').click(function() {
			var option = $('.question_option_row table>tbody').html();
			$('.add_option tbody').append(option);
			$('.add_option tbody').find('tr:last').find('input[name="option_title[]"]').attr('name','option_title['+i+']');
			$('.add_option tbody').find('tr:last').find('input[name="file[]"]').attr('name','file['+i+']');
			$('.add_option tbody').find('tr:last').find('input[name="is_true"]').val(i);
			i++;
		});

		$('select[name="question_type"]').on('change',function() {
			var question_type=$('select[name="question_type"]').val();
			if(question_type=='image') {
				$('.option_type').hide();
				$('.image_type').show();
				$('.question_option').show();
				$('.question_point_add').show();
				$('.not_add_option').hide();
			}
			if(question_type=='option') {
				$('textarea').attr('disabled',true);
				$('.textarea_type').hide();
				$('.image_type').hide();
				$('.option_type').show();
				$('.question_option').show();
				$('.question_point_add').show();
				$('.not_add_option').hide();
			}
			else if(question_type=='') {
				$('.question_point_add').hide();
				$('.not_add_option').show();
			}

		});
		$('select[name="question_type"]').trigger('change');
	});

	function delete_question_option(ele,option_id) {
		var parent=$(ele).parent().parent();
		$(parent).remove();
		$('#quiz_master_form').append('<input type="hidden" name="deleted_question_option[]" value="'+option_id+'"/>');
	}

	function delete_image(ele) {
		var id=$(ele);
		$('#quiz_master_form').append('<input type="hidden" name="deleted_image" value="1"/>');
		$(id).parent().remove();
	}
</script>