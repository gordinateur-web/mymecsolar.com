<?php
$base_url = base_url();
$form_model=array();
if(!empty($edit_data)) {
	$form_model=$edit_data;
}
/* Fill old data */
$old_data=$this->session->flashdata('old_data');
if(!empty($old_data)) {
	$form_model=$old_data;  
}
echo $this->form->form_model($form_model, $base_url.'quiz/quiz_question_save/',array('name'=>'save_quiz_question','id'=>'quiz_question_form', 'class'=>'validate-form', 'onsubmit'=>'return quiz_question_validation(this);'));
if(!empty($edit_data)) {
	echo $this->form->form_hidden('question_id');
}
echo $this->form->form_hidden('quiz_master_id', $quiz_master_id);
?>

<div class="row">
	<div class="box-body">
		<div id="model_errors"></div>

		<div class="col-md-12">

		<div class="col-sm-12">
			<div class="form-group">
				<label>Question Title <span class="text-danger">*</span></label>
				<?php 
					$other_option=array(
						'class'=>'form-control',
						'placeholder'=>'Question Title',
						'data-validation'=>'required',
						'Title'=>'Question Title',
					);
					echo $this->form->form_input('question_title', $other_option); 
				?>
			</div>
		</div>

		<div class="col-md-6 col-sm-6">
			<div class="form-group">
				<label>Question Type <span class="text-danger">*</span></label>
				<?php 
				$other_option=array(
					'class'=>'form-control',
					'placeholder'=>'Question Type',
					'data-validation'=>'required',
					'Title'=>'Question Type',
				);
				if(!empty($edit_data['question_type'])) {
					$other_option['disabled']='disabled';
				}
				$option=array();
				$option['']='SELECT';
				$option['Option']=array(
					'image'=>'Option Image',
					'option'=>'Option text',
				);
				if($quiz_type=='quiz') {
					$option['Text']=array(
							'textarea'=>'Textarea',
							'html_content'=>'html content',
					);
				}
				
				echo $this->form->form_dropdown('question_type',$option,'','', $other_option); 
				?>
			</div>
		</div>

		<?php if($quiz_mode=='1') { ?>
		<div class="col-md-6 col-sm-6">
			<div class="form-group">
				<label>Question Point <span class="text-danger">*</span></label>
				<?php 
				$other_option=array(
					'class'=>'form-control number_field',
					'placeholder'=>'Question Point',
					'data-validation'=>'required',
					'Title'=>'Question Point',
				);
				echo $this->form->form_input('question_point', $other_option); 
				?>
			</div>
		</div>
		<?php } ?>
		<div class="clearfix"></div>

		<div class="col-md-6 col-sm-6">
			<div class="form-group">
				<label>Question Status <span class="text-danger">*</span></label>
				<?php 
				$other_option=array(
					'class'=>'form-control',
					'placeholder'=>'Question Status',
					'data-validation'=>'required',
					'Title'=>'Question Status',
				);
				$option= array(
					'1'=>'Enable',
					'0'=>'Disable',
				);
				echo $this->form->form_dropdown('enable',$option,'','', $other_option); 
				?>
			</div>
		</div>
		<div class="clearfix"></div>

		<div class="question_option col-md-12">
		<h4 class="pull-left">Options </h4>
		
		<a class="pull-right btn btn-sm btn-primary question_point_add">
			<i class="fa fa-plus"></i> Add Option
		</a>
		<div class="clearfix"></div>
		<span class="true_error text-danger" align="center" style="display: none;">Please select true option</span>
		<div class="clearfix"></div>
		<div class="table-responsive">
			<table class='table table-bordered table-striped dataTable add_option' id="full-height-datatable">
				<thead>
					<tr>
						<th>Title</th>
						<?php if($quiz_mode=='1') { ?>
						<th>is true</th>
						<?php } ?>
						<th>Option</th>
					</tr>
				</thead>
				<tbody class="">
					<tr class="not_add_option">
						<td colspan="2"><p class="text-danger"> Please select question type</p></td>
					</tr>
					<?php if(!empty($question_option_list)) {
					 foreach ($question_option_list as $key => $question_option) { ?>
					<tr>
						<td>
							<div class="image_type">
								<!-- <input type="file" name="file[]" > -->
								<?php
								echo $this->form->form_upload('file['.$question_option['option_id'].']','','',''); 
								 ?>

								<?php if(!empty($question_option['media_path'])) { ?>
								<div class="thumbnail" style="width: 100px;">
									<img src="<?php echo $base_url.$question_option['media_path']; ?>" height="50">
								</div>
								<?php } ?>
							</div>
							<div class="option_type">
								<?php 
								$other_option=array(
									'class'=>'form-control',
									'placeholder'=>'Title'
								);
								echo $this->form->form_input('option_title['.$question_option['option_id'].']','',$question_option['option_title'],$other_option); 
								?>
							</div>
						</td>
						<?php  if($quiz_mode=='1') { ?>
							<td><input type="radio" name="is_true" value="<?php echo $question_option['option_id']; ?>" 
							<?php if($question_option['is_true']==1) {
								echo 'checked';
								} ?>
							> True <br></td>
						<?php } ?>
						<td>
							<a class="pointer btn btn-sm btn-danger" onclick="delete_question_option(this, <?php echo $question_option['option_id']; ?>);">
								<i class="fa fa-trash-o fa-margin"></i> Delete
							</a>
						</td>
					</tr>
					<?php } } ?>
				</tbody>
			</table>
		</div>
		</div>

		<div class="html_body">
			<?php 
				$other_option=array(
					'class'=>'form-control ckeditor',
				);
				echo $this->form->form_textarea('html_content','','',$other_option); 
			?>
		</div>

		</div>
	</div>
	
</div>

<div class="box-footer with-border">
	<div class="box-tools pull-right">
		<input type="reset" class="btn btn-default" value="Reset">
		<input type="submit" class="btn btn-primary" value="Submit">
	</div>
</div>
<?php echo $this->form->form_close(); ?>

<div class="question_option_row" style="display: none;">
	<table>
		<tbody>
			<td>
				<div class="image_type">
					<input type="file" name="file[]" data-validation="required" title="Option image" class="form-control" >
				</div>
				<div class="option_type">
					<input type="text" name="option_title[]" data-validation="required" placeholder="Title" title="Option title" class="form-control" >
				</div>
				<!-- <div class="html_type">
					<textarea rows="2" name="option_title[]" class="form-control"></textarea>
				</div> -->
			</td>
			<?php if($quiz_mode=='1') { ?>
			<td><input type="radio" id="" name="is_true" value="0"> True <br></td>
			<?php } ?>
			<td>
				<a class="pointer btn btn-sm btn-danger" onclick="delete_question_option(this);">
					<i class="fa fa-trash-o fa-margin"></i> Delete
				</a>
			</td>
		</tbody>
	</table>
</div>

<script type="text/javascript" src="https://ajax.aspnetcdn.com/ajax/jquery.validate/1.11.1/jquery.validate.js"></script>
<script src="<?php echo base_url().'public/plugins/ckeditor/ckeditor.js';?>" type="text/javascript"></script>
<script type="text/javascript">
	$(document).ready(function() {	

		var i=0;
		$('.question_point_add').click(function()  {
			var option = $('.question_option_row table>tbody').html();
			$('.add_option tbody').append(option);
			$('.add_option tbody').find('tr:last').find('input[name="option_title[]"]').attr('name','option_title['+i+']');
			$('.add_option tbody').find('tr:last').find('input[name="file[]"]').attr('name','file['+i+']');
			$('.add_option tbody').find('tr:last').find('input[name="is_true"]').val(i);
			i++;
		});

		$('select[name="question_type"]').on('change',function(){
			var question_type=$('select[name="question_type"]').val();
			if(question_type=='image') {
				$('textarea').attr('disabled',true);
				//$('input[type="file"][name="file[]"]').removeAttr('disabled',true);
				$('.option_type').hide();
				$('.textarea_type').hide();
				$('.image_type').show();
				$('.question_option').show();
				$('.not_add_option').hide();
				$('.question_point_add').show();
				$('.html_body').hide();
			}
			if(question_type=='option') {
				//$('input[type="file"][name="file[]"]').attr('disabled',true);
				$('textarea').attr('disabled',true);
				$('.textarea_type').hide();
				$('.image_type').hide();
				$('.option_type').show();
				$('.question_option').show();
				$('.not_add_option').hide();
				$('.question_point_add').show();
				$('.html_body').hide();
			}
			if(question_type=='textarea') {
				$('textarea').removeAttr('disabled',true);
				$('input[type="file"][name="file[]"]').attr('disabled',true);
				$('.question_option').hide();
				$('.not_add_option').hide();
				$('.html_body').hide();
			}
			if(question_type=='html_content') {
				$('.question_option').hide();
				$('.not_add_option').hide();
				$('.html_body').show();
			}
			else if(question_type=='') {
				$('.not_add_option').show();
				$('.question_point_add').hide();
				$('.html_body').hide();
			}

		});
		$('select[name="question_type"]').trigger('change');

		jQuery.validate({
			form : '#quiz_question_form'
		});

		CKEDITOR.replace('ckeditor');
		CKEDITOR.config.disableNativeSpellChecker = false;
	});

	function quiz_question_validation() {
		var is_true=$('input[name=is_true]','table.add_option');
		if(is_true.length == 0) {
			$('.true_error').hide();
			return true;
		}

		if (!$('input[name=is_true]:checked').val() ) {    
		    $('.true_error').show();
		    return false;
		}
		else {
			$('.true_error').hide();
			return true;
		}
		return false;
	}

	function delete_question_option(ele,option_id) {
		var parent=$(ele).parent().parent();
		$(parent).remove();
		$('#quiz_question_form').append('<input type="hidden" name="deleted_question_option[]" value="'+option_id+'"/>');
	}

	function delete_image(ele) {
		var id=$(ele);
		$('#quiz_question_form').append('<input type="hidden" name="deleted_image" value="1"/>');
		$(id).parent().remove();
	}
	
</script>