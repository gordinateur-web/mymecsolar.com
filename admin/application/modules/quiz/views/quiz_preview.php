<div class="row">
	<?php $base_url=base_url();?>
	<div class="col-md-12">
		<div class="box box-primary">
			<div class="box-header with-border">
				<div class="box-title">
					<h3 class="box-title"><?php echo $quiz_master[0]['quiz_title']; ?></h3>
				</div>
			</div>

			<div class="panel-body">
				<?php if(empty($quiz_question)) { ?>
				<div class="alert alert-info alert-dismissible">
					<i class="icon fa fa-ban"></i> No question available for quiz preview.
				</div>
				<?php } ?>

				<div class="quiz-container">
					<!-- <h1 class="red-text">Quiz: <span class="text-capitalize"><?php echo $quiz_master[0]['quiz_title']; ?></span></h1> -->
					<p><?php echo $quiz_master[0]['quiz_description']; ?></p>
					<p><?php echo $quiz_master[0]['quiz_instruction']; ?></p>
					
					<h4 class="start_quiz_msg text-success"></h4>

					<!-- Quiz question and option render  -->
					<?php $this->quiz_model->quiz_question_render($quiz_master, $quiz_question, $quiz_question_option); ?>

				</div>
			</div>


		</div>
	</div>
</div>