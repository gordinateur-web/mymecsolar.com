<div class="row">
	<?php $base_url=base_url();?>
		<div class="col-md-12">
			<div class="box box-primary">
				<div class="box-header with-border">
					<div class="box-title">
						<a href="<?php echo base_url().'quiz/quiz_master_add' ?>" class="btn btn-sm btn-primary" title="Add New"><i class="fa fa-plus"></i> Add Quiz/Poll Master</a>
						
					</div>
					<div class="pull-right">
						<?php if (isset($pagination_links)){ echo $pagination_links; } ?>
					</div>
				</div>

				<div class="panel-body">
					<div class="box-body table-responsive">
						<table class='table table-bordered table-striped dataTable' id="full-height-datatable">
							<thead>
								<tr>
									<th>Quiz/Poll</th>
									<th>Description</th>
									<th>Visibility</th>
									<th>Type</th>
									<th>Start</th>
									<th>End</th>
									<!-- <th width="5%">Passing Mark</th> -->
									<th width="5%">Action</th>
								</tr>
							</thead>
							<tbody>
							<?php foreach($quiz_master as $quiz_master_row) { 
							  	$encrypt_id=encrypt_id($quiz_master_row['quiz_master_id']);
							   ?>
								<tr>
									<td><?php echo $quiz_master_row['quiz_title']; ?></td>
									<td><?php echo $quiz_master_row['quiz_description']; ?></td>
									<td><?php echo $quiz_master_row['quiz_visibility']; ?></td>
									<td><?php echo $quiz_master_row['quiz_type']; ?></td>
									<td><?php echo dateformat($quiz_master_row['started_on']); ?></td>
									<td><?php echo dateformat($quiz_master_row['end_on']); ?></td>
									<!-- <td><?php echo $quiz_master_row['passing_marks']; ?></td> -->
									<td>
										<div class="btn-group options">
											<button type="button" class="btn btn-default btn-sm" dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
												<i class="fa fa-cog"></i> Options
											</button>
											<ul class="dropdown-menu">
												<?php if($quiz_master_row['quiz_type']=='quiz') { ?>
												<li>
													<a href="<?php echo base_url().'quiz/quiz_question_list/'.$encrypt_id; ?>" > 
														<i class="fa fa-question-circle-o"></i> Question
													</a>
												</li>
												<?php } ?>
												<li>
													<a href="<?php echo base_url().'quiz/quiz_preview/'.$encrypt_id; ?>" > 
														<i class="fa fa-list-ol" aria-hidden="true"></i>
														<?php 
														if($quiz_master_row['quiz_type']=='quiz') {
															echo 'Preview Quiz';
														}
														if($quiz_master_row['quiz_type']=='poll') {
															echo 'Preview Poll';
														} ?>
													</a>
												</li>
												<?php if($quiz_master_row['quiz_type']=='quiz') { ?>
												<li>
													<a href="<?php echo base_url().'quiz/quiz_result/'.$encrypt_id; ?>" > 
														<i class="fa fa-check-square-o" aria-hidden="true"></i>Quiz Result
													</a> 
												</li>
												<?php } 
												if($quiz_master_row['quiz_type']=='poll') { ?>
												<li>
													 <a href="#" class="" onclick="get_modaldata('Poll Result',base_url+'quiz/poll_result/<?php echo $encrypt_id; ?>')" ><i class="fa fa-check-square-o"></i> Poll Result</a>
												</li>
												<?php } 
												if($quiz_master_row['quiz_type']=='quiz' && $quiz_master_row['quiz_mode']=='0')  {
												?>

												<li>
													<a href="<?php echo base_url().'quiz/poll_result/'.$encrypt_id.'/export'; ?>" > 
														<i class="fa fa-file-excel-o" aria-hidden="true"></i> Export
													</a>
												</li>
												<?php } ?>
												<li>
													<a href="<?php echo base_url().'quiz/quiz_master_add/'.$encrypt_id; ?>" > 
														<i class=" fa fa-edit fa-margin table-list-edit"></i> Edit
													</a>
												</li>
												<li>
													<a data-id="<?php echo $quiz_master_row['quiz_master_id']; ?>" class="pointer delete-menu-master">
														<i class="fa fa-trash-o fa-margin"></i> Delete
													</a>
												</li>
											</ul>
										</div>
									</td>
								</tr>
							<?php }?>  
							</tbody>
						</table>
					</div>
				</div>
			</div>
	</div>
</div>

<script type="text/javascript">
	$(document).ready(function() {
		$('.delete-menu-master').click(function() {
			var id=$(this).attr('data-id');
			swal({
				title: "Are you sure?",
				text: "Quiz Master to delete?",
				type: "warning",
				showCancelButton: true,
				confirmButtonColor: "#DD6B55",
				confirmButtonText: "Yes, Delete it!",
				closeOnConfirm: false
			},
			function() {
				$.ajax({
					url     : base_url+"quiz/quiz_master_delete/"+id,
					type    : 'POST',
					success : function(data) {
						data=$.parseJSON(data);
						if(data.status == '0') {
							swal({
								title: "Are you sure?",
								text: "Some user already attended this quiz",
								type: "warning",
								showCancelButton: true,
								confirmButtonColor: "#DD6B55",
								confirmButtonText: "Yes, Delete it!",
								closeOnConfirm: true,
							}, function() {
								window.location = base_url+'quiz/quiz_master_del/'+id;
							});
						}
						else {
							swal("success", data.message, "success");
						}
					},
				});
			});
		});
	});

</script>


