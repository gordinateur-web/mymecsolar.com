<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Quiz extends CI_Controller {

	public function __construct() {
		parent::__construct();
		/* Permission for adminpanel access, Quiz module access */
		permission_check(array(1,12),'and');

		$this->load->model('quiz/quiz_model');
		$this->load->model('menu/menu_model');
		$this->load->model('user/user_model');
	}

	public function index() {
		$data['title']="Quiz/Poll Master";

		/* Total count of quiz master  */
		$count_filter['SELECT']='count(*) as cnt';
		$quiz_cnt=$this->quiz_model->get_quiz_master($count_filter);

		/* list pagination config  */
		$config['per_page'] = '100';
		$data['pagination_links']=get_pagination_links($quiz_cnt[0]['cnt'], $config, false);
		$page=$this->input->get('per_page');
		if(!$page) {
			$page=0;
		}

		/* get quiz master list */
		$filter_quiz['LIMIT']=array($config['per_page'],$page);
		$data['quiz_master']=$this->quiz_model->get_quiz_master($filter_quiz);
		//dsm($data['quiz_master']);die;
		view_with_master('quiz/quiz_master_list',$data);
	}

	public function quiz_master_add($quiz_master_id=false) {
		$data=array();

		/* get branch */
		$data['branch']=$this->user_model->get_branch();

		/* get grade */
		$data['grade']=$this->user_model->get_grade_master();

		/* get region */
		$data['region']=$this->user_model->get_branch_region();

		$data['title']="Add Quiz/Poll";
		if(!empty($quiz_master_id)) {
			$edit_data=$this->quiz_model->get_quiz_master(array('quiz_master_id'=>$quiz_master_id));
			$data['edit_data']=$edit_data[0];
			$quiz_visibility=$this->quiz_model->get_quiz_visibility(array('quiz_master_id'=>$quiz_master_id));
			$branch_array=array_column($quiz_visibility, 'branch_id');
			$region_array=array_column($quiz_visibility, 'region_id');
			$grade_array=array_column($quiz_visibility, 'grade_master_id');

			if(!empty($quiz_visibility[0]['branch_id'])) {
				$data['edit_data']['branch_id']=$branch_array;
			}
			if(!empty($quiz_visibility[0]['region_id'])) {
				$data['edit_data']['region_id']=$region_array;
			}
			if(!empty($quiz_visibility[0]['grade_master_id'])) {
				$data['edit_data']['grade_master_id']=$grade_array;
			}
			
			$edit_data=$this->quiz_model->get_quiz_question(array('quiz_master_id'=>$quiz_master_id));
			//dsm($edit_data);die;
			$data['edit_data']['question_id']=$edit_data[0]['question_id'];
			$data['edit_data']['question_title']=$edit_data[0]['question_title'];
			$data['edit_data']['question_type']=$edit_data[0]['question_type'];
			$data['edit_data']['enable']=$edit_data[0]['enable'];


			$data['question_option_list']=$this->quiz_model->get_question_option(array('quiz_question_option.question_id'=>$data['edit_data']['question_id']));
			//dsm($data['question_option_list']);die;
			
		}
		view_with_master('quiz/quiz_master_add',$data);
	}

	public function quiz_master_save() {
		$this->load->library('form_validation');
		$validate_rules=array(
			array(
				'field'=>'quiz_title',
				'label'=>'Title',
				'rules'=>'required'
			),
			array(
				'field'=>'quiz_visibility',
				'label'=>'Visibility',
				'rules'=>'required'
			),
			
		);

		$quiz_master_id= $this->input->post('quiz_master_id');
		if(empty($quiz_master_id)) {
			$validate_rules[]=array(
				array(
					'field'=>'quiz_type',
					'label'=>'Type',
					'rules'=>'required'
				),
			);
		}

		$this->form_validation->set_rules($validate_rules);

		if ($this->form_validation->run() == FALSE) {
			set_message(validation_errors());
			$this->session->set_flashdata('old_data',$this->input->post());
			redirect_back();
			return 0;
		}
		$post_array= $this->input->post();

		$question_id= $this->input->post('question_id');
		$deleted_question_option= $this->input->post('deleted_question_option');
		
		$user_id=$this->session->userdata('user_id');

		$quiz_master_data=array(
			'quiz_title'=>$post_array['quiz_title'],
			//'quiz_description'=>$post_array['quiz_description'],
			//'quiz_instruction'=>$post_array['quiz_instruction'],
			'quiz_visibility'=>$post_array['quiz_visibility'],
			//'quiz_type'=>$post_array['quiz_type'],
			'started_on'=>mysql_dateformat($post_array['started_on']),
			'end_on'=>mysql_dateformat($post_array['end_on']),
		);

		if(!empty($post_array['quiz_type'])) {
			$quiz_master_data['quiz_type']=$post_array['quiz_type'];
		}

		if(!empty($post_array['quiz_mode'])) {
			$quiz_master_data['quiz_mode']=$post_array['quiz_mode'];
		}
		else {
			$quiz_master_data['quiz_mode']='0';
		}
		if(!empty($post_array['quiz_result'])) {
			$quiz_master_data['quiz_result']=$post_array['quiz_result'];
		}
		else {
			$quiz_master_data['quiz_result']='0';
		}
		if(!empty($post_array['passing_marks'])) {
			$quiz_master_data['passing_marks']=$post_array['passing_marks'];
		}
		if(!empty($post_array['quiz_description'])) {
			$quiz_master_data['quiz_description']=$post_array['quiz_description'];
		}
		if(!empty($post_array['quiz_instruction'])) {
			$quiz_master_data['quiz_instruction']=$post_array['quiz_instruction'];
		}

		/* quiz question add */
		$quiz_question_data=array(
			//'quiz_master_id'=>$quiz_master_id,
			'question_title'=>$post_array['question_title'],
			//'question_type'=>$post_array['question_type'],
			'enable'=>$post_array['enable'],
		);

		/* edit quiz master */
		if(!empty($quiz_master_id)) {
			$quiz_master_data['updated_at']=date('Y-m-d H:i:s');
			$quiz_master_data['updated_by']=$user_id;
			$this->quiz_model->update_quiz_master($quiz_master_data, $quiz_master_id);

			/* delete quiz visibility */
			$this->quiz_model->delete_quiz_visibility(array('quiz_master_id'=>$quiz_master_id));


			/* edit quiz question */
			$quiz_question_data['updated_at']=date('Y-m-d H:i:s');
			$quiz_question_data['updated_by']=$user_id;
			$this->quiz_model->update_quiz_question($quiz_question_data, $question_id);

			/* delete question option */
			if(!empty($deleted_question_option)) {
				$filter_question_option['WHERE_IN']=array('option_id'=>$deleted_question_option);
				$this->quiz_model->delete_question_option($filter_question_option);
			}

			/* get question */
			$question=$this->quiz_model->get_quiz_question(array('quiz_question.question_id'=>$question_id));

			/* get question option */
			$option=$this->quiz_model->get_question_option(array('quiz_question_option.question_id'=>$question_id));
			$option=array_column($option, 'option_id');

			/* Edit question option */
			if(!empty($post_array['option_title'])) {
				$file_array=$_FILES;
				foreach ($post_array['option_title'] as $key => $value) {
					
					/* upload image  */
					if($question[0]['question_type']=='image') {
						$file_id=$this->quiz_model->upload_image($quiz_master_id, $user_id, $file_array, $key);
						unset($question_option_data['file_id']);
						if(!empty($file_id)) {
							$question_option_data['file_id']=$file_id;
						}
						$question_option_data['option_title']=null;
					}
					elseif($question[0]['question_type']=='option') {
						if(!empty($value)) {
							$question_option_data['option_title']=$value;
							$question_option_data['file_id']='0';
						}
					}

					if(!empty($post_array['is_true'])) {
						if($key==$post_array['is_true']) {
							$question_option_data['is_true']='1';
						}
						else {
							$question_option_data['is_true']='0';
						}
					}
					
					$question_option_data['question_id']=$question_id;
					$question_option_data['updated_at']=date('Y-m-d H:i:s');
					$question_option_data['updated_by']=$user_id;

					if(in_array($key, $option)) {
						$this->quiz_model->update_question_option($question_option_data, $key);
					}
					else {
						//if(!empty($value)) {
							$this->quiz_model->add_question_option($question_option_data);
						//}
					}
				}
			}
		}
		/* add new quiz master */
		else {

			$quiz_master_data['created_at']=date('Y-m-d H:i:s');
			$quiz_master_data['created_by']=$user_id;
			$quiz_master_id=$this->quiz_model->add_quiz_master($quiz_master_data);

			
			$quiz_question_data['quiz_master_id']=$quiz_master_id;
			$quiz_question_data['question_type']=$post_array['question_type'];
			$quiz_question_data['created_at']=date('Y-m-d H:i:s');
			$quiz_question_data['created_by']=$user_id;
			$question_id=$this->quiz_model->add_quiz_question($quiz_question_data);

			/* add question option */
			if(!empty($post_array['option_title'])) {
				$file_array=$_FILES;
				foreach ($post_array['option_title'] as $key => $value) {

					/* upload image  */
					if($post_array['question_type']=='image') {
						$file_id=$this->quiz_model->upload_image($quiz_master_id, $user_id,$file_array, $key);

						if(!empty($file_id)) {
							$question_option_data['file_id']=$file_id;
						}
					}
					if(!empty($value)) {
						if($post_array['question_type']=='option') {
							$question_option_data['option_title']=$value;
						}
					}

					$question_option_data['is_true']='0';
					$question_option_data['question_id']=$question_id;
					$question_option_data['created_at']=date('Y-m-d H:i:s');
					$question_option_data['created_by']=$user_id;
					$this->quiz_model->add_question_option($question_option_data);
				}
			}
		}

		/* add quiz visibility  */
		$visibility_array=array();
		$visibility_column='';

		if($post_array['quiz_visibility']=='Branch') {
			$visibility_column='branch_id';
			$visibility_array=$post_array['branch_id'];
		}
		elseif($post_array['quiz_visibility']=='Employee grade') {
			$visibility_column='grade_master_id';
			$visibility_array=$post_array['grade_master_id'];
		}
		elseif($post_array['quiz_visibility']=='Region') {
			$visibility_column='region_id';
			$visibility_array=$post_array['region_id'];
		}

		foreach ($visibility_array as $key=>$value) {
			$quiz_visibility_data[$visibility_column] = $value;
			$quiz_visibility_data['quiz_master_id'] = $quiz_master_id;
			$this->quiz_model->add_quiz_visibility($quiz_visibility_data);
		}

		set_message('Quiz master data saved','success');
		redirect(base_url().'quiz');

	}

	public function quiz_master_delete($id) {
		$user_id=$this->session->userdata('user_id');
		if(!empty($id)) {
			$quiz=$this->quiz_model->get_quiz(array('quiz.quiz_master_id'=>$id));

			if(empty($quiz)) {
				/* get quiz question */
				$question=$this->quiz_model->get_quiz_question(array('quiz_master_id'=>$id));
				$option=$this->quiz_model->get_question_option(array('quiz_master_id'=>$id));
				$question_id=array_column($question, 'question_id');
				$option_id=array_column($option, 'option_id');

				if(!empty($question_id)) {
					/* delete question and option */
					$question_data['deleted_at']=date('Y-m-d H:i:s');
					$question_data['deleted_by']=$user_id;
					$this->quiz_model->update_quiz_question($question_data, $question_id);
					$this->quiz_model->update_question_option($question_data, $option_id);
				}

				/* delete quiz master */
				$quiz_data['deleted_at']=date('Y-m-d H:i:s');
				$quiz_data['deleted_by']=$user_id;
				$this->quiz_model->update_quiz_master($quiz_data, $id);

				$return=array(
					'status'=>'1',
					'message'=>'Quiz master deleted successfully',
				);
				echo json_encode($return);
				//set_message('Quiz master deleted successfully','success');
				//redirect_back();
			}
			else {
				$return=array(
					'status'=>'0'
				);
				echo json_encode($return);
			}
		}
		else {
			set_message('Please select valid quiz master');
			redirect_back();
		}
		
		return 0;
	}

	public function quiz_master_del($id) {
		$user_id=$this->session->userdata('user_id');
		/* get quiz question */
		$question=$this->quiz_model->get_quiz_question(array('quiz_master_id'=>$id));
		$option=$this->quiz_model->get_question_option(array('quiz_master_id'=>$id));
		$question_id=array_column($question, 'question_id');
		$option_id=array_column($option, 'option_id');

		if(!empty($question_id)) {
			/* delete question and option */
			$question_data['deleted_at']=date('Y-m-d H:i:s');
			$question_data['deleted_by']=$user_id;
			$this->quiz_model->update_quiz_question($question_data, $question_id);
			$this->quiz_model->update_question_option($question_data, $option_id);
		}

		/* delete quiz master */
		$quiz_data['deleted_at']=date('Y-m-d H:i:s');
		$quiz_data['deleted_by']=$user_id;
		$this->quiz_model->update_quiz_master($quiz_data, $id);
		set_message('Quiz master deleted successfully','success');
		redirect_back();
	}

	public function quiz_question_list($quiz_master_id) {
		$data['quiz_master_id']=$quiz_master_id;

		/* Get quiz details */
		$data['quiz_master']=$this->quiz_model->get_quiz_master(array(
			'quiz_master_id'=>$quiz_master_id
		));

		if(empty($data['quiz_master'])) {
			show_404();
		}
		$data['quiz_master']=$data['quiz_master'][0];
		$data['title']='Questions of "<b>'.$data['quiz_master']['quiz_title'].'</b>"';

		/* Total count of quiz master  */
		$count_filter['SELECT']='count(*) as cnt';
		$quiz_question_cnt=$this->quiz_model->get_quiz_question($count_filter);

		/* list pagination config  */
		$config['per_page'] = '100';
		$data['pagination_links']=get_pagination_links($quiz_question_cnt[0]['cnt'], $config, false);
		$page=$this->input->get('per_page');
		if(!$page) {
			$page=0;
		}

		/* get quiz question */
		$filter_quiz_question['LIMIT']=array($config['per_page'],$page);
		$filter_quiz_question['quiz_master_id']=$quiz_master_id;
		$data['quiz_question_list']=$this->quiz_model->get_quiz_question($filter_quiz_question);

		view_with_master('quiz/quiz_question_list',$data);
	}

	public function quiz_question_add($quiz_master_id=false,$question_id=false) {
		$data=array();

		/* Get quiz details */
		$data['quiz_master']=$this->quiz_model->get_quiz_master(array(
			'quiz_master_id'=>$quiz_master_id
		));

		$data['quiz_master_id']=$quiz_master_id;
		$data['quiz_type']=$data['quiz_master'][0]['quiz_type'];
		$data['quiz_mode']=$data['quiz_master'][0]['quiz_mode'];

		if(!empty($question_id)) {
			$edit_data=$this->quiz_model->get_quiz_question(array('question_id'=>$question_id));
			$data['edit_data']=$edit_data[0];
			$data['question_option_list']=$this->quiz_model->get_question_option(array('quiz_question_option.question_id'=>$question_id));
			//dsm($data['question_option_list']);die;
		}
		$this->load->view('quiz/quiz_question_add',$data);
	}

	public function quiz_question_save() {
		$post_array=$this->input->post();
		/*dsm($_FILES);
		dsm($post_array);*/
		$this->load->library('form_validation');
		$validate_rules[]=array(
			'field'=>'question_title',
			'label'=>'Question Title',
			'rules'=>'required'
		);
		

		/* validation for question point */
		$quiz_master_id=$this->input->post('quiz_master_id');
		/* get quiz master */
		$quiz_master=$this->quiz_model->get_quiz_master(array('quiz_master_id'=>$quiz_master_id));
		if($quiz_master[0]['quiz_mode']=='1') {
			$validate_rules[]=array(
				'field'=>'question_point',
				'label'=>'Question Point',
				'rules'=>'required'
			);
		}

		if($post_array['question_type']=='option') {
			$validate_rules[]=array(
				'field'=>'option_title[]',
				'label'=>'Question Option',
				'rules'=>'required'
			);
		}
		if($post_array['question_type']=='image') {
			if(empty($_FILES['file']['name'])) {
				set_message('The option image field is required.');
				redirect_back();
			}
		}

		/* validation for question type */
		$question_id=$this->input->post('question_id');
		if(empty($question_id)) {
			$validate_rules[]=array(
				'field'=>'question_type',
				'label'=>'Question Type',
				'rules'=>'required'
			);
		}

		$this->form_validation->set_rules($validate_rules);
		if ($this->form_validation->run() == FALSE) {
			set_message(validation_errors());
			$this->session->set_flashdata('old_data',$this->input->post());
			redirect_back();
			return 0;
		}
		
		$user_id=$this->session->userdata('user_id');
		$deleted_question_option= $this->input->post('deleted_question_option');

		$quiz_question_data=array(
			'quiz_master_id'=>$quiz_master_id,
			'question_title'=>$post_array['question_title'],
			'enable'=>$post_array['enable'],
		);
		if(!empty($post_array['question_type'])) {
			$quiz_question_data['question_type']=$post_array['question_type'];
		}
		if(!empty($post_array['question_point'])) {
			$quiz_question_data['question_point']=$post_array['question_point'];
		}
		if(!empty($post_array['html_content'])) {
			$quiz_question_data['html_content']=$post_array['html_content'];
		}

		/* edit question */
		if(!empty($question_id)) {
			$quiz_question_data['updated_at']=date('Y-m-d H:i:s');
			$quiz_question_data['updated_by']=$user_id;
			$this->quiz_model->update_quiz_question($quiz_question_data, $question_id);

			/* delete question option */
			if(!empty($deleted_question_option)) {
				$filter_question_option['WHERE_IN']=array('option_id'=>$deleted_question_option);
				$this->quiz_model->delete_question_option($filter_question_option);
			}

			/* get question */
			$question=$this->quiz_model->get_quiz_question(array('quiz_question.question_id'=>$question_id));

			/* get question option */
			$option=$this->quiz_model->get_question_option(array('quiz_question_option.question_id'=>$question_id));
			$option=array_column($option, 'option_id');

			/* Edit question option */
			if(!empty($post_array['option_title'])) {
				$file_array=$_FILES;
				foreach ($post_array['option_title'] as $key => $value) {
					
					/* upload image  */
					if($question[0]['question_type']=='image') {
						$file_id=$this->quiz_model->upload_image($quiz_master_id, $user_id, $file_array, $key);
						unset($question_option_data['file_id']);
						if(!empty($file_id)) {
							$question_option_data['file_id']=$file_id;
						}
						$question_option_data['option_title']=null;
					}
					elseif($question[0]['question_type']=='option') {
						if(!empty($value)) {
							$question_option_data['option_title']=$value;
							$question_option_data['file_id']='0';
						}
					}

					if(!empty($post_array['is_true'])) {
						if($key==$post_array['is_true']) {
							$question_option_data['is_true']='1';
						}
						else {
							$question_option_data['is_true']='0';
						}
					}
					
					$question_option_data['question_id']=$question_id;
					$question_option_data['updated_at']=date('Y-m-d H:i:s');
					$question_option_data['updated_by']=$user_id;
					
					if(in_array($key, $option)) {
						$this->quiz_model->update_question_option($question_option_data, $key);
					}
					else {
						//if(!empty($value)) {
							$this->quiz_model->add_question_option($question_option_data);
						//}
					}
				}
			}
		}
		/* add new question */
		else {
			$quiz_question_data['created_at']=date('Y-m-d H:i:s');
			$quiz_question_data['created_by']=$user_id;
			$question_id=$this->quiz_model->add_quiz_question($quiz_question_data);

			/* add question option */
			if(!empty($post_array['option_title'])) {
				$file_array=$_FILES;
				foreach ($post_array['option_title'] as $key => $value) {

					/* upload image  */
					if($post_array['question_type']=='image') {
						$file_id=$this->quiz_model->upload_image($quiz_master_id, $user_id,$file_array, $key);

						if(!empty($file_id)) {
							$question_option_data['file_id']=$file_id;
						}
					}
					if(!empty($value)) {
						if($post_array['question_type']=='option') {
							$question_option_data['option_title']=$value;
						}
					}

					if($key==$post_array['is_true']) {
						$question_option_data['is_true']='1';
					}
					else {
						$question_option_data['is_true']='0';
					}
					
					$question_option_data['question_id']=$question_id;
					$question_option_data['created_at']=date('Y-m-d H:i:s');
					$question_option_data['created_by']=$user_id;
					$this->quiz_model->add_question_option($question_option_data);
					
				}
			}
		}
		set_message('Quiz question data saved','success');
		redirect(base_url().'quiz/quiz_question_list/'.$quiz_master_id);

	}

	public function quiz_question_delete($id) {
		$user_id=$this->session->userdata('user_id');

		if(!empty($id)) {
			$question_data['deleted_at']=date('Y-m-d H:i:s');
			$question_data['deleted_by']=$user_id;

			$option=$this->quiz_model->get_question_option(array('quiz_question_option.question_id'=>$id));
			$option_id=array_column($option, 'option_id');
			if(!empty($option_id)) {
				$this->quiz_model->update_question_option($question_data, $option_id);
			}
			$this->quiz_model->update_quiz_question($question_data, $id);

			set_message('Quiz Question deleted successfully','success');
			redirect_back();
		}
		else {
			set_message('Please select valid question');
			redirect_back();
		}
		return 0;
	}

	/* 
		Save order of Quiz question
	*/
	public function quiz_question_save_order() {
		$ids=$this->input->post('ids');

		foreach ($ids as $key => $value) {
			$this->db->where('question_id',$value);
			$this->db->update('quiz_question',array('question_weight'=>$key+1));
		}
		$return=array("status"=>'1',"message"=>"Reorder successfully");
		echo json_encode($return);	
	}

	public function quiz_preview($quiz_master_id) {
		$data['title']="Quiz Preview";
		$data['all_answered']=false;
		$data['submit']=false;
		$branch_id=$this->session->userdata('branch_id');
		/* get quiz master */
		$filter_quiz['quiz_master_id']=$quiz_master_id;
		$data['quiz_master']=$this->quiz_model->get_quiz_master($filter_quiz);
		$data['quiz_visibility']=$this->quiz_model->get_quiz_visibility($filter_quiz);

		/* get quiz question */
		$filter_quiz['enable']='1';
		$data['quiz_question']=$this->quiz_model->get_quiz_question($filter_quiz);
		$quiz_question_array=array_column($data['quiz_question'], 'question_id');
		
		$data['quiz_question_option']=array();
		if(!empty($quiz_question_array)) {
			/* get quiz question option */
			$filter_option['WHERE_IN']=array('quiz_question_option.question_id'=>$quiz_question_array);
			$data['quiz_question_option']=$this->quiz_model->get_question_option($filter_option);
			$data['quiz_question_option']=parent_child($data['quiz_question_option'],'question_id');
		}

		view_with_master('quiz/quiz_preview',$data);
	}

	public function quiz_result($quiz_master_id) {
		$data['title']="Quiz Result";

		/* get user list from user given quiz answers */
		$filter_quiz['quiz.quiz_master_id']=$quiz_master_id;
		$data['user_list']=$this->quiz_model->get_quiz($filter_quiz);
		//dsm($data['user_list']);die;

		/* get quiz master */
		$data['quiz_master']=$this->quiz_model->get_quiz_master(array('quiz_master_id'=>$quiz_master_id));
		//dsm($data['quiz_master']);die;

		view_with_master('quiz/quiz_result',$data);
	}

	public function user_quiz_review($quiz_master_id, $quiz_id) {
		$data['title']="Quiz Review";
		$data['all_answered']=false;
		$data['submit']=false;

		$branch_id=$this->session->userdata('branch_id');
		/* get quiz master */
		$filter_quiz['quiz_master_id']=$quiz_master_id;
		$data['quiz_master']=$this->quiz_model->get_quiz_master($filter_quiz);
		$data['quiz_visibility']=$this->quiz_model->get_quiz_visibility($filter_quiz);
		
		/* check for branch user */
		if($data['quiz_master'][0]['quiz_visibility']=='Branch') {
			$data['quiz_visibility']=array_column($data['quiz_visibility'], 'branch_id');
			if(!in_array($branch_id,$data['quiz_visibility'])) {
				show_404();
			}
		}

		/* check for employee grade */
		if($data['quiz_master'][0]['quiz_visibility']=='Employee grade') {
			$data['quiz_visibility']=array_column($data['quiz_visibility'], 'grade_master_id');
			$employee_grade=$this->session->userdata('employee_grade');
			if(!in_array($employee_grade,$data['quiz_visibility'])) {
				show_404();
			}
		}

		/* check for region */
		if($data['quiz_master'][0]['quiz_visibility']=='Region') {
			$branch=$this->user_model->get_branch(array('branch_id'=>$branch_id));
			$region=$this->user_model->get_branch_region(array('branch_region_id'=>$branch[0]['branch_region_id']));
			$data['quiz_visibility']=array_column($data['quiz_visibility'], 'region_id');
			if(!in_array($region[0]['branch_region_id'],$data['quiz_visibility'])) {
				show_404();
			}
		}

		/* get quiz question */
		$filter_quiz['enable']='1';
		$data['quiz_question']=$this->quiz_model->get_quiz_question($filter_quiz);
		$quiz_question_array=array_column($data['quiz_question'], 'question_id');
		
		$data['quiz_question_option']=array();
		if(!empty($quiz_question_array)) {
			/* get quiz question option */
			$filter_option['WHERE_IN']=array('quiz_question_option.question_id'=>$quiz_question_array);
			$data['quiz_question_option']=$this->quiz_model->get_question_option($filter_option);
			$data['quiz_question_option']=parent_child($data['quiz_question_option'],'question_id');
			//dsm($data['quiz_question_option']);die;
		}

		/* get user quiz answer review */
		$filter_quiz_answer['quiz_id']=$quiz_id;
		$data['edit_data']=$this->quiz_model->get_quiz_answer($filter_quiz_answer);
		$data['edit_data']=parent_child($data['edit_data'], 'quiz_question_id');
		//dsm($data['edit_data']);

		view_with_master('quiz/quiz_review',$data);
	}

	public function poll_result($quiz_master_id, $export=false) {
		$data=array();
		$data['all_answered']=false;
		/* get quiz master */
		$filter_quiz_master['quiz_master_id']=$quiz_master_id;
		$data['quiz_master']=$this->quiz_model->get_quiz_master($filter_quiz_master);

		/* get quiz question */
		$filter_quiz_question['quiz_master_id']=$quiz_master_id;
		$data['quiz_question']=$this->quiz_model->get_quiz_question($filter_quiz_question);
		//dsm($data['quiz_question']);die;
		/* check quiz question available */
		if(empty($data['quiz_question'])) {
			if($export==true) {
				set_message('Question not available');
				redirect(base_url().'quiz');
			}
			else {
				echo '<div class="alert alert-info alert-dismissible">
						<i class="icon fa fa-ban"></i> No question available for quiz preview.
					</div>';
			}
			die;
		}

		/* get quiz question option list */
		$filter_question_option['quiz_question_option.question_id']=$data['quiz_question'][0]['question_id'];
		$data['question_option']=$this->quiz_model->get_question_option($filter_question_option);

		/* get user list from user given quiz answers */
		$filter_quiz['quiz.quiz_master_id']=$quiz_master_id;
		$data['quiz']=$this->quiz_model->get_quiz($filter_quiz);

		$data['quiz']=array_column($data['quiz'], 'quiz_id');

		$data['quiz_ans']=array();
		$data['quiz_vote']=array();
		if(!empty($data['quiz'])) {
			/* get quiz answer */
			$filter_quiz_ans['SELECT']='count(*) as vote, count(option_id) as cnt,option_id';
			$filter_quiz_ans['WHERE_IN']=array('quiz_id'=>$data['quiz']);
			$filter_quiz_ans['GROUP_BY']='option_id';
			$data['quiz_ans']=$this->quiz_model->get_quiz_answer($filter_quiz_ans);
			$data['quiz_ans']=parent_child($data['quiz_ans'], 'option_id');

			$filter_quiz_vote['SELECT']='count(*) as vote, option_id';
			$filter_quiz_vote['WHERE_IN']=array('quiz_id'=>$data['quiz']);
			$data['quiz_vote']=$this->quiz_model->get_quiz_answer($filter_quiz_vote);
		}
		

		/* export question or option or answer count */
		/* get quiz question */
		$filter_quiz_question['quiz_master_id']=$quiz_master_id;
		$quiz_question=$this->quiz_model->get_quiz_question($filter_quiz_question);
		//dsm($quiz_question);
		$quiz_question_id=array_column($quiz_question, 'question_id');

		/* get quiz question option list */
		$filter_question_option_export['WHERE_IN']=array('quiz_question_option.question_id'=>$quiz_question_id);
		$question_option=$this->quiz_model->get_question_option($filter_question_option_export);

		$quiz_question=parent_child($quiz_question,'question_id');
		$data['quiz_export']=array();
		foreach ($quiz_question as $question_key => $question) {
			foreach ($question_option as $option_key => $option) {

				$data['quiz_export'][$option_key]['option_id']=$option['option_id'];
				$data['quiz_export'][$option_key]['question_title']=$quiz_question[$option['question_id']][0]['question_title'];
				$data['quiz_export'][$option_key]['option_title']=$option['option_title'];
				if(!empty($data['quiz_ans'][$option['option_id']][0]['cnt'])) {
					$data['quiz_export'][$option_key]['answer']=$data['quiz_ans'][$option['option_id']][0]['cnt'];
				}
				else{
					$data['quiz_export'][$option_key]['answer']='0';
				}
			}	
		}
		//dsm($data['quiz_export']);die;
		if($export=='export') {
			$options['firstRow']=array(
				'No.',
				'Question',
				'Option',
				'answer',
			);
			$options['columns']=array(
				'option_id',
				'question_title',
				'option_title',
				'answer',
			);
			$this->load->helper('download');
			if($data['question_available']==true) {
				$data['quiz_export']=array();
			}

			$export_file=url_title($data['quiz_master'][0]['quiz_title'],'-',true).'.csv';
			convertToCSV($data['quiz_export'],$options,$export_file);
		}
		$this->load->view('quiz/poll_result',$data);
	}

}