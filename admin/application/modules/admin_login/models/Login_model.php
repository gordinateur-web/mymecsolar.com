<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');
  
class Login_model extends CI_Model {
	
	public $login_table;
	/* Login table primary key */
	public $login_table_primary;

	/* user is enabled or disabled check column */
	public $login_enable_column;

	/* Maintain login history */
	public $login_log_table;

	/* Forget token column */
	public $login_forget_column;

	/* Remember me token column name */
	public $login_remember_column;

	/* Redirect after login successfully */
	public $login_redirect;

	/* Login table username column name for check */
	public $username_column;

	/* Login table password column name for check */
	public $password_column;

	/* Email column for sending reset password link */
	public $login_email_column;


	public function __construct() {
		parent::__construct();

		$this->login_redirect=base_url()."dashboard";
		$this->username_column='email';
		$this->password_column='password';
		$this->login_table='users';
		$this->login_table_primary='user_id';
		$this->login_log_table='login_history';
		$this->login_enable_column='enable';
		$this->login_forget_column='forgot_token';
		$this->login_email_column='email';
		$this->login_remember_column='remember_me_token';
		$this->config->load('custom_content');
	}
	function app_login($username,$password) {
		$username=htmlspecialchars($username,ENT_QUOTES);
		$this->db->where($this->username_column,$username);

		$user_filter=array();
		$user_filter[$this->username_column]=$username;
		$result_login=$this->get_active_users($user_filter);

		/*if username exists*/
		if(count($result_login) > 0) {
			$row_login=$result_login[0];
			if(strcmp($row_login['password'],md5($password))==0) {
				/* Maintain Login History */
				$this->login_history($row_login);

				/* Unset sensitive data */
				unset($row_login['password'],$row_login['forgot_token'],$row_login['temp_password']);

				/* Set Other than user table in session */
				$row_login = $this->attach_session($row_login);
				return $row_login;
			}
			else {
				return false;
			}
		} else {
			return false;
		}
	}

	/* Log the login in history table */
	public function login_history($row_login) {
		$table=$this->login_log_table;
		if(!empty($table)) {
			$this->load->library('user_agent');
			$agent = $this->agent->browser().' '.$this->agent->version();
			$login_history=array(
				$this->login_table_primary=>$row_login[$this->login_table_primary],
				'created_at'=>date('Y-m-d H:i:s'),
				'browser'=>$agent,
				'ip'=>$this->input->ip_address()
			);
			$this->db->insert('login_history',$login_history);
		}		
	}

	public function check_old_password($old_pass) {
		$uid=$this->session->userdata($this->login_table_primary);

		$result_user=$this->get_active_users(array($this->login_table_primary=>$uid));

		if(count($result_user) > 0) {
			$row_user=$result_user[0];
			if(strcmp($row_user[$this->password_column],$old_pass)==0) {
				return true;
			} else {
				return false;
			}
		} else {
			return false;
		}
	}

	public function change_password($old,$new,$confirm) {
		$user_id=$this->session->userdata('user_id');
		if($this->check_old_password(md5($old))) {
			$data['password']=md5($new);
			$data['temp_password']=$new;
			$data['updated_at']=date('Y-m-d H:i:s');
			$this->db->where('user_id',$user_id);
			$this->db->update('users',$data);
			return true;
		} 
		else {
			return false;
		}
	}

	public function get_users($filter=false) {
		if($filter) {
			apply_filter($filter);
		}
		$rs_login=$this->db->get($this->login_table);
		return $rs_login->result_array();
	}

	public function get_active_users($filter=false) {
		$filter[$this->login_enable_column]='1';
		return $this->get_users($filter);
	}

	/* forgot password */
	function forgot_token_update($user_data) {
		$this->load->model('email_model');
		$token=token(6);
		$token_data['forgot_token']=$token;
		$token_data['email']=$user_data[$this->login_email_column];
		$this->email_model->send_forgotpwd_token($token_data);	

		$update_data[$this->login_forget_column]=$token;
		$this->db->where($this->login_table_primary,$user_data[$this->login_table_primary]);
		return $this->db->update($this->login_table,$update_data);	
	}

	function verify_token($verification_token) {		
		$result_user=$this->get_active_users(array($this->login_forget_column=>$verification_token));

		if(isset($result_user[0])) {
		/*
			$user_data[$this->login_forget_column]=token(6);
			$this->db->where($this->login_table_primary,$result_user[0][$this->login_table_primary]);
			$this->db->update($this->login_table,$user_data);*/
			return true;

		} else {
			return false;
		}		
	}

	/* set session from cookie */
	public function check_remember() {
		$token=$this->input->cookie(APP_NAME.'_remember');
		if($token) {	
			$result_users=$this->get_active_users(array($this->login_remember_column=>$token));

			if(!empty($result_users)) {
				$result_users=$result_users[0];
				$this->set_remember_me($result_users[$this->login_table_primary]);
				
				/* Unset sensitive data */
				unset($result_users['password'],$result_users['forgot_token'],$result_users['temp_password']);

				/* Set Other than user table in session */
				$result_users = $this->attach_session($result_users);
				$this->session->set_userdata($result_users);
			}
		}
	}

	/*
		Used to set session after login
		Return data to set in session
	*/
	public function attach_session($login_data) {
		$this->load->model('user/user_model');

		/* Attach permission array to session */
		$permission = $this->user_model->get_role_permission(array('role.role_id'=>$login_data['role_id']));
		if(!empty($permission)) {
			/* Define blank array */
			$login_data['permission_id']=$login_data['permission_title']=array();
			/* Attach permission list id and title in session */
			foreach ($permission as $permission_row) {
				$login_data['permission_id'][]=$permission_row['permission_id'];
				$login_data['permission_title'][]=$permission_row['permission_title'];
			}
		}
		
		/* user_info table attach */
		/*$this->db->where('user_id',$login_data['user_id']);
		$rs=$this->db->get('user_info');
		$result=$rs->result_array();
		if(!empty($result)) {
			$login_data=array_merge($result[0], $login_data);
		}*/

		/* Attach content type permission  */
		$login_data['content_type_permission']=array();
		$this->load->model('custom_content/content_type_model');
		$content_permission=$this->content_type_model->get_content_type_permission(array(
			'role_id'=>$login_data['role_id'],
			'SELECT'=>'content_type_id, permission_id'
		));


		/*
			Arrange array in below format
			array(
				'1'=>array(1,3,4,5),
				'2'=>array(1,3,4,5),
			)
			main array key is content_type_id
		*/
		if(!empty($content_permission)) {
			foreach ($content_permission as $permission_row) {
				$login_data['content_type_permission'][$permission_row['content_type_id']][]=$permission_row['permission_id'];
			}
		}
		
		return $login_data;
	}

	public function set_remember_me($user_id=false) {
		$this->load->helper('cookie');
		/* One week */
		$expire=644800;
		$remember_token=token(20);
		$cookie = array(
		    'name'   => APP_NAME.'_remember',
		    'value'  => $remember_token,
		    'expire' => $expire,
		    'path'   => '/'
		);

		$expiry_date=date('Y-m-d H:i:s',time()+$expire);

		$this->input->set_cookie($cookie);

		/* Update user table with token */
		$this->db->where('user_id',$user_id);
		$this->db->update('users',array('remember_me_token'=>$remember_token, 'remember_me_expire'=>$expiry_date));
	}	
}