<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Login extends CI_Controller {

	public function __construct() {
		parent::__construct();
		$this->load->model('login_model');
	}
	
	public function index($sso_login=false) {
		$this->load->model('custom_content/content_model');
		$user_id=$this->session->userdata($this->login_model->login_table_primary);
		
		if(!empty($user_id)) {
			redirect($this->login_model->login_redirect);	
		}
		$data['title']="Sign me in";
		$this->load->helper('form');

		/* login page backgound image change from content management */

		/* get login background image fields name */
		$data['login_image_fields']=$this->config->item('login_image_fields');

		/* get login background image */
		$login_image['content.content_type_id']=$data['login_image_fields']['content_type_id'];
		$login_image['content.content_published']=1;
		$login_image['WHERE']="(content.content_status is null or content.content_status='Approved')";
		$data['login_image']=$this->content_model->get_content_with_value($login_image);
		//dsm($data['login_image']);die;

		$this->load->view('admin_login/login',$data);
	}

	public function check() { 
		$this->load->library('form_validation');
		$this->form_validation->set_rules($this->login_model->username_column, 'Username', 'required');
		$this->form_validation->set_rules($this->login_model->password_column, 'Password', 'required');

		if ($this->form_validation->run() == false) {
			set_message(validation_errors());
			redirect_back();
		}

		$username=$this->input->post($this->login_model->username_column);
		$password=$this->input->post($this->login_model->password_column);
		$remember_me=$this->input->post('remember_me');
		$sso_login=$this->input->post('sso_login');

		$this->load->model('login_model');
		$login_data=$this->login_model->app_login($username, $password);
		/* If login success */
		if($login_data){
			$this->session->set_userdata($login_data);
			$back_to=$this->input->post('back_to');
			/* set cookies code*/
			if(!empty($remember_me)) {
				$this->login_model->set_remember_me($login_data['user_id']);
			}
			
			/* redirect user back to sampark */
			if(!empty($sso_login)) {
				$sso_data['post_data']=array(
					'employee_id'=>encrypt($login_data['employee_id'], SSO_ENCRYPTION_KEY)
				);
				$sso_data['sso_url']=SSO_REDIRECT_URL;
				return $this->load->view('sso_redirect',$sso_data);
			}

			/* Redirect user to back page */
			if(!empty($back_to)) {
				redirect(base_url().$back_to);
			}
			/* Redirect to defined page */
			else {
				redirect($this->login_model->login_redirect);
			}
		}
		else {
			set_message("Username or password is wrong","error");
			redirect_back();
		}
	}
	
	public function logout() {
		$this->session->sess_destroy();
		$this->load->helper('cookie');
		delete_cookie(APP_NAME.'_remember');
		redirect(base_url());
	}

	public function change_password() {
		if(!$this->session->userdata($this->login_model->login_table_primary)) {
			redirect(base_url());
			die;
		}
		$this->load->helper('form');
		$this->load->view('admin_login/change_password');
	}

	public function save_change_password() {
		if(!$this->session->userdata($this->login_model->login_table_primary)) {
			redirect(base_url());
			die;
		}

		$this->load->library('form_validation');

		$this->form_validation->set_rules('password', 'Old Password', 'required');
		$this->form_validation->set_rules('new_passsword', 'New Password', 'required');
		$this->form_validation->set_rules('confirm_passsword', 'Confirm Password', 'required|matches[new_passsword]|callback_password_strength_check');

		if ($this->form_validation->run() == false) {
			set_message(validation_errors());
			redirect_back();
		}

		$passsword=$this->input->post('password');
		$new_passsword=$this->input->post('new_passsword');
		$confirm_passsword=$this->input->post('confirm_passsword');

		$this->load->model('login_model');
		$rs=$this->login_model->change_password($passsword,$new_passsword,$confirm_passsword);	
		//var_dump($rs);die;
		if($rs) {
			set_message("Password Change Successfully","success");
			redirect_back();
			die;
		}
		else {
			set_message("Old Password Is Wrong");
			redirect_back();
			die;
		}
	}

	public function password_strength_check($str) {
	    //dsm(func_get_args());die;
	    $password=$str;
	    $strength = 1;

	    if (strlen($password) < 7) {
	      $this->form_validation->set_message('password_strength_check', 'Password must contain minimum 7 character with at least 1 number character.');
	      return false;
	    }
	    /*if it has numbers and characters, increase strength value*/
	    /*if (1 === preg_match("/([0-9])/",$password)) {
	      $strength += 1;
	    }*/
	    
	    //if($strength > 1) {
	      return true;
	    /*}
	    else {
	    	$this->form_validation->set_message('password_strength_check', 'Password must contain minimum 7 character with at least 1 number character.');
	    	return false;	
    	}*/	    
	}

	/* forgot password */
	public function forgot_password() {
		/*dsm($this->input->post()); die;*/
		$email=$this->input->post('email');

		$result_users=$this->login_model->get_active_users(array($this->login_model->username_column=>$email));

		if(isset($result_users[0])) {
			$res=$this->login_model->forgot_token_update($result_users[0]);	
			if($res) {
				set_message('To reset your account password please check your mail box for received reset password link','success');
				redirect(base_url());				
			}		
			else {
				set_message('Something went wrong');
				redirect(base_url().'login');
			}			
		}
		else {
			set_message('User does not exist');
			redirect(base_url().'admin_login/login');
		}
	}

	public function verify_password_token($forgot_token) {

		/* $forgot_token is required */
		if(strlen($forgot_token) < 2) {
			show_404();
			die;
		}

		$res=$this->login_model->verify_token($forgot_token);
		if ($res) {
			set_message('Thank you for verification of your profile, Now you can reset your password in to application','success');
			$data['pageTitle']="Reset Password";
			$data['forgot_token']=$forgot_token;
			$this->load->view('admin_login/reset_password',$data);
		} else {
			set_message('link is expired, Please try again');
			redirect(base_url().'admin_login/login');
		}	
	}

	public function save_reset_password() {
		/*dsm($this->input->post()); die;*/
		$this->load->library('form_validation');
		$this->form_validation->set_error_delimiters('<div class="error">', '</div>');
		$this->form_validation->set_rules('password', 'Password', 'required');
		$this->form_validation->set_rules('cfpassword', 'Confirm Password', 'required|matches[password]|callback_password_strength_check');	

		if ($this->form_validation->run() == FALSE) {
			set_message(validation_errors());
			redirect_back();
			return 0;
		}

		$password=$this->input->post('password');
		$cfpassword=$this->input->post('cfpassword');
		$forgot_token=$this->input->post('forgot_token');

		/* Get user */
		$result_user=$this->login_model->get_active_users(array($this->login_model->login_forget_column=>$forgot_token));
		if(isset($result_user[0])) {
			$user_data=array(
				$this->login_model->password_column=>md5($password),
				'temp_password'=>$password,
				$this->login_model->login_forget_column=>'',
			);

			$this->db->where($this->login_model->login_table_primary,$result_user[0][$this->login_model->login_table_primary]);
			$this->db->update($this->login_table,$user_data);

			set_message('Password reset successfully','success');
			redirect(base_url());
		}
		else {
			set_message("Link expired, please try again");
		}
	}
}