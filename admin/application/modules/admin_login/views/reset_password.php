<!DOCTYPE html>
<html>
	<head>
		<meta charset="UTF-8">
		<title><?php echo APP_NAME; ?> | Sign in</title>
		<meta content='width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no' name='viewport'>
		<!-- Font Awesome -->
		<link rel="stylesheet" href="<?php echo base_url().'public/plugins/font-awesome-4.5.0/css/font-awesome.min.css'; ?>">
		<!-- Theme style -->
		<link rel="stylesheet" href="<?php echo base_url().'public/bootstrap/css/bootstrap.min.css'; ?>">
		<link rel="stylesheet" href="<?php echo base_url().'public/dist/css/Admin.min.css'; ?>">
		<style type="text/css">
			.gordinateur {
				color: #349ba8;
			}
		</style>		
	</head>		
	</head>
	<body class="login-page">
		<div class="login-box">
			<div class="login-logo">
				Reset Password
			</div><!-- /.login-logo -->
			<div class="login-box-body">
				<div class="row">
					<div id="error_div"></div>
					<?php $this->load->view("show_msg"); ?>
				</div>
				<form name="resetpwdform" action="<?php echo base_url().'login/save_reset_password'; ?>" method="post">
					<input type='hidden' name='forgot_token' value="<?php echo $forgot_token; ?>">
					<div class="form-group has-feedback">
						<input type="password" name="password" class="form-control" placeholder="New Password"/>
						<span class="glyphicon glyphicon-lock form-control-feedback"></span>
					</div>
					<div class="form-group has-feedback">
						<input type="password" name="cfpassword" class="form-control" placeholder="Confirm Password"/>
						<span class="glyphicon glyphicon-lock form-control-feedback"></span>
					</div>
					<div class="row">
						<div class="col-xs-6"></div><!-- /.col -->
						<div class="col-xs-6">
							<button type="submit" class="btn btn-primary btn-block btn-flat">Reset Password</button>
						</div><!-- /.col -->
					</div>
				</form>
			</div><!-- /.login-box-body -->
			<p id="g-branding" class="login-box-msg">Design By <a class="gordinateur" href="http://gordinateur.com/" target="_blank">G-Ordinateur Pvt. Ltd.</a></p>
			
		</div><!-- /.login-box -->

		<!-- jQuery 2.1.4 -->
	    <script src="<?php echo base_url().'public/plugins/jquery/jquery-3.1.0.min.js'; ?>"></script>
	    <script src="<?php echo base_url().'public/bootstrap/js/bootstrap.min.js'; ?>"></script>
	    <script src="<?php echo base_url().'public/js/validate.cus.js'; ?>" type="text/javascript"></script>
		<script type="text/javascript">
			var validator = new FormValidator('resetpwdform', [{
			    name: 'password',
			    display: 'New Password',
			    rules: 'required|callback_password_check'
			},
			{
			    name: 'cfpassword',
			    display: 'Confirm Password',
			    rules: 'required|matches[password]'
			}], 
			function(errors, event) {
				var err_html='<div class="alert alert-danger alert-dismissable"><button aria-hidden="true" class="close" type="button" data-dismiss="alert">x</button><ul id="error_list">%s</ul></div>';
				if (errors.length > 0) {
					var errorString = '';
					errorLength = errors.length;
					for (var i = 0; i < errorLength; i++) {
						errorString += '<li>' + errors[i].message + '</li>';
					}
					errorString=err_html.replace('%s',errorString);
					$('#error_div').html(errorString);
				}
			});	

			validator.registerCallback('password_check', function(value) {
				var password=value;
				var strength = 1;
				if(value!='') { 
					var cfpassword=$('input[name="cfpassword"]').val();	
					if(password!=cfpassword) {
						return false;
					}
					if (password.length < 7) {
						return false;
					}
					/* if it has numbers and characters, increase strength value */
					if (password.match(/([0-9])/)) {
						strength += 1;
					}

					if(strength > 1) {
						return true;
					}
					return false;
				}
				else {
					return true;
				}	
			})
			.setMessage('password_check', 'Please password must contain at least 1 number and should be minimum 7 character.');						
		</script>    
	</body>
</html>	