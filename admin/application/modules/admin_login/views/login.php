<!DOCTYPE html>
<html>
	<head>
		<meta charset="UTF-8">
		<title><?php echo APP_SHORT_NAME; ?> | Sign in</title>
		<meta content='width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no' name='viewport'>
		<!-- Font Awesome -->
		<link rel="stylesheet" href="<?php echo base_url().'public/plugins/font-awesome-4.7.0/css/font-awesome.min.css'; ?>">
		<!-- Theme style -->
		<link rel="stylesheet" href="<?php echo base_url().'public/bootstrap/css/bootstrap.min.css'; ?>">
		<link rel="stylesheet" href="<?php echo base_url().'public/AdminLte/css/Admin.min.css'; ?>">
		<style type="text/css">
			.gordinateur {
				color: #349ba8;
			}
		</style>		
	</head>
	<body class="login-page">
		<div class="login-box">
			<div class="login-logo">
				<a href="<?php echo(base_url()); ?>"><b><?php echo APP_SHORT_NAME; ?></b> Login</a>
			</div><!-- /.login-logo -->
			<div class="login-box-body">
				<p class="login-box-msg">Sign in to start your session</p>
				<div class="row">
					<div id="error_div"></div>
					<?php $this->load->view("admin_layout/show_msg"); ?>
				</div>
				<?php 
					$this->load->helper('form');
					echo form_open(base_url().'admin_login/login/check',array(
						'method'=>'post',
						'name'=>'loginform',
						'id'=>'login_form'
					)); 
				?>
					<input type="hidden" name="back_to" value="<?php echo $this->input->get('back_to'); ?>">
					<div class="form-group has-feedback">
						<input type="text" name="email" title="Username" class="form-control" placeholder="Email" data-validation="required" />
						<span class="glyphicon glyphicon-envelope form-control-feedback"></span>
					</div>
					<div class="form-group has-feedback">
						<input type="password" name="password" class="form-control" placeholder="Password" data-validation="required" />
						<span class="glyphicon glyphicon-lock form-control-feedback"></span>
					</div>
					<div class="row">
						<div class="col-xs-8"><label><input type="checkbox" name="remember_me" value="1" /> Remember Me</lable></div><!-- /.col -->
						<div class="col-xs-4">
							<button type="submit" class="btn btn-primary btn-block btn-flat">Sign In</button>
						</div><!-- /.col -->
					</div>
				</form>
				<br/><a href="javascript:void(0)" onclick="$(forgotpwd).modal('show');" class="pull-right">I forgot my password</a><br/>
			</div><!-- /.login-box-body -->
			<p id="g-branding" class="login-box-msg">Design By <a class="gordinateur" href="http://gordinateur.com/" target="_blank">G-Ordinateur Pvt. Ltd.</a></p>
		</div><!-- /.login-box -->

		<!-- Modal -->
		<div class="modal fade bs-example-modal-sm" id="forgotpwd" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" data-backdrop="static">
		    <div class="modal-dialog modal-sm">
		        <div class="modal-content">
		            <div class="modal-header">
		                <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
		                <h4 class="modal-title" id="myModalLabel">Forgot Password</h4>
		            </div>
		            <form name="forgot_password" novalidate action="<?php echo base_url().'admin_login/login/forgot_password'; ?>" id="forgot_password" method="post">
		               <div class="modal-body">
							<div class="row">
								<div id="show_error"></div>
							</div>
		                   <div class="form-group">
		                       <label>Email <span class="text-danger">*</span></label>
		                       <input type="email" class="form-control" id="email" name="email" placeholder="Enter registered email" data-validation="required"/>
		                   </div>
		               </div>
		               <div class="modal-footer">
		                   <input type="submit" name="submit"  class="btn btn-primary" value="Submit">
		               </div>                      
		            </form>
		        </div>
		    </div>
		</div>

		<!-- jQuery 2.1.4 -->
	    <script src="<?php echo base_url().'public/plugins/jquery/jquery-3.1.1.min.js'; ?>"></script>
	    <script src="<?php echo base_url().'public/bootstrap/js/bootstrap.min.js'; ?>"></script>
	    <script src="<?php echo base_url().'public/plugins/jquery-validation/jquery.form-validator.min.js'; ?>" type="text/javascript"></script>
		<script type="text/javascript">
			$(document).ready(function() {
                jQuery.validate({
                    form : '#login_form',
                });

                jQuery.validate({
                    form : '#forgot_password',
                });
            });			
		</script>    
	</body>
</html>	