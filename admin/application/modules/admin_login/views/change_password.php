<?php 
echo form_open('login/save_change_password', array('name' => 'passwordform')); 
?>
	<div class="modal-body">
	    <div class="form-group">
	        <label>Old Password</label>
	        <input type="password" class="form-control" id="opasssword" name="password" placeholder="Enter Old Passsword "/>
	    </div>
	    <div class="form-group">
	        <label>New Password</label>
	        <input type="password" class="form-control" id="npasssword" name="new_passsword" placeholder="Enter New Passsword "/>
	    </div>
	    <div class="form-group">
	        <label>New Confirm Password</label>
	        <input type="password" class="form-control" id="ncpasssword" name="confirm_passsword" placeholder="Enter New Confirm Passsword "/>
	    </div>
	     <div class="form-group">
	        <input type="submit" name="submit"  class="btn btn-flat btn-primary" value="Submit">
	    </div>
	</div>                      
<?php echo form_close(); ?>