<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');
 
 
class Content_model extends CI_Model
{

	/*
		Get content table data with joining with content type table
	*/
	function get_content($filter=false) {
		if($filter) {
			apply_filter($filter);
		}

		if(empty($filter['SELECT'])) {
			$this->db->select('content_type.*,  users.*, content.*');
		}
		else {

		}

		/* Ignore deleted content types and content */
		$this->db->where('content.deleted_at is null',null,false);
		
		/* Don't return version content */
		$this->db->where('content.parent_content_id','0');
		$this->db->where('content_type.deleted_at is null',null,false);
		$this->db->join('content_type','content_type.content_type_id=content.content_type_id');
		$this->db->join('users','content.created_by=users.user_id','left');
		$this->db->order_by('content.stick_ontop','desc');
		$this->db->order_by('content.rank_order','desc');
		$this->db->order_by('content.content_id','desc');
		$rs=$this->db->get('content');
		return $rs->result_array();
	}

	public function get_published_content($filter = false) {
		if(!$filter) {
			$filter=array();
		}

		$filter['content.content_published']=1;
	    $filter['content.deleted_at']=NULL;
	    $filter['WHERE'][]="(content.content_status is null or content.content_status='Approved')";
	    return $this->get_content_with_value($filter);
	}

	public function get_published_value_content($filter = false) {
		if(!$filter) {
			$filter=array();
		}

		$filter['content.content_published']=1;
	    $filter['content.deleted_at']=NULL;
	    $filter['WHERE'][]="(content.content_status is null or content.content_status='Approved')";
	    return $this->get_value_based_content($filter);
	}


	/*
		Add content query
	*/
	public function add_content($content_data) {
		$content_data['created_by']=$this->session->userdata($this->config->item('user_id_insession'));
		$content_data['created_at']=date('Y-m-d H:i:s');
		
		$this->db->insert('content',$content_data);
		return $this->db->insert_id();
	}

	/*
		$content_type_id(Int) - Content type table id
		$form_hidden_data(array) - list of hidden value pass to form like 
			redirect_to - Used to redirect user after success 
			Ex.array('redirect_to'=>'front/forum/list');

	*/
	public function add_content_form($content_type_id, $form_hidden_data=false) {
		
		/* Get content type data */
		$filter['content_type.content_type_id']=$content_type_id;
		$data['content_type']=$this->content_type_model->get_content_type($filter);

		if(empty($data['content_type'])) {
			return 'invalid content type selected';
		}
		$data['content_type']=$data['content_type'][0];
		$data['form_hidden_data']=$form_hidden_data;

		/* Check content type permission for add new content */
		if($data['content_type']['content_type_category']=='content' || $data['content_type']['content_type_category']=='section_content') {
			$this->content_type_model->check_content_permission($content_type_id,array(1));
		}
		/* Check login for webform if only logged in */
		elseif($data['content_type']['content_type_category']=='webform' && $data['content_type']['loggedin_only']=='1') {
			login_check();
		}

		/* Supply main menu if content type is content */
		//if($data['content_type']['content_type_category']=='content') {
			$this->load->model('menu/menu_model');
			/* Get mail menu id */
			$main_navigation = $this->menu_model->get_menu_master(array(
				'main_navigation'=>'1'
			));


			/* get list of menu links */
			$data['menu_list']=array();
			if(!empty($main_navigation)) {
				$data['menu_list']=$this->menu_model->get_menu_tree($main_navigation[0]['menu_master_id']);
				$data['menu_list']=$this->menu_model->get_menu_link_list($data['menu_list']);
				array_unshift($data['menu_list'], array('menu_link_id'=>0,'link_title'=>'No parent'));
			}
		//}

		/* Set title and get fields list */
		$data['content_type_id']=$content_type_id;
		$data['field_list']=$this->content_type_model->get_content_type_fields(array(
			'content_type_field.content_type_id'=>$content_type_id
		));

		$html_form=$this->load->view("custom_content/content_add",$data, true);
		return $html_form;
	}

	public function edit_content_form($content_id, $form_hidden_data=false) {
		$data['content_id']=$content_id;
		$user_id=$this->session->userdata($this->config->item('user_id_insession'));
		$data['edit_content']=$this->content_model->get_content(array('content.content_id' => $content_id));

		/* If edit content id is not found */
		if(empty($data['edit_content'])) {
			return 'Invalid content selected';
		}

		$data['edit_content']=$data['edit_content'][0];

		$data['form_hidden_data']=$form_hidden_data;

		$permission_to_check=array(2);
		/* Check if own content or not, if not check permission to edit any content */
		if($data['edit_content']['created_by'] != $user_id){
			$permission_to_check[]=3;
		}

		/* Check content type permission */
		$allowed = $this->content_type_model->check_content_permission($data['edit_content']['content_type_id'], $permission_to_check, 0, 0);
		if(!$allowed) {
			return '<div class="alert alert-danger alert-dismissable">
					<button aria-hidden="true" data-dismiss="alert" class="close" type="button">x</button>
						You don\'t have sufficient permission to access this resources
				</div>';
		}

		/* Get content type data */
		$filter['content_type_id']=$data['edit_content']['content_type_id'];
		$content_type_info=$this->content_type_model->get_content_type($filter);
		$data['content_type']=$content_type_info[0];
		

		$node = array();
		$data['title']="Edit <b>".$data['content_type']['content_type_title'].'</b>';
		$data['content_type_id']=$data['edit_content']['content_type_id'];

		/* Supply main menu if content type is content */
		//if($data['content_type']['content_type_category']=='content') {
			$this->load->model('menu/menu_model');
			/* Get mail menu id */
			$main_navigation = $this->menu_model->get_menu_master(array(
				'main_navigation'=>'1'
			));


			/* get list of menu links */
			$data['menu_list']=array();
			if(!empty($main_navigation)) {
				$data['menu_list']=$this->menu_model->get_menu_tree($main_navigation[0]['menu_master_id']);
				$data['menu_list']=$this->menu_model->get_menu_link_list($data['menu_list']);

				/* Add no parent item in array */
				array_unshift($data['menu_list'], array('menu_link_id'=>0,'link_title'=>'No parent'));
			}
		//}
		
		/* Get content type field list */
		$data['field_list']=$this->content_type_model->get_content_type_fields(array(
			'content_type_field.content_type_id'=>$data['edit_content']['content_type_id']
		));

		/* Get values of fields */
		$data['field_data']=$this->content_model->get_content_value(array(
			'content_id' => $content_id
		));
	
		/* Arrange values by field id */
		$field_wise_value=parent_child_array($data['field_data'],'content_type_field_id');
		$machine_names=array_column($data['field_list'],'machine_name','content_type_field_id');
		$data['field_title']=array_column($data['field_list'],'field_title','content_type_field_id');

		$edit_array=$data['edit_content'];
		/*$edit_array=array(
			'content_id'=>$data['edit_content']['content_id'],
			'content_title'=>$data['edit_content']['content_title'],
			'content_published'=>$data['edit_content']['content_published'],
			'stick_ontop'=>$data['edit_content']['stick_ontop'],
		);*/

		foreach ($field_wise_value as $filed_value_row) {
			foreach ($filed_value_row as $field_values) {
				if(isset($machine_names[$field_values['content_type_field_id']])) {
					$machine_name=$machine_names[$field_values['content_type_field_id']];
					$edit_array[$machine_name][$field_values['content_value_id']]=$field_values['content_value'];
				}	
			}
		}

		/* Pass edit data to view */
		$data['edit_array']=$edit_array;


		/* Get section list */
		$data['section_list']=$this->content_type_model->get_content_type_section(array(
			'content_type_id'=>$data['edit_content']['content_type_id']
		));

		/* Get content of all section */
		$data['section_content_list']=array();
		if(!empty($data['section_list'])) {
			$section_content_list=$this->content_model->get_content(array(
				'WHERE_IN'=>array('content_type_section_id'=>array_column($data['section_list'], 'content_type_section_id')),
				'section_content_parent'=>$content_id
			));

			
			$data['section_content_list']=parent_child_array($section_content_list, 'content_type_section_id');
		}



		/* Get uploaded files and images to display */
		$result_files=$this->content_model->get_content_value(array(
			'content_id' => $content_id,
			'media_path !='=>null,
		));

		$data['uploaded_file']=parent_child_array($result_files,'content_type_field_id');
		
		$html_form=$this->load->view("custom_content/content_add",$data, true);
		return $html_form;
	}


	/*
		Update content query
	*/
	function update_content($data, $filter=false) {
		if($filter) {
			apply_filter($filter);
		}
		$data['updated_at']=date('Y-m-d H:i:s');
		$data['updated_by']=$this->session->userdata($this->config->item('user_id_insession'));
		return $this->db->update('content',$data);
	}


	/*
		Plain get content value table without any join
	*/
	function get_content_value($filter=false) {
		if($filter) {
			apply_filter($filter);
		}
		$this->db->order_by('content_value_rank','asc');
		$rs=$this->db->get('content_value');
		return $rs->result_array();
	}

	/*
		Return content with join content_value table
	*/
	public function get_content_and_value($filter=false) {
		if($filter) {
			apply_filter($filter);
		}
		$this->db->order_by('content.rank_order','asc');
		$this->db->order_by('content_value.content_value_rank','asc');
		$this->db->join('content_value','content_value.content_id=content.content_id');
		$this->db->where('content.deleted_at is null',null,false);
		$rs=$this->db->get('content');
		return $rs->result_array();	
	}

	/*
		Plain get content value with join with content fields table
	*/
	function get_value_with_fields($filter=false) {
		if($filter) {
			apply_filter($filter);
		}

		if(empty($filter['SELECT'])) {
			$this->db->select("content_type_field.*, content_value.*");
		}

		$this->db->join('content_type_field', 'content_type_field.content_type_field_id = content_value.content_type_field_id');
		$this->db->order_by('content_value_rank','asc');
		$rs=$this->db->get('content_value');
		return $rs->result_array();
	}

	/*
		Return content with its value of each field

		Use when
			1) render individual content, 
			2) individual content type listing with limit
			3) keyword search, Add filter for content_meta column

		$filter use only for content table
	*/
	function get_content_with_value($filter=false) {
		$content_list = $this->get_content($filter);

		/* If content list is empty */
		if(empty($content_list)) {
			return array();
		}
		//dsm($content_list);die;
		$content_ids=array_column($content_list, 'content_id');
		//dsm($content_ids);die;
		/* Get value table data */
		$content_values = $this->get_content_value(array(
			'WHERE_IN'=>array("content_id"=>$content_ids)
		));
		//dsm($content_values);die;
		return $this->arrange_content_value($content_list, $content_values);
	}

	/*
		Use when you want to add condition bases on content field
		$filter - Add filter for content_type, content and content_value table
	*/
	public function get_value_based_content($filter) {
		if(empty($filter)) {
			show_error("get_value_based_content required filter",500);die;
		}
		else {
			apply_filter($filter);
		}
		if(empty($filter['SELECT'])) {
			$this->db->select("content_type.*, content_value.*, users.*, content.*");
		}

		/* Ignore deleted content types and content */
		$this->db->where('content.deleted_at is null',null,false);
		$this->db->where('content_type.deleted_at is null',null,false);
		$this->db->join('content', 'content_value.content_id = content.content_id');
		$this->db->join('users','content.created_by=users.user_id','left');
		$this->db->join('content_type', 'content_type.content_type_id = content.content_type_id');
		$this->db->group_by('content.content_id');
		$this->db->order_by('content.rank_order','desc');
  		$this->db->order_by('content.stick_ontop','desc');
		$rs=$this->db->get('content_value');
		$content_list = $rs->result_array();
		

		if(empty($content_list)) {
			return array();
		}

		$content_ids=array_column($content_list, 'content_id','content_id');
		
		/* Get value table data */
		$content_values = $this->get_content_value(array(
			'WHERE_IN'=>array("content_id"=>$content_ids)
		));
		
		return $this->arrange_content_value($content_list, $content_values);
	}

	/*
		Return content wise content value data according to machine name of field

		$content_list - Result from content table		
		$content_values - Result from content_value table
	*/
	public function arrange_content_value($content_list, $content_values) {
		/* If content list is empty */
		if(empty($content_list)) {
			return array();
		}

		$content_values=parent_child_array($content_values, 'content_id');

		$return_array=array();
		$i=0;
		foreach ($content_list as $content_row) {
			$return_array[$i]['content']=$content_row;
			if(empty($content_values[$content_row['content_id']])) {
				continue;
			}

			$j=0;
			/* Add each row value in return array based on machine name */
			foreach ($content_values[$content_row['content_id']] as $value_row) {
				$j++;
				/* Define j index value */
				if(!isset($return_array[$i][$value_row['field_machine_name']])) {
					$j=0;
				}
				$return_array[$i][$value_row['field_machine_name']][$j]['content_type_field_id']=$value_row['content_type_field_id'];
				$return_array[$i][$value_row['field_machine_name']][$j]['content_value_id']=$value_row['content_value_id'];
				$return_array[$i][$value_row['field_machine_name']][$j]['content_value']=$value_row['content_value'];
				$return_array[$i][$value_row['field_machine_name']][$j]['content_value_rank']=$value_row['content_value_rank'];
				$return_array[$i][$value_row['field_machine_name']][$j]['content_reference_id']=$value_row['content_reference_id'];
				$return_array[$i][$value_row['field_machine_name']][$j]['content_reference_value']=$value_row['content_reference_value'];
				$return_array[$i][$value_row['field_machine_name']][$j]['media_path']=$value_row['media_path'];
				$return_array[$i][$value_row['field_machine_name']][$j]['media_type']=$value_row['media_type'];
				$return_array[$i][$value_row['field_machine_name']][$j]['media_size']=$value_row['media_size'];
				$return_array[$i][$value_row['field_machine_name']][$j]['media_title']=$value_row['media_title'];
			}
			$i++;
		}
		return $return_array;
	}

	/* 
		Provide filter list based on 
	*/
	public function content_keyword_search($keyword, $filter=false) {
		if($filter) {
			apply_filter($filter);
		}
		$this->db->select('content.content_id, content.content_type_id, content.content_title, content.url_title, content.content_meta, content_parent.content_id as parent_content_id, content_parent.content_type_id as parent_content_type_id, content_parent.content_title as parent_content_title, content_parent.url_title as parent_url_title, content_parent.content_meta as parent_content_meta');
		$this->db->join('content as content_parent','content.section_content_parent=content_parent.content_id','left');
		$this->db->join('content_type','content_type.content_type_id=content.content_type_id');
		$this->db->where('content_type.content_type_category','content');
		//$this->db->where('content.content_meta', $keyword);
		$this->db->order_by('content.content_id','desc');
		$rs=$this->db->get('content');
		return $rs->result_array();
	}

	/*
		Update content_value table, Using now insave_content_data
	*/
	function update_content_value($data, $filter=false) {
		if($filter) {
			apply_filter($filter);
		}
		return $this->db->update('content_value',$data);
	}

	/*
		Change order of files from content/edit_content_data
	*/
	public function save_file_order($content_id) {
		$file_order=$this->input->post('file_order');
		
		/* If not available */
		if(empty($file_order)) {
			return false;
		}

		/* Loop to each content field */
		foreach ($file_order as $content_type_fields => $files) {
			$files=explode(',', $files);
			foreach ($files as $key => $value) {
				$this->db->where('content_id',$content_id);
				$this->db->where('content_type_field_id',$content_type_fields);
				$this->db->where('content_value_id',$value);
				$this->db->update('content_value',array('content_value_rank'=>$key+1));
			}
		}
		return true;
	}


	/*
		$validation_array = Array of rules for file validation
		Expected array format for rules 
		array(
		    'field'=>$field['machine_name'],
		    'label'=>$field['field_title'],
		    'rules'=>$validate_rule
		)
		$file = $_file pass as it is to this function
	*/

	public function file_validation($validation_array, $file, $is_edit=false) {
		$validation_result = array('status' => TRUE, 'message' => '', );
		$error_message='';
		$file_required_flag=false;
		foreach ($validation_array as $key) {
			$rules=explode('|', $key['rules']); /* separation the rules by pipe */
			foreach ($rules as $row_rule) {
				/* searching for "required" in rules*/
				if(strpos($row_rule, 'required') !== FALSE && empty($is_edit)) { 
					foreach ($file[$key['field']]['name'] as $field_index => $name) {            
						if($file_required_flag == true) {
							continue;
						}
						/* if name is empty giving error message */
						if (isset($name) && empty($name)) { 
							$error_message.= "<li>".$key['label']." index ".($field_index+1)." is required</li>";
						}
						else {
							$file_required_flag=true;
						}
					}
				}
				/* searching for "file_type" in rules */
				elseif(strpos($row_rule, 'file_type') !== FALSE) {
					/* picking EXTENSIONs from rule */
					$exts=substr($row_rule, (strpos($row_rule, '[')+1),-1); 
					/* EXTENSIONs string to array */
					$valid_file_format=array_map('trim',explode(',', $exts));

					foreach ($file[$key['field']]['name'] as $field_index => $name) {
						if (!empty($name)) {
							$ext_file=trim(strtolower(pathinfo($name,PATHINFO_EXTENSION))); /* picking EXTENSION from file name */
							if(!in_array($ext_file,$valid_file_format)) { /* checking EXTENSION is present in VALID_FILE_FORMAT array or not */
								$error_message.= "<li>".$name.' is not valid for '.$key['label'].", Only ".implode(', ', $valid_file_format)." Are allowed";
							}
						}
					}
				} 
				/* searching for "file_size" in rules*/
				elseif(strpos($row_rule, 'file_size') !== FALSE) { 
					$size=substr($row_rule, (strpos($row_rule, '[')+1),-1);
					preg_match("/(\d+)(.)/", $size, $matches);

					$number = $matches[1];
					$character = $matches[2];

		            $character=strtoupper($character); // G

		            $valid_file_size=$number; 
		            /* picking file size condition */
		            if ($character == 'M') {
		            	/* converting file size into BYTE */
		            	$valid_file_size=$valid_file_size*1024*1024; 
		            } 
		            elseif ($character == 'K') {
		            	/* converting file size into BYTE */
		            	$valid_file_size=$valid_file_size*1024; 
		            } 
		            else {
		            	$valid_file_size=0;
		            }

		            foreach ($file[$key['field']]['size'] as $field_index => $size) {
		            	/* checking file size with valid size as per cndition */
		            	if( $size > $valid_file_size ) { 
		            		$error_message.= "<li>".$file[$key['field']]['name'][$field_index]." could not be larger than ".$number.$character;
		            	}
		            }
		        }
	    	}
		}

		if (!empty($error_message)) {
			$validation_result['status'] = FALSE;
			$validation_result['message'] = $error_message;
		}
		return $validation_result;
	}


	/*
		Upload files while saving content form
		$upload_path - Folder path to upload
		$file - $_FILE['images_file'] format array
		$create_thumbs - Creating thumb or not.
	*/
	function file_upload($upload_path, $file, $create_thumbs=true) {
	    $path_array = array();
	    $valid_img_ext=array('jpg','jpeg','png');
	    $j=0;
	    foreach ($file['name'] as $row_index => $file_name) {
	        /* Ignore empty files */
	        if (empty($file_name)) {
	        	continue;
	        }

            $new_file_name=clean_name($file_name);
            $ext_file=trim(strtolower(pathinfo($new_file_name,PATHINFO_EXTENSION)));
            $basename=trim(strtolower(pathinfo($new_file_name,PATHINFO_FILENAME)));
            $upload_file=$upload_path.'/'.$new_file_name;

            /* If file exist change the file name */
            $i=1;
            while(file_exists($upload_file)) {
                $new_file_name=$basename.'_'.$i.'.'.$ext_file;
                $upload_file=$upload_path.'/'.$new_file_name;
                $i++;
            }

            //$store_path=$upload_file;
            $thumb_folder_150 = $upload_path.'/150';
            $thumb_folder_300 = $upload_path.'/300';
            $thumb_folder_500 = $upload_path.'/500';

            if(move_uploaded_file($file['tmp_name'][$row_index],$upload_file)) {
                /* Return array with all needed info */
                $path_array[$j]=array(
                	'media_path'=>$upload_file,
                	'media_size'=>$file['size'][$row_index],
                	'media_type'=>$ext_file,
                );

                /* Create thumb of uploaded images */
                if($create_thumbs && in_array(strtolower($ext_file),$valid_img_ext)) {
	                image_thumb($upload_file, 150, 150,$new_file_name,$thumb_folder_150);
	                image_thumb($upload_file, 300, 300,$new_file_name,$thumb_folder_300);
	                image_thumb($upload_file, 500, 500,$new_file_name,$thumb_folder_500);
	            }
            }
            $j++;
	    }
	    return $path_array;
	}


	/*
		Generate edit image html code while editing content
		Using in content/edit_content_data
	*/
	function generate_edit_image($file_row) {
		$file_ext=trim(strtolower(pathinfo($file_row['media_path'],PATHINFO_EXTENSION)));
		$image_ext=array('jpg','jpeg','png','gif');

		$html="";
		$icon="";
		$pdf_icon="./public/AdminLte/img/pdf_icon.png";
		$video_icon="./public/AdminLte/img/video_icon.png";
		$file_icon="./public/AdminLte/img/file_icon.png";
		
		if(in_array($file_ext,$image_ext)) {
			$html.='<div class="col-md-3" id="'.$file_row['content_value_id'].'"><div class="thumbnail">
			<img src="'.get_thumb(base_url().$file_row['media_path'],300).'" title="'.$file_row['media_title'].'" style="height: 125px;" >
			<div class="caption">
				<a class="pointer" role="button" onclick="delete_img(this,0)" id="'.$file_row['content_value_id'].'" style="float:right;" data-toggle="tooltip" data-placement="top" title="" data-original-title="delete image"><i class="fa fa-fw fa-trash-o"></i></a>
				<a class="pointer" role="button" onclick="edit_img_title(this,'.$file_row['content_value_id'].')" style="float:right;" data-toggle="tooltip" data-placement="top" title="" data-original-title="Edit image title"><i class="fa fa-fw fa-pencil"></i></a>
				<a href="'.base_url().$file_row['media_path'].'" class="pointer"  style="float:right;" data-toggle="tooltip" data-placement="top" title="" data-original-title="View Image" target="_blank"><i class="fa fa-fw fa-eye"></i></a>
				<input type="hidden" name="'.$file_row['field_machine_name'].'_edit[]" value="'.$file_row['content_value_id'].'"/>
				<p style="text-align: center;" class="media_title">'.$file_row['media_title'].'</p>
			</div></div></div>';
		}
		else {
			/*$html.='<div class="btn btn-app" id="'.$file_row['content_value_id'].'">
			<span class="badge bg-red">
				<span><a class="pointer" style="color:#fff" role="button" onclick="delete_img(this,1)" id="'.$file_row['content_value_id'].'" data-toggle="tooltip" data-placement="top" title="" data-original-title="delete file"><i class="fa fa-fw fa-trash-o"></i></a></span>
			</span>
			<i class="fa  fa-file-o" data-href="'.base_url().$file_row['media_path'].'"></i>'.$file_row['media_title'].'
			</div>';*/
			if($file_row['media_type']=='pdf') {
				$icon=$pdf_icon;
			}
			else if($file_row['media_type']=='mp4' || $file_row['media_type']=='AVI' || $file_row['media_type']=='wmv') {
				$icon=$video_icon;
			}
			else {
				$icon=$file_icon;
			}
			$html.='<div class="col-md-3" id="'.$file_row['content_value_id'].'"><div class="thumbnail">
			<img src="'.base_url().$icon.'" title="'.$file_row['media_title'].'" style="height: 125px;" >
			<div class="caption">
				<a class="pointer" role="button" onclick="delete_img(this,0)" id="'.$file_row['content_value_id'].'" style="float:right;" data-toggle="tooltip" data-placement="top" title="" data-original-title="delete image"><i class="fa fa-fw fa-trash-o"></i></a>
				
				<a href="'.base_url().$file_row['media_path'].'" class="pointer"  style="float:right;" data-toggle="tooltip" data-placement="top" title="" data-original-title="View Video" target="_blank"><i class="fa fa-fw fa-eye"></i></a>
				<input type="hidden" name="'.$file_row['field_machine_name'].'_edit[]" value="'.$file_row['content_value_id'].'"/>
				<p style="text-align: center;" class="media_title">'.basename($file_row['media_path']).'</p>
			</div></div></div>';
		}
		return $html;
	}

	public function get_likes_count($filter=false) {
		if($filter) {
			apply_filter($filter);
		}

		if(empty($filter['SELECT'])) {
			$this->db->select('content.*, count(*) as cnt_like');
		}
		$this->db->join('likes','content.content_id=likes.content_id','left');
		$this->db->group_by('likes.content_id');
		$this->db->order_by('cnt_like','desc');
		$rs=$this->db->get('content');
		return $rs->result_array();
	}

	/*
		Process for sending email to owner and sender
		$content_type(array) - content_type table row
		$content_type_id - Newly added content type array
 	*/
	public function webform_process($content_type, $content_id) {
		/* Ignore for content */
		if($content_type['content_type_category']=='content' || $content_type['content_type_category']=='section_content' ) {
			return 0;
		}

		$this->load->model('email_model');
		/* get content for */
		$content=$this->get_content_with_value(array('content.content_id'=>$content_id));
		$content=current($content);

		/* Check if need to send mail to owner */
		if($content_type['send_email']=='1' && !empty($content_type['send_email_to'])) {
			$subject = $content_type['owner_email_subject'];
			/* Set email subject */
			if(empty($subject)) {
				$subject='New form submission for '.$content_type['content_type_title'].' from '.$_SERVER['HTTP_HOST'];
			}

			/* Set from address */
			$from_address=false;
			if(!empty($content_type['send_email_from'])) {
				$from_address=$content_type['send_email_from'];
			}

			/*  creating email body */
			$html_body='<style>table {font-family: arial, sans-serif;border-collapse: collapse;max-width: 500px; width:100%}td, th {border: 1px solid #dddddd;text-align: left;padding: 8px;}tr:nth-child(even) {background-color: #dddddd;}</style>
			Hello,<br/>
			'.$content_type['content_type_title'].' Webform has been submitted and following information is collected <br><br>
			<table><tbody>';
			foreach ($content as $field => $field_val) {
				if($field == 'content') { 
					continue;
				}

				/* Remove underscore and id from field name */
				$field_name=str_replace('_', ' ', $field);
				$field_name=preg_replace ( '/[0-9]*$/' , '' , $field_name);

				$html_body.='<tr><th>'.$field_name.'</th><td>';
				/* Loop each field value */
				foreach ($field_val as $key => $value) {
					//dsm($value);
					if($key > 0) {
						$html_body.=', ';
					}

					/* Add download path for image */
					if(!empty($value['media_path'])) {
						if(!empty($value['media_title'])) {
							$html_body.='<a href="'.base_url().$value['media_path'].'">'.$value['media_title'].'</a>';
						}
						else {
							$html_body.='<a href="'.base_url().$value['media_path'].'">View File</a>';
						}
						
					}
					else {
						$html_body.=$value['content_value'];
					}
				}
				$html_body.='</td></tr>';
			}
			$html_body.='</tbody></table>';
			//dsm($html_body);die;
			$this->email_model->send_email($content_type['send_email_to'], $subject, $html_body,$from_address);
		}

		/* Send thanks email to sender if thanks_email is not blanks and content value of field is not blank */
		if(!empty(trim(strip_tags($content_type['thanks_email']))) && !empty($content_type['thanks_email_field']) && !empty($content[$content_type['thanks_email_field']][0]['content_value'])) {
			$subject = $content_type['sender_email_subject'];
			/* Set email subject */
			if(empty($subject)) {
				$subject='Thanks for submission of '.$content_type['content_type_title'].' from '.$_SERVER['HTTP_HOST'];
			}

			/* Set from address */
			$from_address=false;
			if(!empty($content_type['send_email_from'])) {
				$from_address=$content_type['send_email_from'];
			}
			$to=$content[$content_type['thanks_email_field']][0]['content_value'];

			$this->email_model->send_email($to, $subject, $content_type['thanks_email']);
		}
	}

	public function get_content_type_section_data($id){
		$this->db->where('content_type_section_id',$id);
		$data=$this->db->get('content_type_section');
		return $rs=$data->result_array();
	}

	public function content_type_data($id){
		$this->db->where('content_type_id',$id);
		$data=$this->db->get('content_type');
		return $rs=$data->result_array();
	}

	public function get_program_url($id){
		return $content_type_id=$this->db->get_where('content',array('content_id'=>$id))->result_array();
	}

	/*
		Get content url prefix based on menu parent
	*/
	public function generate_url_prefix($menu_link_id) {
		$this->load->model('menu/menu_model');
		$return_array=array();
		$link[0]['link_parent_id']=$menu_link_id;
		while ($link[0]['link_parent_id'] > 0) {		
			/* Get link from database */
			$link=$this->menu_model->get_menu_link(array(
				'menu_link.menu_link_id'=>$link[0]['link_parent_id']
			));

			/* Check blank */
			if(!empty($link)) {
				$return_array[]=strtolower(url_title($link[0]['link_title']));
			}
		}

		if(empty($return_array)) {
			return '';
		}

		return implode('/', array_reverse($return_array));
	}

	/* 
		Function Used at a time of content creation with parent menu link
		Add link in main navigation
	*/
	public function add_content_menu($content_array, $content_id) {
		$this->load->model('menu/menu_model');
		/* Get main_navigation menu */
		$main_navigation=$this->menu_model->get_menu_master(array(
			'main_navigation'=>'1'
		));

		if(empty($main_navigation)) {
			return set_message("Main Navigation is not defined","warning");
		}

		$link_insert=array(
			'menu_master_id'=>$main_navigation[0]['menu_master_id'],
			'link_title'=>$content_array['content_title'],
			'link_path'=>$content_array['url_title'],
			'link_status'=>'1',
			'link_parent_id'=>$content_array['menu_parent'],
			/* Add at last in parent */
			'link_weight'=>$content_id,
			'front_visible'=>1,
			'content_id'=>$content_id,
			'created_at'=>date('Y-m-d H:i:s'),
			'created_by'=>$this->session->userdata('user_id'),
		);

		if(empty($content_array['content_published'])) {
			$link_insert['front_visible']=0;
		}

		return $this->menu_model->add_menu_link($link_insert);
	}

	/* Update menu link of relative content */
	public function update_content_menu($content_array, $content_id, $link_unlink) {
		$this->load->model('menu/menu_model');

		/* Check if content is available in menu link */
		$link = $this->menu_model->get_menu_link(array('content_id'=>$content_id));

		/* Adding new link to menu if previously not added */
		if(empty($link) && !empty($link_unlink)) {
			return $this->add_content_menu($content_array, $content_id);
		}
		
		$link_insert=array(
			//'link_title'=>$content_array['content_title'],
			'link_path'=>$content_array['url_title'],
			'link_parent_id'=>$content_array['menu_parent'],
			'updated_at'=>date('Y-m-d H:i:s'),
			'link_status'=>'1',
			'updated_by'=>$this->session->userdata('user_id')
		);

		/* If unchecked main menu link, than disable the menu link */
		if(empty($link_unlink)) {
			$link_insert['link_status']=0;
		}

		if(empty($content_array['content_published'])) {
			$link_insert['front_visible']=0;
		}

		$this->menu_model->update_menu_link_filter($link_insert, array(
			'content_id'=>$content_id,
		));
	}

	/* Return unique url of supplied title by appending number at the end */
	public function get_unique_url_title($url_title, $content_id=false) {
		$is_unique=false;
		$increment='';

		/* Append numeric value until not unique found */
		while ($is_unique==false) {
			$unique_filter=array(
				'content.url_title'=>$url_title.$increment,
			);

			/* Ignore unique validation for own content while editing */
			if(!empty($content_id)) {
				$unique_filter['content.content_id !=']=$content_id;
			}

			$unique_result=$this->get_content($unique_filter);
		
			if(count($unique_result) < 1) {
				$is_unique=true;
				$url_title = $url_title.$increment;
			}
			else {
				$increment++;
			}
		}
		return $url_title;
	}

	/* 
		Return array of breadcrumbs in following pattern
		array(
			array(
				'link'=>'http://localhost/base/page-title',
				'title'=>'title',
				'active'=>'0'
			),
			array(
				'link'=>'http://localhost/base/page-title2',
				'title'=>'title2',
				'active'=>'1'
			)
		)
	*/
	public function get_breadcrumbs_array($content_id) {
		$content=$this->get_content(array(
			'content.content_id'=>$content_id
		));

		if(empty($content)) {
			return array();
		}

		$content=$content[0];
		$home_link=array(
			'link'=>base_url(),
			'title'=>'Home',
			'active'=>'0'
		);

		/* If no menu parent, return home page link */
		if(empty($content['menu_parent'])) {
			return array($home_link);
		}

		$return_array=array();
		$top_node=false;
		$menu_parent=$content['menu_parent'];
		/* Get menu parent untill its top node */
		while ($top_node==false) {
			$link=$this->menu_model->get_menu_link(array(
				'menu_link_id'=>$menu_parent
			));

			/* If not found active link, break the loop */
			if(empty($link)) {
				$top_node=true;
			}
			else {
				$url=$link[0]['link_path'];
				/* If inside url */
				if (filter_var($url, FILTER_VALIDATE_URL) === FALSE) {
					$url=base_url().$link[0]['link_path'];
				}
				$return_array[]=array(
					'link'=>$url,
					'title'=>$link[0]['link_title'],
					'active'=>'0'
				);

				/* Link dont have parent, break the loop */
				if(empty($link[0]['link_parent_id'])) {
					$top_node=true;
				}
				/* Set link parent for find its parent */
				else {
					$menu_parent=$link[0]['link_parent_id'];
				}
			}
		}

		/* If return_array blank return home page link */
		if(empty($return_array)) {
			return array($home_link);
		}

		/* Reverse the order to display parent to child way */
		$return_array=array_reverse($return_array);
		array_unshift($return_array, $home_link);
		return $return_array;
	}


}