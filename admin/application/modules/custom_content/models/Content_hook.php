<?php 

/*
	Hooks
		before_add_MACHINE_NAME
		before_update_MACHINE_NAME
		after_update_MACHINE_NAME
		after_add_MACHINE_NAME
		before_delete_MACHINE_NAME
		after_delete_MACHINE_NAME

	Just create method with 
*/

class Content_hook extends CI_Model {

	public function __construct() {
		parent::__construct();
		$this->load->model('user/user_model');
		$this->load->model('notification/notification_model');
		$this->announcement_update_publish=false;
	}

	public function before_add_contact_us() {
		$post_array=$this->input->post();
		$captcha_url="https://www.google.com/recaptcha/api/siteverify?secret=".CAPTCH_SECRETE.'&response='.$_POST['g-recaptcha-response'];
		$curl_response = file_get_contents($captcha_url);
		$curl_response = json_decode($curl_response,true);
        //dsm($curl_response);die; 
		if(empty($curl_response['success'])) {
			$this->db->trans_complete();
			$this->session->set_flashdata('old_data',$post_array);
			set_message("You didn't verified captcha, please try again.");
			redirect_back();
			die;
	     }
	}
}