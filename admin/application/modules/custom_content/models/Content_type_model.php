<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');
 
 
class Content_type_model extends CI_Model
{
	public function __construct($value='') {
		$this->load->model('custom_content/content_model');
	}

	function get_content_type($filter=false) {
		if(!empty($filter)) {
			apply_filter($filter);
		}

		
		$this->db->where('deleted_at IS NULL', null, false); 
		$this->db->order_by('content_type.content_type_id','desc');
		$rs=$this->db->get('content_type');
		return $rs->result_array();
	}
	function get_content_type_singular($filter=false) {
		if(!empty($filter)) {
			apply_filter($filter);
		}
		//$this->db->where("is_singular",0);
		$this->db->where('content_type_category','content'); 	
		$this->db->where('deleted_at IS NULL', null, false); 	
		$this->db->order_by('content_type.content_type_id','desc');
		$rs=$this->db->get('content_type');
		return $rs->result_array();
	}

	function get_users($filter=false) {
		if(!empty($filter)) {
			apply_filter($filter);
		}
		$this->db->where('deleted_at IS NULL', null, false);
		$rs=$this->db->get('users');
		return $rs->result_array();
	}

	function update_content_type($data, $filter=false) {
		if(!empty($filter)) {
			apply_filter($filter);
		}
		$data['updated_at']=date('Y-m-d H:i:s');
		$data['updated_by']=$this->session->userdata($this->config->item('user_id_insession'));
		return $this->db->update('content_type',$data);
	}

	function add_content_type($data) {
		$data['created_at']=date('Y-m-d H:i:s');
		$user_id_insession=$this->config->item('user_id_insession');
		$data['created_by']=$this->session->userdata($user_id_insession);
		$this->db->insert('content_type',$data);
		return $this->db->insert_id();
	}

	/*
		Return content type field table with join field_type
	*/
	function get_content_type_fields($filter=false) {
		if(!empty($filter)) {
			apply_filter($filter);
		}

		if(empty($filter['SELECT'])) {
			$this->db->select('content_type_field.*,field_type.field_type_title');
		}
		$this->db->where('content_type_field.deleted_at IS NULL', null, false);
		$this->db->join('field_type','field_type.field_type_id=content_type_field.field_type_id');
		$this->db->where('content_type_field.deleted_at IS NULL', null, false);
		$this->db->order_by('content_type_field.rank','asc');

		$rs=$this->db->get('content_type_field');
		return $rs->result_array();
	}
	
	/*
		Add content type field
	*/
	function add_field($data) {
		$data['created_at']=date('Y-m-d H:i:s');
		$data['created_by']=$this->session->userdata($this->config->item('user_id_insession'));
		$this->db->insert('content_type_field',$data);
		return $this->db->insert_id();
	}

	/* Delete section */
	public function delete_setion($filter) {
		if(empty($filter)) {
			return false;
		}
		apply_filter($filter);
		dsm($filter);
		$data['content_type_section.deleted_at']=date('Y-m-d H:i:s');
		return $this->db->update('content_type_section',$data);
	}

	/* Get section of content type */
	public function get_content_type_section($filter) {
		if(!empty($filter)) {
			apply_filter($filter);
		}
		$this->db->where('content_type_section.deleted_at IS NULL', null, false);
		$rs=$this->db->get('content_type_section');
		return $rs->result_array();
	}

	/*
		update content type field
	*/
	public function update_field($data, $filter) {
		if(!empty($filter)) {
			apply_filter($filter);
		}
		$data['updated_at']=date('Y-m-d H:i:s');
		$data['updated_by']=$this->session->userdata($this->config->item('user_id_insession'));
		return $this->db->update('content_type_field',$data);
	}



	public function get_field_type($filter=false) {
		if(!empty($filter)) {
			apply_filter($filter);
		}
		$rs=$this->db->get('field_type');
		return $rs->result_array();
	}

	public function get_role($filter=false) {
		if(!empty($filter)) {
			apply_filter($filter);
		}
		$this->db->where('deleted_at IS NULL', null, false);
		$rs=$this->db->get($this->config->item('role_table'));
		return $rs->result_array();
	}

	public function get_content_permission_master($filter=false) {
		if(!empty($filter)) {
			apply_filter($filter);
		}
		$this->db->where('status','1');
		$this->db->where('module','content');
		$rs=$this->db->get('module_permission_master');
		return $rs->result_array();
	}

	public function get_webform_permission_master($filter=false) {
		if(!empty($filter)) {
			apply_filter($filter);
		}
		$this->db->where('status','1');
		$this->db->where('module','webform');
		$rs=$this->db->get('module_permission_master');
		return $rs->result_array();
	}

	function is_exists($table_name, $filter=false) {
		if(!empty($filter)) {
			apply_filter($filter);
		}
		$row=$this->db->get($table_name);
		if($row->num_rows() > 0) {
			return true;
		} else {
			return false;
		}
	}

	/*
		$field - Array from content_type_fields table
		$filled_data - Array 

		array(
			'content_value_id'=>'Value to be filled'
		)

		return  - Array (
			['control'] => str
			['hidden_control'] => str
		}
	*/
	public function generate_fields($field,$filled_data=false, $is_edit=false) {

		$return=array(
			'field_control'=>'',
			'hidden_control'=>''
		);

		$name=$field['machine_name'].'[]';
		$str_name=' name="'.$name.'"'; 
		$image_title=$field['machine_name']."_imgtitle[]";
		$placeholder=$field['placeholder'];
		$str_placeholder=' placeholder="'.$field['placeholder'].'"';
		$value=$field['default_value'];
		$str_value=' value="'.$field['default_value'].'"';
		$str_class='form-control form-field '.$field['field_type_title'];

		$wrapper_class="col-md-12";
		$str_editor_cls=' rich_editor ';

		/* Variable used for attribute data-validation */
		$data_validation='';
		/* Extra attributes of field */
		$validation_attr='';

		/* Add pattern attribute from custom_validation */
		if(!empty($field['custom_validation'])) {
			$data_validation.=' custom';
			$validation_attr.=' data-validation-regexp="'.$field['custom_validation'].'"';
		}

		/* Add url field validation */
		if($field['field_type_id'] == 12) {
			$data_validation.=' url';
		}

		/* Add html5 validation attributes */
		$pattern='';
		if(!empty($field['field_validation'])) {
			$val_array=explode('|', $field['field_validation']);
			foreach ($val_array as $key => $val) {
				if($val=='required') {
					/* Ignore required attribute for image and file control while editing */
					if(($field['field_type_id']==6 || $field['field_type_id']==7) && $is_edit) { }
					else {
						$data_validation.=' required';
					}
				}
				elseif(strpos($val, 'min:')!==false) {
					$min=explode(':', $val);
					$validation_attr.=' min="'.$min[1].'"';
				}
				elseif(strpos($val, 'max:')!==false) {
					$max=explode(':', $val);
					$validation_attr.=' max="'.$max[1].'"';
				}
				elseif(strpos($val, 'max_length:')!==false) {
					if(strpos($data_validation, 'length') === false) {
						$data_validation.=' length';	
					}
					$maxc=explode(':', $val);
					$validation_attr.=' data-validation-length="max'.$maxc[1].'"';
				}
				elseif(strpos($val, 'min_length:')!==false) {
					if(strpos($data_validation, 'length') === false) {
						$data_validation.=' length';	
					}
					$minc=explode(':', $val);
					$validation_attr.=' data-validation-length="min'.$minc[1].'"';
				}
				elseif(strpos($val, 'is_file_type:')!==false) {
					$data_validation.=' mime';
					$file_val=explode(':', $val);
					$validation_attr.=' data-validation-allowing="'.$file_val[1].'"';
					$validation_attr.=' data-validation-error-msg-mime="Select valid '.$file_val[1].' Files"';
				}
				elseif(strpos($val, 'file_size:')!==false) {
					$data_validation.=' size';
					$file_val=explode(':', $val);
					$validation_attr.=' data-validation-max-size="'.$file_val[1].'"';
					$validation_attr.=' data-validation-error-msg-size="You can not upload file larger than '.$file_val[1].'b"';
				}
				/* Attach rows for textarea in attribute */
				elseif(strpos($val, 'rows:')!==false) {
					$rows=explode(':', $val);
					$validation_attr.=' rows="'.$rows[1].'"';
				}
				/* Attach cols for textarea in attribute */
				elseif(strpos($val, 'cols:')!==false) {
					$cols=explode(':', $val);
					$validation_attr.=' cols="'.$cols[1].'"';
				}
			}
		}

		/* Attach validation rule in attribute */
		if(!empty($data_validation)) {
			$validation_attr.=' data-validation="'.trim($data_validation).'"';

			/* Add option attribute when required is not available */
			if(strpos($data_validation, 'required') === false) {
				$validation_attr.=' data-validation-optional="true"';
			}
		}


		/* For extra attributes */
		if(!empty($field['extra_attribute'])) {
			//$validation_attr.=' '.$field['extra_attribute'];
			$attributes_list=$this->get_attr_value($field['extra_attribute']);
			//dsm($attributes_list);die;
			$validation_attr.=' '.$this->form->_attributes_to_string($attributes_list);
		}

		/* For custom control class */
		if(!empty($field['css_classes'])) {
			$str_class.=' '.$field['css_classes'];
		}

		$str_tag="";
		$str_tagmid="";
		$str_endtag="";
		$select_options=array();
		$select_key="key";
		$select_value="value";
		$clone_fields="";
		$group_id='group_'.$field['machine_name'];
		$clone_id='clone_'.$field['machine_name'];
		$help_text='';
		$control_class="col-md-8";
		$addbtn_class="col-md-4";
		$help_text='<span class="help-block form-error"></span>';
		if($field['help_text']!='') {
			$help_text.='<p class="help-text">'.$field['help_text'].'</p>';
		}

		/* Create basic attribute text */
		switch ($field['field_type_id']) {
			/* Textbox */
			case 1:
				$str_tag='<input type="text" ';
				$str_endtag='/>';
				break;

			/* number */
			case 2:
				$str_tag='<input type="number" ';
				$str_endtag='/>';
				break;

			/* textarea */
			case 3:

				$str_tag='<textarea ';
				$str_tagmid=" >";
				$str_endtag='</textarea>';
				break;

			/* wysiwyg_editor */
			case 4:
				$str_tag='<textarea ';
				$str_tagmid=" >";
				$str_endtag='</textarea>';
				$str_class=$str_editor_cls;
				break;
			
			/* select_box */
			case 5:
				$select_options=$this->get_option_array($field['field_options']);
				$str_tag="<select ";
				$str_tagmid=" >";
				$str_endtag="</select>";
				break;
			/* file */
			case 6:
				$str_tag='<input type="file" ';
				$str_endtag='/>';
				break;

			/* image */
			case 7:
				$str_tag='<input type="file" accept="image/*" ';
				$str_endtag='/>';
				break;

			/* content_references */
			case 8:

				$filter=array(
					'content.content_type_id'=>$field['content_reference_id'],
				);
				$contents=$this->content_model->get_content($filter);
				$select_options=$contents;
				$select_value='content_title';
				$select_key='content_id';
				$str_tag="<select ";
				$str_tagmid=" >";
				$str_endtag="</select>";
				/* Define multiple selection */
				if($field['number_of_values'] != '1') {
					$str_tag="<select multiple ";
				}
				break;
			/*case 9:
				$checkbox_options=$this->content_model->get_option_array($field['field_options']);
				break;*/
			
			/* Calender */
			case 11:
				$str_tag='<input type="text" ';
 				$str_class.=" ".$field['datepicker_type'];
				$str_endtag='/>';
				break;

			/* URL */
			case 12:
				$str_tag='<input type="url" ';
				$str_endtag='/>';
				break;

			/* table_reference */
			case 13:
				$select_options=$this->get_tableref_data($field['custom_table'], $field['table_key_column'], $field['table_value_column']);
				$select_value=explode(',', $field['table_value_column']);
				$select_value=array_map('trim', $select_value);

				$select_key=$field['table_key_column'];
				$str_tag="<select ";
				$str_tagmid=" >";
				$str_endtag="</select>";
				/* Define multiple selection */
				if($field['number_of_values'] != '1') {
					$str_tag="<select multiple ";
				}
				break;

			/* Hidden */
			case 15:
				$str_tag='<input type="hidden"';
				$str_endtag='/>';
				break;

			/* html content */
			case 1:
				$str_tag='<input type="text" ';
				$str_endtag='/>';
				break;
		}

		/* Attach class attribute and title attribute */
		$str_class=' class="'.$str_class.'" title="'.$field['field_title'].'"';

		/* Fill the data with $filled_data */
		/* For unlimited value */
		if($field['number_of_values'] == '0') {
			if($filled_data === false || count($filled_data) < 1) {
				/* Generate combobox for select box type and content reference type */
				$other=$str_class.$validation_attr." multiple";
				if(in_array($field['field_type_id'], array(5,8,13))) {
					$field_control=generate_combobox($name, $select_options,$select_key,$select_value, $field['default_value'],$other,false);
					/* Add hidden control for select box value to capture */
					$field_control.='<input type="hidden" class="hidden-select-val" name="val_'.$name.'" />';
				}
				/* Image title field to add */
				else if($field['field_type_id'] == 7 || $field['field_type_id'] == 6) {
					$field_control=$str_tag.$str_name.$other.$str_placeholder.$str_value.$str_tagmid.$str_endtag;
					$field_control=$this->generate_image_field($field_control,$image_title, $help_text);
					$help_text='';
				}
				/* For textbox and rich editor */
				else if($field['field_type_id'] == 3 || $field['field_type_id'] == 4) {
					$field_control=$str_tag.$str_name.$other.$str_placeholder.$str_tagmid.$value.$str_endtag;
				}
				else {
					$field_control=$str_tag.$str_name.$other.$str_placeholder.$str_value.$str_tagmid.$str_endtag;	
				}

				/* Ignore unlimited field for selectbox control */
				if(in_array($field['field_type_id'], array(5,8,13))) {
					$return['field_control'].=$field_control.$help_text;
				}
				else {
					/* Clone copy of unlimited add */
					$clone_fields.=$this->generate_clonefield($clone_id,$field_control.$help_text);

					$field_html='<div class="'.$control_class.'">'.$field_control.$help_text.'</div>';

					/* add more button */
					$add_btn='<div class="'.$addbtn_class.'"><button type="button" class="btn btn-block btn-info add_more_control" data-parent="'.$group_id.'" data-clone_id="'.$clone_id.'">Add More</button></div>';

					$return['field_control'].='<div class="row unlimited-wrapper">'.$field_html.$add_btn.'</div>';
					$return['hidden_control'].=$clone_fields;
				}
			}
			/* While editing, Fill data to control */
			else {
				$filled_data_i=0;
				$clone_field='';
				/* Ignore unlimited field for selectbox control */
				if(in_array($field['field_type_id'], array(5,8,13))) {
					$selected_options=$filled_data;
					$other=$str_class.$validation_attr." multiple";
					$field_control=generate_combobox($name, $select_options,$select_key,$select_value, $selected_options,$other, false);
					/* Add hidden control for select box value to capture */
					$field_control.='<input type="hidden" class="hidden-select-val" name="val_'.$name.'" />';
					$return['field_control'].=$field_control.$help_text;
				}
				else {
					foreach ($filled_data as $filled_key => $filled_val) {
						$name=$field['machine_name'].'['.$filled_key.']';
						$str_name=' name="'.$name.'"';
						$display_existing="";
						$other=$str_class.$validation_attr;
						
						/* Image and file control */
						if($field['field_type_id'] == 7 || $field['field_type_id'] == 6) {
							$str_name=' name="'.$field['machine_name'].'[]" ';

							$field_control=$str_tag.$str_name.$other.$str_placeholder.$str_value.$str_tagmid.$str_endtag;
							if($field['field_type_id'] == 7) {
								$field_control=$this->generate_image_field($field_control,$image_title,$help_text);
								$help_text='';
							}
							/* Ignore image filed to be replicate */
							if($filled_data_i > 0) {
								continue;
							}
						}
						/* For textbox and rich editor */
						else if($field['field_type_id'] == 3 || $field['field_type_id'] == 4) {
							$field_control=$str_tag.$str_name.$other.$str_placeholder.$str_tagmid.$filled_val.$str_endtag;
						}
						else {
							$val=' value="'.$filled_val.'"';
							$field_control=$str_tag.$str_name.$str_placeholder.$other.$val.$str_tagmid.$str_endtag;
						}

						$field_html='<div class="'.$control_class.'">'.$field_control.$help_text.'</div>';

						/* Add and remove button */
						if($filled_data_i==0) {
							$clone_field=$field_control.$help_text;
							$action_button='<div class="'.$addbtn_class.'"><button type="button" class="btn btn-block btn-info add_more_control" data-parent="'.$group_id.'" data-clone_id="'.$clone_id.'">Add More</button></div>';
						}
						else {
							$action_button='<div class="col-md-4">
							<button type="button" class="btn btn-block btn-danger remove_more_control" >Remove</button></div>';
						}

						$return['field_control'].='<div class="row unlimited-wrapper" data-content_value_id="'.$filled_key.'">'.$field_html.$action_button.'</div>';
						$filled_data_i++;
					} /* End filled_data foreach */

					/* Clone copy of unlimited add */
					$clone_fields.=$this->generate_clonefield($clone_id,$clone_field);
					$return['hidden_control'].=$clone_fields;
				}
			} /* End else of $field['number_of_values'] == '0' */
		}
		else {
			if(empty($filled_data)) {
				for ($i=0; $i < $field['number_of_values'] ; $i++) { 
					/* Generate combobox for select box type and content reference type */
					$other=$str_class.$validation_attr;
					if(in_array($field['field_type_id'], array(5,8,13))) {
						$field_control=generate_combobox($name, $select_options,$select_key,$select_value, $field['default_value'],$other);
						$field_control.='<input type="hidden" class="hidden-select-val" name="val_'.$name.'" />';
					}
					/* Generate checkbox list */
					else if($field['field_type_id'] == 9) {
						$field_control=$this->checkboxlist_from_array($name, $checkbox_options);
					}
					/* Image title field to add */
					else if($field['field_type_id'] == 7 || $field['field_type_id'] == 6) {
						$field_control=$str_tag.$str_name.$other.$str_placeholder.$str_value.$str_tagmid.$str_endtag;
						$field_control=$this->generate_image_field($field_control,$image_title,$help_text);
						$help_text="";
					}
					/* For textbox and rich editor */
					else if($field['field_type_id'] == 3 || $field['field_type_id'] == 4) {
						$field_control=$str_tag.$str_name.$other.$str_placeholder.$str_tagmid.$value.$str_endtag;
					}
					/* For html content */
					else if($field['field_type_id'] == 14) {
						$field_control=$str_tag.$str_endtag;
						$field_control.=$field['html_content'];

					}
					/* For normal inputs */
					else {
						$field_control=$str_tag.$str_name.$str_placeholder.$other.$str_value.$str_tagmid.$str_endtag;	
					}

					$return['field_control'].=$field_control.$help_text;
				}
			}
			else {
				$field_control='';
				for ($i=0; $i < $field['number_of_values']; $i++) {
					$value=array_values($filled_data)[0];
					$array_key=array_keys($filled_data)[0];
					$name=$field['machine_name'].'['.$array_key.']';
					$str_name=' name="'.$name.'"';
					$other=$str_class.$validation_attr;
					/* Generate combobox for select box type and content reference type */
					if(in_array($field['field_type_id'], array(5,8,13))) {
						$control=generate_combobox($name, $select_options,$select_key,$select_value, $value,$other);
						$control.='<input type="hidden" class="hidden-select-val" name="val_'.$name.'" />';
					}
					/* Image title field to add */
					else if($field['field_type_id'] == 7 || $field['field_type_id'] == 6) {
						$str_name=' name="'.$field['machine_name'].'[]" ';

						$control=$str_tag.$str_name.$other.$str_placeholder.$str_value.$str_tagmid.$str_endtag;

						//if($field['field_type_id'] == 7) {
							$control=generate_image_field($control,$image_title,$help_text);
							$help_text='';
						//}
					}
					/* For textbox and rich editor */
					else if($field['field_type_id'] == 3 || $field['field_type_id'] == 4) {
						$control=$str_tag.$str_name.$other.$str_placeholder.$str_tagmid.$value.$str_endtag;
					}
					else {
						$val=' value="'.$value.'"';
						$control=$str_tag.$str_name.$str_placeholder.$val.$other.$str_tagmid.$str_endtag;	
					}
					$field_control.=$control;
				}
				$return['field_control'].=$field_control.$help_text;
			}
		}
		return $return;
	}


	public function get_content_type_permission($filter=false) {
		if(!empty($filter)) {
			apply_filter($filter);
		}
		$this->db->join('module_permission_master','module_permission_master. module_permission_master_id=content_type_permission.permission_id');
		$rs=$this->db->get('content_type_permission');
		return $rs->result_array();
	}

	/*
		Update permission content while add and update content type
	*/
	public function set_content_type_permission($content_type_id, $post_array) {
		
		/* Delete all permission of content and add new */
		$this->db->where('content_type_id',$content_type_id);
		$this->db->delete('content_type_permission');

		$insert_data=array();

		/* Get master permission */
		$permission_master=$this->content_type_model->get_content_permission_master();
		$webform_permission= $this->content_type_model->get_webform_permission_master();
		$permission_master=array_merge($permission_master,$webform_permission);
		

		foreach ($permission_master as $permission_row) {
			if(empty($post_array[$permission_row['module_permission_master_id']])) {
				continue;
			}
			foreach ($post_array[$permission_row['module_permission_master_id']] as $role_id) {
				$insert_data[]=array(
					'content_type_id'=>$content_type_id,
					'role_id'=>$role_id,
					'permission_id'=>$permission_row['module_permission_master_id']
				);
			}
		}
		/* Insert batch */
		if(!empty($insert_data)) {
			$this->db->insert_batch('content_type_permission',$insert_data);
		}
		return true;
	}


	/*
		Return codeigniter form validation pattern array based on machine name of each field
	*/
	function content_validation($field_list,$is_edit=0) {
		$field_name = 'field';
		$label = 'label';

		/* to return validations */
		$validations=array();
		/* Set up validations */
		$value_validations=array();
		/* File uploading validations */
		$file_validations=array();

		foreach ($field_list as $field) {
			$rules=explode('|', $field['field_validation']);

			$validate_rule='';
			$i=0;
			foreach ($rules as $rule) {
				$has_value=explode(':', $rule);
				
				/* Ignore required rule for file control while editing */
				if($is_edit) {
					if($has_value[0]=='required' && ($field['field_type_id']==6 || $field['field_type_id']==7) ) {
						continue;
					}
				}

				/* Ignore rows and cols rules */
				if(strpos($has_value[0], "cols") !== false || strpos($has_value[0], "rows") !== false) {
					continue;
				}

				if(count($has_value) > 1) {
					$rule=$has_value[0].'['.$has_value[1].']';
				}
				else {
					$rule=$has_value[0];	
				}

				/* Join multiple rules */
				if($i==0) {
					$validate_rule=$rule;
				}
				else {
					$validate_rule.='|'.$rule;
				}
				$i++;
			}

			/* For custom validation, Regex validation */
			if(!empty($field['custom_validation'])) {
				if(empty($validate_rule)) {
					$validate_rule='regex_match[/'.$field['custom_validation'].'/]';
				}
				else {
					$validate_rule.='|regex_match[/'.$field['custom_validation'].'/]';
				}
			}

			/* Define codeigniter validation rules and file upload validation rule */
			if($field['field_type_id']=='6' || $field['field_type_id']=='7') {
				$file_validations[]=array(
					'field' => $field['machine_name'],
					'label' => $field['field_title'],
					'rules' => str_replace(" ", "",$validate_rule)
				);
			}
			else {
				if (!empty($validate_rule)) {
					$value_validations[]=array(
						'field' => $field['machine_name'].'[]',
						'label' => $field['field_title'],
						'rules' => $validate_rule
					);
				}

			}
		}

		$validations['value_validations']=$value_validations;
		$validations['file_validations']=$file_validations;
		
		return $validations;
	}

	/* Used while generating field
	*/	
	public function get_option_array($options) {
		$array=explode(PHP_EOL,$options);
		$option_list=array();
		$i=0;
		foreach ($array as $value) {
			$val_array=explode('|',$value);

			$option_list[$i]['key']=trim($val_array[0]);
			$option_list[$i]['value']=trim($val_array[0]);

			if(!empty($val_array[1])) {
				$option_list[$i]['value']=trim($val_array[1]);
			}
			$i++;
		}
		return $option_list;
	}

	public function get_attr_value($options) {
		$array=explode(PHP_EOL,$options);
		$option_list=array();
		$i=0;
		foreach ($array as $value) {
			$val_array=explode('|',$value);
			$option_list[$val_array[0]]=$val_array[0];	
			if(!empty($val_array[1])) {
				$option_list[$val_array[0]]=$val_array[1];
			}
			$i++;
		}
		return $option_list;
	}

	public function checkboxlist_from_array($name, $array) {
		
	}


	function generate_image_field($field_control,$name, $help_block='') {
	  $div='<div class="row">
	            <div class="col-md-6"><div class="form-group">'.$field_control.$help_block.'</p>
	            </div></div>
	            <div class="col-md-6">
	            <div class="form-group">
	              <input type="text" placeholder="File title" name="'.$name.'"class="form-control" value="" maxlength="220">
	            </div>
	            </div>
	          </div>';
	  return $div;
	}

	function generate_clonefield($id,$control) {
		preg_match_all('/\[(.*?)\]/', $control, $matches);
		/* Remove control id from name */
		if(!empty($matches[1])) {
			$control=str_replace($matches[1],'',$control);
		}
		$return='<div style="display:none" id="'.$id.'">
		<div class="row unlimited-wrapper">
			<div class="col-md-8">'.$control.'</div>
			<div class="col-md-4">
				<button type="button" class="btn btn-block btn-danger remove_more_control" >Remove</button>
			</div></div></div>';
		return $return;
	}

	/* Return table ref data, used in generate_fields */
	public function get_tableref_data($table, $key_column, $value_column) {
		if(empty($table) || empty($key_column) || empty($value_column)) {
			return array();
		}

		$this->db->select($key_column.", ".$value_column);
		$rs=$this->db->get($table);
		$result_array=$rs->result_array();
		return $result_array;
	}


	/*
		Check if user has permission to perform action on content type
		$content_type - (int) ID from content type table
		$permission - (array) Permission id from
	*/
	public function check_content_permission($content_type_id, $permission, $role_id=false, $redirect_back=1, $check_type='and') {
		if(!$role_id) {
			$role_id=$this->session->userdata($this->config->item('role_id_insession'));
		}

		/*$user_permission=$this->get_content_type_permission(array(
			'content_type_permission.content_type_id'=>$content_type_id,
			'role_id'=>$role_id
		));
		$user_permission=array_column($user_permission, 'permission_id');
		*/

		$user_permission=$this->session->userdata('content_type_permission');

		/* Check user permission */
		if(empty($user_permission[$content_type_id])) {
			return redirect_action($redirect_back);
		}

		$user_permission=$user_permission[$content_type_id];
		/* If user has content admin access allow him to every thing */
		if(in_array(8, $user_permission)) {
			return true;
		}

		/* If multiple permission to check */
		if(is_array($permission)) {
			$available_permission=false;
			$unavailable_permission=false;

			/* Checking permission */
			foreach ($permission as $permission_id) {
				if(in_array($permission_id, $user_permission)) {
					$available_permission=true;
				}
				else {
					$unavailable_permission=true;
				}
			}

			/* when single permission required */
			if(strtolower($check_type) == 'or') {
				if($available_permission) {
					return true;	
				}
				else {
					return redirect_action($redirect_back);
				}
				
			}
			/* When all permission required */
			else {
				if($available_permission && $unavailable_permission===false) {
					return true;
				}
				else {
					return redirect_action($redirect_back);
				}
			}
		}
		/* String supplied */
		else {
			/* Supplied permission matched */
			if(in_array($permission, $user_permission)) {
				return true;
			}
		}

		return redirect_action($redirect_back);
	}

}