<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');
 
 
class Media_model extends CI_Model
{

	public function add_media($media_data) {
		$user_id=$this->config->item('user_id_insession');

		if(empty($user_id)) {
			$user_id='user_id';			
		}

		$media_data['created_by']=$this->session->userdata($user_id);
		$media_data['created_at']=date('Y-m-d H:i:s');
		$this->db->insert('media',$media_data);
		return $this->db->insert_id();
	}	

	public function get_media($filter=false) {
		if($filter){
			apply_filter($filter);	
		}
		$this->db->order_by('media_id','DESC');
		$rs=$this->db->get('media');
		return $rs->result_array();
	}

}