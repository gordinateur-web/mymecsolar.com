<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Content_view_model extends CI_Model
{
	function get_content($filter=false) {
		if($filter) {
			apply_filter($filter);
		}
		$this->db->select('content.*, content_type.content_type_title');
		$this->db->join('content_type', 'content_type.content_type_id = content.content_type_id');
		$rs=$this->db->get('content');
		return $rs->result_array();
	}

	function get_content_value($filter=false) {
		if($filter) {
			apply_filter($filter);
		}
		$this->db->select('content_type_fields.content_type_fields_title, content_type_fields.machine_name, content_type_fields.field_type_id, content_value.*, files.imagepath');
		$this->db->join('files', 'files.fid = content_value.file_id','left');
		$this->db->join('content_type_fields', 'content_type_fields.content_type_fields_id = content_value.content_type_fields_id');
		$rs=$this->db->get('content_value');
		return $rs->result_array();
	}

	function get_content_value_new($filter=false) {
		if($filter) {
			apply_filter($filter);
		}
		if($this->session->userdata('role') == 'user') {
			$this->db->where('content_type.user_id',$this->session->userdata('uid'));
		}
		$this->db->select('files.imagepath');
		$this->db->join('files', 'files.fid = content_value.file_id','left');
		$this->db->join('content', 'content.content_id = content_value.content_id');
		$this->db->join('content_type', 'content_type.content_type_id = content.content_type_id');
		$this->db->join('content_type_fields', 'content_type_fields.content_type_fields_id = content_value.content_type_fields_id');
		$rs=$this->db->get('content_value');
		return $rs->result_array();
	}

	function get_searched_content($filter=false) {
		if($filter) {
			apply_filter($filter);
		}
		$this->db->select('content_value.*, content.content_type_id, content.content_title');
		$this->db->group_by('content_value.content_id');
		// $this->db->join('content_type', 'content_type.content_type_id = content.content_type_id');
		$this->db->join('content', 'content.content_id = content_value.content_id');
		$rs=$this->db->get('content_value');
		return $rs->result_array();
	}

	function get_file_by_id($filter=false) {
		if($filter) {
			apply_filter($filter);
		}
		$rs=$this->db->get('files');
		return $rs->result_array();
	}

	function get_content_field_value($content_id) {
		$condition['content_id']=$content_id;
		if(!empty($condition)) {
			$node = array( );
			$result=$this->content_view_model->get_content_value($condition);
			foreach ($result as $key) {
				$node[$key['machine_name']] = ''; 
				/* making blank array with machine_name as keys */
			}
			
			foreach ($node as $machine_name => $blank) {
				$temp = array( );
				foreach ($result as $key => $value) {
					if($value['machine_name'] == $machine_name) {
						$temp[] = $value;
					}
				}
				$node[$machine_name] = $temp;
			}
			return $node;
		}
	}
}