
<div class="col-md-12">
	<div class="box box-primary">
		<div class="box-header with-border">
			<div class="box-title">
				<h3></h3>
			</div>
		</div>
		<?php 
		echo $this->form->form_model(array(),base_url().'custom_content/content/import_content', array('name' => 'content_upload_save')); 
		?>
		<div class="box-body">
			<div class="col-md-4">
				<div class="form-group">
					<label class="control-label">Import file</label>
					<?php echo $this->form->form_upload('file',array("class"=>'form-control',"required"=>"required")); ?>
					<p class="help-block">Upload Json File</p>
				</div>
			</div>

			<div class="col-md-4">
				<div class="form-group">
					<label class="control-label">Overwrite</label><br>
					<label>
						<input type="checkbox" name="overwrite" value="1">
						Overwrite content
					</label>
				</div>
			</div>
		</div>
		<!-- /.box-body -->
		<div class="box-footer">
			<button type="submit" class="btn btn-primary pull-right">Upload</button>
		</div>
		<!-- /.box-footer -->
		<?php echo $this->form->form_close(); ?>

	</div>
</div>