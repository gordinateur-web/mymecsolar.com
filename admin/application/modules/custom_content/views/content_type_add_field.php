<div class="box box-primary">
    <div class="box-header with-border">
        <div class="pull-right">
            <a class="btn btn-sm btn-primary" id="save_order" title="Save Order" ><i class="fa fa-sort"></i> Save Order</a>
        </div>
    </div>
    <div class="box-body">
        <div id="save_err" class="alert alert-warning alert-dismissable" style="display:none;">
            <button aria-hidden="true" data-dismiss="alert" class="close" type="button">x</button>
            <span>Please click on Save order to change the order of Fields</span>
        </div> 
        <div class="table-responsive">
            <table class="table table-striped table-bordered table-hover">
                <thead>
                    <tr>
                        <th width="5">#</th>
                        <th>Field Label</th>
                        <th>Machine Name</th>
                        <th>Field Type</th>
                        <!-- <th>Field Validation</th> -->
                        <th>Values</th>
                        <th width="5">Operations</th>
                    </tr>
                </thead>
                <tbody id="sortable">
                    <?php 
                    $i=0;
                    foreach($field_list as $row): 
                        ?>
                    <tr id="<?php echo $row['content_type_field_id']; ?>">
                        <td><i class="cursor-move fa fa-fw fa-arrows"></i></td>
                        <td><?php echo $row['field_title']; ?></td>
                        <td><?php echo $row['machine_name']; ?></td>
                        <td><?php echo $row['field_type_title']; ?></td>
                        <!-- <td><?php echo $row['field_validation']; ?></td> -->
                        <td><?php echo $row['number_of_values']; ?></td>
                        <td>
                            <a href="<?php echo base_url().'custom_content/content_type/set_attribute/'.$row['content_type_field_id']; ?>" data-toggle="tooltip" data-placement="top" title="Edit">
                                <i class="fa fa-edit"></i>
                            </a>&nbsp;&nbsp;&nbsp;
                            <a data-fieldid="<?php echo $row['content_type_field_id']; ?>" class="pointer delete-field" data-toggle="tooltip" data-placement="top" title="Delete">
                                <i class="fa fa-trash-o"></i>
                            </a>
                        </td>
                    </tr>
                    <?php 
                    $i++;
                    endforeach; ?>
                </tbody>
            </table>
        </div>

        <?php
            $form_model=array(
                'content_type_id'=>$content_type_id,
            );
            echo $this->form->form_model($form_model,base_url()."custom_content/content_type/save_add_fields", array('name'=>'add_fields','class'=>'validate-form')); ?>
            <div class="row" >
                <div class="col-sm-12" >
                    <div class="col-md-7">  
                        <h4>Add new field</h4>
                    </div>
                    <div class="col-md-7">  
                        <label>Field Label <span class="text-danger">*</span></label>
                        <?php
                            echo $this->form->form_hidden('content_type_id');
                            echo $this->form->form_input('field_title',array(
                                'class'=>'form-control',
                                'placeholder'=>"Add Field Title",
                                'data-validation'=>'required',
                                'title'=>'Field Label',
                            ));
                        ?>
                    </div>                               
                    <div class="col-md-4">  
                        <label>Field Type <span class="text-danger">*</span></label>   
                        
                        <?php echo $this->form->form_dropdown_fromdatabase('field_type_id',$field_type_list,'field_type_id','field_type_title','','class="form-control" id="field_type_id" data-validation="required" title="Field Label" ');?>
                    </div>          
                    <div class="col-md-1">
                        <input type="submit" value="Add Field" class="btn btn-sm btn-primary"  style="margin-top:25px;"/>
                    </div>
                </div>
            </div>
        <?php echo $this->form->form_close(); ?>
    </div><!-- /.box-body -->
</div>
<link href="<?php echo base_url().'public/plugins/jquery-ui/jquery-ui.min.css';?>" rel="stylesheet" />
<script src="<?php echo base_url().'public/plugins/jquery-ui/jquery-ui.min.js';?>"></script>



<script type="text/javascript">
    $(document).ready(function() {
        $("#sortable" ).sortable({
            handle: ".cursor-move"
        });
        $("#sortable" ).disableSelection();
        $("#sortable" ).on("sortstop", function( event, ui ) {
            $('#save_err').show();
        });
        $('#save_order').click(function() {
            sortable_list();
        });

        /* Delete action */
        $('.delete-field').click(function() {
            var field_id=$(this).attr('data-fieldid');
            swal({
                title: "Are you sure?",
                text: "Delete this fields",
                type: "warning",
                showCancelButton: true,
                confirmButtonColor: "#DD6B55",
                confirmButtonText: "Yes, delete it!",
                closeOnConfirm: true
            },
            function(){
                window.location.href=base_url+'custom_content/content_type/delete_field/'+field_id;
            });
        });

    });

    function sortable_list() {
        var sortedIDs =$( "#sortable" ).sortable("toArray");
        $.ajax({
            url     : base_url+"custom_content/content_type/save_field_order",
            type    : 'POST',
            data    : {'ids':sortedIDs},
            success : function(data){
                data=$.parseJSON(data);
                remove_loading();
                if(data.status == '1') {
                    $('#save_err').hide();
                    $('#save_err').show();
                    $('#save_err').removeClass('alert-warning');
                    $('#save_err').addClass('alert-success');
                    $('#save_err').find('span').html('');
                    $('#save_err').find('span').html(data.message);
                }
                else {
                    swal("Oops...", data.message, "error");
                }
            }
        });
    }
</script>