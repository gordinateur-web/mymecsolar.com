<div class="row" style="margin-bottom: 10px">
	<div class="col-sm-12">
		<a href="<?php echo base_url().'custom_content/content/add_content_data/'.$content_type.'?content_type_section_id='.$content_type_section_id.'&section_content_parent='.$section_content_parent; ?>" class="btn btn-primary btn-sm">
			<i class="fa fa-plus"></i> Add New <?php echo $section_title; ?>
		</a>
	</div>
</div>

<div class="table-responsive">
	<table class="table table-striped table-bordered table-hover">
		<thead>
  			<tr>
				<th>Content Name</th>
				<th width="20%">Content Type</th>
				<th width="10%">Owner</th>
				<th width="15%">Updated</th>
				<th width="5%">Status</th>
				<th width="5%">Publish</th>
				<th width="5%">Action</th>
  			</tr>
		</thead>
		<tbody>
		<?php  
		foreach($content_list as $row) {
			$encrypt_id=encrypt_id($row['content_id']);
		?>
			<tr id="<?php echo $row['content_id']; ?>">
				<td><?php echo $row['content_title']; ?></td>
				<td><?php echo $row['content_type_title']; ?></td>
				<td><?php echo $row['name']; ?></td>
				<!-- <td><?php //echo '<a href="'.base_url().'comment/index?content_id='.$row['content_id'].'" target="_blank">'.$row['total_comment'].'</a>'; ?></td> -->
				<td><?php echo dateformat($row['created_at'],0,1); ?></td>
				<td><?php 
					if($row['content_status']=='Approved' || $row['content_status']==null ) {
						echo '<label class="label label-success">Approved</label>';
					}
					else if($row['content_status']=='pending') {
						echo '<label class="label label-warning">Pending</label>';
					}
					else if($row['content_status']=='blocked') {
						echo '<label class="label label-danger">Blocked</label>';
					}
					?>
				</td>
				<td><?php
					if($row['content_published']==1) {
						echo '<label class="label label-success">Publish</label>';
					}
					else {
						echo '<label class="label label-danger">Unpublish</label>';
					}
					?>
				</td>
					
					
				<td>
					<div class="btn-group options">
						<button type="button" class="btn btn-default btn-sm" dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
							<i class="fa fa-cog"></i> Options
						</button>
						 
						<ul class="dropdown-menu">
							<?php if($this->content_type_model->check_content_permission($row['content_type_id'], 8,false, 0)) { ?>
							<li>
								<a class="pointer table-list-edit" data-editurl="custom_content/content/change_status" data-editid="<?php echo $row['content_id']; ?>" data-modeltitle="Change Content Status">
								<i class="fa fa-unlock fa-margin"></i> Change Status
								</a>
							</li>
							<?php } ?>
							<?php if($this->content_type_model->check_content_permission($row['content_type_id'], 2,false, 0) || $this->content_type_model->check_content_permission($row['content_type_id'],3,false, 0)) { ?>
							<li>
								<a href="<?php echo base_url()."custom_content/content/edit_content_data/".$encrypt_id; ?>">
									<i class="fa fa-edit fa-margin"></i> Edit
								</a>
							</li>
							<?php } ?>
							<?php if($this->content_type_model->check_content_permission($row['content_type_id'], 4,false, 0) || $this->content_type_model->check_content_permission($row['content_type_id'], 5,false, 0)) { ?>
							<li>
								<a data-id="<?php echo $encrypt_id; ?>" class="pointer delete-content">
									<i class="fa fa-trash-o fa-margin"></i> Delete
								</a>
							</li>
							<?php } ?>
						</ul>
					</div>
				</td>
			</tr>
		<?php } ?>

		<?php if(empty($content_list)) { ?>
			<tr>
				<td colspan="7">No Related data found</td>
			</tr>
		<?php } ?>

		</tbody>
	</table>
</div>
<script type="text/javascript">
	$(document).ready(function() {
		$('.delete-content').click(function() {
			var id=$(this).attr('data-id');
			swal({
				title: "Are you sure?",
				text: "Content type to delete?",
				type: "warning",
				showCancelButton: true,
				confirmButtonColor: "#DD6B55",
				confirmButtonText: "Yes, delete it!",
				closeOnConfirm: false
			},
			function(){
				window.location = base_url+'custom_content/content/delete_content/'+id;
			});
		});
	})

</script>