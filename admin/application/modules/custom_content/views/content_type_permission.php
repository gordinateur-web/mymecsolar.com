<div class="box box-primary">
	<?php
		$edit_data=array();
		$hidden=array();
		if(!empty($edit_content_type)) {
			$edit_data = $edit_content_type;
			$hidden=array('content_type_id'=>$edit_data['content_type_id']);
		}
		echo $this->form->form_model($edit_data, base_url()."custom_content/content_type/save_content_type_permission",array('name'=>'add_content_type','method'=>'post','id'=>'content_type_form'), $hidden);
	?>
	<div class="box-body">
		<div class="form-group col-md-12">
			<div class="alert alert-warning" role="alert"> 
				<strong>Warning!</strong> Permission update will only affect after session logout.
			</div>
			<table class="table table-bordered">
				<thead>
					<tr>
						<th>Permission</th>
						<?php
							$role_title=$this->config->item('role_table_title');
							foreach ($role_list as $role) {
								echo '<th>'.$role[$role_title].'&nbsp;&nbsp;&nbsp;<input type="checkbox" class="check_all" data-role="'.$role['role_id'].'"/></th>';
							}
						?>
					</tr>
				</thead>
				<tbody>
					<?php
						foreach ($permission_list as $permission) {
							echo '<tr>';
							echo '<td>'.$permission['permission_title'].'</td>';
							foreach ($role_list as $role) {
								echo '<td>';
								echo $this->form->form_checkbox($permission['module_permission_master_id'].'['.$role['role_id'].']',$role['role_id']);
								echo '</td>';
							}
							echo '</tr>';
						}
					?>
				</tbody>
			</table>
		</div>
	</div>

    <div class="modal-footer">
	    <div class="pull-right">
	      	<button type="reset" class="btn btn-default">Clear</button>
	      	<button type="submit" class="btn btn-primary">Submit</button>
	    	<button onclick="window.history.back();" class="btn btn-default">Close</button>
	    </div>
	</div><!-- /.box-footer -->
</div>
<script type="text/javascript">
	$(document).ready(function() {
            /* Check all permission for role */
            $('.check_all').change(function() {
            	var role_id=$(this).attr('data-role');
            	if($(this).is(":checked")) {
            		$('input[name*="['+role_id+']"]').prop('checked',true);
            	}
            	else {
            		$('input[name*="['+role_id+']"]').prop('checked',false);
            	}
            });
        });

</script>