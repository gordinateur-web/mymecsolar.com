<div class="box box-primary">
  	<div class="box-header with-border">
  		<?php 
  		/* Permission check for content type  */
  		if(permission_check(array(2),'and',0)) { //dsm($content_list);die; ?>
  		<a href="<?php echo base_url()."custom_content/content_type/add_content_type"; ?>" class="btn btn-primary btn-sm"><i class="fa fa-plus"></i> Add New</a>
  		<?php } ?>
  	</div>
  	<div class="box-body">
		<div class="table-responsive">
	  		<table class="table table-striped table-bordered table-hover full-height-datatable">
				<thead>
		  			<tr>
						<th>Title</th>
						<th>Description</th>
						<th width="10%">Type</th>
						<th width="10%">Contents</th>
						<th width="5%">Status</th>
						<th width="5%">Action</th>
		  			</tr>
				</thead>
				<tbody>
				<?php  
					foreach($content_list as $row) {
						$encrypt_id=encrypt_id($row['content_type_id']);
				?>
					<tr id="<?php echo $row['content_type_id']; ?>">
						<td><?php echo $row['rank_prefix'].$row['content_type_title']; ?></td>
						<td><?php echo $row['content_type_description']; ?></td>
						<td><?php echo $row['content_type_category']; ?></td>
						<td>
							<?php
								if(!empty($content_count[$row['content_type_id']])) {
									echo $content_count[$row['content_type_id']];
								}
								else {
									echo '0';
								}
							?>
						</td>
						<td><?php echo get_status_markup($row['content_type_status']); ?></td>
						<td>
							<div class="btn-group options">
								<button type="button" class="btn btn-default btn-sm" dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
									<i class="fa fa-cog"></i> Options
								</button>
								 
								<ul class="dropdown-menu">
									<?php if($row['content_type_category']=='content' || $row['content_type_category']=='section_content') { 
										?>
									<li>
										<a href="<?php echo base_url()."custom_content/content/add_content_data/".$encrypt_id; ?>">
											<i class="fa fa-plus fa-margin"></i> Add Content
										</a>
									</li>
									<?php } ?>
									<li>
										<a href="<?php echo base_url()."custom_content/content/view_content/".$encrypt_id; ?>">
											<i class="fa fa-eye fa-margin"></i> View Content
										</a>
									</li>
									<?php 
										if(permission_check(array(2),'and',0)) { 
									?>
									<li>
										<a href="<?php echo base_url()."custom_content/content_type/add_content_type/".$encrypt_id; ?>">
											<i class="fa fa-edit fa-margin"></i> Edit
										</a>
									</li>
									<li>
										<a href="<?php echo base_url()."custom_content/content_type/add_fields/".$encrypt_id; ?>">
											<i class="fa fa-bars fa-margin"></i> Manage Fields
										</a>
									</li>
									<li>
										<a data-id="<?php echo $encrypt_id; ?>" class="pointer delete-content-type">
											<i class="fa fa-trash-o fa-margin"></i> Delete
										</a>
									</li>
									<?php } ?>

									<?php if(permission_check(array(11,2),'or',0)) { ?>
									<li>
										<a href="<?php echo base_url()."custom_content/content_type/change_permission/".$encrypt_id; ?>">
											<i class="fa fa-shield fa-margin"></i> Permission
										</a>
									</li>
									<?php } ?>
									
								</ul>
							</div>
						</td>
					</tr>
				<?php } ?>
				</tbody>
	  		</table>
	  	</div>
  	</div>
</div>
<script type="text/javascript">
	$(document).ready(function() {
		$('.delete-content-type').click(function() {
			var id=$(this).attr('data-id');
			swal({
				title: "Are you sure?",
				text: "Content type to delete?",
				type: "warning",
				showCancelButton: true,
				confirmButtonColor: "#DD6B55",
				confirmButtonText: "Yes, delete it!",
				closeOnConfirm: false
			},
			function(){
				window.location = base_url+'custom_content/content_type/delete_content_type/'+id;
			});
		});
	})

</script>