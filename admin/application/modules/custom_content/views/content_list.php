<?php $base_url=base_url(); ?>
<div class="box box-primary">
  	<div class="box-header with-border">
  		<a data-toggle="modal" data-target="#search_content" class="btn btn-primary btn-sm"><i class="fa fa-search"></i> Search</a>
  		<?php if(!empty($clearfilter)) { ?>
  		<a href="<?php echo $base_url.'custom_content/content'; ?>" class="btn btn-primary btn-sm"><i class="fa fa-search-minus"></i> Clear Search</a>
  		<?php } ?>
  		<span class="pull-right"><?php if (isset($pagination['links'])){ echo $pagination['links']; } ?></span>
  	</div>
  	<div class="box-body">
		<div class="table-responsive">
	  		<table class="table table-striped table-bordered table-hover full-height-datatable">
				<thead>
		  			<tr>
						<th>Content Name</th>
						<th width="20%">Content Type</th>
						<th width="10%">Owner</th>
						<th width="15%">Updated</th>
						<th width="5%">Status</th>
						<th width="5%">Publish</th>
						<th width="5%">Action</th>
		  			</tr>
				</thead>
				<tbody>
				<?php  
				foreach($content_list as $row) {
					$encrypt_id=encrypt_id($row['content_id']);
				?>
					<tr id="<?php echo $row['content_id']; ?>">
						<td><?php echo $row['content_title']; ?></td>
						<td><?php echo $row['content_type_title']; ?></td>
						<td><?php echo $row['name']; ?></td>
						<!-- <td><?php //echo '<a href="'.base_url().'comment/index?content_id='.$row['content_id'].'" target="_blank">'.$row['total_comment'].'</a>'; ?></td> -->
						<td><?php echo dateformat($row['created_at'],0,1); ?></td>
						<td><?php 
							if($row['content_status']=='Approved' || $row['content_status']==null ) {
								echo '<label class="label label-success">Approved</label>';
							}
							else if($row['content_status']=='pending') {
								echo '<label class="label label-warning">Pending</label>';
							}
							else if($row['content_status']=='blocked') {
								echo '<label class="label label-danger">Blocked</label>';
							}
							?>
						</td>
						<td><?php
							if($row['content_published']==1) {
								echo '<label class="label label-success">Publish</label>';
							}
							else {
								echo '<label class="label label-danger">Unpublish</label>';
							}
							?>
						</td>
							
							
						<td>
							<div class="btn-group options">
								<button type="button" class="btn btn-default btn-sm" dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
									<i class="fa fa-cog"></i> Options
								</button>
								 
								<ul class="dropdown-menu">
									<?php if($this->content_type_model->check_content_permission($row['content_type_id'], 8,false, 0)) { ?>
									<li>
										<a class="pointer table-list-edit" data-editurl="custom_content/content/change_status" data-editid="<?php echo $row['content_id']; ?>" data-modeltitle="Change Content Status">
										<i class="fa fa-unlock fa-margin"></i> Change Status
										</a>
									</li>
									<?php } ?>
									<?php if($this->content_type_model->check_content_permission($row['content_type_id'], 2,false, 0) || $this->content_type_model->check_content_permission($row['content_type_id'],3,false, 0)) { ?>
									<li>
										<a href="<?php echo base_url()."custom_content/content/edit_content_data/".$encrypt_id; ?>">
											<i class="fa fa-edit fa-margin"></i> Edit
										</a>
									</li>
									<?php } ?>
									<?php if($this->content_type_model->check_content_permission($row['content_type_id'], 4,false, 0) || $this->content_type_model->check_content_permission($row['content_type_id'], 5,false, 0)) { ?>
									<li>
										<a data-id="<?php echo $encrypt_id; ?>" class="pointer delete-content">
											<i class="fa fa-trash-o fa-margin"></i> Delete
										</a>
									</li>
									<?php } ?>
								</ul>
							</div>
						</td>
					</tr>
				<?php } ?>
				</tbody>
	  		</table>
	  	</div>
  	</div>
</div>

<!-- Modal -->
<div class="modal fade" id="search_content" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" data-backdrop="static">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
				<h4 class="modal-title" id="myModalLabel">Search Content</h4>
			</div>
			<div class="modal-body">
				<?php 
					$form_model=array();
					/* Fill old data */
					$old_data=$this->input->get();
					if(!empty($old_data)) {
						$form_model=$old_data;  
					}
					echo $this->form->form_model($form_model, $base_url.'custom_content/content/', array('name'=>'search_content','id'=>'search_content_form', 'method'=>'get')); 
				?>
					<div class="row">
						<div class="col-md-12">
							<div id="error_div"></div>
						</div>
						<div class="col-md-12">
							<div class="col-md-12">
								<label>Content Name</label>
								<?php 
								$other_option=array(
									'class'=>'form-control',
									'placeholder'=>'Content Name',
								);
								echo $this->form->form_input('content_title',$other_option); 
								?>
							</div>
							<div class="clearfix"></div><br>

							<div class="col-md-6 col-sm-6">
								<div class="form-group">
									<label>Status</label>
									<?php 
									$other_option=array(
										'class'=>'form-control',
										'placeholder'=>'status',
									);
									$option=array(''=>'All','Approved'=>'Approved','pending'=>'Pending','blocked'=>'Blocked');
									echo $this->form->form_dropdown('content_status', $option,'','', $other_option); 
									?>
								</div>
							</div>
							<div class="col-md-6 col-sm-6">
								<div class="form-group">
									<label>Publish</label>
									<?php 
									$other_option=array(
										'class'=>'form-control',
										'placeholder'=>'status',
									);
									$option=array(''=>'All','1'=>'Publish','0'=>'Unpublish');
									echo $this->form->form_dropdown('content_published', $option,'','', $other_option); 
									?>
								</div>
							</div>
							<div class="clearfix"></div><br>

							<div class="col-md-12">
								<label>Keyword</label>
								<?php 
								$other_option=array(
									'class'=>'form-control',
									'placeholder'=>'Keyword',
								);
								echo $this->form->form_input('keyword',$other_option); 
								?>
							</div>
							<div class="clearfix"></div><br>

							<div class="col-md-12">
								<label>Content Type</label>
								<?php 
								$other_option=array(
									'class'=>'form-control select2',
									'placeholder'=>'Content Type',
									'style'=>'width:100%'
								);
								echo $this->form->form_dropdown_fromdatabase('content_type_id',$content_type_list,'content_type_id','content_type_title','', $other_option); 
								?>
							</div>							
						</div>
					</div>
					<div class="row"><br/>
						<div class="modal-footer">
							<input type="submit" name="submit"  class="btn btn-primary" value="Search">
							<input type="reset" name="reset"  class="btn btn-default" value="Reset">
							<button type="button" class="btn btn-danger" data-dismiss="modal">Close</button>
						</div>							      			
					</div>							      			
				</form>
			</div>
		</div>
	</div>
</div>	

<script type="text/javascript">
	$(document).ready(function() {
		$('.delete-content').click(function() {
			var id=$(this).attr('data-id');
			swal({
				title: "Are you sure?",
				text: "Content type to delete?",
				type: "warning",
				showCancelButton: true,
				confirmButtonColor: "#DD6B55",
				confirmButtonText: "Yes, delete it!",
				closeOnConfirm: false
			},
			function(){
				window.location = base_url+'custom_content/content/delete_content/'+id;
			});
		});
	})

</script>