<div class="panel panel-default">
  	<div class="panel-heading">Add Content</div>
	<form role="form" method="post" action="<?php echo base_url()."content/save_content"; ?>">
  	<div class="panel-body">
      	<?php
			if(isset($edit_content)) {
				echo '<input type="hidden" name="content_id" value="'.$edit_content[0]['content_id'].'"/>';
			}
		?>
		<div class="row">
			<div class="form-group col-md-12">
				<label>Content Type <span class="text-danger">*</span></label>   
	          	<?php if(isset($edit_content)) {
	          		echo generate_combobox('content_type_id',$content_type_list,'content_type_id','content_type_title',$edit_content[0]['content_type_id'],'id="content_type_id"');
	          	} else {
	          		echo generate_combobox('content_type_id',$content_type_list,'content_type_id','content_type_title','','id="content_type_id"');
	          	}?>
	        </div>
        	<div class="form-group col-md-12">
				<label>Title<span class="text-danger">*</span></label>
				<input type="text" name="content_title" placeholder="Page title" class="form-control" value="<?php if(isset($edit_content)) {	echo $edit_content[0]['content_title']; } ?>" required>
			</div>
		</div>
  	</div>
    <div class="box-footer">
	    <div class="pull-right">
	      <button type="reset" class="btn btn-default">Clear</button>
	      <button type="submit" class="btn btn-primary">Submit</button>
	    </div>
	    <button onclick="window.history.back();" class="btn btn-default">Close</button>
	</div><!-- /.box-footer -->
	</form>
</div>