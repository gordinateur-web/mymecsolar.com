<?php
	$is_edit=false;
	
	$form_action=base_url()."custom_content/content/save_content_data";
	if(!empty($edit_array)) {
		$is_edit=true;
	}
	else {
		$edit_array=array('content_published'=>1);
	}
	$form_attribute=array(
		"name"=>"add_content_data" ,
		"enctype"=>"multipart/form-data" ,
		"id"=>"content_data_form" ,
		"class"=>"content-form ".$content_type['content_type_machine_name'],
	);	

	$old_data=$this->session->flashdata('old_data');
	if(!empty($old_data)) {
		$edit_array=$old_data;
	}

	echo $this->form->form_model($edit_array,$form_action, $form_attribute);
	if($is_edit) {
		echo $this->form->form_hidden("content_id");
	}


	/* Add hidden value for section and content parent */
	$content_type_section_id=$this->input->get('content_type_section_id');
	$section_content_parent=$this->input->get('section_content_parent');
	if(!empty($content_type_section_id) && !empty($section_content_parent)) {
		echo $this->form->form_hidden("content_type_section_id",$content_type_section_id);
		echo $this->form->form_hidden("section_content_parent",$section_content_parent);
	}
	/* Just pass blank for editing purpose */
	else {

		echo $this->form->form_hidden("content_type_section_id");
		echo $this->form->form_hidden("section_content_parent");
	}

	/* Add extra hidden fields to form */
	if(!empty($form_hidden_data)) {
		foreach ($form_hidden_data as $key => $value) {
			echo $this->form->form_hidden($key,$value);
		}
	}

	

?>
	<div class="box box-primary">
	  	<div class="box-header">
  			<div class="pull-right box-tools"></div>
	  	</div>
	  	<div class="box-body">
	      	<input type="hidden" name="content_type_id" value="<?php echo $content_type_id;?>"/>
	      	<div class="row">
	      		<?php 
	      			/* ignore content title for webform */
	      			if($content_type['content_type_category']!='webform') {
	      		?>
	      			<!-- Title of content -->
		      		<div class="col-sm-6">
		      			<div class="form-group">
							<label>Content title<span class="text-danger">*</span></label>
							<?php
								$title_attribute=array(
									"placeholder"=>"Unique content title",
									"data-validation"=>"required length",
									"class"=>"form-control form-field",
									"title"=>"Content title",
									"data-validation-length"=>"max150",
								);
								echo $this->form->form_input("content_title",$title_attribute);
							?>
						</div>
					</div>

					<div class="col-sm-2">
						<div class="form-group">
							<label>&nbsp;</label>
							<label class="label-checkbox checkbox-inline">
							<?php 
								echo $this->form->form_checkbox('content_published',1,'','');
								echo "Published";
							?>
							</label>
							<label class="label-checkbox checkbox-inline">
								<?php 
									//echo $this->form->form_checkbox('stick_ontop',1);
									//echo "Sticky at top of lists";
								?>
							</label>
						</div>
					</div>

					<div class="col-sm-3">
						<div class="form-group">
							<label>Select Section</label>
							<?php 
								echo $this->form->form_dropdown_fromdatabase('menu_parent', $menu_list, 'menu_link_id','link_title',false, array('class'=>'form-control'), 'Select');
								echo '<label class="label-checkbox checkbox-inline" style="margin-top:0 !important">';
								
								/* Check the checkbox */
								/*$main_menu_link=false;
								if(isset($edit_array['menu_parent']) && $edit_array['menu_parent']!='') {
									$main_menu_link=true;
								}*/

								echo $this->form->form_checkbox('main_menu_link',1,'');
								echo "Add Link to Navigation";
								echo '</label>';
							?>
						</div>
					</div>

				<?php } /* Not webform */ ?>

				<div class="clearfix"></div>
				<?php
					$clone_fields='';
					/* Loop through each field of content */
					foreach ($field_list as $field) {
						/* Set validations */
						$validations=explode('|', $field['field_validation']);
						
						/* Create fields */
						$group_id='group_'.$field['machine_name'];
						$clone_id='clone_'.$field['machine_name'];

						/* For wrapper class */
						$wrapper_class="col-sm-12";
						if(!empty($field['wrapper_classes'])) {
							$wrapper_class=$field['wrapper_classes'];
						}
				?>
					<div class="<?php echo $wrapper_class; ?> field-group" id="<?php echo $group_id; ?>" style="<?php if($field['field_type_id']==15) { echo "display:none"; } ?>">
						<?php 
							/* Ignore label for html content */
							if($field['field_type_id']!=14) { 
						?>
							<label>
								<?php 
									echo $field['field_title']; 
									if (array_search('required',$validations) !== false ) {
										echo '<span class="text-danger">*</span>';
									} 
								?>
							</label>
						<?php } ?>
							<div class="form-group">
							<?php 
								$fill_data=false;
								if(isset($edit_array[$field['machine_name']])) {
									$fill_data=$edit_array[$field['machine_name']];
								}
								
								$field_control=$this->content_type_model->generate_fields($field,$fill_data, $is_edit);
								echo $field_control['field_control'];
								$clone_fields.=$field_control['hidden_control'];
							?>
							</div>
					</div>
				<?php
					/* field_list end here */
					}
				?>

			</div>
			<div class="modal-footer">
			    <div class="pull-right">
			      	<button type="reset" class="btn btn-default">Reset</button>
			      	<button type="submit" class="btn btn-primary">Submit</button>
			    	<button onclick="window.history.back();" class="btn btn-danger">Cancel</button>
			    </div>
			</div><!-- /.box-footer -->
		</div>
	</div>
	<?php if($is_edit) { ?>
			<!-- Section list of available -->
			<?php if(!empty($section_list)) { ?>
				<div class="box box-primary">
				  	<div class="box-header">
				  		<h3 class="box-title">
				  			<i class="fa fa-puzzle-piece" aria-hidden="true"></i> Sections
				  		</h3>
				  		<div class="pull-right box-tools">
					  		<button type="button" class="btn btn-primary btn-xs btn-flat" data-widget="collapse"><i class="fa fa-minus"></i>
			                </button>
		                </div>
				  	</div>
				  	<div class="box-body">
				  		<div class="row">
				  			<div class="col-md-12">
				  				<div class="nav-tabs-custom">
				  					<?php 
				  						$li_list='';
				  						$content_list='';
				  						$active='active';
				  						foreach ($section_list as $section_key=>$section_row) {
				  							$view_content=array();
				  							/* Get content from section_content_list for specific section */
				  							if(!empty($section_content_list[$section_row['content_type_section_id']])) {
				  								$view_content=$section_content_list[$section_row['content_type_section_id']];
				  							}
				  							$view_data=array(
				  								'content_list'=>$view_content,
				  								/* Parent content id */
				  								'section_content_parent'=>$content_id,
				  								/* Content type id of child content */
				  								'content_type'=>$section_row['content_id'],
				  								'section_title'=>$section_row['section_title'],
				  								/* section id, primary key of content_type_section */
				  								'content_type_section_id'=>$section_row['content_type_section_id']
				  							);
				  							$li_list.='<li class="'.$active.'"><a href="#tab_'.$section_key.'" data-toggle="tab">'.$section_row['section_title'].'</a></li>';
				  							$content_list.='<div class="tab-pane '.$active.'" id="tab_'.$section_key.'">'.$this->load->view('content_list_table', $view_data, true).'</div>';
				  							$active='';
										} 
									?>
            						<ul class="nav nav-tabs">
            							<?php echo $li_list; ?>
            						</ul>
            						<div class="tab-content">
              							<?php echo $content_list; ?>
              						</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			<?php } ?>
			<!-- Section List END -->


			<!-- Uploaded Files section -->
			<?php foreach ($uploaded_file as $field_files) {  ?>	
				<div class="box box-primary">
				  	<div class="box-header">
				  		<h3 class="box-title">
				  			<i class="fa fa-upload" aria-hidden="true"></i> Uploaded files of <b>
				  			<?php echo $field_title[$field_files[0]['content_type_field_id']]; ?></b>
				  		</h3>
				  		<div class="pull-right box-tools">
					  		<button type="button" class="btn btn-primary btn-xs btn-flat" data-widget="collapse"><i class="fa fa-minus"></i>
			                </button>
		                </div>
				  	</div>
				  	<div class="box-body" id="uploaded_list">
				  		<div class="row">
				  			<div class="col-md-12">
						  		<div  class="files-shortable" data-content_type_fields_id="<?php echo $field_files[0]['content_type_field_id']; ?>">
							  		<?php
							  			foreach ($field_files as $row_child) {
							  				echo $this->content_model->generate_edit_image($row_child);
							  			}
							  		?>
						  		</div>
					  		</div>
				  		</div>
				  	</div>
				</div>
	<?php 	} /* foreach of upload files end here */	
		} /* is_edit end here */	 
	?>

<?php echo $this->form->form_close(); ?>
<div id="clone_container">
<?php
	echo $clone_fields;
?>
</div>
<style type="text/css">
	.files-shortable .thumbnail {
	    box-sizing: content-box;
	}
	.thumbnail .caption {
	    word-wrap: break-word;
	}
</style>
<script src="<?php echo base_url().'public/plugins/ckeditor/ckeditor.js';?>"></script>
<link href="<?php echo base_url().'public/plugins/jquery-ui/jquery-ui.min.css';?>" rel="stylesheet" />
<script src="<?php echo base_url().'public/plugins/jquery-ui/jquery-ui.min.js';?>"></script>

<script type="text/javascript">
	$(document).ready(function() {
		/* set value='' for all clone fields */
		blank_clone_val();
		$('input[name="content_title"]').focus();

		/* Fill value of each select hidden control */
		$('.hidden-select-val').each(function() {
			var selected_text = $(this).parent().find('select option:selected').text();
			$(this).val(selected_text);
		});

		var form_element='.content-form';
		jQuery.validate({
            modules : 'file, security',
			form:form_element,
			validateHiddenInputs:false,
			scrollToTopOnError:false,
			onError:function($form) {
				setTimeout(function() {
					$first_ele = $form.find('.has-error').first();
					var $ele=$first_ele.find('.form-field');
					$ele.focus();
				},10);
			},
			beforeValidation:function() {
				alert('hi');
			}
        });

		/* Change value of hidden select on change of select */
		$('select.form-field').change(function() {
			var val=$(this).find('option:selected');
			var name=$(this).attr('name');
			$(this).parent().find('[name="val_'+name+'"]').remove();
			var hidden='';
			$(val).each(function() {
				var text=$(this).text();
				hidden+='<input type="hidden" name="val_'+name+'" value="'+text+'" />';
			});
			$(this).parent().append(hidden);
		});

		/* Add control for unlimited fields */
		$('.add_more_control').click(function() {
			var parent=$(this).attr('data-parent');	
			var clone_id=$(this).attr('data-clone_id');
			$('#'+parent).append($('#'+clone_id).html());
		});

		/* Remove fields */
		$(document).on('click','.remove_more_control', function() {
			$parent=$(this).parent().parent();
			var content_value_id=$parent.attr('data-content_value_id');
			if(content_value_id) {
				var hidden='<input type="hidden" name="deleted[]" value="'+content_value_id+'"/>';
				$('form[name="add_content_data"]').append(hidden);
			}
			$parent.remove();
		});

		/* Add uploader plgin */
		$('textarea.rich_editor').each(function() {
			CKEDITOR.replace($(this).attr('name'));
			CKEDITOR.config.disableNativeSpellChecker = false;
			CKEDITOR.config.allowedContent=true;
			CKEDITOR.config.protectedSource.push(/<i[^>]*><\/i>/g);
			CKEDITOR.config.contentsCss=[base_url+'public/front_assets/css/bootstrap.min.css',base_url+'public/front_assets/css/bootstrap-custom.css',base_url+'public/front_assets/css/genric.css',base_url+'public/front_assets/css/media.css',base_url+'public/front_assets/css/animate.css'];
			
			//CKEDITOR.config.extraPlugins = 'smiley';
			CKEDITOR.config.extraPlugins = 'cleanuploader,smiley,html5video,btgrid';
			/* Ignore to remove empty i tag */
			CKEDITOR.dtd.$removeEmpty['i'] = false
			CKEDITOR.dtd.$removeEmpty['span'] = false
		});

		/* Sortable functionality */
		$('.files-shortable').sortable();
    	$('.files-shortable').disableSelection();

    	/* Add value to hidden variable on stop */
    	$( ".files-shortable" ).on( "sortstop", function( event, ui ) {
	        var content_type_fields=$(this).attr('data-content_type_fields_id');
	        var sortedIDs =$(this).sortable("toArray");
	        var hidden_input='<input type="hidden" name="file_order['+content_type_fields+']" value="'+sortedIDs+'"/>';
	        /* Check if field available */
			var available=$('[name="file_order['+content_type_fields+']"]').attr('type');        
			if(typeof available !== 'undefined') {
				$('[name="file_order['+content_type_fields+']"]').remove();
			}
			$('form[name="add_content_data"]').append(hidden_input);
	    });

	});

	function delete_img(ele,num) {
		var id={};
		id.content_value_id=ele.id;
		swal({
				title: "Are you sure?",
				text: "You will not be able to recover this file!",
				type: "warning",
				showCancelButton: true,
				confirmButtonColor: "#DD6B55",
				confirmButtonText: "Yes, delete it!",
				cancelButtonText: "No, cancel pls!",
				closeOnConfirm: false,
				closeOnCancel: true
			},
			function(isConfirm){
				if (isConfirm) {
					loading();
					$.ajax({
						  url     : base_url+"custom_content/content/delete_content_media",
						  type    : 'POST',
						  data    : id,
						  success : function(data) {
						  	$('#overlay').remove();
							data=$.parseJSON(data);
							console.log(data);
							if(data.status.toString() == '1') {
								$(ele).parent().parent().parent().remove();
								$( ".files-shortable" ).sortable("refresh");
								setTimeout(function() {
									swal({
										type:'success',
										title:'Deleted..!',
										text:data.message
									})
								},100);
							}
							else {
								swal({
									type:'error',
									title:'Error',
									text:data.message
								})
							}
						  }
					});
				}
		});
	}

	function edit_img_title(ele, content_value_id) {
		var parent=$(ele).parent();
		var title=$(parent).find('.media_title').text();
		$(parent).find('.media_title').hide();
		$(parent).append('<input typ="text" name="changed_media_title['+content_value_id+']" value="'+title+'"/>');
	}

	/* Removed filled value from clone dives */
	function blank_clone_val() {
		$('#clone_container .form-field').each(function() {
			$(this).removeAttr('value');
			$(this).text('');
			$(this).find('option').removeAttr('selected');
		});
	}

</script>
<script type="text/javascript">
	$(document).ready(function() {
		<?php
			/* Database loaded js */
			if(!empty($content_type['form_js'])) {
				echo $content_type['form_js'];
			}
		?>
	});
</script>
<style type="text/css">
	<?php
		/* Database loaded css */
		if(!empty($content_type['form_css'])) {
			echo $content_type['form_css'];
		}
	?>
</style>