<?php $base_url=base_url(); ?>
<div class="box box-primary">
  	<div class="box-header with-border">
  		<?php
  		/* Check Add new content permission */
  		if($content_list['is_singular'] == 0){
		if($this->content_type_model->check_content_permission($content_type_id, 1,false, 0)) {
		if($content_list['content_type_category'] != "webform") { 
		?>
			<a href="<?php echo $base_url.'custom_content/content/add_content_data/'.$content_type_id; ?>" class="btn btn-sm btn-primary" title="Add New" ><i class="fa fa-plus"></i> Add New</a>
		<?php } 
		} }?>

		<?php if($this->content_type_model->check_content_permission($content_type_id, 8,false, 0) && ($content_type['content_type_category']!='webform')) { ?>
		<a class="btn btn-sm btn-primary" id="save_order" title="Save Order" ><i class="fa fa-sort"></i> Save Order</a>
		<?php } ?>
  		
  		<a data-toggle="modal" data-target="#search_content" class="btn btn-primary btn-sm"><i class="fa fa-search"></i> Search</a>

  		<?php if(!empty($clearfilter)) { ?>
  		<a href="<?php echo $base_url.'custom_content/content/view_content/'.$content_type_id; ?>" class="btn btn-primary btn-sm"><i class="fa fa-search-minus"></i> Clear Search</a>
  		<?php } ?>

  		 <a href="<?php echo base_url()."custom_content/content/export_content/?content_type_id=".$content_type_id."&".http_build_query($this->input->get()); ?>" class="btn btn-sm btn-primary" title="Reset Search"><i class="fa fa-file-excel-o"></i> Export</a>


  		<span class="pull-right"><?php if (isset($pagination)){ echo $pagination; } ?></span>
  	</div>
  	<div class="box-body">
  		<div id="order-warning" class="alert alert-warning alert-dismissable" style="display:none;">
            <span>Please click on Save order to change the order of Fields</span>
        </div> 

        <div id="save-msg" class="alert alert-success alert-dismissable" style="display:none;">
            <span></span>
        </div> 

		<div class="table-responsive">
	  		<table class="table table-striped table-bordered table-hover full-height-datatable">
				<thead>
		  			<tr>

		  			<?php 
		  			/* Ignore title and reorder column in table */
		  			if($content_type['content_type_category']=='content' || $content_type['content_type_category']=='section_content' ) { ?>
			  			<th width="5">#</th>
			  			<th>Title</th>
		  			<?php } ?>
		  			<?php  foreach ($content_type_field as $key => $field_title) {  ?>
						<th><?php echo $field_title['field_title']; ?></th>
					<?php } ?>

					<?php if($content_type['content_type_category']=='content' || $content_type['content_type_category']=='section_content' ) { ?>
						<th width="5%">Status</th>
						<th width="5%">Publish</th>
					<?php } ?>

					<?php if($content_type['content_type_category']=='webform') { ?>
						<th>Time</th>
					<?php } ?>

						<th width="5%">Action</th>
		  			</tr>
				</thead>
				<tbody id="sortable">
				<?php foreach($content_value_list as $value_row) {
					$encrypt_id=encrypt_id($value_row['content']['content_id']);
					echo '<tr id="'.$encrypt_id.'">';
					if($content_type['content_type_category']=='content' || $content_type['content_type_category']=='section_content' ) {
						echo '<td><i class="cursor-move fa fa-fw fa-arrows"></i></td><td>'.$value_row['content']['content_title'].'</td>';
					}
					foreach ($content_type_field as $field_title) {
						if(isset($value_row[$field_title['machine_name']])) {
							if(!empty($value_row[$field_title['machine_name']][0]['content_reference_value'])) {
								$content_values=array_column($value_row[$field_title['machine_name']], 'content_reference_value');
								echo '<td>'.word_limiter(strip_tags(implode(', ', $content_values)),20).'</td>';
							}
							else if(!empty($value_row[$field_title['machine_name']][0]['content_value'])) {
								$content_values=array_column($value_row[$field_title['machine_name']], 'content_value');
								echo '<td>'.word_limiter(strip_tags(implode(', ', $content_values)),20).'</td>';
							}
							else if(!empty($value_row[$field_title['machine_name']][0]['media_path']) && $value_row[$field_title['machine_name']][0]['media_type']=='jpg' || $value_row[$field_title['machine_name']][0]['media_type']=='jpeg' || $value_row[$field_title['machine_name']][0]['media_type']=='png' || $value_row[$field_title['machine_name']][0]['media_type']=='gif') {
								echo '<td> <img src="'.$base_url.$value_row[$field_title['machine_name']][0]['media_path'].'" width="100" ></td>';
							}
							else if(!empty($value_row[$field_title['machine_name']][0]['media_path'])) {
								echo '<td><a href="'.$base_url.$value_row[$field_title['machine_name']][0]['media_path'].'" target="_blank" >Download</a></td>';
							}
							else {
								echo '<td></td>';
							}
						}
						else {
								echo '<td></td>';
						}
					}
					?>
					<?php if($content_type['content_type_category']=='content' || $content_type['content_type_category']=='section_content' ) { ?>
					<td><?php
						if($value_row['content']['content_status']=='Approved' || $value_row['content']['content_status']==null) {
							echo '<label class="label label-success">Approved</label>';
						}
						else if($value_row['content']['content_status']=='pending') {
							echo '<label class="label label-warning">Pending</label>';
						}
						else if($value_row['content']['content_status']=='blocked') {
							echo '<label class="label label-danger">Blocked</label>';
						}
						?>
					</td>

					<td><?php
						if($value_row['content']['content_published']==1) {
							echo '<label class="label label-success">Publish</label>';
						}
						else {
							echo '<label class="label label-danger">Unpublish</label>';
						}
						?>
					</td>
					<?php } ?>

					<?php if($content_type['content_type_category']=='webform') {
						echo '<td>'.dateformat($value_row['content']['created_at'],0, 1).'</td>';
					} ?>

					<td>
							<div class="btn-group options">
								<button type="button" class="btn btn-default btn-sm" dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
									<i class="fa fa-cog"></i> Options
								</button>
								 
								<ul class="dropdown-menu">
									<?php if(($content_type['content_type_category']=='content' || $content_type['content_type_category']=='section_content')  && $this->content_type_model->check_content_permission($content_type_id, 2,false, 0) && $this->content_type_model->check_content_permission($content_type_id,3,false, 0)) {  ?>
									<li>
										<a href="<?php echo base_url()."custom_content/content/edit_content_data/".$encrypt_id; ?>">
											<i class="fa fa-edit fa-margin"></i> Edit
										</a>
									</li>
									<?php } ?>
									<?php if($this->content_type_model->check_content_permission($content_type_id, array(4,10),false, 0, 'or') || $this->content_type_model->check_content_permission($content_type_id, 5,false, 0)) { ?>
									<li>
										<a data-id="<?php echo $encrypt_id; ?>" class="pointer delete-content-value">
											<i class="fa fa-trash-o fa-margin"></i> Delete
										</a>
									</li>
									<?php } ?>
									<?php if(($content_type['content_type_category']=='content' || $content_type['content_type_category']=='section_content') && $this->content_type_model->check_content_permission($content_type_id, 8,false, 0)) { ?>
									<li>
										<a class="pointer table-list-edit" data-editurl="custom_content/content/change_status" data-editid="<?php echo $value_row['content']['content_id']; ?>" data-modeltitle="Change Content Status">
											<i class="fa fa-unlock fa-margin"></i> Change Status
										</a>
									</li>
									<?php } ?>
								</ul>
							</div>
						</td>

				<?php }  echo '</tr>'; ?> 
				</tbody>
	  		</table>
	  	</div>
  	</div>
</div>

<!-- Modal -->
<div class="modal fade" id="search_content" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" data-backdrop="static">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
				<h4 class="modal-title" id="myModalLabel">Search Content</h4>
			</div>
			<div class="modal-body">
				<?php 
					$form_model=$this->input->get();
					/* Fill old data */
					$old_data=$this->session->flashdata('old_data');
					if(!empty($old_data)) {
						$form_model=$old_data;  
					}
					echo $this->form->form_model($form_model, $base_url.'custom_content/content/view_content/'.$content_type_id.'/', array('name'=>'search_content','id'=>'search_content_form', 'method'=>'get')); 
				?>
					<div class="row">
						<div class="col-md-12">
							<div id="error_div"></div>
						</div>
						
						<div class="col-md-12">
							<?php if($content_type['content_type_category']=='content' || $content_type['content_type_category']=='section_content' ) { ?>
							<div class="col-md-12">
								<label>Content Title</label>
								<?php 
								$other_option=array(
									'class'=>'form-control',
									'placeholder'=>'Content Name',
									//'data-validation'=>'required',
								);
								echo $this->form->form_input('content_title',$other_option); 
								?>
							</div>
							<div class="clearfix"></div><br>
							<div class="col-md-6 col-sm-6">
								<div class="form-group">
									<label>Status</label>
									<?php 
									$other_option=array(
										'class'=>'form-control',
										'placeholder'=>'status',
									);
									$option=array(''=>'All','Approved'=>'Approved','pending'=>'Pending','blocked'=>'Blocked');
									echo $this->form->form_dropdown('content_status', $option,'','', $other_option); 
									?>
								</div>
							</div>
							<div class="col-md-6 col-sm-6">
								<div class="form-group">
									<label>Publish</label>
									<?php 
									$other_option=array(
										'class'=>'form-control',
										'placeholder'=>'status',
									);
									$option=array(''=>'All','1'=>'Publish','0'=>'Unpublish');
									echo $this->form->form_dropdown('content_published', $option,'','', $other_option); 
									?>
								</div>
							</div>
							<div class="clearfix"></div>



							<h4> field value search </h4>
							<div class="col-md-6">
								<label>Field Type</label>
								<?php 
								$other_option=array(
									'class'=>'form-control',
									'placeholder'=>'Field Type',
								);
								echo $this->form->form_dropdown_fromdatabase('content_type_field_id',$search_fields,'content_type_field_id','field_title','', $other_option); 
								?>
							</div> 
							<?php  }  ?>

							<div class="col-md-6">
								<label>Keyword</label>
								<?php 
								$other_option=array(
									'class'=>'form-control',
									'placeholder'=>'Keyword',
								);
								echo $this->form->form_input('keyword',$other_option); 
								?>
							</div>	

						</div>
					</div>
					<div class="row"><br/>
						<div class="modal-footer">
							<input type="submit" name="submit"  class="btn btn-primary" value="Search">
							<input type="reset" name="reset"  class="btn btn-default" value="Reset">
							<button type="button" class="btn btn-danger" data-dismiss="modal">Close</button>
						</div>							      			
					</div>							      			
				</form>
			</div>
		</div>
	</div>
</div>	

<link href="<?php echo base_url().'public/plugins/jquery-ui/jquery-ui.min.css';?>" rel="stylesheet" />
<script src="<?php echo base_url().'public/plugins/jquery-ui/jquery-ui.min.js';?>"></script>

<script type="text/javascript">
	$(document).ready(function() {

		jQuery.validate({
			form : '#search_content_form',
		});

		$('input[name="keyword"]').on('change', function() {
			var keyword=$('input[name="keyword"]').val();
			if(keyword!='') {
				$('select[name="content_type_field_id"]').attr('data-validation','required');
			}
			else {
				$('select[name="content_type_field_id"]').removeAttr('data-validation','required');
			}
		});

		$("#sortable" ).sortable({
            handle: ".cursor-move"
        });
        $("#sortable" ).disableSelection();
        $("#sortable" ).on("sortstop", function( event, ui ) {
            $('#order-warning').show();
            $('#save-msg').hide();
        });
        $('#save_order').click(function() {
            sortable_list();
        });

		$('.delete-content-value').click(function() {
			var id=$(this).attr('data-id');
			swal({
				title: "Are you sure?",
				text: "Content type to delete?",
				type: "warning",
				showCancelButton: true,
				confirmButtonColor: "#DD6B55",
				confirmButtonText: "Yes, delete it!",
				closeOnConfirm: false
			},
			function(){
				window.location = base_url+'custom_content/content/delete_content/'+id;
			});
		});
	})

	function sortable_list() {

        var sortedIDs =$( "#sortable" ).sortable("toArray").reverse();
        $.ajax({
            url     : base_url+"custom_content/content/content_save_order",
            type    : 'POST',
            data    : {'ids':sortedIDs},
            success : function(data){
                data=$.parseJSON(data);
                remove_loading();
                if(data.status == '1') {
                    $('#order-warning').hide();
                    $('#save-msg').show();
                    $('#save-msg').find('span').html('');
                    $('#save-msg').find('span').html(data.message);
                }
                else {
                    swal("Oops...", data.message, "error");
                }
            }
        });
    }

</script>