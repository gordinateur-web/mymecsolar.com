<?php
$base_url = base_url();
$form_model=array();
if(!empty($edit_data)) {
	$form_model=$edit_data;
}
/* Fill old data */
$old_data=$this->session->flashdata('old_data');
if(!empty($old_data)) {
	$form_model=$old_data;  
}
echo $this->form->form_model($form_model, $base_url.'custom_content/content/status_save/',array('name'=>'save_status_save','id'=>'status_save_form', 'class'=>'validate-form'));
if(!empty($edit_data)) {
	echo $this->form->form_hidden('content_id');
}
?>

<div class="row">
	<div class="box-body">
		<div id="model_errors"></div>

		<div class="col-md-6 col-sm-6">
			<div class="form-group">
				<label>Status<span class="text-danger">*</span></label>
				<?php 
				$other_option=array(
					'class'=>'form-control',
					'placeholder'=>'status',
					'data-validation'=>'required'
				);
				$option=array('Approved'=>'Approved','pending'=>'Pending','blocked'=>'Blocked');
				echo $this->form->form_dropdown('content_status', $option,'','', $other_option); 
				?>
			</div>
		</div>
		
	</div>
	
</div>

<div class="box-footer with-border">
	<div class="box-tools pull-right">
		<input type="reset" class="btn btn-primary" value="Reset">
		<input type="submit" class="btn btn-danger" value="Submit">
	</div>
</div>
<?php echo $this->form->form_close(); ?>

