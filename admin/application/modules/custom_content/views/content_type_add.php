<style>
	.label_singular{
		margin: 21px;
	}
</style>
<div class="box box-primary">
	<?php
		$edit_data=array();
		$hidden=array();
		if(!empty($edit_content_type)) {
			$edit_data = $edit_content_type;
			$hidden=array('content_type_id'=>$edit_data['content_type_id']);
		}
		
		echo $this->form->form_model($edit_data, base_url()."custom_content/content_type/save_content_type",array('name'=>'add_content_type','method'=>'post','id'=>'content_type_form'), $hidden);
	?>
  	<div class="box-body">
		<div class="form-group row">
			<div class="form-group col-sm-5">
				<label>Title<span class="text-danger">*</span></label>
				<?php
					echo $this->form->form_input('content_type_title',array(
						'placeholder'=>'Content type title',
						'class'=>'form-control',
						'data-validation'=>'required',
						'title'=>'Title',
					));
				?>
				<div class="">Content should be unique</div>
			</div>

			<div class="form-group col-sm-2">
		        <label>Status <span class="text-danger">*</span></label>
				<?php
					$comment_rules=array(
						'1'=>'Enabled',
						'0'=>'Disabled',
					);
					echo $this->form->form_dropdown('content_type_status',$comment_rules,'',array(
						'class'=>'form-control',
						'data-validation'=>'required',
					));
				?>
			</div>

			<div class="form-group col-sm-2">
		        <label>Type <span class="text-danger">*</span></label>
				<?php
					$content_type_category=content_type_category();
					$type_attr = array(
						'class'=>'form-control',
						'data-validation'=>'required',
					);

					/* readonly for content type which has content */
					if(!empty($content_count[0]['count'])) {
						$type_attr['disabled']='disabled';
					}

					echo $this->form->form_dropdown('content_type_category',$content_type_category,false,$type_attr);
				?>
			</div>
			<div class="form-group col-sm-2">
				<label class='label_singular'>
					<?php
					echo $this->form->form_checkbox('is_singular',1);
					echo 'Is Singular.';
					?>
				</label>
			</div>

	        <div class="form-group col-md-12">
		        <label>Description (About the Content type)</label>
				<?php
					echo $this->form->form_textarea('content_type_description',array(
						'placeholder'=>'Enter content type description',
						'class'=>'form-control',
					));
				?>
			</div>
			<div class="clearfix"></div>

			<div id="webform-fields">
				<div class="col-sm-12">
					<h4>Web form settings</h4>
				</div>
				<div class="form-group col-sm-3">
					<label>Send email <span class="text-danger">*</span></label>
					<?php 
						$type_attr = array(
							'class'=>'form-control',
							'data-validation'=>'required',
						);
						$yes_option=array('0'=>'No','1'=>'Yes');
						echo $this->form->form_dropdown('send_email',$yes_option,'',$type_attr);
					?>
				</div>
				<div class="form-group col-sm-3">
					<label>Send SMS</label>
					<?php 
						$type_attr = array(
							'class'=>'form-control',
							'data-validation'=>'required',
						);
						$yes_option=array('0'=>'No','1'=>'Yes');
						echo $this->form->form_dropdown('send_sms',$yes_option,'',$type_attr);
					?>
				</div>
				<div class="form-group col-sm-3">
					<label>Send Email to </label>
					<?php 
						echo $this->form->form_input('send_email_to',array(
						'placeholder'=>'Email Send to',
						'class'=>'form-control',
						//'data-validation'=>'required',
						'title'=>'Send to',
					));
					?>
				</div>

				<div class="form-group col-sm-3">
					<label>Send SMS to </label>
					<?php 
						echo $this->form->form_input('send_sms_to',array(
						'placeholder'=>'Email Send to',
						'class'=>'form-control',
						//'data-validation'=>'required',
						'title'=>'Send to',
					));
					?>
				</div>
				<div class="clearfix"></div>

				<div class="form-group col-sm-3">
					<label>From Email</label>
					<?php 
						echo $this->form->form_input('send_email_from',array(
						'placeholder'=>'From Email address',
						'class'=>'form-control',
						'title'=>'From email',
						//'data-validation'=>'email',
					));
					?>
				</div>

				<div class="form-group col-sm-3">
					<label>Admin Email Subject</label>
					<?php 
						echo $this->form->form_input('owner_email_subject',array(
						'placeholder'=>'Admin email subject',
						'class'=>'form-control',
						'title'=>'Admin email subject',
						//'data-validation'=>'required',
					));
					?>
				</div>

				<div class="form-group col-sm-3">
					<label>Sender email Subject</label>
					<?php 
						echo $this->form->form_input('sender_email_subject',array(
						'placeholder'=>'Sender email subject',
						'class'=>'form-control',
						'title'=>'Sender email subject',
					));
					?>
				</div>

				<div class="form-group col-sm-3">
					<label>Sender email Field</label>
					<?php 
						echo $this->form->form_input('thanks_email_field',array(
						'placeholder'=>'Content type field',
						'class'=>'form-control',
						'title'=>'Sender email Field',
					));
					?>
					<span class="help-text">Email field machine name</span>
				</div>
				<div class="clearfix"></div>

				<div class="form-group col-sm-3">
					<label>Success message <span class="text-danger">*</span></label>
					<?php 
						echo $this->form->form_input('success_message',array(
						'placeholder'=>'Success message',
						'class'=>'form-control',
						'title'=>'Sender email subject',
						//'data-validation'=>'required',
					));
					?>
				</div>

				<div class="form-group col-sm-3">
					<label>Success redirect</label>
					<?php 
						echo $this->form->form_input('redirect_to',array(
						'placeholder'=>'Redirect after success URL',
						'class'=>'form-control',
						'title'=>'Sender email subject',
					));
					?>
				</div>
				<div class="clearfix"></div>
				<div class="form-group col-sm-7">
					<label>Sender Email template  <span class="text-danger">*</span></label>
					<?php 
						echo $this->form->form_textarea('thanks_email',array(
							'placeholder'=>'Thanks email to sender',
							'class'=>'form-control',
							'id'=>'thanks_email'
						));
					?>
				</div>
			</div>

			<div class="form-group col-md-12">
				<div class="col-sm-6">
					<h4>Settings</h4>
					<div class="form-group" id="content_setting">
						<div class="checkbox">
							<label>
								<?php
									echo $this->form->form_checkbox('include_in_search',1);
									echo 'Include content in search result.';
								?>
							</label>
						</div>

						<div class="checkbox">
							<label>
								<?php
									echo $this->form->form_checkbox('publish_approval',1);
									echo 'Admin Approval needed for Publish post.';
								?>
							</label>
						</div>

						<div class="checkbox">
							<label>
								<?php
									echo $this->form->form_checkbox('allow_like',1);
									echo 'Allow Users to Like this content post.';
								?>
							</label>
						</div>
						<div class="checkbox">
							<label>
								<?php
									echo $this->form->form_checkbox('allow_share',1);
									echo 'Allow Users to Share this content post.';
								?>
							</label>
						</div>
						<div class="checkbox">
							<label>
								<?php
									echo $this->form->form_checkbox('allow_comment',1);
									echo 'Allow Users to Comment on this content post.';
								?>
							</label>
						</div>
						<div class="checkbox">
							<label>
								<?php
									echo $this->form->form_checkbox('comment_approval',1);
									echo 'Admin Approval needed for comment.';
								?>
							</label>
						</div>
						<div class="checkbox">
							<label>
								<?php
									echo $this->form->form_checkbox('like_notification',1);
									echo 'Send notification to post owner when someone Like post.';
								?>
							</label>
						</div>
						<div class="checkbox">
							<label>
								<?php
									echo $this->form->form_checkbox('comment_notification',1);
									echo 'Send notification to post owner when someone comment on post.';
								?>
							</label>
						</div>
					</div>

					<div class="form-group" id="webform_setting" style="display: none">
						<div class="checkbox">
							<label>
								<?php
									echo $this->form->form_checkbox('loggedin_only',1);
									echo 'Allow only logged-in users to submit form';
								?>
							</label>
						</div>
					</div>
				</div>
				<div class="col-sm-6">
					<div id="content-sections">
						<h4 style="float: left;">Sections</h4>
						<button class="btn btn-primary btn-sm pull-right" id="add_section" type="button">Add Section</button>
						<table class="table table-bordered" id="section_container">
							<thead>
								<tr>
									<td>Title</td>
									<td>Content type</td>
									<td>#</td>
								</tr>
								<?php 
									if(!empty($section_list)) {
										foreach ($section_list as $section_row) { ?>
										<tr>
											<td>
												<?php echo $this->form->form_input('section_title['.$section_row['content_type_section_id'].']',array('class'=>'form-control')) ?>
											</td>
											<td>
												<?php
													echo $this->form->form_dropdown_fromdatabase('section_content_type['.$section_row['content_type_section_id'].']',$content_type_list,'content_type_id','content_type_title','',array('class'=>'form-control'));
												?>
											</td>
											<td>
												<a style="cursor: pointer;" title="remove section" class="section_remove" data-id="<?php echo $section_row['content_type_section_id'] ?>"><i class="fa fa-trash"></i></a>
											</td>
										</tr>
								<?php } } ?>
							</thead>
							<tbody>
								
							</tbody>
						</table>
					</div>
				</div>

			</div>

			<div class="form-group col-md-12">
				<h4>Permissions</h4>
				<table class="table table-bordered">
					<thead>
						<tr>
							<th>Permission</th>
							<?php
								$role_title=$this->config->item('role_table_title');
								foreach ($role_list as $role) {
									echo '<th>'.$role[$role_title].'&nbsp;&nbsp;&nbsp;<input type="checkbox" class="check_all" data-role="'.$role['role_id'].'"/></th>';
								}
							?>
						</tr>
					</thead>
					<tbody>
						<?php
							/* content permission list */
							foreach ($permission_list as $permission) {
								echo '<tr class="content_permission_list">';
								echo '<td>'.$permission['permission_title'].'</td>';
								foreach ($role_list as $role) {
									echo '<td>';
									echo $this->form->form_checkbox($permission['module_permission_master_id'].'['.$role['role_id'].']',$role['role_id']);
									echo '</td>';
								}
								echo '</tr>';
							}

							/* Webform permission list */
							foreach ($webform_permission as $permission) {
								echo '<tr class="webform_permission_list" style="display:none">';
								echo '<td>'.$permission['permission_title'].'</td>';
								foreach ($role_list as $role) {
									echo '<td>';
									echo $this->form->form_checkbox($permission['module_permission_master_id'].'['.$role['role_id'].']',$role['role_id']);
									echo '</td>';
								}
								echo '</tr>';
							}
						?>
					</tbody>
				</table>
			</div>

			<div class="form-group col-md-12">
				<a class="pull-right btn btn-default" id="more_options">More Options</a>
			</div>
			<div class="clearfix"></div>
			<div id="code_editor" style="display: none;">
				<div class="form-group col-sm-6">
			        <label>Javascript code</label>
					<?php
						echo $this->form->form_textarea('form_js',array(
							'placeholder'=>'Javascript Code',
							'class'=>'form-control',
							'data-validation'=>'required',
							'id'=>"content_js",
							"rows"=>"15"
						));
					?>
				</div>

				<div class="form-group col-sm-6">
			        <label>CSS code</label>
					<?php
						echo $this->form->form_textarea('form_css',array(
							'placeholder'=>'Css Code',
							'class'=>'form-control',
							'data-validation'=>'required',
							'id'=>'content_css',
							"rows"=>"15"
						));
					?>
				</div>
			</div>
  	</div>
    <div class="modal-footer">
	    <div class="pull-right">
	      	<button type="reset" class="btn btn-default">Clear</button>
	      	<button type="submit" class="btn btn-primary">Submit</button>
	    	<button onclick="window.history.back();" class="btn btn-default">Close</button>
	    </div>
	</div><!-- /.box-footer -->
	<?php echo $this->form->form_close(); ?>
</div>
<div id="section_clone_rows" style="display: none">
	<table>
		<tbody>
			<tr>
				<td>
					<input type="input" name="section_title[]" class="form-control">
				</td>
				<td>
					<?php
						echo $this->form->form_dropdown_fromdatabase('section_content_type[]',$content_type_list,'content_type_id','content_type_title','',array('class'=>'form-control'));
					?>
				</td>
				<td>
					<a style="cursor: pointer;" title="remove section" class="section_remove"><i class="fa fa-trash"></i></a>
				</td>
			</tr>
		</tbody>
	</table>
</div>
<script type="text/javascript" src="<?php echo base_url()."public/plugins/edit_area/edit_area_full.js" ?>"></script>
<script src="<?php echo base_url().'public/plugins/ckeditor/ckeditor.js';?>"></script>
<script type="text/javascript">
	var code_init=false;
	$(document).ready(function() {
		/* handler for Show and hide code editor div */
		$('#more_options').click(function() {
			$('#code_editor').toggle();
			init_editor();
		});

		/* Show code editor if css or js has value */
		if($('#content_css').val()!='' || $('#content_js').val()!='') {
			$('#more_options').trigger('click');
		}

        jQuery.validate({
            form : '#content_type_form',
        });

        CKEDITOR.replace('thanks_email');


        /* Check all permission for role */
        $('.check_all').change(function() {
        	var role_id=$(this).attr('data-role');
        	if($(this).is(":checked")) {
        		$('input[name*="['+role_id+']"]').prop('checked',true);
        	}
        	else {
        		$('input[name*="['+role_id+']"]').prop('checked',false);
        	}
        });

        /* Uncheck all setting based on type */
        $('select[name="content_type_category"]').change(function() {
        	var type=$(this).val();
        	if(type=='content') {
        		$('input[type="checkbox"]','#webform_setting').prop('checked', false);
        		$('#webform_setting, .webform_permission_list, #webform-fields').hide();
        		$('#content_setting,.content_permission_list, #content-sections').show();
        	}
        	else if(type=='webform') {
        		$('input[type="checkbox"]','#content_setting').prop('checked', false);
        		$('#content_setting, .content_permission_list, #content-sections').hide();
        		$('#webform_setting, .webform_permission_list, #webform-fields').show();
        	}
        	else if(type=='section_content') {
        		$('input[type="checkbox"]','#webform_setting').prop('checked', false);
        		$('#webform_setting, .webform_permission_list, #webform-fields,  #content-sections').hide();
        		$('#content_setting,.content_permission_list').show();
        	}

        });
        $('select[name="content_type_category"]').trigger('change');

        $('#add_section').click(function() {
        	$('#section_container tbody').append($('#section_clone_rows tbody').html());
        });

        $(document).on('click','.section_remove',function() {
        	$(this).parent().parent().remove();
        	var id=$(this).attr('data-id');
        	if(typeof id !='undefined' && id!='') {
        		$("#content_type_form").append('<input type="hidden" name="deleted_section[]" value="'+id+'"/>');
        	}
        });

    });

	function init_editor(argument) {
		/* Init code editor on div visible */
		if(code_init==false) {
			editAreaLoader.init({
				id : "content_css",
				syntax: "css",
				start_highlight: true
			});

			editAreaLoader.init({
				id : "content_js",
				syntax: "js",
				start_highlight: true
			});
			code_init=true;
		}
	}
</script>