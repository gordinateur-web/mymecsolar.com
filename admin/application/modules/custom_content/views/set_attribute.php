<?php
$validation_array = explode("|",$field['field_validation']);
?>
<div class="box box-primary">
	<?php
	$attribute=array(
		'name'=>'set_attribute',
		'class'=>'validate-form',
	);

	$old_data=$this->session->flashdata('old_data');
	if(!empty($old_data)) {
		/* Overwrite old post data */
		$field=array_replace($field, $old_data);
	}

	echo $this->form->form_model($field,base_url()."custom_content/content_type/save_attribute",$attribute);
	echo $this->form->form_hidden("content_type_field_id");
	?>
	<div class="box-body">
		<div class="row">
			<div class="col-sm-6">
				<div class="form-group">
					<label>Label <span class="text-danger">*</span></label>
					<?php
						$attribute=array(
							'placeholder'=>"Field Label",
							"class"=>"form-control",
							"data-validation"=>"required"
						);
						echo $this->form->form_input('field_title',$attribute);
					?>
				</div>
			</div>

			<!-- End not for html content -->
			<?php if($field['field_type_id']!=14) { ?>
				<div class="col-sm-2">
					<div class="form-group">
						<div class="checkbox" style="margin-top: 20px">
							<label>
								<?php
									$is_require=false;
									if(in_array("required", $validation_array)) {
										$is_require=true;
									}
									echo $this->form->form_checkbox('is_require',1,null,$is_require);
									echo 'Required';
								?>
							</label>
							<label>
								<?php
									echo $this->form->form_checkbox('show_in_listview',1);
									echo ' view in list';
								?>
							</label>
						</div>
					</div>
				</div>

				<div class="col-sm-2">
					<div class="form-group">
						<label>Number of values</label>
						<?php
						$selected_num=$field['number_of_values'];
						$select_options=array(
							'1'=>'1','2'=>'2','3'=>'3','4'=>'4','5'=>'5','6'=>'6','7'=>'7','8'=>'8','9'=>'9','10'=>'10','0'=>'Unlimited'
						);
						
						/* Single and multiple value for select, content ref and table ref field */
						if($field['field_type_id']==5 || $field['field_type_id']==8 || $field['field_type_id']==13) {
							$select_options=array(
								'1'=>'Single','0'=>'Multiple'
							);
						}
						
						echo $this->form->form_dropdown('number_of_values',$select_options,null,array(
							"class"=>"form-control",
							"data-validation"=>"required"
						));
						?>
					</div>
				</div>
			<?php } ?>

			<div class="col-sm-2">
				<div class="form-group">
					<label>Rank</label>
					<?php
					$attribute=array(
						'placeholder'=>"Rank order of field",
						"class"=>"form-control number-field",
						"data-validation"=>"required,number"
					);
					echo $this->form->form_input('rank',$attribute);
					?>
				</div>
			</div>
			<div class="clearfix"></div>

			<?php if($field['field_type_id']!=14) { ?>
				<div class="col-sm-6">
					<div class="form-group">
						<label>Custom Validation</label>
						<?php
						$attribute=array(
							'placeholder'=>"Enter custom validation here",
							"class"=>"form-control",
						);
						echo $this->form->form_input('custom_validation',$attribute);
						?>
						<span class="help-block">Regex validation, Don't include Delimiter use direct expression</span>
					</div>
				</div>

				<div class="col-sm-6">
					<div class="form-group">
						<label>Default Value</label>
						<?php
							$attribute=array(
								'placeholder'=>"Enter default value if any",
								"class"=>"form-control",
							);
							echo $this->form->form_input('default_value',$attribute);
						?>
					</div>
				</div>
				<div class="clearfix"></div>
			<?php } /* End not for html content */ ?>


			<!-- For text fields and textarea -->
			<?php if ($field['field_type_id'] == 1 || $field['field_type_id'] == 3) { ?>
				<div class="col-sm-6">
					<div class="form-group">
						<label>Min Length</label>
						<?php
							$attribute=array(
								'placeholder'=>"Length in number",
								"class"=>"form-control number-field",
								"data-validation"=>"number",
								"data-validation-optional"=>"true"
							);
							$min_length_val="";
							$min_length=preg_grep('/^min_length/',$validation_array);
							if(count($min_length)>0) {
								foreach ($min_length as $key => $value) {
									$temp = explode(":",$value);
									$min_length_val = $temp[1];
								}
							}

							echo $this->form->form_input('min_len',$attribute, $min_length_val);
						?>
					</div>
				</div>
				<div class="col-sm-6">
					<div class="form-group">
						<label>Max Length</label>
						<?php
							$attribute=array(
								'placeholder'=>"Length in number",
								"class"=>"form-control number-field",
								"data-validation"=>"number",
								"data-validation-optional"=>"true"
							);

							$maxlen_val='';
							$max_length=preg_grep('/^max_length/',$validation_array);
							if(count($max_length)>0) {
								foreach ($max_length as $key => $value) {
									$temp = explode(":",$value);
									$maxlen_val=$temp[1];
								}
							}

							echo $this->form->form_input('max_len',$attribute, $maxlen_val);
						?>
					</div>
				</div>
			<?php } /* text fields and textarea end */ ?>


			<!-- For number fields -->
			<?php if ($field['field_type_id'] == 2) { ?>
				<div class="col-sm-6">
					<div class="form-group">
						<label>Min</label>
						<?php
							$attribute=array(
								'placeholder'=>"Min value in number",
								"class"=>"form-control number-field",
								"data-validation"=>"number",
								"data-validation-optional"=>"true"
							);
							$min_vals='';
							$min=preg_grep('/^min/',$validation_array);
							if(count($min)>0) {
								foreach ($min as $key => $value) {
									$temp = explode(":",$value);
									$min_vals=$temp[1];
								}
							}
							echo $this->form->form_input('min',$attribute, $min_vals);
						?>
					</div>
				</div>
				<div class="col-sm-6">
					<div class="form-group">
						<label>Max</label>
						<?php
							$attribute=array(
								'placeholder'=>"Max value in number",
								"class"=>"form-control number-field",
								"data-validation"=>"number",
								"data-validation-optional"=>"true"
							);
							$max_vals='';
							$max=preg_grep('/^max/',$validation_array);
							if(count($max)>0) {
								foreach ($max as $key => $value) {
									$temp = explode(":",$value);
									$max_vals=$temp[1];
								}
							}
							echo $this->form->form_input('max',$attribute, $max_vals);
						?>
					</div>
				</div>
			<?php } /* Number fields end */ ?>


			<!-- For textarea and Editor fields -->
			<?php if ($field['field_type_id'] == 3 || $field['field_type_id'] == 4) { ?>
				<div class="col-sm-6">
					<div class="form-group">
						<label>Clos</label>
						<?php
							$attribute=array(
								'placeholder'=>"Columns in number",
								"class"=>"form-control number-field",
								"data-validation"=>"number",
								"data-validation-optional"=>"true"
							);
							$cols_val='';
							$cols=preg_grep('/^cols/',$validation_array);
							if(count($cols)>0) {
								foreach ($cols as $key => $value) {
									$temp = explode(":",$value);
									$cols_val=$temp[1];
								}
							}
							echo $this->form->form_input('cols',$attribute, $cols_val);
						?>
					</div>
				</div>
				<div class="col-sm-6">
					<div class="form-group">
						<label>Rows</label>

						<?php
							$attribute=array(
								'placeholder'=>"Rows in number",
								"class"=>"form-control number-field",
								"data-validation"=>"number",
								"data-validation-optional"=>"true"
							);
							$rows_val='';
							$rows=preg_grep('/^rows/',$validation_array);
							if(count($rows)>0) {
								foreach ($rows as $key => $value) {
									$temp = explode(":",$value);
									$rows_val=$temp[1];
								}
							}
							echo $this->form->form_input('rows',$attribute, $rows_val);
						?>
					</div>
				</div>
			<?php } /* textarea and Editor fields end */ ?>

			<!-- For file control -->
			<?php if ($field['field_type_id'] == 6 || $field['field_type_id'] == 7) { ?>
				<div class="col-sm-6">
					<div class="form-group">
						<label>File Type</label>

						<?php
							$attribute=array(
								"class"=>"form-control",
								"placeholder"=>'Allowed file types without dot'
							);
							$filetype_vals='';
							$file_type=preg_grep('/^is_file_type/',$validation_array);
							if(count($file_type)>0) {
								foreach ($file_type as $key => $value) {
									$temp = explode(":",$value);
									$filetype_vals=$temp[1];
								}
							}
							echo $this->form->form_input('file_type',$attribute, $filetype_vals);
						?>
						<span class="text-danger">Enter the Option in following format - "jpg,jpeg,png,doc,pdf" just like this</span><br>
					</div>
				</div>
				<div class="col-sm-6">
					<div class="form-group">
						<label>Max File Size</label>
						<?php
							$attribute=array(
								"class"=>"form-control",
								"placeholder"=>"Max file size"
							);
							$filetype_vals='';
							$file_size=preg_grep('/^file_size/',$validation_array);
							if(count($file_size)>0) {
								foreach ($file_size as $key => $value) {
									$temp = explode(":",$value);
									$filetype_vals=$temp[1];
								}
							}
							echo $this->form->form_input('file_size',$attribute, $filetype_vals);
						?>
						<span class="text-danger">Enter the Option in following format - "10M" for 10mb limit</span><br>
					</div>
				</div>
			<?php } /* end file control */ ?>

			<!-- For Placeholder -->
			<?php if ($field['field_type_id'] == 1 || $field['field_type_id'] == 2 || $field['field_type_id'] == 3  || $field['field_type_id'] == 11 || $field['field_type_id'] == 12) { ?>
				<div class="col-sm-12">
					<div class="form-group">
						<label>Placeholder </label>
						<?php
							$attribute=array(
								'placeholder'=>"Placeholder",
								"class"=>"form-control",
							);
							echo $this->form->form_input('placeholder',$attribute);
						?>
					</div>
				</div>
			<?php } /*End placeholder*/ ?>


			<!-- For select box, checkbox and radio button options -->
			<?php if ($field['field_type_id'] == 5 || $field['field_type_id'] == 9 || $field['field_type_id'] == 10) { ?>
				<div class="col-sm-12">
					<div class="form-group">
						<label>Options List
						</label>
						<!-- Ignore multiple option for radio button -->
						<?php if($field['field_type_id'] != 10) { ?>
						<label style="float:right">
							<?php
								echo $this->form->form_checkbox("allow_multiple_option",'1');
								echo "Allow Multiple option";
							?>	
						</label>
						<?php } ?>

						<?php
							$attribute=array(
								'placeholder'=>"Enter select box options",
								"class"=>"form-control",
							);
							echo $this->form->form_textarea('field_options',$attribute);
						?>
						<span class="help-block">
							The possible values this field can contain. Enter one value per line, in the format key|label.<br>
							The key is the stored value. The label will be used in displayed values and edit forms.<br>
							The label is optional: if a line contains a single string, it will be used as key and label.
						</span>
					</div>
				</div>
			<?php } ?>

			<!-- Reference field -->
			<?php if ($field['field_type_id'] == 8) { ?>
				<div class="col-sm-12">
					<div class="form-group">
						<label>Select Content type <span class="text-danger">*</span></label>
						<?php 
							$attribute=array(
								"class"=>"form-control",
								"data-validation"=>"required"
							);
							echo $this->form->form_dropdown_fromdatabase('content_reference_id',$content_type_list,'content_type_id','content_type_title',null,$attribute);
						?>
					</div>
				</div>
			<?php } ?>

			<!-- Date picker options -->
			<?php if ($field['field_type_id'] == 11) { ?>
				<div class="col-sm-12">
					<div class="form-group">
						<label>Date picker Type <span class="text-danger">*</span></label>
						<?php
							$attribute=array(
								"class"=>"form-control",
								"data-validation"=>"required"
							);
							$datepicker_list=array(
								"datepicker"=>"Date picker",
								"monthyear-picker"=>"Month-Year picker",
								"year-picker"=>"Year picker"
							);
							echo $this->form->form_dropdown('datepicker_type',$datepicker_list,null,$attribute);
						?>
					</div>
				</div>
			<?php } /* Datepicker option end */ ?>

			<!--  Table reference Column -->
			<?php if ($field['field_type_id'] == 13) { ?>
				<div class="clearfix"></div>
				<div class="col-sm-4">
					<div class="form-group">
						<label>Table Name <span class="text-danger">*</span></label>
						<?php
							$attribute=array(
								"class"=>"form-control",
								"placeholder"=>"Database Table",
								"data-validation"=>"required"
							);
							echo $this->form->form_input('custom_table',$attribute);
						?>
					</div>
				</div>
				<div class="col-sm-4">
					<div class="form-group">
						<label>Table key column <span class="text-danger">*</span></label>
						<?php
							$attribute=array(
								"class"=>"form-control",
								"placeholder"=>"Table key column ",
								"data-validation"=>"required"
							);
							echo $this->form->form_input('table_key_column',$attribute);
						?>
					</div>
				</div>
				<div class="col-sm-4">
					<div class="form-group">
						<label>Table Value column <span class="text-danger">*</span></label>
						<?php
							$attribute=array(
								"class"=>"form-control",
								"placeholder"=>"Table value column",
								"data-validation"=>"required"
							);
							echo $this->form->form_input('table_value_column',$attribute);
						?>
					</div>
				</div>
			<?php } ?>

			<?php if($field['field_type_id']==14) { ?>
				<div class="clearfix"></div>
				<div class="col-sm-12">
					<div class="form-group">
						<label>Html Content</label>
						<?php
							$attribute=array(
								"class"=>"form-control ckeditor",
							);
							echo $this->form->form_textarea('html_content',$attribute);
						?>
					</div>
				</div>
			<?php } ?>

			<!-- Ignore for html content -->
			<?php if($field['field_type_id']!=14) { ?>
				<div class="clearfix"></div>
				<div class="col-sm-6">
					<div class="form-group">
						<label>Extra Attribute</label>
						<?php
							$attribute=array(
								'placeholder'=>"Extra Field Attribute",
								"class"=>"form-control",
							);
							echo $this->form->form_textarea('extra_attribute',$attribute);
						?>
						<span class="help-block"></span>
						<span class="help-block">
							Any Extra Attributes you wants to supply to field<br/>
							The possible values this field can contain. Enter one value per line, in the format key|label.<br>
						</span>
					</div>
				</div>
				<div class="col-sm-6">
					<div class="form-group">
						<label>Help Text</label>
						<?php
							$attribute=array(
								'placeholder'=>"Enter field description",
								"class"=>"form-control",
							);
							echo $this->form->form_textarea('help_text',$attribute);
						?>
					</div>
				</div>
				<div class="clearfix"></div>
				<div class="col-sm-6">
					<div class="form-group">
						<label>CSS classes</label>
						<?php
							$attribute=array(
								'placeholder'=>"Field Label",
								"class"=>"form-control",
							);
							echo $this->form->form_input('css_classes',$attribute);
						?>
						<span class="help-block">Apply a class to the field. Predefined classes <b>email-field</b></span>
					</div>
				</div>
				<?php } ?>
				<!-- End not for html content -->
				<div class="col-sm-6">
					<div class="form-group">
						<label>Wrapper CSS classes</label>
						<?php
							$attribute=array(
								'placeholder'=>"Wrapper Classes",
								"class"=>"form-control",
							);
							echo $this->form->form_input('wrapper_classes',$attribute);
						?>
						<span class="help-block">Apply a class to the wrapper around both the field and its label. Use <strong>clearcol</strong> Class for every first element in row</span>
					</div>
				</div>
		</div>
		<div class="modal-footer">
			<div class="pull-right">
				<button type="reset" class="btn btn-default">Clear</button>
				<button type="submit" class="btn btn-primary">Submit</button>
				<button onclick="window.history.back();" class="btn btn-default">Close</button>
			</div>
		</div>
	</form>	
</div>
<script type="text/javascript" src="<?php echo base_url()."public/plugins/ckeditor/ckeditor.js"; ?>"></script>
<script type="text/javascript">
	$(document).ready(function() {
		CKEDITOR.replace('html_content');
		CKEDITOR.config.disableNativeSpellChecker = false;
		CKEDITOR.config.allowedContent=true;
	});
</script>




