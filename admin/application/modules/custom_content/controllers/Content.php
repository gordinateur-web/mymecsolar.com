<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Content extends CI_Controller {
	
	public function __construct() {
		parent::__construct();

		/* Permission for adminpanel access */
		$this->load->model('content_model');
		$this->load->model('content_type_model');
		$this->load->model('media_model');
		$this->load->model('content_hook');
		$this->config->load('custom_content');
	}
	
	/*
		List all content type's data
	*/
	public function index() {
		permission_check(array(1));
		$data['title']='Content List';
		/* get content type list */
		$data['content_type_list']=$this->content_type_model->get_content_type();

		
		/* search content */
		$content_title=$this->input->get('content_title');
		$content_status=$this->input->get('content_status');
		$content_published=$this->input->get('content_published');

		$keyword=$this->input->get('keyword');
		$content_type_id=$this->input->get('content_type_id');
		$filter=array();
		if(!empty($content_title)) {
			$filter['LIKE']=array('content.content_title'=>$content_title);
			$data['clearfilter']=true;
		}
		if(!empty($content_status)) {
			$filter['WHERE'][]="content.content_status='".$content_status."'";
			$data['clearfilter']=true;
		}
		if(!empty($content_published) || $content_published=='0') {
			$filter['WHERE'][]="content.content_published='".$content_published."'";
			$data['clearfilter']=true;
		}

		if(!empty($content_type_id)) {
			$filter['content.content_type_id']=$content_type_id;
			$data['clearfilter']=true;
		}
		if(!empty($keyword)) {
			if(strlen($keyword)>=3) {
				$keyword=addslashes($keyword);
				$filter['WHERE'][]="(content.content_meta like '%".$keyword."%') or (content.content_title like '%".$keyword."%')";
			}
			$data['clearfilter']=true;
		} 

		$data['old_data']=$this->input->get();
		/* Total count of client  */
		$count_filter=$filter;
		$count_filter['SELECT']='count(*) as cnt';
		$content_cnt=$this->content_model->get_content($count_filter);

		/* list pagination config  */
		$this->load->library('pagination');
		$config=get_pagination_config();
		$config['base_url'] = base_url().'custom_content/content/?';
		$page=$this->input->get('per_page');
		if(!$page) {
			$page=0;
		}

		$data['pagination'] = get_pagination_links($content_cnt[0]['cnt']);
		

		/* Get content list */
		$filter['LIMIT']=array($config['per_page'],$page);
		$filter['WHERE']="(content_type.content_type_category='content' or content_type.content_type_category='section_content')";
		$filter['ORDER_BY']=array('content.content_id'=>'DESC');
		$data['content_list']=$this->content_model->get_content($filter);
		
		view_with_master('content_list',$data);
	}

	public function content_type_list() {
		permission_check(array(1));
		$data['title']="Content Types List";
		
		/* For search model */
		$data['content_list']=$this->content_type_model->get_content_type(array(
			'ORDER_BY'=>array('content_type.list_rank'=>'asc')
		));

		/* Count of each content in each content type  */
		$data['content_count']=$this->content_model->get_content(array(
			'SELECT'=>'count(*) as cnt, content.content_type_id',
			'GROUP_BY'=>'content.content_type_id'
		));
		$data['content_count']=array_column($data['content_count'],'cnt','content_type_id');
		view_with_master('custom_content/content_type_list',$data);
	}

	/* 
		table field - content_status 
		status = null     - Approved
		status = pending  - Pending
		status = blocked  - Blocked
	*/
	public function change_status($content_id) {
		$data=array();
		if(!empty($content_id)) {
			$filter_content['content.content_id']=$content_id;
			$content_data=$this->content_model->get_content($filter_content);
			
			/* Check content type permission for content admin */
			$this->content_type_model->check_content_permission($content_data[0]['content_type_id'], array(8), false, 2);

			$filter_content['content.content_id']=$content_id;
			$content_data=$this->content_model->get_content($filter_content);
			$data['edit_data']=$content_data[0];
		}
		$this->load->view('custom_content/content_status',$data);
	}

	public function status_save() {
		/* get post value */
		$content_id=$this->input->post('content_id');
		$content_status=$this->input->post('content_status');

		if(!empty($content_id)) {
			$filter_content['content.content_id']=$content_id;
			$content_data=$this->content_model->get_content($filter_content);
				
			/* Check content type permission */
			$this->content_type_model->check_content_permission($content_data[0]['content_type_id'], array(8));

			$status_data['content_status']=$content_status;
			$this->content_model->update_content($status_data, array(
				'content_id'=>$content_id
			));
		}
		
		set_message("Content status changed successfully","success");
		redirect_back();
	}

	/*
		Add any content with content type id
	*/
	public function add_content_data($content_type_id) {
		permission_check(array(1));
		if(empty($content_type_id)) {
			show_404();die;
		}

		/* Get content type data */
		$filter['content_type.content_type_id']=$content_type_id;
		$filter['WHERE']="(content_type.content_type_category='content' or content_type.content_type_category='section_content')";
		$data['content_type']=$this->content_type_model->get_content_type($filter);
		if(empty($data['content_type'])) {
			show_404();
		}

		$data['content_type']=$data['content_type'][0];
		$data['title']=$data['page_title']="Add ".$data['content_type']['content_type_title'];

		$get_sections_if_exists=$this->db->get_where('content_type_section',array('content_type_id'=>$content_type_id))->result_array();

		// if($content_type_id == $get_sections_if_exists[0]['content_type_id']){
		// 	$view_data= $this->content_model->edit_content_form($content_id);

		// view_with_master($view_data, $data, false, true);
		// }
		$view_data= $this->content_model->add_content_form($content_type_id);

		view_with_master($view_data, $data, false, true);
	}

	
	/*
		edit existing content
	*/
	public function edit_content_data($content_id) {
		permission_check(array(1));
		$user_id=$this->session->userdata($this->config->item('user_id_insession'));
		$data['edit_content']=$this->content_model->get_content(array('content.content_id' => $content_id));

		/* If edit content id is not found */
		if(empty($data['edit_content'])) {
			show_404();
			die;
		}
		$data['edit_content']=$data['edit_content'][0];

		/* Get content type data */
		$filter['content_type_id']=$data['edit_content']['content_type_id'];
		$content_type_info=$this->content_type_model->get_content_type($filter);
		$data['content_type']=$content_type_info[0];

		$node = array();
		$data['title']="Edit <b>".$data['content_type']['content_type_title'].'</b>';

		$view_data= $this->content_model->edit_content_form($content_id);

		view_with_master($view_data, $data, false, true);
	}
	
	/*
		New method to save add form content
	*/
	public function save_content_data() {
		$this->load->library('form_validation');
		$this->load->helper('url');
		$is_edit=false;
		$post_array=$this->input->post();
		//dsm($post_array);die;

		/* Get fields from database */
		$content_type_id=$this->input->post('content_type_id');
		$content_id=$this->input->post('content_id');
		$main_menu_link=$this->input->post('main_menu_link');

		/* Used to check if old field or new field for edit content */
		$old_content_data=array();
		
		/* Content type id is required */
		if(empty($content_type_id)) {
			set_message("Invalid content id selected");
			redirect_back();
			return 0;
		}

		/* Get content type data */
		$content_type=$this->content_type_model->get_content_type(array('content_type_id'=>$content_type_id));
		if(empty($content_type)) {
			set_message("Invalid content type selected");
			redirect_back();
			return 0;
		}
		$content_type=$content_type[0];

		/* field list and its Validation  */
		$field_list=$this->content_type_model->get_content_type_fields(array('content_type_id'=>$content_type_id));
		$validations = $this->content_type_model->content_validation($field_list);


		/* Permission checking based on category */
		if($content_type['content_type_category']=='content' || $content_type['content_type_category']=='section_content' ) {
			/* Get current user permission for this content type */
			$user_permission=$this->content_type_model->get_content_type_permission(array(
				'content_type_permission.content_type_id'=>$content_type['content_type_id'],
				'role_id'=>$this->session->userdata($this->config->item('role_id_insession'))
			));

			/* Check user permission */
			if(empty($user_permission)) {
				set_message("You don't have sufficient permission to access this resources");
				redirect_back();
			}
			$user_permission=array_column($user_permission,'permission_id');

			/* Check Permission for content edit/Add */
			$permission_to_check=array(1);
			if(!empty($content_id)) {
				$is_edit=true;
				$permission_to_check=array(1,2);
			}

			/* Check content type permission */
			$this->content_type_model->check_content_permission($content_type_id, $permission_to_check);

			$validations['value_validations'][]=array(
				'field'=>'content_title',
				'label'=>'Content title',
				'rules'=>'required|max_length[150]',
			);
		}
		elseif($content_type['content_type_category']=='webform') {
			if($content_type['loggedin_only']=='1') {
				login_check();
			}
		}

		/* Check form validation */
		if(!empty($validations['value_validations'])) {
			$this->form_validation->set_rules($validations['value_validations']);
			if ($this->form_validation->run() == FALSE) {
				$this->session->set_flashdata('old_data',$post_array);
				set_message(validation_errors());
				redirect_back();die;
			}
		}

		/* Check content title name unique */
		if($content_type['content_type_category']=='content' || $content_type['content_type_category']=='section_content') {
			$unique_filter=array(
				'content.content_title'=>$post_array['content_title'],
				'content.content_type_id'=>$post_array['content_type_id']
			);

			/* Ignore unique validation for own content while editing */
			if($is_edit) {
				$unique_filter['content.content_id !=']=$content_id;
			}

			$unique_result=$this->content_model->get_content($unique_filter);
		
			if(count($unique_result) > 0) {
				set_message($post_array['content_title'].' Is already Exist');
				$this->session->set_flashdata('old_data',$post_array);
				redirect_back();die;
			}
		}



		/* Check file validation */
		$file_validations_result=$this->content_model->file_validation($validations['file_validations'],$_FILES, $is_edit);
		if ($file_validations_result['status'] == FALSE) {
			$this->session->set_flashdata('old_data',$post_array);
			set_message($file_validations_result['message']);
			redirect_back();die;
		}



		$insert_data['content_title']=$this->input->post('content_title');
		$insert_data['url_title']=url_title($insert_data['content_title'],'-',true);
		$insert_data['content_published']=$this->input->post('content_published');
		$insert_data['stick_ontop']=$this->input->post('stick_ontop');
		$insert_data['menu_parent']=$this->input->post('menu_parent');
		$insert_data['main_menu_link']=$this->input->post('main_menu_link');
		if(empty($insert_data['main_menu_link'])) {
			$insert_data['main_menu_link']=0;
		}

		/* Set url_title for menu items contents  */
		if(isset($post_array['menu_parent']) && $post_array['menu_parent']!='') {
			$url_prefix=$this->content_model->generate_url_prefix($post_array['menu_parent']);
			if(!empty($url_prefix)) {
				$insert_data['url_title']=$url_prefix.'/'.url_title($insert_data['content_title'],'-',true);;	
			}
		}

		/* Check unique url_title field for content, 
			if not unique append numeric value and return unique title 
		*/
		$insert_data['url_title']=$this->content_model->get_unique_url_title($insert_data['url_title'], $content_id);

		/* Set zero if unpublished */
		if(empty($insert_data['content_published'])) {
			$insert_data['content_published']=0;
		}

		/* Check content type permission, if admin approval needed or not 
			Ignore for content admin
		*/
		$success_message_attach="";
		if($content_type['publish_approval'] == '1' && !in_array(8, $user_permission) && !$is_edit) {
			$insert_data['content_status']='pending';	
			$success_message_attach="Your content will be published after approval of the system admin.";		
		}

		$this->db->trans_begin();
		/* update content */
		if($is_edit) {

			/* Call before update Hook */
			$update_hook='before_update_'.$content_type['content_type_machine_name'];
			if(method_exists($this->content_hook,$update_hook)) {
				$this->content_hook->$update_hook($content_id);
			}


			$this->content_model->update_content($insert_data, array(
				'content_id'=>$content_id
			));

			/* Delete deleted fields */
			$deleted=$this->input->post('deleted');
			if(!empty($deleted)) {
				$this->db->where('content_id',$content_id);
				$this->db->where_in('content_value_id',$deleted);
				$this->db->delete('content_value');
			}

			/* Get old values from  database */
			$old_content_data=$this->content_model->get_content_value(array(
				'content_id' => $content_id
			));
			if(!empty($old_content_data)) {
				$old_content_data=array_column($old_content_data, 'content_value_id');				
			}

			/* Update order of files */
			$this->content_model->save_file_order($content_id);

			/* Update link of menu of updated content */
			$this->content_model->update_content_menu($insert_data, $content_id, $main_menu_link);
		}
		/* Add new content */
		else {
			/* Call before add Hook */
			$update_hook='before_add_'.$content_type['content_type_machine_name'];
			if(method_exists($this->content_hook,$update_hook)) {
				$this->content_hook->$update_hook();
			}

			$insert_data['content_type_id']=$post_array['content_type_id'];
			$insert_data['content_type_section_id']=0;
			$insert_data['section_content_parent']=0;

			/* Add section id and content parent */
			if(!empty($post_array['content_type_section_id']) && !empty($post_array['section_content_parent'])) {
				$insert_data['content_type_section_id']=$post_array['content_type_section_id'];
				$insert_data['section_content_parent']=$post_array['section_content_parent'];
			}


			/* Add approve status for blank status */
			if(empty($insert_data['content_status'])) {
				$insert_data['content_status']='Approved';
			}

			$content_id = $this->content_model->add_content($insert_data);

			/* Add menu item for added content */
			if(!empty($main_menu_link)) {
				$this->content_model->add_content_menu($insert_data, $content_id);
			}
		}
		
		$fields=array_column($field_list, 'machine_name');
		$fields_id=array_column($field_list, 'content_type_field_id');
		$fields_types=array_column($field_list, 'field_type_id');
		$number_of_values=array_column($field_list, 'number_of_values');
		
		/* Check upload directory structure */
		$uploading_path=UPLOAD_DIR.url_title($content_type['content_type_title']);
		$uploading_path=strtolower($uploading_path);
		check_upload_dir($uploading_path);

		/* Store all content data in content table for search purpose */
		$content_meta='';
		$content_meta.=$this->input->post('content_title');

		/* Save each field data in content_value table */
		foreach ($fields as $field_index => $field_name) {
			$file_id = array( );
			$content_value = array();
			$old_options=array();
			$content_value['content_id'] = $content_id;
			$content_value['content_type_field_id'] = $fields_id[$field_index];
			$content_value['field_machine_name'] = $field_name;
			$content_value['content_type_id']=$content_type['content_type_id'];

			/* Get old values from database to compare for update, insert and delete for all selectbox control */
			if($is_edit && in_array($fields_types[$field_index],array(5,8,13))) {
				$selectbox_filter['content_id']=$content_id;
				$selectbox_filter['content_type_field_id']=$fields_id[$field_index];
				$old_options = $this->content_model->get_content_value($selectbox_filter);
				$old_options = array_column($old_options, 'content_value', 'content_value_id');
			}

			/* If input type=file */
			if ($fields_types[$field_index] == 6 || $fields_types[$field_index] == 7 ) {
				$stored_paths = $this->content_model->file_upload($uploading_path, $_FILES[$field_name]);
				foreach ($stored_paths as $path_index => $path_value) {
					$media_title='';
					//$media_title=basename($path_value['media_path']);
					
					/* For image take title from input string */
					if (!empty($post_array[$field_name.'_imgtitle'][$path_index])) {
						$media_title = $post_array[$field_name.'_imgtitle'][$path_index];
					}

					$file_data=array(
						'media_path'=>$path_value['media_path'],
						'media_type'=>$path_value['media_type'],
						'media_size'=>$path_value['media_size'],
						'media_title'=>$media_title,
						'media_alt'=>$media_title,
					);
					$this->media_model->add_media($file_data);

					/* Insert data in content value table */
					$content_value['content_value'] = '';
					$content_value['media_path'] = $path_value['media_path'];
					$content_value['media_type'] = $path_value['media_type'];
					$content_value['media_size'] = $path_value['media_size'];
					$content_value['media_title'] = $media_title;
					$content_value['media_alt'] = $media_title;
					$this->db->insert('content_value',$content_value);
				}
			} 
			else {
				if(empty($post_array[$field_name])) {
					continue;
				}
				foreach ($post_array[$field_name] as $data_index => $data_value) {
					/* For reference content type and table reference type */
					if ($fields_types[$field_index] == 8 || $fields_types[$field_index] == 13 ) {	
						$content_value['content_reference_id'] = $data_value;
						
						/* Add value of content_reference_value field */
						$content_value['content_reference_value']=$post_array['val_'.$field_name][$data_index];
						
						/* Collect all field data in one column for search purpose */
						$content_meta.=' '.$content_value['content_reference_value'];
					}
					else {
						/* Collect all field data in one column for search purpose */
						$content_meta.=' '.$data_value;	
					}
					
					$content_value['content_value'] = $data_value;

					/* Update value if edit and content_value_id not available */
					if($is_edit && in_array($data_index, $old_content_data)) {
						$this->db->where('content_value_id',$data_index);
						$this->db->where('content_id',$content_id);
						$this->db->update('content_value',$content_value);
						unset($old_options[$data_index]);
					}
					/* Insert new value */
					else {
						/* check selectbox values */
						if($is_edit && in_array($fields_types[$field_index],array(5,8,13))) {
							/* check if value is available in old array */
							$content_key=array_search($content_value['content_value'], $old_options);

							/* Add value of content_reference_value field */
							$content_value['content_reference_value']=$post_array['val_'.$field_name][$data_index];

							/* Remove from old array */
							if($content_key	!== false) {
								unset($old_options[$content_key]);
							}
							else {
								$this->db->insert('content_value',$content_value);	
							}
						}
						else {
							$this->db->insert('content_value',$content_value);
						}
					}
				}

				/* Delete remaining options from database */
				if(!empty($old_options)) {
					$this->db->where_in('content_value_id',array_keys($old_options));
					$this->db->where('content_id',$content_id);
					$this->db->delete('content_value');
				}
			}
		}

		/* Change file titles */
		$changed_media_title=$this->input->post('changed_media_title');
		if(!empty($changed_media_title)) {
			foreach ($changed_media_title as $content_value_id => $media_title) {
				$this->db->where('content_value_id',$content_value_id);
				$this->db->where('content_id',$content_id);
				$this->db->update('content_value',array('media_title'=>$media_title));
			}
		}

		/* update rank_order by insert_id and add content_meta */
		$content_update=array(
			'content_meta'=>strip_tags(trim($content_meta)),
		);

		if(!$is_edit) {
			$content_update['rank_order']=$content_id;
		}

		$this->content_model->update_content($content_update, array(
			'content_id'=>$content_id
		));

		/* Call after update Hook function for specific content type */
		if($is_edit) {
			$update_hook='after_update_'.$content_type['content_type_machine_name'];
			if(method_exists($this->content_hook,$update_hook)) {
				$this->content_hook->$update_hook($content_id);
			}
		}
		/* Call after add Hook function for specific content type */
		else {
			$add_hook='after_add_'.$content_type['content_type_machine_name'];
			if(method_exists($this->content_hook,$add_hook)) {
				$this->content_hook->$add_hook($content_id);
			}
		}

		/* Process for webform email send */
		$this->content_model->webform_process($content_type, $content_id);
		
		$this->db->trans_complete();
		/* Fail transaction */
		if ($this->db->trans_status() === FALSE) {
			$this->db->trans_rollback();
			$this->session->set_flashdata('old_data',$post_array);
			set_message($this->db->_error_message());
			redirect_back();
		}
		/* Success transaction */
		else {
			$this->db->trans_commit();

			if($content_type['content_type_category']=='content' || $content_type['content_type_category']=='section_content' ) {
				set_message($content_type['content_type_title']." data saved successfully","success");
				set_message($success_message_attach,"success");

				$redirect_to = $this->input->post('redirect_to');

				/* Send to adminpanel content list */
				if(empty($redirect_to)) {
					/* Redirect to parent edit page */
					if(!empty($post_array['content_type_section_id']) && !empty($post_array['section_content_parent'])) {
						redirect(base_url().'custom_content/content/edit_content_data/'.$post_array['section_content_parent']);
					}
					/* Redirect to list of content */
					else {
						$get_sections_if_exists=$this->db->get_where('content_type_section',array('content_type_id'=>$content_type_id))->result_array();

						if(empty($get_sections_if_exists)) {
							redirect(base_url().'custom_content/content/view_content/'.$post_array['content_type_id']);		
						}
						if($content_type_id == $get_sections_if_exists[0]['content_type_id']){
							//echo $content_id;die;
							redirect(base_url().'custom_content/content/edit_content_data/'.$content_id);
						}
						redirect(base_url().'custom_content/content/view_content/'.$post_array['content_type_id']);	
					}
					
				}
			}
			elseif($content_type['content_type_category']=='webform') {
				set_message($content_type['success_message'],"success");
				$redirect_to = $content_type['redirect_to'];
				
				/* Redirect to back page if no redirect URL added */
				if(empty($redirect_to)) {
					redirect_back();
				}
			}

			/* Outside URL */
			if(filter_var($redirect_to, FILTER_VALIDATE_URL)) {
				redirect($redirect_to);
			}

			/* Append base URL if relative path */
			redirect(base_url().$redirect_to);
		}
	}


	/*
		List individual content type's data
	*/
	public function view_content($content_type_id) {
		permission_check(array(1));
		$data['content_type_id']=$content_type_id;

		/* get content type */
		$data['content_type']=$this->content_type_model->get_content_type(array(
			'content_type.content_type_id'=>$content_type_id
		));
		if(empty($data['content_type'])) {
			show_404();die;
		}
		$data['content_type']=$data['content_type'][0];


		/* Check permission for webform view data */
		if($data['content_type']['content_type_category']=='webform') {
			$this->content_type_model->check_content_permission($content_type_id, array('9'));
		}		


		/* Get filed list for search model */
		$condition_search = array(
			'content_type_id' => $content_type_id, 
			'content_type_field.show_in_listview' => '1',
			'WHERE'=>'content_type_field.field_type_id not in(6,7,8)'
		);

		/* Fields list for dataTables */
		$condition = array(
			'content_type_id' => $content_type_id, 
			'content_type_field.show_in_listview' => '1',
		);

		/* get content type field */
		$data['content_type_field']=$this->content_type_model->get_content_type_fields($condition);
		$data['search_fields']=$this->content_type_model->get_content_type_fields($condition_search);

		
		/* list pagination config  */
		$this->load->library('pagination');
		$config=get_pagination_config();
		$config['base_url'] = base_url().'custom_content/content/view_content/'.$content_type_id.'/?';

		$page=$this->input->get('per_page');
		if(!$page) {
			$page=0;
		}

		/* search content */
		$field_type=false;
		$content_title=$this->input->get('content_title');
		$content_status=$this->input->get('content_status');
		$content_published=$this->input->get('content_published');
		$keyword=$this->input->get('keyword');
		$keyword=trim($keyword);
		$keyword=addslashes(trim($keyword));
		//dsm($keyword);
		$content_type_field_id=$this->input->get('content_type_field_id');

		$filter=array();
		$filter['content.content_type_id']=$content_type_id;
		if(!empty($content_title)) {
			$filter['LIKE']=array('content.content_title'=>$content_title);
			$data['clearfilter']=true;
		}
		if(!empty($content_status)) {
			$filter['WHERE'][]="content.content_status='".$content_status."'";
			$data['clearfilter']=true;
		}
		if(!empty($content_published)) {
			$filter['WHERE'][]="content.content_published='".$content_published."'";
			$data['clearfilter']=true;
		}

		if(!empty($content_type_field_id) && !empty($keyword)) {
			$filter['content_value.content_type_field_id']=$content_type_field_id;
			$field_type=true;
			$data['clearfilter']=true;

			if(strlen($keyword)>=2) {
				$filter["WHERE"][]="(content_value.content_value like '%".$keyword."%') or (content_value.content_reference_value like '%".$keyword."%')";
			}
		} 

		$data['old_data']=$this->input->get();
		
		/* Total count of client  */
		$count_filter=$filter;
		$count_filter['SELECT']='count(*) as cnt';
		if($field_type==true) {
			unset($count_filter['content.content_type_id']);
			$count_filter['content_type_field.content_type_id']=$content_type_id;
			//$count_filter['GROUP_BY']='content_value.content_id';
			$content_cnt=$this->content_model->get_value_with_fields($count_filter);
					}
		else {
			$content_cnt=$this->content_model->get_content($count_filter);	
		}

		$config['total_rows'] =$content_cnt[0]['cnt'];
		
		$this->pagination->initialize($config);	
		$data['pagination'] = $this->pagination->create_links(); 

		/* Get content list */
		$filter['LIMIT']=array($config['per_page'],$page);

		if($field_type==true) {
			$data['content_value_list']=$this->content_model->get_value_based_content($filter);
		}
		else {
			$data['content_value_list']=$this->content_model->get_content_with_value($filter);
		}

		/* get title of content type */
		$content_list=$this->content_type_model->get_content_type(array('content_type.content_type_id'=>$content_type_id));

		//dsm($content_list);die;
		$data['title']='List of '.$content_list[0]['content_type_title'];
		if(!empty($content_list[0]['content_type_description'])){
		$data['content_type_description']=$content_list[0]['content_type_description'];
		$pos=strpos($data['content_type_description'], ' ', 1);
		$parent_name=substr($data['content_type_description'],19,$pos);
		$data['parent_name']=' for '.$parent_name;
		}


		//dsm($content_list[0]);die;
		$config['page_num']=$page;
		$data['config']=$config;
		$data['content_list']=$content_list[0];
		view_with_master('content_value_list',$data);
	}


	/* 
		Save order of content from view_content
	*/
	public function content_save_order() {
		$ids=$this->input->post('ids');
		if(!empty($ids)) {
			$filter_content['content.content_id']=$ids[0];
			$content_data=$this->content_model->get_content($filter_content);
		}
		/* Check content type permission */
		$this->content_type_model->check_content_permission($content_data[0]['content_type_id'], array(5));

		foreach ($ids as $key => $value) {
			$this->db->where('content_id',$value);
			$this->db->update('content',array('rank_order'=>$key+1));
		}
		$return=array("status"=>'1',"message"=>"Reorder successfully");
		echo json_encode($return);	
	}

	public function search_content() {
		permission_check(array(1));
		$data['title']="Search Result";

		$condition_data['content.content_title']=$this->input->post('content_title');
		$condition_data['content.content_type_id']=$this->input->post('content_type_id');


		$str="";
		foreach ($condition_data as $key => $value) {
			if (!empty($value)) {
				if ($str=="") {
					$str.= $key." LIKE '%".$value."%'";
				} else {
					$str.= " AND ".$key." LIKE '%".$value."%'";
				}
			}
		}

		$condition['WHERE']=$str;
		$data['content_list']=$this->content_model->get_content($condition);
		$data['content_type_list']=$this->content_type_model->get_content_type();
		$data['contant']=$this->load->view('content_list',$data,true);
		$this->load->view('master',$data);
		
	}

	/* Used in content/search_content */
	public function search_content_user() {
		permission_check(array(1));
		$data['title']="Search Result";
		$content_type_id=$this->input->post('content_type_id');
		$condition1 = array('content_type_id' => $content_type_id, 'content_type_field.show_in_listview' => '1',  );
		$data['field_list']=$this->content_type_model->get_content_type_fields('',$condition1);

		$condition_data['content.content_title']=$this->input->post('content_title');
		$condition_data['content.content_type_id']=$content_type_id;
		$condition_data['content_value.content_type_value']=$this->input->post('content_type_value');
		$condition_data['content_value.content_type_field_id']=$this->input->post('content_type_field_id');

		$str="";
		foreach ($condition_data as $key => $value) {
			if (!empty($value)) {
				if ($str=="") {
					$str.= $key." LIKE '%".$value."%'";
				} else {
					$str.= " AND ".$key." LIKE '%".$value."%'";
				}
			}
		}

		// apply condition pending
		$condition['WHERE']=$str;
		$condition['SELECT'] = 'content_type_field.content_type_fields_title, content_type_field.content_type_id, content_type_field.field_type_id, content.content_type_id, content.content_title,content.published, content_value.content_id, content_value.content_type_field_id, content_value.content_type_value';

		$data['content_value_list']=$this->content_model->get_content_value_new($condition);
		if(!empty($data['content_value_list'])) {
			$content_id = array_column($data['content_value_list'], 'content_id');
			$condition['WHERE_IN'] = array('content_value.content_id' => $content_id, );
			unset($condition['WHERE']);
			$data['content_value_list']=$this->content_model->get_content_value_new($condition);
		}
		
		$data['content_type_id']=$content_type_id;

		$data['content_type_list']=$this->content_type_model->get_content_type();
		$data['contant']=$this->load->view('content_list_with_value',$data,true);
		$this->load->view('master',$data);
	}

	public function delete_content($content_id) {

		if(empty($content_id)) {
			set_message("Invalid content selected");	
			redirect_back();
		}

		$filter_content['content.content_id']=$content_id;
		$content_data=$this->content_model->get_content($filter_content);

		if(empty($content_data)) {
			set_message("Invalid content selected");	
			redirect_back();
		}

		/* Get content type data */
		$content_type=$this->content_type_model->get_content_type(array('content_type_id'=>$content_data[0]['content_type_id']));
		if(empty($content_type)) {
			show_404();	
		}
		$content_type=$content_type[0];

		/* Permission for content */
		if($content_type['content_type_category']=='content' || $content_type['content_type_category']=='section_content' ) {		
			$required_permission=array(4);
			$user_id=$this->session->userdata($this->config->item('user_id_insession'));
			if($content_data[0]['created_by'] != $user_id) {
				$required_permission[]=5;
			}
		}

		/* Permission for webform */
		elseif($content_type['content_type_category']=='webform') {
			$required_permission=array(10);
		}

		/* Check content type permission */
		$this->content_type_model->check_content_permission($content_data[0]['content_type_id'], $required_permission);

		$this->db->trans_begin();

		/* Call before delete Hook */
		$update_hook='before_delete_'.$content_type['content_type_machine_name'];
		if(method_exists($this->content_hook,$update_hook)) {
			$this->content_hook->$update_hook($content_id);
		}

		/* Delete content */
		$delete_array=array(
			'deleted_at'=>date('Y-m-d H:i:s'),
			'updated_by'=>$this->session->userdata($this->config->item('user_id_insession')),
		);

		$this->content_model->update_content($delete_array,array(
			'content_id'=>$content_id
		));

		/* Call after delete Hook */
		$update_hook='after_delete_'.$content_type['content_type_machine_name'];
		if(method_exists($this->content_hook,$update_hook)) {
			$this->content_hook->$update_hook($content_id);
		}

		$this->db->trans_complete();
		/* Fail transaction */
		if ($this->db->trans_status() === FALSE) {
			$this->db->trans_rollback();
			set_message($this->db->_error_message());
			redirect_back();
		}
		/* Success transaction */
		else {
			$this->db->trans_commit();
			set_message("Content deleted successfully","success");
			redirect_back();
		}
	}

	/*
		Delete file from edit form
	*/
	function delete_content_media() {
		permission_check(array(1),'and',3);
		$content_value_id=$this->input->post('content_value_id');
		if(empty($content_value_id)) {
			$json_data['status']=0;
			$json_data['message']='Please select media file to delete';
			echo json_encode($json_data);die;
		}
	
		$condition1['SELECT'] = 'content_type_field.number_of_values, content_type_field.field_validation, content_value.content_id, content_value.content_type_field_id';
		$condition1['content_value.content_value_id'] = $content_value_id;
		$check_detail=$this->content_model->get_value_with_fields($condition1);
		if(empty($check_detail)) {
			$json_data['status']=1;
			$json_data['message']='File deleted successfully';
			echo json_encode($json_data);die;
		}

		$validations=explode('|', $check_detail[0]['field_validation']);
		
		/* Check required validation for media files */
		if(in_array('required', $validations)) {
			$condition2['SELECT'] = 'COUNT(*) AS cnt,';
			$condition2['content_value.content_id'] = $check_detail[0]['content_id'];
			$condition2['content_value.content_type_field_id'] = $check_detail[0]['content_type_field_id'];
			$check_count=$this->content_model->get_content_value($condition2);
			if($check_detail[0]['number_of_values']==0) {
				$check_detail[0]['number_of_values']=1;
			}
			if($check_count[0]['cnt']>$check_detail[0]['number_of_values']) {
				$this->db->where('content_value_id',$content_value_id);
				if($this->db->delete('content_value')) {
					$json_data['status']=1;
					$json_data['message']='File deleted successfully';
					echo json_encode($json_data);
				}
				else {
					$json_data['status']=0;
					$json_data['message']='Error while database operation';
					echo json_encode($json_data);
				}
			} else {
				$json_data['status']=0;
				$json_data['message']='You can not delete file because, minimum '.$check_detail[0]['number_of_values'].' file is required';
				echo json_encode($json_data);
			}
		} 
		else {
			$this->db->where('content_value_id',$content_value_id);
			if($this->db->delete('content_value')) {
				$json_data['status']=1;
				$json_data['message']='File deleted successfully';
				echo json_encode($json_data);
			}
			else {
				$json_data['status']=0;
				$json_data['message']='Error while database operation';
				echo json_encode($json_data);
			}
		}
	}


	public function export_content() {
		$get_array=$this->input->get();
		//dsm($get_array);die;
		
		if(!empty($get_array['content_type_id'])) {
			$filter['content.content_type_id']=$get_array['content_type_id'];
		}
		if(!empty($get_array['content_title'])) {
			$filter['content.content_title']=$get_array['content_title'];
		}
		if(!empty($get_array['content_status'])) {
			$filter['content.content_status']=$get_array['content_status'];
		}
		if(!empty($get_array['content_published'])) {
			$filter['content.content_published']=$get_array['content_published'];
		}
		if(!empty($get_array['content_type_field_id']) && !empty($get_array['keyword'])) {
			$filter['content.content_type_field_id']=$get_array['content_type_field_id'];
			$filter['content.keyword']=$get_array['keyword'];
		}

		$filter['SELECT']='content.*';
		$content_list = $this->content_model->get_content($filter);
		//print_last_query();

		if(empty($content_list)) {
			set_message('content not available');
			redirect_back();
		}
		//dsm($content_list);die;

		$content_ids=array_column($content_list, 'content_id');
		//dsm($content_ids);die;
		/* Get value table data */
		$content_values = $this->content_model->get_content_value(array(
			'WHERE_IN'=>array("content_id"=>$content_ids)
		));

		$content_values=parent_child_array($content_values, 'content_id');
		$export_array=array();
		foreach ($content_list as $key =>$content) {
			$export_array[$key]['content']=$content;
			$export_array[$key]['content_value']=$content_values[$content['content_id']];
		}

		//dsm($export_array);die;

		$this->load->helper('download');
		force_download('content_export.csv',json_encode($export_array));
	}

	public function import_json() {
		$data['title']='Import Content';
		view_with_master('import_content', $data);
	}

	public function import_content() {
		$post_array=$this->input->post();

		$file=$_FILES;
		//dsm($_FILES);die;

		/*$upload_config['upload_path']= UPLOAD_DIR.'import_json';
        $upload_config['allowed_types']= 'txt|JSON';
        $upload_config['file_ext_tolower']= true;

		// Check if upload folder available 
		if(!file_exists($upload_config['upload_path'])) {
			mkdir($upload_config['upload_path']);
		}

		// Upload pdf file 
        $this->load->library('upload', $upload_config);
        if (!$this->upload->do_upload('file')) {
            set_message($this->upload->display_errors());
            redirect_back();
        }
        else {
            $upload_data=$this->upload->data();
        }
        dsm($upload_data);die;*/
        //$file='./uploads/import_json/content_export.json';

        if(empty($file)) {
        	set_message('file not selected!');
        	redirect_back();
        }
        //dsm($file);die;
        $filename=$_FILES['file']['name'];
        $target='./uploads/import_json/'.$_FILES['file']['name'];

        $ext = pathinfo($filename, PATHINFO_EXTENSION);
		if( $ext !== 'json' ) {
			set_message('Only json file requred');
        	redirect_back();
		}

        if(!move_uploaded_file($_FILES['file']['tmp_name'], $target)) {
        	set_message('Sorry, there was a problem uploading your file.');
        	redirect_back();
        }

        $overwrite=0;
        if(!empty($post_array['overwrite'])) {
        	$overwrite=1;
        }
        //dsm($overwrite);die;

		$content_with_value = json_decode(file_get_contents($target));
		//$content_with_value = json_decode(file_get_contents($file));
		$content_with_value =object_to_array($content_with_value);
		//dsm($content_with_value);die;

		if(empty($content_with_value)) {
			return false;
		}

		$insert_content_count=0;
		$update_content_count=0;
		foreach ($content_with_value as $key => $content) {
			/* If empty content array */
			if(empty($content['content'])) {
				continue;
			}

			$content_id=false;

			// Insert all content and content_value with new primary keys
			if($overwrite == 1) {
				// get content with content_id and content_type_id
				$old_content = $this->content_model->get_content(array(
					'content.content_id'=>$content['content']['content_id'],
					'content.content_type_id'=>$content['content']['content_type_id'],
				));

				if(!empty($old_content)) {
					$content_id=$content['content']['content_id'];
				}
			}


			// unset primary key
			unset($content['content']['content_id']);
			$content['content']['created_from']='json_import';

			// Update
			if($content_id) {
				$this->content_model->update_content($content['content'], array('content_id'=>$content_id));

				// Delete all content_value of that content_id
				$this->db->where('content_value.content_id', $content_id);
				$this->db->delete('content_value');
				$update_content_count++;
			}
			// New insert
			else {
				$content_id = $this->content_model->add_content($content['content']);
				$insert_content_count++;
			}

			//update content_id of each row in content_value
			foreach ($content['content_value'] as $key => $value) {
				// unset primary key
				unset($content['content_value'][$key]['content_value_id']);
				$content['content_value'][$key]['content_id']=$content_id;
			}

			//  Insert batch
			$this->db->insert_batch('content_value', $content['content_value']);
		}
		set_message('Content imported successfully <br/> Record inserted : '.$insert_content_count.', Record Updated : '.$update_content_count, 'success');
		redirect_back();
	}

/*
	function bulk_delete() {
		$content_id=$this->input->post('content_id');
		if($content_id!='' && count($content_id) > 0) {
			$this->db->where_in('content_id',$content_id);
			if($this->db->delete('content')) {

				$this->db->where_in('content_id',$content_id);
				$this->db->delete('content_value');

				$json_data['status']=1;
				$json_data['message']='Selected content deleted successfully';
				echo json_encode($json_data);
			}
			else {
				$json_data['status']=0;
				$json_data['message']='Error while database operation';
				echo json_encode($json_data);
			}
		}
		else {
			$json_data['status']=0;
			$json_data['message']='No content selected to delete';
			echo json_encode($json_data);
		}
	}
*/
	/*public function model_test() {
		$content = $this->content_model->get_value_based_content(array(
			'field_machine_name'=>'publish_date_1',
			'content_value'=>'pue'
		));
		dsm($content);		
	}*/

	
}