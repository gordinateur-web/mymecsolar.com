<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Content_type extends CI_Controller {
	
	public function __construct() {
		parent::__construct();

		/* Permission for adminpanel access, custom content access */
		permission_check(array(1,2));

		$this->load->model('content_type_model');
		$this->load->model('content_model');
		$this->config->load('custom_content');
	}
	
	/*
		List/ Search of added content types
	*/
	public function index() {
		$data['title']="Content Types List";
		/* For search model */
		$data['content_list']=$this->content_type_model->get_content_type(array(
			'ORDER_BY'=>array('content_type.content_type_title'=>'asc')
		));

		/* Count of each content in each content type  */
		$data['content_count']=$this->content_model->get_content(array(
			'SELECT'=>'count(*) as cnt, content.content_type_id',
			'GROUP_BY'=>'content.content_type_id'
		));
		$data['content_count']=array_column($data['content_count'],'cnt','content_type_id');
		view_with_master('custom_content/content_type_list',$data);
	}

	/*
		Add or edit content type
	*/
	public function add_content_type($content_type_id=false) {
		permission_check(2);
		$data = array( );
		$data['title']="Add Content Type";
		/* Get master data */
		$data['role_list']=$this->content_type_model->get_role();
		$data['permission_list']=$this->content_type_model->get_content_permission_master();
		$data['webform_permission']=$this->content_type_model->get_webform_permission_master();
		$content_type_filter=array();
		
		if(!empty($content_type_id)) {
			$content_type_id=decrypt_id($content_type_id);
			$data['edit_content_type']=$this->content_type_model->get_content_type(array(
				'content_type_id' => $content_type_id, 
			));

			if(!empty($data['edit_content_type'])) {
				$data['edit_content_type']=$data['edit_content_type'][0];
				$data['title']="Edit ".$data['edit_content_type']['content_type_title'];

				/* Get permissions */
				$content_type_permission=$this->content_type_model->get_content_type_permission(array(
					'content_type_permission.content_type_id' => $content_type_id, 
				));

				/* Get content count */
				$data['content_count']=$this->content_model->get_content(array(
					'content.content_type_id'=>$content_type_id,
					'SELECT'=>'count(*) as count',
				));

				/* Get section list */
				$data['section_list']=$this->content_type_model->get_content_type_section(array(
					'content_type_id'=>$content_type_id
				));
				
				
				/* Arrange permission array according to form library */
				if(!empty($content_type_permission)) {
					$data['edit_permission']=array();
					foreach ($content_type_permission as $permission_row) {
						$data['edit_content_type'][$permission_row['permission_id']][$permission_row['role_id']]=$permission_row['role_id'];
					}
				}

				/* Arrange section array according to form library */
				if(!empty($data['section_list'])) {
					foreach ($data['section_list'] as $section) {
						$data['edit_content_type']['section_content_type'][$section['content_type_section_id']]=$section['content_id'];
						$data['edit_content_type']['section_title'][$section['content_type_section_id']]=$section['section_title'];
					}
				}
				//dsm($data['edit_content_type']);die;

				/* Filter for content type, ignore current content types */
				$content_type_filter['content_type_id !=']=$content_type_id;
			}
		}

		/* Content type list for section add, edit */
		$data['content_type_list']=$this->content_type_model->get_content_type($content_type_filter);

		view_with_master('custom_content/content_type_add',$data);
	}


	/*
		Save content type form
	*/
	public function save_content_type() {
		permission_check(2);
		$this->form_validation->set_rules('content_type_title', 'Title', 'required');
		$this->form_validation->set_rules('content_type_status', 'Status', 'required');

		if ($this->form_validation->run() == FALSE) {
			set_message(validation_errors());
			redirect_back();
		}

		$post_data=$this->input->post();
		$content_type_id=$this->input->post('content_type_id');
		$insert_data['content_type_title']=$post_data['content_type_title'];
		$insert_data['content_type_status']=$post_data['content_type_status'];
		$insert_data['content_type_description']=$post_data['content_type_description'];
		$insert_data['form_js']=$post_data['form_js'];
		$insert_data['form_css']=$post_data['form_css'];


		$insert_data['publish_approval']=$this->input->post('publish_approval');
		$insert_data['include_in_search']=$this->input->post('include_in_search');
		$insert_data['allow_like']=$this->input->post('allow_like');
		$insert_data['allow_share']=$this->input->post('allow_share');
		$insert_data['allow_comment']=$this->input->post('allow_comment');
		$insert_data['comment_approval']=$this->input->post('comment_approval');
		$insert_data['like_notification']=$this->input->post('like_notification');
		$insert_data['comment_notification']=$this->input->post('comment_notification');
		$insert_data['loggedin_only']=$this->input->post('loggedin_only');
		
		/* Webform settings */
		$insert_data['send_email']=$this->input->post('send_email');
		$insert_data['send_sms']=$this->input->post('send_sms');
		$insert_data['send_email_to']=$this->input->post('send_email_to');
		$insert_data['send_sms_to']=$this->input->post('send_sms_to');
		$insert_data['send_email_from']=$this->input->post('send_email_from');
		$insert_data['owner_email_subject']=$this->input->post('owner_email_subject');
		$insert_data['sender_email_subject']=$this->input->post('sender_email_subject');
		$insert_data['success_message']=$this->input->post('success_message');
		$insert_data['redirect_to']=$this->input->post('redirect_to');
		$insert_data['thanks_email']=$this->input->post('thanks_email');
		$insert_data['thanks_email_field']=$this->input->post('thanks_email_field');

		$insert_data['is_singular']=$this->input->post('is_singular');
		

		$this->db->trans_begin();

		/* Update content type */
		if(!empty($content_type_id)) {
			/* unset category if not supplied */
			if(empty($insert_data['content_type_category'])) {
				unset($insert_data['content_type_category']);
			}
			
			$this->content_type_model->update_content_type($insert_data, array('content_type_id' => $content_type_id, ));
			
			/* Update content permission */
			$this->content_type_model->set_content_type_permission($content_type_id, $post_data);
			$message="Content type edited successfully";
			$redirect=base_url()."custom_content/content_type";
		}
		/* Insert new content type */
		else {
			$insert_data['content_type_category']=$post_data['content_type_category'];
			/* Generate machine name */
			$insert_data['content_type_machine_name']=clean_field_name($post_data['content_type_title']);

			/* Update content permission */
			$content_type_id=$this->content_type_model->add_content_type($insert_data);

			/* Update content permission */
			$this->content_type_model->set_content_type_permission($content_type_id, $post_data);

			$message="Content type added successfully";
			$redirect=base_url().'custom_content/content_type/add_fields/'.$content_type_id;
		}

		/* Delete deleted section */
		if(!empty($post_data['deleted_section'])) {
			$delete_section_filter=array(
				'WHERE_IN'=>array(
					'content_type_section_id'=>$post_data['deleted_section']
				),
				'content_type_id'=>$content_type_id
			);
			$this->content_type_model->delete_setion($delete_section_filter);
		}

		/* Update or add sections */
		if(!empty($post_data['section_title'])) {
			/* Get old sections */
			$old_sections=$this->content_type_model->get_content_type_section(array(
				'content_type_id'=>$content_type_id
			));
			$old_sections_id=array();
			if(!empty($old_sections)) {
				$old_sections_id=array_column($old_sections, 'content_type_section_id');
			}

			/* Loop each sections */
			foreach ($post_data['section_title'] as $section_id => $section_title) {
				$section_array=array(
					'content_type_id'=>$content_type_id,
					'section_title'=>$section_title,
					'content_id'=>$post_data['section_content_type'][$section_id]
				);
				/* Update section by comparing with old sections */
				if(in_array($section_id, $old_sections_id)) {
					$this->db->where('content_type_section_id',$section_id);
					$this->db->update('content_type_section',$section_array);
				}
				/* Insert sections */
				else {
					$this->db->insert('content_type_section',$section_array);
				}
			}
		}


		$this->db->trans_complete();
		/* Fail transaction */
		if ($this->db->trans_status() === FALSE) {
			$this->db->trans_rollback();
			set_message($this->db->_error_message());
			redirect_back();
		}
		/* Success transaction */
		else {
			$this->db->trans_commit();
			set_message($message, 'success');
			redirect($redirect);
		}
	}


	/*
		Add fields in content type
	*/
	public function add_fields($content_type_id) {
		permission_check(2);
		/* If id not pass */
		if(empty($content_type_id)) {
			show_404();
		}
		
		/* Decrypt id and content information */
		$content_type_id=decrypt_id($content_type_id);
		$data['content_type_id']=$content_type_id;
		$data['content_type_info']=$this->content_type_model->get_content_type(array(
			'content_type_id' => $content_type_id
		));

		/* If not found */
		if(empty($data['content_type_info'])) {
			show_404();
		}

		/* Assign first row to variable */
		$data['content_type_info']=$data['content_type_info'][0];
		$data['title']= 'Fields of "'.$data['content_type_info']['content_type_title'].'"';
		
		/* Get list of fields for content type */
		$field_condition = array(
			'ORDER_BY'=>array('content_type_field.rank' => 'asc'),
			'content_type_field.content_type_id'=>$content_type_id,
		);
		
		$data['field_list']=$this->content_type_model->get_content_type_fields($field_condition);
		
		/* Master data of field type for new field add */
		$data['field_type_list']=$this->content_type_model->get_field_type();
	
		view_with_master('custom_content/content_type_add_field',$data);
	}

	/* New method rewritten */
	public function save_add_fields() {
		permission_check(2);
		/* Set validations */
		$this->form_validation->set_rules('content_type_id', 'Content type', 'required');
		$this->form_validation->set_rules('field_title', 'Title', 'required');
		$this->form_validation->set_rules('field_type_id', 'Field type', 'required');

		if ($this->form_validation->run() == FALSE) {
			set_message(validation_errors());
			redirect_back();die;
		}

		$post_data=$this->input->post();

		$insert_data['field_title']=$post_data['field_title'];
		$insert_data['content_type_id']=$post_data['content_type_id'];
		$insert_data['field_type_id']=$post_data['field_type_id'];
		$insert_data['number_of_values']='1';

		/* Set default datepicker type */
		if($insert_data['field_type_id']=='11') {
			$insert_data['datepicker_type']='datepicker';
		}
		
		$insert_data['machine_name']=strtolower(clean_field_name($insert_data['field_title'])).'_'.$insert_data['content_type_id'];
		
		/* Check machine name existing check */
		$is_exists=$this->content_type_model->is_exists('content_type_field',array(
			'content_type_id'=>$post_data['content_type_id'],
			'machine_name'=>$insert_data['machine_name'],
			'deleted_at is null'=>null,
		));
		
		if($is_exists) {
			set_message("Filed ".$field_title." is already exist, Please provide some different value.");
			redirect_back();die;
		} 

		/* Set auto rank to newly added field */
		$condition['SELECT'] = 'COUNT(*) AS cnt';
		$condition['content_type_id'] = $insert_data['content_type_id'];
		$cnt_result=$this->content_type_model->get_content_type_fields($condition);
		
		$insert_data['rank'] = $cnt_result[0]['cnt']+1;

		/* Add new field */
		$this->content_type_model->add_field($insert_data);
		$content_type_fields_id=$this->db->insert_id();
		set_message("Content type field added successfully, now set Attributes and Validations","success");
		redirect(base_url().'custom_content/content_type/set_attribute/'.$content_type_fields_id);
	}

	/* Save newly added form field */
	public function save_field() {
		permission_check(2);

		/* Set validations */
		$this->form_validation->set_rules('content_type_id', 'Content type', 'required');
		$this->form_validation->set_rules('content_type_title', 'Title', 'required');
		$this->form_validation->set_rules('field_type_id', 'Field type', 'required');

		if ($this->form_validation->run() == FALSE) {
			set_message(validation_errors());
			redirect_back();die;
		}

		$content_type_fields_id=$this->input->post('content_type_fields_id');

		if(isset($content_type_fields_id) && $content_type_fields_id!='') {
			$ftype=$this->input->post('ftype');

			if($ftype==8) {
				$this->form_validation->set_rules('content_reference_id', 'Reference', 'required');
			}

			$this->form_validation->set_rules('content_type_fields_title', 'Field Title', 'required');
			
			if ($this->form_validation->run() == FALSE) {
				set_message(validation_errors());
				redirect_back();
			}

			$content_type_id=$this->input->post('content_type_id');
			$data['placeholder']=$this->input->post('placeholder');
			$data['content_type_fields_title']=$this->input->post('content_type_fields_title');
			$data['number_of_values']=$this->input->post('number_of_values');
			$data['default_value']=$this->input->post('default_value');
			$data['custom_validation']=$this->input->post('custom_validation');
			$data['help_text']=$this->input->post('help_text');
			$data['combobox_options']=$this->input->post('combobox_options');
			$data['content_reference_id']=$this->input->post('content_reference_id');
			$data['rank']=$this->input->post('rank');
			$data['css_classes']=$this->input->post('css_classes');
			$data['wrapper_classes']=$this->input->post('wrapper_classes');
			$data['extra_attribute']=$this->input->post('extra_attribute');
			$data['extra_attribute']=str_replace('"', "'", $data['extra_attribute']);
			$is_require=$this->input->post('is_require');
			$show_in_listview=$this->input->post('show_in_listview');

			$data['content_type_fields_validations']='';
			if(isset($is_require) && $is_require!='') {
				$data['content_type_fields_validations'].='required';
			}
			if(isset($show_in_listview) && $show_in_listview=='') {
				$data['show_in_listview']=0;
			} else {
				$data['show_in_listview']=1;				
			}
			$min_len=$this->input->post('min_len');
			if(isset($min_len) && $min_len!='') {
				if(!empty($data['content_type_fields_validations'])) {
					$data['content_type_fields_validations'].='|';
				}
				$data['content_type_fields_validations'].='min_length:'.$min_len;
			}
			$max_len=$this->input->post('max_len');
			if(isset($max_len) && $max_len!='') {
				if(!empty($data['content_type_fields_validations'])) {
					$data['content_type_fields_validations'].='|';
				}
				$data['content_type_fields_validations'].='max_length:'.$max_len;
			}
			/*
			$regex=$this->input->post('regex');
			if(isset($regex) && $regex!='') {
				if(!empty($data['content_type_fields_validations'])) {
					$data['content_type_fields_validations'].='|';
				}
				$data['content_type_fields_validations'].='regex:'.$regex;
			}
			*/
			$cols=$this->input->post('cols');
			if(isset($cols) && $cols!='') {
				if(!empty($data['content_type_fields_validations'])) {
					$data['content_type_fields_validations'].='|';
				}
				$data['content_type_fields_validations'].='cols:'.$cols;
			}
			$rows=$this->input->post('rows');
			if(isset($rows) && $rows!='') {
				if(!empty($data['content_type_fields_validations'])) {
					$data['content_type_fields_validations'].='|';
				}
				$data['content_type_fields_validations'].='rows:'.$rows;
			}
			$min=$this->input->post('min');
			if(isset($min) && $min!='') {
				if(!empty($data['content_type_fields_validations'])) {
					$data['content_type_fields_validations'].='|';
				}
				$data['content_type_fields_validations'].='min:'.$min;
			}
			$max=$this->input->post('max');
			if(isset($max) && $max!='') {
				if(!empty($data['content_type_fields_validations'])) {
					$data['content_type_fields_validations'].='|';
				}
				$data['content_type_fields_validations'].='max:'.$max;
			}
			$file_type=$this->input->post('file_type');
			if(isset($file_type) && $file_type!='') {
				if(!empty($data['content_type_fields_validations'])) {
					$data['content_type_fields_validations'].='|';
				}
				$data['content_type_fields_validations'].='is_file_type:'.$file_type;
			}
			$file_size=$this->input->post('file_size');
			if(isset($file_size) && $file_size!='') {
				if(!empty($data['content_type_fields_validations'])) {
					$data['content_type_fields_validations'].='|';
				}
				$data['content_type_fields_validations'].='file_size:'.$file_size;
			}


			$this->content_type_model->update_content_type_fields($data, $content_type_fields_id);
			set_message("Attributes and Validations updated successfully","success");
			redirect(base_url().'content_type/add_fields/'.$content_type_id);

		} 
		else {

			$this->form_validation->set_rules('content_type_fields_title', 'Field Title', 'required');
			$this->form_validation->set_rules('field_type_id', 'Field Type', 'required');

			if ($this->form_validation->run() == FALSE) {
				set_message(validation_errors());
				redirect_back();
			}

			$data['content_type_fields_title']=$this->input->post('content_type_fields_title');
			$data['content_type_fields_title']=trim($data['content_type_fields_title']);
			$data['content_type_id']=$this->input->post('content_type_id');
			$check['machine_name']=strtolower(clean($data['content_type_fields_title'])).'_'.$data['content_type_id'];
			$verify=$this->content_type_model->is_exists('content_type_fields',$check);
			if($verify) {
				set_message("Field name is already present !");
				redirect_back();
			} else {
				$data['machine_name']=$check['machine_name'];
			}
			$condition['SELECT'] = 'COUNT(*) AS cnt';
			$cnt_result=$this->content_type_model->get_content_type_fields($data['content_type_id'], $condition);
			$data['rank'] = $cnt_result[0]['cnt']+1;
			$data['field_type_id']=$this->input->post('field_type_id');
			$this->content_type_model->insert_content_type_fields($data);
			$content_type_fields_id=$this->db->insert_id();
			set_message("Content type field added successfully, now set Attributes and Validations","success");
			redirect(base_url().'content_type/set_attribute/'.$content_type_fields_id);
		}
	}

	public function save_field_order() {
		permission_check(2);

		$ids=$this->input->post('ids');
		foreach ($ids as $key => $value) {
			$update_data=array(
				'rank'=>$key+1
			);
			$this->content_type_model->update_field($update_data,array(
				'content_type_field_id'=>$value
			));
		}
		$return=array("status"=>'1',"message"=>"Order saved successfully");
		echo json_encode($return);	
	}

	public function set_attribute($content_type_field_id) {
		permission_check(2);
		if(empty($content_type_field_id)) {
			show_404();die;
		}

		/* Get field data from database */
		$data['field']=$this->content_type_model->get_content_type_fields(array(
			'content_type_field_id'=>$content_type_field_id
		));

		/* If content not found show 404 error */
		if(empty($data['field'])) {
			show_404();die;
		}
		$data['field']=$data['field'][0];
		$data['title']='Set Attribute for <b>'.$data['field']['field_title'].'</b>';
		
		/* If content reference type */
		if($data['field']['field_type_id'] == 8) {
			$condition1['content_type.content_type_id !='] = $data['field']['content_type_id'];
			$data['content_type_list']=$this->content_type_model->get_content_type($condition1);
		}
		$data['content_type_field_id']=$content_type_field_id;
		view_with_master('custom_content/set_attribute',$data);
	}

	/* Set Attribute of fields */
	public function save_attribute() {
		permission_check(2);
		$content_type_field_id=$this->input->post('content_type_field_id');

		/* Check if $content_type_field_id id available */
		if(empty($content_type_field_id)) {
			set_message("Invalid field selected, Please try again");
			redirect_back();die;
		}

		/* Get available data from database */
		$field_data = $this->content_type_model->get_content_type_fields(array(
			'content_type_field.content_type_field_id'=>$content_type_field_id
		));

		/* Check if Field available */
		if(empty($field_data)) {
			set_message("Invalid field selected, Please try again");
			redirect_back();die;
		}
		$field_data=$field_data[0];

		/* Set validations */
		$this->form_validation->set_rules('content_type_field_id', 'Content type field', 'required');
		$this->form_validation->set_rules('field_title', 'Title', 'required');
		$this->form_validation->set_rules('rank', 'Rank', 'required');

		/* Ignore number of fields for content reference */
		if($field_data['field_type_id'] != '14') {
			$this->form_validation->set_rules('number_of_values', 'number_of_values', 'required');
		}

		/* Required fields for slectbox, checkbox and radio button */
		if($field_data['field_type_id'] == '5' || $field_data['field_type_id'] == '9' || $field_data['field_type_id'] == '10') {
			$this->form_validation->set_rules('field_options', 'Options', 'required');
		}

		/* Required fields for Content reference field */
		if($field_data['field_type_id'] == '8') {
			$this->form_validation->set_rules('content_reference_id', 'Content reference', 'required');
		}
		
		/* Required fields for Content reference field */
		if($field_data['field_type_id'] == '11') {
			$this->form_validation->set_rules('datepicker_type', 'Datepicker type', 'required');
		}

		/* Required fields for table reference field */
		if($field_data['field_type_id'] == '13') {
			$this->form_validation->set_rules('custom_table', 'Table', 'required');
			$this->form_validation->set_rules('table_key_column', 'Key column', 'required');
			$this->form_validation->set_rules('table_value_column', 'Value column type', 'required');
		}

		if ($this->form_validation->run() == FALSE) {
			set_message(validation_errors());
			redirect_back();die;
		}

		$post_data=$this->input->post();
		/* Check if table and column are exist */
		if($field_data['field_type_id'] == '13') {
			/* Check if table exist or not */
			$rs=$this->db->query("SHOW TABLES LIKE ?",$post_data['custom_table']);
			$result=$rs->result_array();
			if(empty($result)) {
				set_message("Table ".$post_data['custom_table']." Dose not exist");
				$this->session->set_flashdata('old_data',$post_data);
				redirect_back();die;
			}

			/*check if column is exist */
			$rs=$this->db->query("SHOW COLUMNS FROM ".$post_data['custom_table']." like ?",$post_data['table_key_column']);
			$result=$rs->result_array();
			if(empty($result)) {
				set_message("Column ".$post_data['table_key_column']." Dose not exist");
				$this->session->set_flashdata('old_data',$post_data);
				redirect_back();die;
			}

			/* Allow comma separated multiple column, so check each field */
			$post_data['table_value_column']=explode(',', $post_data['table_value_column']);
			foreach ($post_data['table_value_column'] as $key=>$val_col) {
				$val_col_query="SHOW COLUMNS FROM ".$post_data['custom_table']." like ?";
				/*check if column is exist */
				$rs=$this->db->query($val_col_query,trim($val_col));
				$result=$rs->result_array();
				if(empty($result)) {
					set_message("Column ".$val_col." Dose not exist");
					$this->session->set_flashdata('old_data',$post_data);
					redirect_back();die;
				}
			}
		}

		/* Fixed data */
		$insert_data['field_title']=$this->input->post('field_title');
		$insert_data['rank']=$this->input->post('rank');
		$insert_data['number_of_values']=$this->input->post('number_of_values');
		$insert_data['custom_validation']=$this->input->post('custom_validation');
		$insert_data['default_value']=$this->input->post('default_value');
		$insert_data['extra_attribute']=trim($this->input->post('extra_attribute'));
		$insert_data['help_text']=$this->input->post('help_text');
		$insert_data['wrapper_classes']=$this->input->post('wrapper_classes');
		$insert_data['css_classes']=$this->input->post('css_classes');

		/* control depend upon fields */
		$insert_data['placeholder']=$this->input->post('placeholder');
		$insert_data['content_reference_id']=$this->input->post('content_reference_id');
		$insert_data['field_options']=$this->input->post('field_options');
		$insert_data['allow_multiple_option']=$this->input->post('allow_multiple_option');
		$insert_data['html_content']=$this->input->post('html_content');
		$insert_data['datepicker_type']=$this->input->post('datepicker_type');
		$insert_data['custom_table']=$this->input->post('custom_table');
		$insert_data['table_key_column']=$this->input->post('table_key_column');
		$insert_data['table_value_column']=$this->input->post('table_value_column');
		$insert_data['show_in_listview']=$this->input->post('show_in_listview');
		$insert_data['number_of_values']=$this->input->post('number_of_values');

		/* Number of value 1 for html content */
		if($field_data['field_type_id'] == '14') {
			$insert_data['number_of_values']=1;
		}

		/* Set validation rules */
		$insert_data['field_validation']='';
		$is_require=$this->input->post('is_require');
		if(!empty($is_require)) {
			$insert_data['field_validation'].='required';
		}

		/* Length Validation */
		$min_len=$this->input->post('min_len');
		if(!empty($min_len)) {
			if(!empty($insert_data['field_validation'])) {
				$insert_data['field_validation'].='|';
			}
			$insert_data['field_validation'].='min_length:'.$min_len;
		}
		$max_len=$this->input->post('max_len');
		if(isset($max_len) && $max_len!='') {
			if(!empty($insert_data['field_validation'])) {
				$insert_data['field_validation'].='|';
			}
			$insert_data['field_validation'].='max_length:'.$max_len;
		}

		/* Cols and rows validation for textarea */
		$cols=$this->input->post('cols');
		if(!empty($cols)) {
			if(!empty($insert_data['field_validation'])) {
				$insert_data['field_validation'].='|';
			}
			$insert_data['field_validation'].='cols:'.$cols;
		}
		$rows=$this->input->post('rows');
		if(!empty($rows)) {
			if(!empty($insert_data['field_validation'])) {
				$insert_data['field_validation'].='|';
			}
			$insert_data['field_validation'].='rows:'.$rows;
		}

		/* Min and max validation for number field */
		$min=$this->input->post('min');
		if(isset($min) && $min!='') {
			if(!empty($insert_data['field_validation'])) {
				$insert_data['field_validation'].='|';
			}
			$insert_data['field_validation'].='min:'.$min;
		}
		$max=$this->input->post('max');
		if(isset($max) && $max!='') {
			if(!empty($insert_data['field_validation'])) {
				$insert_data['field_validation'].='|';
			}
			$insert_data['field_validation'].='max:'.$max;
		}

		/* File type and file size validation for image and file control */
		$file_type=$this->input->post('file_type');
		if(isset($file_type) && $file_type!='') {
			if(!empty($insert_data['field_validation'])) {
				$insert_data['field_validation'].='|';
			}
			$insert_data['field_validation'].='is_file_type:'.$file_type;
		}
		$file_size=$this->input->post('file_size');
		if(isset($file_size) && $file_size!='') {
			if(!empty($insert_data['field_validation'])) {
				$insert_data['field_validation'].='|';
			}
			$insert_data['field_validation'].='file_size:'.$file_size;
		}

		/* Add new field */
		$this->content_type_model->update_field($insert_data, array(
			'content_type_field.content_type_field_id'=>$content_type_field_id
		));
		set_message("Attribute data updated","success");
		redirect(base_url()."custom_content/content_type/add_fields/".$field_data['content_type_id']);
	}

	public function delete_field($content_type_fields_id) {
		permission_check(2);
		if(empty($content_type_fields_id)) {
			show_404();die;
		}
		$update_field=array(
			'deleted_at'=>date('Y-m-d H:i:s'),
			'deleted_by'=>$this->session->userdata($this->config->item('user_id_insession'))
		);
		$this->content_type_model->update_field($update_field, array(
			'content_type_field_id'=>$content_type_fields_id
		));
		set_message("Content field deleted.","success");
		redirect_back();
	}

	function delete_content_type($content_type_id) {
		permission_check(2);
		if(empty($content_type_id)) {
			show_404();
		}

		/* decrypt id */
		$content_type_id=decrypt_id($content_type_id);

		/* Soft delete */
		$update_array=array(
			'deleted_at'=>date('Y-md H:i:s'),
			'deteled_by'=>$this->session->userdata($this->config->item('user_id_insession'))
		);

		/* Where condition */
		$filter=array(
			'content_type_id'=>$content_type_id
		);
		$this->content_type_model->update_content_type($update_array,$filter);

		set_message("Content type deleted successfully","success");
		redirect_back();
	}

	/* Change permission of content type */
	public function change_permission($content_type_id) {
		permission_check(array(2,11),'or');
		if(empty($content_type_id)) {
			show_404();
		}

		$data['edit_content_type']=$this->content_type_model->get_content_type(array(
			'content_type_id' => decrypt_id($content_type_id)
		));

		/* If invalid id pass */
		if(empty($data['edit_content_type'])) {
			show_404();
		}
		$data['role_list']=$this->content_type_model->get_role();
		$data['permission_list']=$this->content_type_model->get_content_permission_master();

		$data['edit_content_type']=$data['edit_content_type'][0];
		$data['title']="Edit ".$data['edit_content_type']['content_type_title'].' Permission';

		/* Get permissions */
		$content_type_permission=$this->content_type_model->get_content_type_permission(array(
			'content_type_permission.content_type_id' => $content_type_id, 
		));
		
		/* Arrange permission array according to form library */
		if(!empty($content_type_permission)) {
			$data['edit_permission']=array();
			foreach ($content_type_permission as $permission_row) {
				$data['edit_content_type'][$permission_row['permission_id']][$permission_row['role_id']]=$permission_row['role_id'];
			}
		}

		view_with_master('custom_content/content_type_permission',$data);
	}

	/* Save content type permission */
	public function save_content_type_permission($value='') {
		/* Check permission for content type and permission */
		permission_check(array(2,11),'or');

		$this->form_validation->set_rules('content_type_id', 'content_type', 'required');


		if ($this->form_validation->run() == FALSE) {
			set_message(validation_errors());
			redirect_back();
		}

		$content_type_id=$this->input->post('content_type_id');
		$post_data=$this->input->post();
		$this->db->trans_begin();
		$this->content_type_model->set_content_type_permission($content_type_id, $post_data);
		$this->db->trans_complete();
		/* Fail transaction */
		if ($this->db->trans_status() === FALSE) {
			$this->db->trans_rollback();
			set_message($this->db->_error_message());
			redirect_back();
		}
		/* Success transaction */
		else {
			$this->db->trans_commit();
			set_message("Content type permission updated", 'success');
			redirect(base_url()."custom_content/content/content_type_list");
		}

	}


}