<div id="container">
	<div class="row">
		<!-- Comment box -->
		<?php if(permission_check(array(100,6),'and',0)) { ?>
			<div class="col-md-6">
				<div class="box box-primary">
					<div class="box-header">
						<i class="fa fa-comments-o"></i>
						<h3 class="box-title">Comment</h3>
						<div class="box-tools pull-right" data-toggle="tooltip" title="Status">
						</div>
					</div>
					<div class="box-body chat" id="comment-list">
						<?php 
							foreach ($comment_list as $key => $comment) { 
								 $profile_pic= base_url().USER_PROFILE_PIC;
								if(!empty($comment['profile_pic'])) {
	                                $profile_pic= base_url().$comment['profile_pic'];
	                            }

						?>
						<div class="item">
							<img src="<?php echo $profile_pic; ?>" alt="user image" class="online">
							<p class="message">
								<a class="name">
									<small class="text-muted pull-right"><i class="fa fa-clock-o"></i>
										<?php echo dateformat($comment['updated_at'], 'F d, Y'); ?>
									</small>
									<?php echo $comment['name']; ?>
								</a>
								<?php echo strip_tags($comment['comment_body']); ?><br>
								<b>Content:</b> <br>
								<?php echo $comment['content_title']; ?>
							</p>
						</div>
						<?php } ?>
					</div>
					<div class="box-footer clearfix">
						<label class="label label-success">Active : <?php echo $active_cmnt_cnt[0]['cnt']; ?></label> 
						<label class="label label-warning">Pending : <?php echo $pending_cmnt_cnt[0]['cnt']; ?></label> 
						<label class="label label-info">Marked Span : <?php echo $marked_spam_cmnt_cnt[0]['cnt']; ?></label> 
						<label class="label label-danger">Blocked : <?php echo $blocked_cmnt_cnt[0]['cnt']; ?></label> 
					</div>
				</div>
			</div>
		<?php } ?>

		<?php if(permission_check(array(1,6),'and',0) && 1==2) { ?>
		<div class="col-md-6">
			<div class="box box-primary">
				<div class="box-header">
					<i class="fa fa-comments-o"></i>
					<h3 class="box-title">Comment</h3>
					<div class="box-tools pull-right" data-toggle="tooltip" title="Status">
					</div>
				</div>
				<div class="box-body chat" id="comment-list">
					<div class="col-sm-6 col-xs-12">
						<div class="info-box">
							<span class="info-box-icon bg-green"><i class="fa fa-globe"></i></span>

							<div class="info-box-content">
								<span class="info-box-text">Total</span>
								<span class="info-box-number"><?php echo $active_cmnt_cnt[0]['cnt']; ?></span>
							</div>
						</div>
					</div>
					<div class="col-sm-6 col-xs-12">
						<div class="info-box">
							<span class="info-box-icon bg-yellow"><i class="fa fa-bell-o"></i></span>

							<div class="info-box-content">
								<span class="info-box-text">Pending</span>
								<span class="info-box-number"><?php echo $pending_cmnt_cnt[0]['cnt']; ?></span>
							</div>
						</div>
					</div>
					<div class="col-sm-6 col-xs-12">
						<div class="info-box">
							<span class="info-box-icon bg-aqua"><i class="fa fa-comment-o"></i></span>

							<div class="info-box-content">
								<span class="info-box-text">Spam</span>
								<span class="info-box-number"><?php echo $marked_spam_cmnt_cnt[0]['cnt']; ?></span>
							</div>
						</div>
					</div>
					<div class="col-sm-6 col-xs-12">
						<div class="info-box">
							<span class="info-box-icon bg-aqua"><i class="fa fa-ban"></i></span>

							<div class="info-box-content">
								<span class="info-box-text">Blocked</span>
								<span class="info-box-number"><?php echo $blocked_cmnt_cnt[0]['cnt']; ?></span>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
		<?php } ?>

		<!-- Content type value count  -->
		<div class="col-md-6">
			<div class="box box-primary">
				<div class="box-header">
					<h3 class="box-title">Content Types</h3>
				</div>
				<!-- /.box-header -->
				<div class="box-body no-padding">
					<table class="table table-hover">
						<thead >
							<tr>
								<th>Content Type</th>
								<th>Count</th>
							</tr>
						</thead>
					</table>
					<div id="content-type-list">
						<table class="table table-hover">
								<?php if(!empty($content_list)) {
									foreach ($content_list as $key => $row) { ?>
									<tr>
										<td><?php echo $row['content_type_title']; ?></td>
										<td>
											<span class="badge bg-red">
												<?php	echo $row['cnt']; ?>	
											</span>
									    </td>
									</tr>
								<?php } } ?>
						</table>
					</div>
				</div>
			</div>
		</div>

	</div>
</div>
<style type="text/css">
	
	.box-footer .label {
		font-size: 13px;
	}
</style>

<script type="text/javascript">
	$(document).ready(function() {
		$('#comment-list').slimScroll({
			height: '250px',
			'min-height': '250px',
		});

		$('#content-type-list').slimScroll({
			height: '254px',
			width:'100%'
		});
	});
</script>

