

<div id="container">
	<div class="row">

		<!-- Content type value count  -->
		<div class="col-md-12">
			<div class="box box-primary">
				<div class="box-header">
					<h3 class="box-title">Content Types</h3>
				</div>
				<!-- /.box-header -->
				<div class="box-body no-padding">
					<table class="table table-hover">
						<thead >
							<tr>
								<th>Name</th>
								<th>Email</th>
								<th>Browser</th>
								<th>IP</th>
								<th>Date</th>
							</tr>
						</thead>
					</table>
					<div id="login_history-list">
						<table class="table table-hover">
								<?php if(!empty($login_history_list)) {
									foreach ($login_history_list as $key => $login_history) { ?>
									<tr>
										<td><?php echo $login_history['name']; ?></td>
										<td><?php echo $login_history['email'] ?></td>
										<td><?php echo $login_history['browser'] ?></td>
										<td><?php echo $login_history['ip'] ?></td>
										<td><?php echo dateformat($login_history['created_at'],'d F, Y') ?></td>
									</tr>
								<?php } } ?>
						</table>
					</div>
				</div>
			</div>
		</div>

	</div>
</div>

<script type="text/javascript">
	$(document).ready(function() {
		

		$('#login_history-list').slimScroll({
			height: '700px',
			width:'100%'
		});
	});
</script>

