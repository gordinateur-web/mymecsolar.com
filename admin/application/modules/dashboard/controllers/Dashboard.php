<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Dashboard extends CI_Controller {

	public function __construct() {
		parent::__construct();
		/* Permission for adminpanel access, menu module access */
		permission_check(array(1),'and');
		$this->load->model('dashboard/dashboard_model');
		$this->load->model('comment/comment_model');
		$this->load->model('user/user_model');
		$this->load->model('custom_content/content_type_model');
		$this->load->model('content_model');
		$this->config->load('custom_content');
		
	}

	public function index() {
		$data['title']="Dashboard";
		
		/* get comment */
		$latest_comment['LIMIT']='20';
		$data['comment_list']=$this->comment_model->get_comment_content($latest_comment);

		/* Total count of active comment  */
		$count_active['SELECT']='count(*) as cnt';
		$count_active['comment.comment_status']='active';
		$data['active_cmnt_cnt']=$this->comment_model->get_comment_content($count_active);
		//dsm($data['active_cmnt_cnt']);die;

		/* Total count of active comment  */
		$count_pending['SELECT']='count(*) as cnt';
		$count_pending['comment.comment_status']='pending';
		$data['pending_cmnt_cnt']=$this->comment_model->get_comment_content($count_pending);

		/* Total count of active comment  */
		$count_marked_spam['SELECT']='count(*) as cnt';
		$count_marked_spam['comment.comment_status']='marked_spam';
		$data['marked_spam_cmnt_cnt']=$this->comment_model->get_comment_content($count_marked_spam);

		/* Total count of active comment  */
		$count_blocked['SELECT']='count(*) as cnt';
		$count_blocked['comment.comment_status']='blocked';
		$data['blocked_cmnt_cnt']=$this->comment_model->get_comment_content($count_blocked);
		

		/* get content type with content count */
		/* For search model */
		/*$data['content_list']=$this->content_type_model->get_content_type(array(
			'ORDER_BY'=>array('content_type.content_type_title'=>'asc'),
			'content_type.content_type_category'=>'content',
		));*/
	

		/* Count of each content in each content type  */
		$data['content_list']=$this->content_model->get_content(array(
			'SELECT'=>'count(*) as cnt, content.content_type_id, content_type.content_type_title',
			'content_type.content_type_category'=>'content',
			'ORDER_BY'=>array('content_type.list_rank'=>'asc'),
			'GROUP_BY'=>'content.content_type_id'
		));
		//$data['content_count']=array_column($data['content_count'],'cnt','content_type_id');
		
		view_with_master('dashboard/dashboard',$data);
	}

	public function login_history() {
		$data['title']="Login History";

		/* get login history list */
		$filter_login_history['LIMIT']='100';
		$data['login_history_list']=$this->user_model->get_login_history($filter_login_history);
		//dsm($data['login_history_list']);die;
	
		view_with_master('dashboard/login_history',$data);
	}

	public function get_subcategory($category=false) {
		$this->load->model('custom_content/content_model');
		/* get did you know fields name */
		$data['knowledge_bank_subcategory_fields']=$this->config->item('knowledge_bank_subcategory_fields');

		/* get did you know list */
		$filter_subcategory['content_value.content_value']=$category;
		$filter_subcategory['content_value.field_machine_name']=$data['knowledge_bank_subcategory_fields']['category'];
		$filter_subcategory['content.content_type_id']=$data['knowledge_bank_subcategory_fields']['content_type_id'];
		$filter_subcategory['content.content_published']=1;
		$filter_subcategory['WHERE']="(content.content_status is null or content.content_status='Approved')";
		$subcategory=$this->content_model->get_value_based_content($filter_subcategory);
		
		foreach ($subcategory as $key => $value) {
			$subcategory[$key]=array(
				'content_id'=>$value['content']['content_id'],
				'content_title'=>$value['content']['content_title'],
			);
		}
		echo json_encode ($subcategory);
	}
}