<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Custom_block_model extends CI_model {

	public function __construct() {
	}

	/*
		for custom block 1
	*/

	public function block_application_form() {
		$apply=$this->uri->segment('1');
		if($apply=='apply'){
	  	$content_type_id='8';
      	/* Get content type data */
      	$filter['content_type.content_type_id']=$content_type_id;
      	$data['contect_content']=$this->content_type_model->get_content_type($filter);

      
		$data['contect_content']=$data['contect_content'][0];
		$view_data= $this->content_model->add_content_form($content_type_id);
		$data['contact_us_form']=$view_data;
  
  		$data['page_fields']=$this->config->item('page_fields');
	
		$data['application_form']=$this->content_model->get_content_with_value(array(
			'content.content_id'=>242
		));
		
		$data['application_form']=$data['application_form'][0];

		/* Set title and meta tags */
		$data['page_title']=$data['application_form']['content']['content_title'];
		return $this->load->view("block/custom_block/apply",$data, true);
		}
	}

	public function block_feedback_form() {
		$apply=$this->uri->segment('1');
		if($apply=='feedback'){
		  	$content_type_id='7';
	      	/* Get content type data */
	      	$filter['content_type.content_type_id']=$content_type_id;
	      	$data['contect_content']=$this->content_type_model->get_content_type($filter);
			$data['contect_content']=$data['contect_content'][0];

			$view_data= $this->content_model->add_content_form($content_type_id);
			$data['contact_us_form']=$view_data;
	  		
	  		$data['page_fields']=$this->config->item('page_fields');
		
			$data['feedback_form']=$this->content_model->get_content_with_value(array(
				'content.content_id'=>244
			));
			
			$data['feedback_form']=$data['feedback_form'][0];
			
			/* Set title and meta tags */
			$data['page_title']=$data['feedback_form']['content']['content_title'];
			return $this->load->view("block/custom_block/feedback",$data, true);
		}
	}

	public function block_sidebar_menu_content() {
		/* load menu model */
	   	$this->load->model('menu/menu_model');
	   	// Get current url path
	    $page_name=uri_string();


	    /* current menu */
	    $data['link_parent_id']=$this->menu_model->get_active_menus($page_name,'');
	    
	    

	    if(empty($data['link_parent_id'])) {
	    	return '';
	    }
	    $link_parent=$data['link_parent_id']['link_parent_id'];

	    /* parent of current menu */
	    $main_parent=$this->menu_model->get_sub_menu($link_parent);
	    
	    if(empty($main_parent)) {
	    	return '';
	    }

	    /* get menu list */
	    $menu_parent=$main_parent['link_parent_id'];
	    if($main_parent['link_parent_id'] == '0') {
	    	$main_parent=$this->menu_model->get_sub_menu($data['link_parent_id']['menu_link_id']);
	    	$menu_parent=$main_parent['link_parent_id'];
	    }
	    ///dsm($menu_parent);die;

	    /* menu list */
	    $data['menu']=$this->menu_model->get_child_menu($menu_parent);

	    return $this->load->view("block/custom_block/sidebar_menu_content",$data, true);
	}

	public function block_sidebar_submenu_content() {
		/* load menu model */
	   	$this->load->model('menu/menu_model');
	   	// Get current url path
	    $page_name=uri_string();
	    /* current menu */
	    $sub_menu_active=$this->menu_model->get_active_menus($page_name);
	    $data['menu_active']=$this->menu_model->get_sub_menu($sub_menu_active['link_parent_id']);
	   // $data['link_parent_id']=$data['menu_active']['menu_link_id'];
	    //dsm($data['link_parent_id']);die;
	   
	    if(empty($sub_menu_active)) {
	    	return '';
	    }

	    /* sub menu list */
	    $data['sub_menu']=$this->menu_model->get_child_menu($sub_menu_active['menu_link_id']);
	    if(empty($data['sub_menu'])) {
	    	$data['sub_menu']=$this->menu_model->get_child_menu($sub_menu_active['link_parent_id']);
	    }

	    return $this->load->view("block/custom_block/sidebar_submenu_content",$data, true);
	}
}