<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Block_model extends CI_model {

	public function __construct() {
		$this->load->model('block/custom_block_model');
	}

	public function add_block($data) {
		$data['created_at']=date('Y-m-d H:i:s');
		$data['created_by']=$this->session->userdata('user_id');
		$this->db->insert('block',$data);
		return $this->db->insert_id();
	}
	
	public function check_block_title_unique($value) {
		$edit_id=$this->input->post('block_id');
		return check_unique($value, 'block_title','block','block_id', $edit_id);
	}

	public function update_block($data, $edit_id) {
		$data['updated_at']=date('Y-m-d H:i:s');
		$data['updated_by']=$this->session->userdata('user_id');
		$this->db->where('block_id',$edit_id);
		return $this->db->update('block',$data);
	} 

	/*
		return blocks only which has assign region with their region
	*/
	public function get_active_block($filter=false) {
		if($filter){
			apply_filter($filter);	
		}
		/* Ignore deleted content types and content */
		$this->db->where('block.deleted_at is null',null,false);
		$this->db->where('block_visibility.deleted_at is null',null,false);
		$this->db->join('block','block_visibility.block_id=block.block_id','left');
		$this->db->join('region_master','region_master.region_master_id=block_visibility.region_id');
		$rs=$this->db->get('block_visibility');
		return $rs->result_array();
	}


	public function get_block($filter=false) {
		if($filter){
			apply_filter($filter);	
		}
		/* Ignore deleted content types and content */
		$this->db->where('deleted_at is null',null,false);
		$rs=$this->db->get('block');
		return $rs->result_array();
	}

	public function delete_block($filter=false) {
		if($filter){
			apply_filter($filter);
		}
		$this->db->delete('block');
	}

	public function add_block_visibility($data) {
		$this->db->insert('block_visibility',$data);
		return $this->db->insert_id();
	}

	public function update_block_visibility($data, $edit_id) {
		$this->db->where('block_visibility_id',$edit_id);
		return $this->db->update('block_visibility',$data);
	} 

	public function get_block_visibility($filter=false) {
		if($filter){
			apply_filter($filter);	
		}
		/* Ignore deleted content types and content */
		$this->db->where('deleted_at is null',null,false);
		$rs=$this->db->get('block_visibility');
		return $rs->result_array();
	}

	public function delete_block_visibility($data, $filter=false) {
		if($filter){
			apply_filter($filter);
		}
		return $this->db->update('block_visibility',$data);
	}

	/*
		Return theme specific region list from region_master table
	*/
	public function get_region($filter=false) {
		if($filter){
			apply_filter($filter);	
		}
		$rs=$this->db->get('region_master');
		return $rs->result_array();
	}

	/*
		How to register custom block
		1) Add new custom block from block/block_add/
		2) Create function in Custom_block_model with prefix block_ and block_machine_name
		3) System will call this function when its required
		
		#recommendation
		Store all required custom block view inside modules/block/views/custom_block folder
	*/

	/*
		Return view of custom block
	*/
	public function get_custom_block_view($block_machine_name, $view_data=false) {
		if(empty($block_machine_name)) {
			show_error("get_custom_block_view function required Block machine name",500);
		}

		$block=$this->get_block(array(
			'block_machine_name'=>$block_machine_name,
			'block_type'=>'custom',
		));

		/* Check if valid block or not */
		if(empty($block)) {
			//show_error($block_machine_name." is not valid block",500);
			return '';
		}

		$method_name='block_'.$block_machine_name;
		/* Check if method exist in custom_block_model */
		if(!method_exists($this->custom_block_model,$method_name)) {
			show_error('block_'.$block_machine_name." Method dose not exist in custom_block_model",500);	
			return '';
		}

		/* Return method function data */
		return $this->custom_block_model->$method_name($view_data);
	}

	/*
		
	*/
	public function get_blocks_by_region($region) {
		
	}


	/*
		Return region list with block assigned to them for current loaded page
		$page_uri - 
		$view_data - All data which is passed to view.
	*/
	public function get_page_blocks($page_uri=false,$view_data=false) {
		$return_array=array();
		/* Get block and regions */
		$region_list=$this->get_region();

		$active_block=$this->get_active_block(array(
			'block_visibility.enable'=>'1',
			'ORDER_BY'=>array('block_visibility.block_weight'=>'asc')
		));
		$active_block=parent_child_array($active_block,'region_id');

		/* Current page url for compare in block list */
		if(!$page_uri) {
			$page_uri=uri_string();
		}

		foreach ($region_list as $region_row) {
			$i=0;
			/* if not block assigned */
			if(empty($active_block[$region_row['region_master_id']])) {
				continue;
			}

			/* Loop through each block */
			foreach ($active_block[$region_row['region_master_id']] as $block_row) {
				/* Check if current block is visible in this region */
				if($this->check_block_visibility($region_row['region_master_id'], $block_row, $page_uri)){
					
					$return_array[$region_row['region_machine_name']][$i]=$block_row;
					
					/* Get html content of custom block */
					if($block_row['block_type']=='custom') {
						$return_array[$region_row['region_machine_name']][$i]['block_body']=$this->get_custom_block_view($block_row['block_machine_name'],$view_data);
					}
					$i++;
				}
			}
		}
		return $return_array;
	}

	/*
		$region_id - Row from region master table
		$region_block_list - Block assigned to particular region, contain array result of block table join with block and block visibility
		$url - URL on which visibility to check

		@return true or false based on check
	*/
	public function check_block_visibility($region_id, $block_row, $url) {

		/* Check if assigned to region */
		if($block_row['region_id']!=$region_id) {
			return false;
		}

		/* Check if URL with pages match or not */
		$url_list=explode('/', $url);
		$page_available=false;

		/* Check if full url available */
		if(strpos($block_row['pages_list'], $url.PHP_EOL)!==false) {
			$page_available=true;
		}
		else {
			$url_concat='';
			foreach ($url_list as $value) {
				$url_concat.='/'.$value;
				$url_concat=trim($url_concat,'/');
				if(strpos($block_row['pages_list'], $url_concat.'/*'.PHP_EOL)!==false || strpos($block_row['pages_list'], $url_concat.PHP_EOL)!==false) {
					$page_available=true;
				}
			}
		}

		if($block_row['visibility_type'] == 'listed_pages' && $page_available==true) {
			return true;
		}
		if($block_row['visibility_type'] == 'ignore_listed' && !$page_available) {
			return true;
		}

		return false;
	}

	/* 
		Return html content of block with necessary wrapper divs
		Block with row from block table

	 */
	public function render_block($block) {
		$html='
			<div class="block block-'.$block['block_machine_name'].'">
				<div class="block-content">
				'.$block['block_body'].'
				</div>
			</div>
		';
		return $html;
	}

	
}