<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Block extends CI_Controller {

	public function __construct() {
		parent::__construct();
		/* Permission for adminpanel access, Block module access */
		permission_check(array(1,4));

		$this->load->model('block_model');

	}

	public function index() {
		$data['title']="Blocks";
		
		$data['block_list']=$this->block_model->get_block();
		
		/* Get visible region of each block */
		$blocks_visible=$this->block_model->get_active_block(array(
			'SELECT'=>"block.block_id, GROUP_CONCAT(region_master.region_title SEPARATOR ', ')as assigned_region",
			'GROUP_BY'=>'block_visibility.block_id'
		));
		$data['blocks_visible']=array_column($blocks_visible, 'assigned_region','block_id');
		view_with_master('block/block_list',$data);
	}

	public function block_visibility_list() {
		$data['title']="Blocks";
		$filter_block['ORDER_BY']=array('block_visibility.block_weight'=>'ASC');
		$blocks=$this->block_model->get_active_block($filter_block);

		$data['block_list']=parent_child($blocks, 'region_master_id');
		//dsm($data['block_list']);die;
		view_with_master('block/block_visibility_list',$data);
	}

	public function block_add($block_id=false) {
		$data['title']="Add Blocks";
		
		if(!empty($block_id)) {
			$edit_block=$this->block_model->get_block(array('block.block_id'=>$block_id));
			$data['edit_data']=$edit_block[0];
		}
		view_with_master('block/block_add',$data);
	}

	public function block_save() {
		$this->load->library('form_validation');
		$validate_rules=array(
			array(
				'field'=>'block_title',
				'label'=>'Title',
				'rules'=>array(
 					'required',
 					array(
 						'check_block_title',
 						array($this->block_model, 'check_block_title_unique')
 					)
 				)
			)
		);

		$this->form_validation->set_rules($validate_rules);
		$this->form_validation->set_message('check_block_title','%s is already Exist');
		if ($this->form_validation->run() == FALSE) {
			set_message(validation_errors());
			$this->session->set_flashdata('old_data',$this->input->post());
			redirect_back();
			return 0;
		}
		/* post array */
		$post_array= $this->input->post();
		$edit_id=$this->input->post('block_id');

		$block_data = array(
			'block_type'=>$post_array['block_type'],
			'block_title'=>$post_array['block_title'],
			'block_desc'=>$post_array['block_desc'],
			'block_body'=>$post_array['block_body'],
		);

		/* edit block data */
		if(!empty($edit_id)) {
			$this->block_model->update_block($block_data, $edit_id);
		}
		else {
			/* insert block data */
			$block_data['block_machine_name']=strtolower(clean_field_name($post_array['block_title']));
			$block_id=$this->block_model->add_block($block_data);
		}
		set_message("Block data saved","success");
		redirect(base_url().'block/');
	}


	public function block_delete($id) {
		$user_id=$this->session->userdata('user_id');
		if(empty($id)) {
			show_404();
		}

		$block_data['deleted_at']=date('Y-m-d H:i:s');
		$block_data['deleted_by']=$user_id;
		$this->block_model->update_block($block_data, $id);
		$this->block_model->update_block_visibility($block_data, $id);
		set_message("Block deleted successfully","success");
		redirect_back();
	}

	public function block_visibility_delete($id) {
		$user_id=$this->session->userdata('user_id');
		if(empty($id)) {
			show_404();
		}
		$block_data['deleted_at']=date('Y-m-d H:i:s');
		$block_data['deleted_by']=$user_id;
		$this->block_model->delete_block_visibility($block_data, array('block_visibility_id'=>$id));
		set_message("Block visibility deleted successfully","success");
		redirect_back();
	}

	public function block_configure($block_id, $action=false) {
		if(empty($block_id)) {
			show_404();
		}

		if(!empty($block_id) && $action=='edit') {
			$edit_block=$this->block_model->get_active_block(array(
				'block_visibility.block_visibility_id'=>$block_id
			));
			if(empty($edit_block)) {
				show_404();
			}
			$data['edit_data']=$edit_block[0];
		}
		else {
			$edit_block=$this->block_model->get_block(array('block.block_id'=>$block_id));
			$data['edit_data']=$edit_block[0];
		}
		$data['action']=$action;
		$data['title']="Block Configure <b>".$data['edit_data']['block_title']."</b>";
		$data['region_list']=$this->block_model->get_region();
		view_with_master('block/block_configure',$data);
	}

	public function block_configure_save() {

		$this->load->library('form_validation');
		$action=$this->input->post('action');
		$block_id=$this->input->post('block_id');
		$region_id=$this->input->post('region_id');
		$block_visibility_id=$this->input->post('block_visibility_id');

		/* Query filter to check unique of block and region id */
		$unique_filter=array(
			'block_visibility.region_id'=>$region_id,
			'block_visibility.block_id'=>$block_id
		);

		$validate_rules=array(
			array(
				'field'=>'region_id',
				'label'=>'Region',
				'rules'=>'required',
			),
			array(
				'field'=>'block_weight',
				'label'=>'Block Weight',
				'rules'=>'required',
			),
			array(
				'field'=>'visibility_type',
				'label'=>'Visibility Type',
				'rules'=>'required',
			),
			array(
				'field'=>'enable',
				'label'=>'Status',
				'rules'=>'required',
			),
			array(
				'field'=>'pages_list',
				'label'=>'Page List',
				'rules'=>'required',
			),
			array(
				'field'=>'block_id',
				'label'=>'Block',
				'rules'=>'required',
			)
		);

		if($action =='edit') {
			$validate_rules[]=array(
				'field'=>'block_visibility_id',
				'label'=>'block_visibility_id',
				'rules'=>'required',
			);
			$unique_filter['block_visibility.block_visibility_id !=']=$block_visibility_id;
		}

		$this->form_validation->set_rules($validate_rules);
		
		if ($this->form_validation->run() == FALSE) {
			set_message(validation_errors());
			$this->session->set_flashdata('old_data',$this->input->post());
			redirect_back();
			return 0;
		}

		/* post array */
		$post_array= $this->input->post();
		
		$action=$this->input->post('action');


		$unique_region=$this->block_model->get_active_block($unique_filter);
		if(!empty($unique_region)) {
			set_message("Block already exist in ".$unique_region[0]['region_title']." available.");
			redirect_back();
		}


		/* Remove training and leading / from each url */
		$page_list = explode(PHP_EOL, trim($post_array['pages_list']));
		$page_string='';
		foreach ($page_list as $page) {
			$page_string.=PHP_EOL.trim(trim($page),'/');
			$page_string=trim($page_string);
		}

		$block_visibility_data=array(
			'region_id'=>$post_array['region_id'],
			'block_weight'=>$post_array['block_weight'],
			'visibility_type'=>$post_array['visibility_type'],
			'pages_list'=>$page_string.PHP_EOL,
			'enable'=>$post_array['enable'],
		);

		/* edit block data */
		if(!empty($block_visibility_id) && $action=='edit') {
			/* get block visibility id */
			$block_visibility=$this->block_model->get_block_visibility(array(
				'block_id'=>$block_id
			));

			/* edit block visibility */
			if(!empty($block_visibility)) {
				$this->block_model->update_block_visibility($block_visibility_data, $block_visibility[0]['block_visibility_id']);
			}
			else {
				set_message("Invalid block selected");
				redirect_back();
			}
		}
		else {
			$block_visibility_data['block_id']=$block_id;
			$this->block_model->add_block_visibility($block_visibility_data);
		}
		
		set_message("Block configure data saved","success");
		redirect(base_url().'block/block_visibility_list');
	}
	
}