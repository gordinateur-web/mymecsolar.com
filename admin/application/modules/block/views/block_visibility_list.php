<div class="row">
	<?php $base_url=base_url();?>
		<div class="col-md-12">
			<div class="box box-primary">
				<div class="box-header with-border">
					<div class="box-title">
						<a href="<?php echo $base_url.'block/block_add/'; ?>" class="btn btn-sm btn-primary" title="Add New"><i class="fa fa-plus"></i> Add</a>
					</div>
				</div>

				<div class="panel-body">
					<div class="box-body table-responsive">
						<table class="table table-bordered table-striped">
							<thead>
								<tr>
									<th>Title</th>
									<th>Region</th>
									<th>Weight</th>
									<th>Enable</th>
									<th width="5%">Action</th>
								</tr>
							</thead>
							<tbody> 
							  <?php 
							   foreach ($block_list as $re_key => $region_row) {
							   	
							   	if($re_key!='no_parent') {
							    ?>
							  	<tr class="warning">
						  			<td colspan="5">
						  				<b><?php echo $region_row[0]['region_title']; ?></b>
						  			</td>
						  		</tr>
							  <?php	foreach ($region_row as $key => $block_row) { 
							  	$encrypt_id=encrypt_id($block_row['block_visibility_id']); ?>
								<tr>
									<td><?php echo $block_row['block_title']; ?></td>
									
									<td><?php echo $block_row['region_title']; ?></td>
									<td><?php echo $block_row['block_weight']; ?></td>
									<td>
										<?php
										if($block_row['enable']=='1') {
											echo '<label class="label label-success">Enable</label>';
										}
										if($block_row['enable']=='0') {
											echo '<label class="label label-danger">Disable</label>';
										}
										?>	
									</td>
									<td>
										<div class="btn-group options">
											<button type="button" class="btn btn-default btn-sm" dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
												<i class="fa fa-cog"></i> Options
											</button>
											 
											<ul class="dropdown-menu">
												<li>
													<a href="<?php echo $base_url.'block/block_configure/'.$encrypt_id.'/edit'; ?>">
														<i class="fa fa-edit fa-margin"></i> Edit visibility
													</a>
												</li>
												
												<li>
													<a data-id="<?php echo $encrypt_id; ?>" class="pointer delete-block-visibility">
														<i class="fa fa-trash-o fa-margin"></i> Delete
													</a>
												</li>
											</ul>
										</div>
									</td>
								</tr>
							<?php } } } ?>  


							</tbody>
						</table>
					</div>
				</div>
			</div>
	</div>
</div>


<script type="text/javascript">
	$(document).ready(function() {
		$('.delete-block-visibility').click(function() {
			var id=$(this).attr('data-id');
			swal({
				title: "Are you sure?",
				text: "Block to delete?",
				type: "warning",
				showCancelButton: true,
				confirmButtonColor: "#DD6B55",
				confirmButtonText: "Yes, delete it!",
				closeOnConfirm: false
			},
			function(){
				window.location = base_url+'block/block_visibility_delete/'+id;
			});
		});
	})

</script>

