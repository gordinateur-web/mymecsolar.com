<?php
$base_url = base_url();
$form_model=array();
if(!empty($edit_data)) {
	$form_model=$edit_data;
}
/* Fill old data */
$old_data=$this->session->flashdata('old_data');
if(!empty($old_data)) {
	$form_model=$old_data;  
}
echo $this->form->form_model($form_model, $base_url.'block/block/block_save/',array('name'=>'save_block','id'=>'block_form', 'class'=>'validate-form'));
if(!empty($edit_data)) {
	echo $this->form->form_hidden('block_id');
}
?>

<div class="row">
	<?php $base_url=base_url();?>
	<div class="col-md-12">
		<div class="box box-primary">
				<div class="box-body">
					<div id="model_errors"></div>
					<div class="col-md-6 col-sm-6">
						<div class="form-group">
							<label>Title <span class="text-danger">*</span></label>
							<?php 
							$other_option=array(
								'class'=>'form-control',
								'placeholder'=>'Title',
								'data-validation'=>'required',
								'title'=>'Title',
							);
							echo $this->form->form_input('block_title', $other_option); 
							?>
						</div>
					</div>

					<div class="col-md-3 col-sm-6">
						<div class="form-group">
							<label>Block Type <span class="text-danger">*</span></label>
							<?php 
							$other_option=array(
								'class'=>'form-control',
								'placeholder'=>'Title',
								'data-validation'=>'required',
								'title'=>'Block Type',
							);
							//$option=array('html'=>'HTML','custom'=>'Custom');
							$option=array('html'=>'HTML','custom'=>'Custom');
							echo $this->form->form_dropdown('block_type',$option,'', $other_option); 
							?>
						</div>
					</div>

					<div class="col-md-12 col-sm-6">
						<div class="form-group">
							<label>Description </label>
							<?php 
							$other_option=array(
								'class'=>'form-control',
								'placeholder'=>'Description',
								'row'=>'2',
							);
							echo $this->form->form_textarea('block_desc', $other_option); 
							?>
						</div>
					</div>

					<div class="col-md-12 col-sm-6 block_body" >
						<div class="form-group">
							<label>Block Body </label>
							<?php 
							$other_option=array(
								'class'=>'form-control',
								'placeholder'=>'Body',
								'row'=>'5',
							);
							echo $this->form->form_textarea('block_body', $other_option); 
							?>
						</div>
					</div>

				</div>

			<div class="box-footer with-border">
				<div class="box-tools pull-right">
					<input type="reset" class="btn btn-default" value="Reset">
					<input type="submit" class="btn btn-primary" value="Submit">
				</div>
			</div>

		</div>
	</div>
</div>

<?php echo $this->form->form_close(); ?>

<script src="<?php echo base_url().'public/plugins/ckeditor/ckeditor.js';?>" type="text/javascript"></script>
<script type="text/javascript">

	$(document).ready(function() {	
		CKEDITOR.replace('block_body');
		CKEDITOR.config.disableNativeSpellChecker = false;
		CKEDITOR.config.allowedContent=true;
		CKEDITOR.config.extraPlugins = 'cleanuploader,smiley';

		$('select[name="block_type"]').on('change',function(){
			var block_type=$('select[name="block_type"]').val();
			if(block_type=='custom') {
				$('.block_body').css('display','none');
			}
			else {
				$('.block_body').css('display','block');
			}

		});
	});

</script>