<?php
$base_url = base_url();
$form_model=array();
if(!empty($edit_data)) {
	$form_model=$edit_data;
}
/* Fill old data */
$old_data=$this->session->flashdata('old_data');
if(!empty($old_data)) {
	$form_model=$old_data;  
}
echo $this->form->form_model($form_model, $base_url.'block/block/block_configure_save',array('name'=>'save_block_configure','id'=>'block_configure_form',"class"=>"validate-form"));
if(!empty($edit_data)) {
	echo $this->form->form_hidden('block_id');
	echo $this->form->form_hidden('block_visibility_id');
	echo $this->form->form_hidden('action', $action);
}
?>

<div class="row">
	<?php $base_url=base_url();?>
	<div class="col-md-12">
		<div class="box box-primary">

			<div class="box-header with-border">
			</div>
				<div class="box-body">
					<div id="model_errors"></div>
					
					<div class="col-md-3 col-sm-6">
						<div class="form-group">
							<label>Region <span class="text-danger">*</span></label>
							<?php 
							$other_option=array(
								'class'=>'form-control',
								'placeholder'=>'Region',
								'data-validation'=>'required',
								'title'=>'Region',
							);
							echo $this->form->form_dropdown_fromdatabase('region_id',$region_list ,'region_master_id','region_title','',$other_option); 
							?>
						</div>
					</div> 

					<div class="col-md-3 col-sm-6">
						<div class="form-group">
							<label>Block Weight <span class="text-danger">*</span></label>
							<?php 
							$other_option=array(
								'class'=>'form-control number-field',
								'placeholder'=>'Weight',
								'data-validation'=>'required',
								'title'=>'Block Weight',
							);
							echo $this->form->form_input('block_weight','','0',$other_option); 
							?>
						</div>
					</div> 
					

					<div class="col-md-3 col-sm-6">
						<div class="form-group">
							<label>Visibility Type <span class="text-danger">*</span></label>
							<?php 
							$other_option=array(
								'class'=>'form-control',
								'placeholder'=>'Visibility Type',
								'data-validation'=>'required',
								'title'=>'Visibility Type',
							);
							$option=array('ignore_listed'=>'Ignore Listed', 'listed_pages'=>'Listed Pages');
							echo $this->form->form_dropdown('visibility_type', $option ,'','',$other_option); 
							?>
						</div>
					</div>

					<div class="col-md-3 col-sm-6">
							<div class="form-group">
								<label>Status <span class="text-danger">*</span></label>
								<?php 
								$other_option=array(
									'class'=>'form-control',
									'placeholder'=>'Status',
									'data-validation'=>'required',
									'title'=>'Status',
									);
								$option= array('1'=>'Enable','0'=>'Disable');
								echo $this->form->form_dropdown('enable',$option,'','', $other_option); 
								?>
							</div>
						</div>
					<div class="clearfix"></div>

					<div class="col-md-12 col-sm-6">
						<div class="form-group">
							<label>Page List <span class="text-danger">*</span></label>
							<?php 
							$other_option=array(
								'class'=>'form-control',
								'placeholder'=>'Page List',
								'data-validation'=>'required',
								'title'=>'Page List',
								'rows'=>'5',
							);
							echo $this->form->form_textarea('pages_list',$other_option); 
							?>
							<span class="help-block">
								Specify pages by using their paths. Enter one path per line. The '*' character is a wildcard. Example paths are blog for the blog page and blog/* for every personal blog.
							</span>

						</div>
					</div> 


				</div>

			<div class="box-footer with-border">
				<div class="box-tools pull-right">
					<input type="reset" class="btn btn-default" value="Reset">
					<input type="submit" class="btn btn-primary" value="Submit">
				</div>
			</div>

		</div>
	</div>
</div>

<?php echo $this->form->form_close(); ?>


<script type="text/javascript">

	$(document).ready(function() {	
		jQuery.validate({
			form : '#block_form'
		});
	});

</script>