<div class="row">
	<?php $base_url=base_url();?>
		<div class="col-md-12">
			<div class="box box-primary">
				<div class="box-header with-border">
					<div class="box-title">
						<a href="<?php echo $base_url.'block/block_add/'; ?>" class="btn btn-sm btn-primary" title="Add New"><i class="fa fa-plus"></i> Add</a>
					</div>
				</div>

				<div class="panel-body">
					<div class="box-body table-responsive">
						<table class='table table-bordered table-striped' id="full-height-datatable">
							<thead>
								<tr>
									<th>Title</th>
									<th>Description</th>
									<th>Type</th>
									<th>Visible-On</th>
									<th width="5%">Action</th>
								</tr>
							</thead>
                    
							<tbody> 
							  <?php	foreach ($block_list as $key => $block_row) {
							  	$encrypt_id=encrypt_id($block_row['block_id']); ?>
								<tr>
									<td><?php echo $block_row['block_title']; ?></td>
									<td><?php echo $block_row['block_desc']; ?></td>
									<td><?php echo $block_row['block_type']; ?></td>
									<td>
										<?php 
											if(!empty($blocks_visible[$block_row['block_id']])) {
												echo $blocks_visible[$block_row['block_id']];
											}
										?>
									</td>
									<td>
										<div class="btn-group options">
											<button type="button" class="btn btn-default btn-sm" dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
												<i class="fa fa-cog"></i> Options
											</button>
											 
											<ul class="dropdown-menu">
												<li>
													<a href="<?php echo $base_url.'block/block_configure/'.$encrypt_id; ?>">
														<i class="fa fa-bars fa-margin"></i> Assign to Region
													</a>
												</li>

											<?php if($block_row['block_type']=='html') { ?>
												<li>
													<a href="<?php echo base_url()."block/block_add/".$encrypt_id; ?>">
														<i class="fa fa-edit fa-margin"></i> Edit
													</a>
												</li>

												<li>
													<a data-id="<?php echo $encrypt_id; ?>" class="pointer delete-block">
														<i class="fa fa-trash-o fa-margin"></i> Delete
													</a>
												</li>
												
											<?php } ?>

											</ul>
										</div>
									</td>
								</tr>
							<?php }  ?>  

							



							</tbody>
						</table>
					</div>
				</div>
			</div>
	</div>
</div>


<script type="text/javascript">
	$(document).ready(function() {
		$('.delete-block').click(function() {
			var id=$(this).attr('data-id');
			swal({
				title: "Are you sure?",
				text: "Block to delete?",
				type: "warning",
				showCancelButton: true,
				confirmButtonColor: "#DD6B55",
				confirmButtonText: "Yes, delete it!",
				closeOnConfirm: false
			},
			function(){
				window.location = base_url+'block/block_delete/'+id;
			});
		});
	})

</script>

