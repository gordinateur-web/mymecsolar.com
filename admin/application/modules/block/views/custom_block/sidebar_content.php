<div class="left-block">
  <div class="panel-group" id="accordion" role="tablist" aria-multiselectable="true">
    <div class="panel panel-default">
      <div class="panel-heading" role="tab" id="headingOne">
        <h4 class="panel-title">
          <a role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseOne" aria-expanded="true" aria-controls="collapseOne">
            <i class="more-less glyphicon glyphicon-plus"></i><?php if(!empty($link_parent_id)){ echo $link_parent_id['link_title']; } ?> 
          </a>
        </h4>
      </div>
      <div id="collapseOne" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingOne">
        <div class="panel-body">
          <ul>
            <?php foreach ($get_child_menu as $key => $child_value) { ?>
            <li><a href="<?php echo base_url().$child_value['link_path'] ?>" title="<?php echo $child_value['link_title']; ?>"><?php echo $child_value['link_title']; ?></a></li>
            <?php } ?>
          </ul>
        </div>
      </div>
    </div>
  </div>
</div>


