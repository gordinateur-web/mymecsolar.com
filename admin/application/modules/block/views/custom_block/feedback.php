<?php //$this->load->view('front_head');
    //$this->load->view('front_header'); ?>
<div class="containt-body">
    <!-- inner-banner -->
    <?php if(!empty($feedback_form[$page_fields['banner_image']][0]['media_path'])){ ?>
    <div class="parallax-container breadcrumb-parallax">
        <div class="parallax" id="inner-parallax">
            <img src="<?php echo base_url().$feedback_form[$page_fields['banner_image']][0]['media_path']; ?>" alt="<?php echo $feedback_form[$page_fields['banner_image']][0]['media_title']; ?>" class="img-responsive">
            <div class="breadcrumb-container">
                <div class="inner-banner">
                    <div class="inner-overlay">
                        <div class="container">
                            <div class="breadcrumb">
                                <small class="green-light-bg"><?php echo $feedback_form[$page_fields['title_label']][0]['content_value']; ?></small>
                                <h2><?php echo $feedback_form[$page_fields['title']][0]['content_value']; ?></h2>
                            </div>
                            <!-- breadcrumb end -->
                        </div>
                        <!-- container end -->
                    </div>
                    <!-- inner-overlay end -->
                </div>
            </div>
        </div>
    </div>
    <?php } ?>
      <!-- inner-banner end -->
      <!-- content section -->
      <div class="content-body">
        <div class="container">
          <!-- left-area -->
          <div class="col-md-3 col-sm-3 col-xs-12 left-area">
            <?php if($feedback_form[$page_fields['has_sidebar']][0]['content_value']==1){ ?>
                <div class="left-block">
                    <!-- Sidebar Title -->
                    <?php if(!empty($feedback_form[$page_fields['has_sidebar']][0]['content_value'])){ ?>
                    <h3 class="title"><?php echo $feedback_form[$page_fields['sidebar_title']][0]['content_value']; ?></h3>
                    <?php } ?>
                    <!-- Sidebar content -->
                    <?php if(!empty($feedback_form[$page_fields['sidebar_content']][0]['content_value'])){ ?>
                    <?php echo $feedback_form[$page_fields['sidebar_content']][0]['content_value']; ?>
                    <?php } ?>
                </div>
                <?php } ?>
            <div class="left-block">
              <ol class="tab">
                <li><a href="<?php echo base_url().'apply/' ?>" title="Apply now"><span>APPLY now</span></a></li>
                <li><a href="<?php echo base_url().'feedback/' ?>" title="Feedback"><span>Feedback</span></a></li>
              </ol>
            </div>
          </div>
          <!-- left-area end -->
          <!-- right-area -->
          <div class="col-md-9 col-sm-9 col-xs-12 right-area light-gray-bg">
            <div class="right-block">
              <div class="row para">
                <div class="col-md-12 col-sm-12 col-xs-12 apply">
                  <div class="white-bg white-box">
                    <?php echo $feedback_form[$page_fields['page_content']][0]['content_value']; ?>
                  </div>
                  <div class="form">
                            <?php if($this->uri->segment(1)=='feedback'){
                                  echo $contact_us_form;
                                  } ?>
                          </div>
                  </div><!-- form end -->
                </div>
              </div> <!-- 2nd row end -->            
            </div>
          </div>
          <!-- right-area end -->
        </div> <!-- container end -->
      </div> <!-- container-fluid end -->
      <!-- content section end -->
    </div> <!-- containt-body end -->
  <script type="text/javascript" src="<?php echo base_url(); ?>public/plugins/jquery-validation/jquery.form-validator.min.js"></script>
<script type="text/javascript">
    $(document).ready(function() {
      jQuery.validate({
        form:'.validate-form',
      });
      
      //$('#content_data_form .box-body .field-group label').remove();
      $('#content_data_form').addClass('custom-form');
      $('#content_data_form').addClass('form');
      $('#content_data_form .modal-footer').css('border-top','0px');
      $('#content_data_form .box-body button.btn.btn-danger').remove();
      $('#content_data_form .box-body button.btn.btn-default').remove();
      $('#content_data_form .modal-footer button.btn').removeClass('btn-primary');
      $('#content_data_form .modal-footer').addClass('custom-input');
      $("#content_data_form .box-body button.btn").html('Apply now<i class="icon-right-arrow"></i>');
      $("#content_data_form .box-body button.btn").addClass('btn-cm red-btn');
      $(".feedback_form .box-body button.btn").html('Submit<i class="icon-right-arrow"></i>');
    });
    
</script>
<style>
    #group_label_8 .form-group input {
      border: 0px;
      background-color: #faf8ef !important;
    }

    #content_data_form .field-group.select label{ 
      display: none;
    }
    #content_data_form .field-group label{
          color: #b7b5b5;
        top: 5px;
        right: 25px;
        font-size: 1em;
        font-weight: normal;
        font-family: 'proxima_nova_rgregular';
        position: absolute;
        margin-bottom: 0;
    }
</style>
<?php $this->load->view('front_footer');die; ?>