<?php if(!empty($sub_menu)) { ?>
<div class="left-block">
  <div class="panel-group" id="accordion" role="tablist" aria-multiselectable="true">
    <div class="panel panel-default">
      <div class="panel-heading" role="tab" id="headingOne">
        <h4 class="panel-title">
          <a role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseOne" aria-expanded="true" aria-controls="collapseOne">
            <i class="more-less glyphicon glyphicon-plus"></i><?php if(!empty($menu_active)){ echo $menu_active['link_title']; } ?> 
          </a>
        </h4>
      </div>
      <div id="collapseOne" class="panel-collapse collapse in" role="tabpanel" aria-labelledby="headingOne">
        <div class="panel-body">
          <ul>
            <?php
            foreach ($sub_menu as $key => $sub_menu_value) { ?>
            <li><a href="<?php echo base_url().$sub_menu_value['link_path']."#from-lhs"; ?>" title="<?php echo $sub_menu_value['link_title']; ?>"><?php echo $sub_menu_value['link_title']; ?></a></li>
            <?php } ?>
          </ul>
        </div>
      </div>
    </div>
  </div>
</div>
<?php }  ?>


