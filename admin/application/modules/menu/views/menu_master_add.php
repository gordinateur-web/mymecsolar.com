<?php
$base_url = base_url();
$form_model=array();
if(!empty($edit_data)) {
	$form_model=$edit_data;
}
/* Fill old data */
$old_data=$this->session->flashdata('old_data');
if(!empty($old_data)) {
	$form_model=$old_data;  
}
echo $this->form->form_model($form_model, $base_url.'menu/menu/menu_master_save/',array('name'=>'save_menu_master','id'=>'menu_master_form', 'class'=>'validate-form'));
if(!empty($edit_data)) {
	echo $this->form->form_hidden('menu_master_id');
}
?>

<div class="row">
	<div class="box-body">
		<div id="model_errors"></div>

		<div class="col-md-6 col-sm-6">
			<div class="form-group">
				<label>Title<span class="text-danger">*</span></label>
				<?php 
				$other_option=array(
					'class'=>'form-control',
					'placeholder'=>'Menu Master Title',
					'data-validation'=>'required'
					);
				echo $this->form->form_input('menu_master_title', $other_option); 
				?>
			</div>
		</div>

		<div class="col-md-6 col-sm-6">
			<div class="form-group">
				<label>Description </label>
				<?php 
				$other_option=array(
					'class'=>'form-control',
					'placeholder'=>'Menu Master Title',
					);
				echo $this->form->form_input('menu_master_desc', $other_option); 
				?>
			</div>
		</div>
		<!-- <div class="col-md-6 col-sm-6">
			<div class="form-group">
				<label>Is main Navigation <span class="text-danger">*</span></label>
				<?php 
				$other_option=array(
					'class'=>'form-control',
					'placeholder'=>'Is main Navigation',
					'data-validation'=>'required'
					);
				$option= array('1'=>'No','0'=>'Yes');
				//echo $this->form->form_dropdown('main_navigation',$option,'','', $other_option); 
				?>
			</div>
		</div> -->
	</div>
	
</div>

<div class="box-footer with-border">
	<div class="box-tools pull-right">
		<input type="reset" class="btn btn-default" value="Reset">
		<input type="submit" class="btn btn-primary" value="Submit">
	</div>
</div>
<?php echo $this->form->form_close(); ?>

