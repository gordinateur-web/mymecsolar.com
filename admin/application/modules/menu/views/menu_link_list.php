<div class="row">
<?php
$base_url = base_url();
$form_model=array();
if(!empty($edit_data)) {
	$form_model=$edit_data;
	dsm($form_model);
}

echo $this->form->form_model($form_model, $base_url.'menu/menu_link_order_save/',array('name'=>'save_link_order','id'=>'link_order_form', ));
?>
		<div class="col-md-12">
			<div class="box box-primary">
				<div class="box-header with-border">
					<div class="box-title">
						<a href="<?php echo $base_url.'menu/menu_link_add/'.$menu_master_id; ?>" class="btn btn-sm btn-primary" title="Add New" ><i class="fa fa-plus"></i> Add Link</a>
						<button type="submit" class="btn btn-sm btn-primary" name="order_save"><i class="fa fa-sort"></i> Save Order</button>
					</div>
				</div>

				<div class="panel-body">
					<div class="box-body table-responsive">
						<table class='table table-bordered table-striped dataTable' id="full-height-datatable">
							<thead>
								<tr>
									<th>Title</th>
									<th>Path</th>
									<th width="5%">Status</th>
									<th width="5%">Weight</th>
									<th width="20%">Parent</th>
									<th width="5%">Action</th>
								</tr>
							</thead>
							<tbody> 
								<?php
									foreach($menu_tree as $menu_link_row) { 
										echo $this->menu_model->render_menu_list($menu_link_row);
									}
								?>
							</tbody>
						</table>
					</div>
				</div>
			</div>
	</div>
	<?php echo $this->form->form_close(); ?>
</div>
<link rel="stylesheet" href="<?php echo base_url().'public/plugins/jquery-tabledrag/assets/jquery.tabledrag.css'; ?>">
<script type="text/javascript" src="<?php echo base_url()."public/plugins/jquery-tabledrag/jquery.tabledrag.min.js"; ?>"></script>
<script type="text/javascript" src="<?php echo base_url()."public/plugins/jquery-tabledrag/jquery.cookie.min.js"; ?>"></script>
<script type="text/javascript">

	$(document).ready(function() {
		//$('#table_data').tableDrag();

		$('.delete-menu-link').click(function() {
			var id=$(this).attr('data-id');
			swal({
				title: "Are you sure?",
				text: "Menu Link to delete?",
				type: "warning",
				showCancelButton: true,
				confirmButtonColor: "#DD6B55",
				confirmButtonText: "Yes, delete it!",
				closeOnConfirm: false
			},
			function(){
				window.location = base_url+'menu/menu_link_delete/'+id;
			});
		});
	}); 
	
</script>
