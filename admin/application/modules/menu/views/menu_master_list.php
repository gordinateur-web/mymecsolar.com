<div class="row">
	<?php $base_url=base_url();?>
		<div class="col-md-12">
			<div class="box box-primary">
				<div class="box-header with-border">
					<div class="box-title">
						<a href="#" class="btn btn-sm btn-primary" onclick="get_modaldata('Add Menu Master',base_url+'menu/menu_master_add/')" title="Add New"><i class="fa fa-plus"></i> Add Menu</a>
						
					</div>
				</div>

				<div class="panel-body">
					<div class="box-body table-responsive">
						<table class='table table-bordered table-striped dataTable' id="full-height-datatable">
							<thead>
								<tr>
									<th>Title</th>
									<th>Description</th>
									<th>Link</th>
									<th width="5%">Action</th>
								</tr>
							</thead>
							<tbody>
							  <?php foreach($menu_master as $menu_master_row) { 
							  	$encrypt_id=encrypt_id($menu_master_row['menu_master_id']);
							  	?>
								<tr>
									<td><?php echo $menu_master_row['menu_master_title']; ?></td>
									<td><?php echo $menu_master_row['menu_master_desc']; ?></td>
									<td><a href="<?php echo base_url().'menu/menu_link_list/'.$encrypt_id; ?>">Links List</a></td>
									<td>
										<div class="btn-group options">
											<button type="button" class="btn btn-default btn-sm" dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
												<i class="fa fa-cog"></i> Options
											</button>
											 
											<ul class="dropdown-menu">
												<li>
													<a class="pointer table-list-edit" data-editurl="menu/menu_master_add" data-editid="<?php echo $encrypt_id; ?>" data-modeltitle="Edit Role"> 
														<i class=" fa fa-edit fa-margin table-list-edit"></i> Edit
													</a>
												</li>
												<?php if($menu_master_row['main_navigation']!='1') { ?>
												<li>
													<a data-id="<?php echo $menu_master_row['menu_master_id']; ?>" class="pointer delete-menu-master">
														<i class="fa fa-trash-o fa-margin"></i> Delete
													</a>
												</li>
												<?php } ?>
											</ul>
										</div>
									</td>
								</tr>
							<?php }?>  
							</tbody>
						</table>
					</div>
				</div>
			</div>
	</div>
</div>

<script type="text/javascript">
	$(document).ready(function() {
		$('.delete-menu-master').click(function() {
			var id=$(this).attr('data-id');
			swal({
				title: "Are you sure?",
				text: "Menu Master to delete?",
				type: "warning",
				showCancelButton: true,
				confirmButtonColor: "#DD6B55",
				confirmButtonText: "Yes, delete it!",
				closeOnConfirm: false
			},
			function(){
				window.location = base_url+'menu/menu_master_delete/'+id;
			});
		});
	})

</script>


