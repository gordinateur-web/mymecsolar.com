<?php
$base_url = base_url();
$form_model=array();
if(!empty($edit_data)) {
	$form_model=$edit_data;
}
/* Fill old data */
$old_data=$this->session->flashdata('old_data');
if(!empty($old_data)) {
	$form_model=$old_data;  
}
echo $this->form->form_model($form_model, $base_url.'menu/menu_link_save/',array('name'=>'save_link_master','id'=>'menu_link_form', 'class'=>'validate-form'));
if(!empty($edit_data)) {
	echo $this->form->form_hidden('menu_link_id');
}
echo $this->form->form_hidden('menu_master_id', $menu_master_id);
?>


	<div class="box box-primary">
		<div class="panel-body">
			<div class="row">
				<div class="box-body">
					<div id="model_errors"></div>
					<div class="col-md-12">
						<div class="col-md-6 col-sm-6">
							<div class="form-group">
								<label>Title <span class="text-danger">*</span></label>
								<?php 
								$other_option=array(
									'class'=>'form-control',
									'placeholder'=>'Title',
									'data-validation'=>'required',
									'title'=>'Title',
									);
								echo $this->form->form_input('link_title', $other_option); 
								?>
							</div>
						</div>

						<div class="col-md-6 col-sm-6">
							<div class="form-group">
								<label>Path<span class="text-danger">*</span></label>
								<?php 
									$other_option=array(
										'class'=>'form-control',
										'placeholder'=>'Path',
										'data-validation'=>'required',
										'title'=>'Path',
									);

									if(!empty($form_model['content_id'])) {
										$other_option['disabled']='disabled';
									}

									echo $this->form->form_input('link_path', $other_option); 
								?>
								<span class="help-text">Use full url for external Link, for internal link use relative path</span>
							</div>
						</div>
						<div class="clearfix"></div>

						<div class="col-md-12 col-sm-12">
							<div class="form-group">
								<label>Description </label>
								<?php 
								$other_option=array(
									'class'=>'form-control',
									'placeholder'=>'Description',
								);
								echo $this->form->form_input('link_desc', $other_option); 
								?>
							</div>
						</div>

						<div class="col-md-6 col-sm-6">
							<div class="form-group">
								<label>Status <span class="text-danger">*</span></label>
								<?php 
								$other_option=array(
									'class'=>'form-control',
									'placeholder'=>'Status',
									'data-validation'=>'required'
									);
								$option= array('1'=>'Enable','0'=>'Disable');
								echo $this->form->form_dropdown('link_status',$option,'','', $other_option); 
								?>
							</div>
						</div>

						<div class="col-md-6 col-sm-6">
							<div class="form-group">
								<label>Visible In front end <span class="text-danger">*</span></label>
								<?php 
									$other_option=array(
										'class'=>'form-control',
										'placeholder'=>'front_visible',
										'data-validation'=>'required'
										);
									$option= array('1'=>'Yes','0'=>'No');
									echo $this->form->form_dropdown('front_visible',$option,'','', $other_option); 
								?>
							</div>
						</div>

						<div class="clearfix"></div>

						<div class="col-md-6 col-sm-6">
							<div class="form-group">
								<label>Link Parent </label>
								<?php 
								$other_option=array(
									'class'=>'form-control',
									'placeholder'=>'Parent',
								);
								if(!empty($form_model['content_id'])) {
									$other_option['disabled']='disabled';
								}
								echo $this->form->form_dropdown_fromdatabase('link_parent_id',$menu_link,'menu_link_id','link_title',false, $other_option); 
								?>
							</div>
						</div>

						<div class="col-md-6 col-sm-6">
							<div class="form-group">
								<label>Weight </label>
								<?php 
								$other_option=array(
									'class'=>'form-control number-field',
									'placeholder'=>'Weight',
									);
								echo $this->form->form_input('link_weight', $other_option); 
								?>
							</div>
						</div>
						<div class="clearfix"></div>

						<div class="col-md-6 col-sm-6">
							<div class="form-group">
								<label>Icon Image </label>
								<?php 
								$other_option=array(
									'class'=>'form-control',
									'placeholder'=>'Icon Image',
									);
								echo $this->form->form_upload('icon_image', $other_option); 
								?>
							</div>
							<?php if(!empty($edit_data['media_path'])) { ?>
							<div class="thumbnail" style="width: 100px;">
								<a href="#" onclick="delete_icon(this)" class="btn btn-xs btn-danger pull-right"><i class="fa fa-trash-o"></i></a>
								<img src="<?php echo $base_url.$edit_data['media_path']; ?>" height="50">
							</div>
							<?php } ?>
						</div>

						<div class="col-md-6 col-sm-6">
							<div class="form-group">
								<label>Icon Class </label>
								<?php 
								$other_option=array(
									'class'=>'form-control ',
									'placeholder'=>'Icon Class',
									);
								echo $this->form->form_input('icon_class', $other_option); 
								?>
							</div>

						</div>
					</div>
				</div>
			</div>

			<div class="box-footer with-border">
				<div class="box-tools pull-right">
					<input type="reset" class="btn btn-default" value="Reset">
					<input type="submit" class="btn btn-primary" value="Submit">
				</div>
			</div>
			<?php echo $this->form->form_close(); ?>

		</div>
	</div>

<script src="<?php echo base_url().'public/plugins/ckeditor/ckeditor.js';?>"></script>

<script type="text/javascript">
	function delete_icon(ele) {
		var id=$(ele);
		$('#menu_link_form').append('<input type="hidden" name="deleted_icon" value="1"/>');
		$(id).parent().remove();
	}

	/* Add uploader plgin */
	$('textarea.rich_editor').each(function() {
		CKEDITOR.replace($(this).attr('name'));
		CKEDITOR.config.disableNativeSpellChecker = false;
		CKEDITOR.config.allowedContent=true;

			//CKEDITOR.config.extraPlugins = 'smiley';
			CKEDITOR.config.extraPlugins = 'cleanuploader,smiley,html5video,btgrid';
		});
</script>