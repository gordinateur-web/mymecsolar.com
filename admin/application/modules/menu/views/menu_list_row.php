<?php $base_url=base_url(); 
$encrypt_id=encrypt_id($menu_link_row['menu_link_id']);
?>
<tr>
	<td>
		<?php
			for ($i=0; $i < $level ; $i++) { 
				echo '<div class="indentation">&nbsp;</div>';
			}
			echo $menu_link_row['link_title']; 
		?>
		
	</td>
	<td><?php echo $menu_link_row['link_path']; ?></td>
	<td>
	<?php
	if($menu_link_row['link_status']=='1') {
		echo '<label class="label label-success">Enable</label>';
	}
	if($menu_link_row['link_status']=='0') {
		echo '<label class="label label-danger">Disable</label>';
	}
	?>	
	</td>
	<td>
		<?php 
		$other_option=array(
			'class'=>'form-control row-weight',
			'placeholder'=>'Weight',
		);
		echo $this->form->form_input('link_weight['.$menu_link_row['menu_link_id'].']', '', $weight[$menu_link_row['menu_link_id']], $other_option); 
		?>
	</td>
	<td>
		<?php 
		$other_option=array(
			'class'=>'form-control row-parent',
			'placeholder'=>'Parent',
		);

		if(!empty($menu_link_row['content_id'])) {
			$other_option['disabled']='disabled';
		}
		/* unset himself id */
		$link_parents=$menu_link_parent;
		$link_parents['0']='No parent';
		unset($link_parents[$menu_link_row['menu_link_id']]);
		echo $this->form->form_dropdown('link_parent_id['.$menu_link_row['menu_link_id'].']',$link_parents,$parent_id[$menu_link_row['menu_link_id']],'', $other_option); 
		?>

	</td>
	<td>
		<div class="btn-group options">
			<button type="button" class="btn btn-default btn-sm" dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
				<i class="fa fa-cog"></i> Options
			</button>
			 
			<ul class="dropdown-menu">
				<li>
					<a href="<?php echo $base_url.'menu/menu_link_add/'.encrypt_id($menu_master_id).'/'.$encrypt_id; ?>" class="pointer"  > 
						<i class="fa fa-edit fa-margin table-list-edit"></i> Edit
					</a>
				</li>
				<?php //if(empty($menu_link_row['content_id'])) { ?>
				<li>
					<a data-id="<?php echo $menu_link_row['menu_link_id']; ?>" class="pointer delete-menu-link">
						<i class="fa fa-trash-o fa-margin"></i> Delete
					</a>
				</li>
				<?php //} ?>
			</ul>
		</div>
	</td>
</tr>