<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Menu_model extends CI_model {

	public function __construct() {
		
	}

	public function add_menu_master($data) {
		$this->db->insert('menu_master',$data);
		return $this->db->insert_id();
	}
	
	/*public function check_name_unique($value) {
		$edit_id=$this->input->post('menu_master_id');
		return check_unique($value, 'menu_master','people','menu_master_id', $edit_id);
	}*/

	public function menu_icon_save($data) {
		$this->db->insert('media',$data);
		return $this->db->insert_id();
	}

	public function update_menu_master($data, $edit_id) {
		$this->db->where('menu_master_id',$edit_id);
		return $this->db->update('menu_master',$data);
	} 

	public function get_menu_master($filter=false) {

		if($filter){
			apply_filter($filter);	
		}
		/* Ignore deleted content types and content */
		$this->db->where('deleted_at is null',null,false);
		$rs=$this->db->get('menu_master');
		return $rs->result_array();
	}

	public function add_menu_link($data) {
		$this->db->insert('menu_link',$data);
		return $this->db->insert_id();
	}

	public function update_menu_link($data, $edit_id) {
		$this->db->where('menu_link_id',$edit_id);
		return $this->db->update('menu_link',$data);
	} 

	public function update_menu_link_filter($data, $filter=false) {
		if($filter){
			apply_filter($filter);	
		}
		return $this->db->update('menu_link',$data);
	} 

	public function get_menu_link($filter=false) {
		if($filter){
			apply_filter($filter);	
		}
		/* Ignore deleted content types and content */
		$this->db->where('menu_link.deleted_at is null',null,false);
		$this->db->where('menu_link.front_visible','1');
		$this->db->join('media','media.media_id=menu_link.icon_image','left');
		$this->db->order_by('link_weight');
		$rs=$this->db->get('menu_link');
		return $rs->result_array();
	}


	/* 
		get menu tree in structure as following
		
		array(
			'0'=>array(
				'link'=>array(
					//menu_link tables all column
				),
				'below'=>array(
					'0'=>array(
						'link'=>array(
							//menu_link tables all column
						),
						'below'=>array(
							
						)
					),
				)
			),
		)

	*/
	function get_menu_tree($menu_master_id, $show_all=true, $filter=false) {
		$filter_menu=$filter;
		$filter_menu['menu_master_id']=$menu_master_id;
		if($show_all == false) {
			$filter_menu['link_status']=1;
		}
		$menu_tree=$this->get_menu_link($filter_menu);
		if(empty($menu_tree)) {
			return array();
		}
		$menu_tree = parent_child_array($menu_tree,'link_parent_id');
		$return_array=array();

		/* Loop through each root items */
		$i=0;
		foreach ($menu_tree[0] as $link) {
			$return_array[$i]=$this->get_recursive_menu($link, $menu_tree);
			$i++;
		}
		return $return_array;
	}

	/*
		Helper function of get_menu_tree
	*/
	public function get_recursive_menu($link, &$menu_tree) {
		$return['link']=$link;
		$return['below']=array();
		if(!empty($menu_tree[$link['menu_link_id']])) {
			foreach ($menu_tree[$link['menu_link_id']] as $key=>$below_link) {
				$return['below'][]=$this->get_recursive_menu($below_link, $menu_tree);
				unset($menu_tree[$link['menu_link_id']][$key]);
			}
		}
		return $return;
	}

	/*
		Render table row for each menu link used in Menu/menu_link_list
	*/
	public function render_menu_list($menu_tree, $level=0) {
		$data['menu_link_row']=$menu_tree['link'];
		$data['level']=$level;
		$return=$this->load->view('menu_list_row',$data, false);
		if(!empty($menu_tree['below'])) {
			$level++;
			foreach ($menu_tree['below'] as $below_row) {
				$return.=$this->render_menu_list($below_row, $level);
			}
		}
		return $return;
	}

	/*
		Return array by appending - in child element and in order
		Currently using for creating select list
	*/
	public function get_menu_link_list($menu_tree, $level=1, $append='') {
		$return =array();
		foreach ($menu_tree as $menu_key => $menu_item) {
			/* Add - before title */
			$menu_item['link']['link_title']=$append.$level.'. '.$menu_item['link']['link_title'];
			$return[]=$menu_item['link'];

			if(!empty($menu_item['below'])) {
				$recursive_return=$this->get_menu_link_list($menu_item['below'],$level+1, $append.'&nbsp;&nbsp;');
				$return=array_merge($return, $recursive_return);
			}
		}
		return $return;
	}

	public function delete_menu_link($filter) {
		apply_filter($filter);
		$this->db->set('deleted_at',date('Y-m-d H:i:s'));
		$this->db->set('deleted_by',$this->session->userdata('user_id'));
		$this->db->update('menu_link');
	}

	public function get_active_menus($page_name) {
		//return $this->db->get_where('menu_link',array('link_path'=>$page_name))->row_array();

		$this->db->where('menu_link.link_path',$page_name);
		$this->db->where('menu_link.deleted_at is null',null,false);
		$this->db->order_by('menu_master_id','asc');
		$this->db->order_by('link_weight','asc');
		$rs=$this->db->get('menu_link');
		return $rs->row_array();
	}

	public function get_sub_menu($link_parent) {
		//return $this->db->get_where('menu_link',array('menu_link_id'=> $link_parent))->row_array();

		$this->db->where('menu_link.menu_link_id',$link_parent);
		$this->db->where('menu_link.front_visible',1);
		$this->db->where('menu_link.deleted_at is null',null,false);
		$rs=$this->db->get('menu_link');
		return $rs->row_array();
	}

	public function get_child_menu($link_parent) {
		//return $this->db->get_where('menu_link',array('link_parent_id'=>$link_parent))->result_array();

		$this->db->where('menu_link.link_parent_id',$link_parent);
		$this->db->where('menu_link.front_visible',1);
		$this->db->where('menu_link.deleted_at is null',null,false);
		$rs=$this->db->get('menu_link');
		return $rs->result_array();
	}

}