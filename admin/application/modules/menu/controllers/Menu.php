<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Menu extends CI_Controller {

	public function __construct() {
		parent::__construct();
		/* Permission for adminpanel access, menu module access */
		permission_check(array(1,3),'and');

		$this->load->model('menu/menu_model');
	}

	public function index() {
		$data['title']="Menu Master";

		$data['menu_master']=$this->menu_model->get_menu_master();
		view_with_master('menu/menu_master_list',$data);
	}

	public function menu_master_add($menu_master_id=false) {
		$data=array();
		if(!empty($menu_master_id)) {
			$edit_data=$this->menu_model->get_menu_master(array('menu_master_id'=>$menu_master_id));
			$data['edit_data']=$edit_data[0];
		}
		$this->load->view('menu/menu_master_add',$data);
	}

	public function menu_master_save() {
		$this->load->library('form_validation');
		$validate_rules=array(
			array(
				'field'=>'menu_master_title',
				'label'=>'Title',
				'rules'=>'required'
			),
		);
		$this->form_validation->set_rules($validate_rules);

		if ($this->form_validation->run() == FALSE) {
			set_message(validation_errors());
			$this->session->set_flashdata('old_data',$this->input->post());
			redirect_back();
			return 0;
		}
		$post_array= $this->input->post();

		$menu_master_data = array(
			'menu_master_title'=>$post_array['menu_master_title'],
			'menu_master_desc'=>$post_array['menu_master_desc'],
			//'main_navigation'=>$post_array['main_navigation'],
		);
		if(!empty($post_array['menu_master_id'])) {
			$this->menu_model->update_menu_master($menu_master_data, $post_array['menu_master_id']);
		}
		else {
			$this->menu_model->add_menu_master($menu_master_data);
		}
		set_message("Menu master data saved","success");
		redirect_back();
	}

	public function menu_master_delete() {
		$id=$this->input->post('id');
		$user_id=$this->session->userdata('user_id');
		if($id) {
			$menu_data['deleted_at']=date('Y-m-d H:i:s');
			$menu_data['deleted_by']=$user_id;
			$this->menu_model->update_menu_master($menu_data, $id);
			$this->menu_model->update_menu_link($menu_data, $id);
			$return=array("status"=>'1', "message"=>"user deleted successfully");
			echo json_encode($return);
		}
		else {
			$return=array("status"=>'0', "message"=>"Please select valid user");
			echo json_encode($return);	
		}
		return 0;
	}

	public function menu_link_list($menu_master_id) {

		if(empty($menu_master_id)) {
			show_404();
		}

		/* get menu master  */
		$menu_master=$this->menu_model->get_menu_master(array('menu_master_id'=>$menu_master_id));
		if(empty($menu_master)) {
			show_404();
		}

		$data['menu_tree']=$this->menu_model->get_menu_tree($menu_master_id);
		$data['menu_master_id']=$menu_master_id;
		$data['menu_link']=$this->menu_model->get_menu_link_list($data['menu_tree']);
		$data['weight']=array_column($data['menu_link'], 'link_weight', 'menu_link_id');
		$data['parent_id']=array_column($data['menu_link'], 'link_parent_id', 'menu_link_id');
		$data['menu_link_parent']=array_column($data['menu_link'], 'link_title','menu_link_id');

		$data['title_data']=$menu_master[0];
		$data['title']="Menu items of <b>".$data['title_data']['menu_master_title']."</b>";
		view_with_master('menu/menu_link_list',$data);
	}

	public function menu_link_add($menu_master_id=false, $menu_link_id=false) {
		$data['title']="Menu Link Add";
		if(!empty($menu_link_id)) {
			$edit_data=$this->menu_model->get_menu_link(array('menu_link_id'=>$menu_link_id));
			$data['edit_data']=$edit_data[0];
		}
		$data['menu_master_id']=$menu_master_id;

		/* get perticular menu master link */
		$filter_link['menu_link.menu_master_id']=$menu_master_id;

		$data['menu_link']=$this->menu_model->get_menu_tree($menu_master_id);
		$data['menu_link']=$this->menu_model->get_menu_link_list($data['menu_link']);

		view_with_master('menu/menu_link_add',$data);
	}

	public function menu_link_order_save() {
		$post_array= $this->input->post();

		foreach ($post_array['link_weight'] as $key => $value) {
			unset($menu_link_data['link_parent_id']);
			$menu_link_data['link_weight']=$value;
			if(!empty($post_array['link_parent_id'][$key])) {
				$menu_link_data['link_parent_id']=$post_array['link_parent_id'][$key];
			}
			$this->menu_model->update_menu_link($menu_link_data, $key);
		}

		set_message("Link Order data saved","success");
		redirect_back();
	}

	public function menu_link_save() {
		$this->load->library('form_validation');
		$post_array= $this->input->post();
		$edit_id= $this->input->post('menu_link_id');
		$content_menu=false;

		$validate_rules=array(
			array(
				'field'=>'link_title',
				'label'=>'Title',
				'rules'=>'required'
			),
			array(
				'field'=>'link_status',
				'label'=>'Status',
				'rules'=>'required'
			)
		);

		/* Ignore validation for content menu */
		if(!empty($edit_id)) {
			$edit_menu=$this->menu_model->get_menu_link(array('menu_link_id'=>$edit_id));
			if(!empty($edit_menu)) {
				if(!empty($edit_menu[0]['content_id'])) {
					$content_menu=true;
				}
			}
		}

		/* Add validation if content menu false */
		if($content_menu!=true) {
			$validate_rules[]=array(
				'field'=>'link_path',
				'label'=>'Path',
				'rules'=>'required'
			);
		}


		$this->form_validation->set_rules($validate_rules);

		if ($this->form_validation->run() == FALSE) {
			set_message(validation_errors());
			$this->session->set_flashdata('old_data',$this->input->post());
			redirect_back();
			return 0;
		}

		$menu_master_id= $this->input->post('menu_master_id');
		$icon_image_id=false;
		$deleted_icon= $this->input->post('deleted_icon');

		/* menu icon upload folder path  */
		$menu_icon = UPLOAD_DIR.'menu/';

		/* logo Upload file */
		$upload_config['upload_path']= $menu_icon;
		$upload_config['allowed_types']= 'gif|jpg|jpeg|png';
		$upload_config['file_ext_tolower']= true;
		$upload_config['max_filename']= 10;

		/* Check if upload folder available */
		if(!file_exists($upload_config['upload_path'])) {
			mkdir($upload_config['upload_path']);
		}

		/* Upload profile pic file */
		$this->load->library('upload', $upload_config);
		$this->upload->initialize($upload_config);
		if(!empty($_FILES['icon_image']['name'])) {
			if (!$this->upload->do_upload('icon_image')) {
				set_message($this->upload->display_errors());
				redirect_back();
			}
			else {
				$upload_data=$this->upload->data();
				$file_data=array(
					'media_title'=>$upload_data['orig_name'],
					'media_alt'=>$upload_data['orig_name'],
					'media_path'=>$upload_config['upload_path'].$upload_data['orig_name'],
					'media_type'=>$upload_data['file_type'],
					'media_size'=>$upload_data['file_size'],
					'created_at'=>date('Y-m-d H:i:s'),
					);	
				$file_id=$this->menu_model->menu_icon_save($file_data);
				$icon_image_id=$file_id;
			}
		}

		$menu_link_data = array(
			'menu_master_id'=>$menu_master_id,
			'link_title'=>$post_array['link_title'],
			'link_path'=>$post_array['link_path'],
			'link_desc'=>$post_array['link_desc'],
			'link_status'=>$post_array['link_status'],
			'link_parent_id'=>$post_array['link_parent_id'],
			'link_weight'=>$post_array['link_weight'],
			'front_visible'=>$post_array['front_visible'],
			//'icon_image'=>$icon_image_id,
			'icon_class'=>$post_array['icon_class'],
		);

		/* Ignore link_parent_id and link_path column for content item */
		if($content_menu==true) {
			unset($menu_link_data['link_parent_id']);
			unset($menu_link_data['link_path']);
		}

		if(!empty($edit_id)) {
			if(!empty($deleted_icon)) {
				$menu_link_data['icon_image']='0';
			}
			if(!empty($icon_image_id)) {
				$menu_link_data['icon_image']=$icon_image_id;
			}
			$this->menu_model->update_menu_link($menu_link_data, $edit_id);
		}
		else {
			$menu_link_data['icon_image']=$icon_image_id;
			$this->menu_model->add_menu_link($menu_link_data);
		}
		set_message("Menu link data saved","success");
		redirect(base_url().'menu/menu_link_list/'.$menu_master_id);
	}

	public function menu_link_delete($id) {
		if($id) {
			$this->menu_model->delete_menu_link(array('WHERE_IN'=>array('menu_link_id'=>$id)));
			set_message("Menu link deleted",'success');
		}
		else {
			set_message("Invalid link selected");
		}
		redirect_back();
	}
}