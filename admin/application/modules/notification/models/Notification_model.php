<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Notification_model extends CI_model {

	public function __construct() {
		
	}

	public function add_notification($data) {
		$this->db->insert('notification',$data);
		return $this->db->insert_id();
	}

	public function update_notification($data, $edit_id) {
		$this->db->where('content_id',$edit_id);
		return $this->db->update('notification',$data);
	} 

	public function get_notification($filter=false) {

		if($filter){
			apply_filter($filter);	
		}
		$rs=$this->db->get('notification');
		return $rs->result_array();
	}

	public function read_notification($data, $edit_id) {
		$this->db->where('user_id',$edit_id);
		return $this->db->update('notification',$data);
	} 
}