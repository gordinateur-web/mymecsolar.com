<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Site extends CI_Controller {

	public function __construct() {
		parent::__construct();
		//login_check();
		$this->config->load('custom_content');
		$this->load->model('block/block_model');
		$this->load->model('comment/comment_model');
		$this->load->model('custom_content/content_model');
		
		$this->load->model('user/user_model');
		$this->load->model('menu/menu_model');
		$this->load->model('subscription/subscription_model');
		$this->load->model('site/site_model');
	}

	public function index() {
		$args=func_get_args();
		$url_title=uri_string();

		/* Check if method available in current controller than run that method */
		if(isset($args[0]) && method_exists($this, $args[0])) {
			$method_name=$args[0];
			unset($args[0]);
			return call_user_func_array(array($this,$method_name), $args);
		}

		/* render home page if blank uri */
		if(empty($url_title)) {
			return $this->site_model->home();
		}

		/* get content with url */
		$content_detail=$this->content_model->get_content(array(
			'url_title'=>$url_title,
			'content.content_published'=>'1',
			'WHERE'=>"(content.content_status is null or content.content_status='Approved')"
		));
		
		/* If no content found show 404 page */
		if(empty($content_detail)) {
			echo 'Content not found';
			show_404();
		}

		/* If content is webform or section content */
		if($content_detail[0]['content_type_category']!='content') {
			echo 'Content type wrong';
			show_404();
		}
		//dsm($content_detail);die;
		$content_type_id_map=$this->config->item('content_type_id_map');
		/* Get template function name */
		//dsm($content_type_id_map);die;
		$content_config=$this->config->item($content_type_id_map[$content_detail[0]['content_type_id']]);
		/* If template method is not defined */
		if(empty($content_config['template_method'])) {
			echo 'Template not defined';
			show_404();
		}

		return $this->site_model->$content_config['template_method']($content_detail[0]);
	}

	/* keyword search */
	public function search() {
		$data['page_title']='Search';

		$keyword = $this->input->get('keyword');
		$data['keyword']=$keyword;
		$data['content_type_id_map']=$this->config->item('content_type_id_map');

		if(empty($keyword) || strlen($keyword) < 3) {
			redirect_back();
		}

		/* get content type id map name */
		
		$search_cnt = $this->content_model->content_keyword_search($keyword,array('SELECT'=>'count(*) as count'));		
		
		/* list pagination config  */
		$config['per_page'] = FRONT_PAGINATION_COUNT;
		$config['base_url'] = base_url().uri_string()."?keyword=".$keyword;
		$data['pagination_links']=get_pagination_links($search_cnt[0]['count'], $config, false);
		$page=$this->input->get('per_page');
		if(!$page) {
			$page=0;
		}
		/* get search list */
		$filter_search['LIMIT']=array($config['per_page'],$page);
		
		$data['search_list'] = $this->content_model->content_keyword_search($keyword,$filter_search);

		view_with_master('search_list',$data, FRONT_MASTER_VIEW);
	}

	/* get apply type */
	public function get_apply_category() {
		$type=$this->input->post('type');

		$filter_apply_category['content_value.field_machine_name']='apply_for_39';
		$filter_apply_category['content_value.content_value']=$type;
		$apply_category_list=$this->content_model->get_value_based_content($filter_apply_category);
		//dsm($apply_category_list);die;

		$category=array();
		foreach ($apply_category_list as $key => $apply_category) {
			$category[$key]['content_id']=$apply_category['content']['content_id'];
			$category[$key]['content_title']=$apply_category['content']['content_title'];
		}
		echo json_encode($category);
	}

		// public function blog_detail($url_title) {
		// $data['page_title']="Blog Detail-".$url_title;
		// $data['meta_keywords']="";
		// $data['meta_description']="";

		// //blog fields
		// $data['blog_fields']=$this->config->item('blog_fields');

		// $url_title=$this->uri->segment(2);
		// $filter_blog_detail['url_title']=$url_title;
		// $filter_blog_detail['content.content_type_id']=$data['blog_fields']['content_type_id'];
		// $filter_blog_detail['content.content_published']=1;
		// $filter_blog_detail['WHERE'][]="(content.content_status is null or content.content_status='Approved')";
		// $data['blog_detail_list']=$this->content_model->get_content_with_value($filter_blog_detail);
		
		// $filter_blog['LIMIT']=5;
		// //$filter_blog['content.content_id <']=$data['blog_detail_list'][0]['content_id'];
		// $filter_blog['content.content_published']=1;
		// $filter_blog['WHERE'][]="(content.content_status is null or content.content_status='Approved')";
		// $filter_blog['content.deleted_at']=NULL;
		// $filter_blog['content.content_type_id']=$data['blog_fields']['content_type_id'];
		// $data['relatedblog_list']=$this->content_model->get_content_with_value($filter_blog);

		// $filter_previous['LIMIT']=5;
		// $filter_previous['content.content_id >']=$data['blog_detail_list'][0]['content_id'];
		// $filter_previous['content.content_published']=1;
		// $filter_previous['WHERE'][]="(content.content_status is null or content.content_status='Approved')";
		// $filter_previous['content.deleted_at']=NULL;
		// $filter_previous['content.content_type_id']=$data['blog_fields']['content_type_id'];
		// $data['previous_blog_list']=$this->content_model->get_content_with_value($filter_previous);
		// //dsm($data['previous_blog_list']);die;
		
		// view_with_master('blog-details',$data,FRONT_MASTER_VIEW);
	}

	
	
}