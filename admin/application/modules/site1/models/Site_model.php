<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Site_model extends CI_model {

	public function __construct() {
		
	}

	public function home($content_data=false) {
		$data=array();
		$data['home_page_fields']=$this->config->item('home_page_fields');

		/* For home page if content data is not supplied, get first content as home page */
		if($content_data==false) {
			$home_filter['content.content_type_id']=$data['home_page_fields']['content_type_id'];
			$home_filter['content.content_published']=1;
			$home_filter['WHERE']="(content.content_status is null or content.content_status='Approved')";
		}
		else {
			$home_filter=array(
				'content.content_id'=>$content_data['content_id']
			);
		}

		$data['home_page']=$this->content_model->get_content_with_value($home_filter);
		
		if(empty($data['home_page'])){
			show_404();die;
		}
		$data['home_page']=$data['home_page'][0];
		//dsm($data['home_page']);die;
		/* Set title and meta tags */
		$data['page_title']=$data['home_page']['content']['content_title'];
		if(!empty($data['home_page'][$data['home_page_fields']['window_title']][0]['content_value'])) {
			$data['page_title']=$data['home_page'][$data['home_page_fields']['window_title']][0]['content_value'];
		}
		$data['meta_description']=$data['home_page'][$data['home_page_fields']['meta_description']][0]['content_value'];
		$data['meta_keywords']=$data['home_page'][$data['home_page_fields']['meta_keywords']][0]['content_value'];


		view_with_master('index',$data, FRONT_MASTER_VIEW);
	}
	
	

}