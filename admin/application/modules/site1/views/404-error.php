<?php require_once("config.php"); ?>
<!DOCTYPE html>
<html lang="en">
    <head>
        <?php include("head.php"); ?>
        <title>404 Error | भाई वांगडे</title>
        <meta content="" name="description">
        <meta content="" name="author">
    </head>
    <body>
        <?php include("header.php"); ?>
        <div class="main">
            <!-- <-----------home slider ----------->
            <section class="breadcum">
                <img src="images/404-error.jpg" class="img-fluid" alt="404-error">
                <!-- <div class="container">
                    <h1 class="orange">।। आमच्याविषयी ।।</h1>
                    <a href="index.php" class="link">होम</a>
                    <span class="slash">/</span>
                    <p class="link active">आमच्याविषयी</p>
                </div> -->
            </section>
            <!-- <-----------home slider ----------->

            <section class="error-page">
                <div class="container text-center">
                    <h1 class="f-bold black">Oops!</h1>
                    <h2 class="f-bold black">404 Not Found</h2>
                    <p class="f-light grey">Sorry, an error has occured, Requested page not found!</p>
                    <a href="index.php" class="btns f-bold">Home</a>
                    <a href="contact.php" class="btns f-bold">Contact</a>
                </div>
            </section>

        </div>
        <?php include("footer.php"); ?>
    </body>
</html> 
