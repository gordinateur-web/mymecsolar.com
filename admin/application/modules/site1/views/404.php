

<style type="text/css">
body { background-image: url(data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAABoAAAAaCAYAAACpSkzOAAAABHNCSVQICAgIfAhkiAAAAAlwSFlzAAALEgAACxIB0t1+/AAAABZ0RVh0Q3JlYXRpb24gVGltZQAxMC8yOS8xMiKqq3kAAAAcdEVYdFNvZnR3YXJlAEFkb2JlIEZpcmV3b3JrcyBDUzVxteM2AAABHklEQVRIib2Vyw6EIAxFW5idr///Qx9sfG3pLEyJ3tAwi5EmBqRo7vHawiEEERHS6x7MTMxMVv6+z3tPMUYSkfTM/R0fEaG2bbMv+Gc4nZzn+dN4HAcREa3r+hi3bcuu68jLskhVIlW073tWaYlQ9+F9IpqmSfq+fwskhdO/AwmUTJXrOuaRQNeRkOd5lq7rXmS5InmERKoER/QMvUAPlZDHcZRhGN4CSeGY+aHMqgcks5RrHv/eeh455x5KrMq2yHQdibDO6ncG/KZWL7M8xDyS1/MIO0NJqdULLS81X6/X6aR0nqBSJcPeZnlZrzN477NKURn2Nus8sjzmEII0TfMiyxUuxphVWjpJkbx0btUnshRihVv70Bv8ItXq6Asoi/ZiCbU6YgAAAABJRU5ErkJggg==);}
.error-template {
	/*padding: 40px 15px;
	text-align: center;*/
	padding: 170px 3px;
    text-align: center;
    margin-bottom: -63px;
}
.error-actions {margin-top:15px;margin-bottom:15px;}
.error-actions .btn { margin-right:10px; }
</style>

<!-- <link href="<?php echo base_url().'public/front_assets/css/bootstrap.css'; ?>"  media="all" rel="stylesheet" /> -->
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">

<!DOCTYPE html>
<html lang="en">
<head>
    <?php
     $this->load->view("site/front_layout/front_head.php"); ?>
</head>
<body class="gray-bg">
        <?php 
            $this->load->view("site/front_layout/front_header.php");
        ?>
        <!--Content Area-->
        <div class="container">

            <div class="container">
                <div class="row">
                    <div class="col-md-12">
                        <div class="error-template">
                            <h1>
                                Oops!</h1>
                            <h2>
                                404 Not Found</h2>
                            <div class="error-details">
                                Sorry, an error has occured, Requested page not found!
                            </div>
                            <div class="error-actions">
                                <a href="<?php echo base_url(); ?>" class="btn btn-primary btn-lg"><i class="fa fa-home" aria-hidden="true"></i>
                                    Take Me Home </a>
                                    <a data-toggle="modal" data-target="#myModal" class="btn btn-default btn-lg contact_us_cursor"><i class="fa fa-envelope" aria-hidden="true"></i> Contact Support </a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

</div>

    <?php 
        $this->load->view("site/front_layout/front_footer.php");
       //$this->load->view("front/front_layout/front_master.php");
    ?>

</body>
</html>
