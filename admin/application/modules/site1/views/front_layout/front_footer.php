<?php 
    $this->load->model('menu/menu_model');
	/* footer menu model */
    $footer_menu_list=$this->menu_model->get_menu_tree(4,0);
    /* footer bottom menu */
    $footer_bottom_menu_list=$this->menu_model->get_menu_tree(5,0);
?>

<footer class="footer-bg">
	<div class="container pad-tb">
		<div class="col-md-4 col-sm-4 col-xs-12">
			<div class="support df">
				<i class="icon-hours-support green-color"></i>
				<p>
				<span>For any queries call us on</span>
				<small><a href="tel:02141-223372/82">02141-223372/82</a></small></p>
			</div>			
			<p class="light-gray proxima_novalight">
				<small class="db">ISO 9001:2008 BANK</small>
				Prevent unauthorised transactions in your account. Update your <span class="proxima_novasemibold dark">Mobile Numbers/Email IDs</span> with us. 
				Receive information of your transactions directly from Stock Exchange on your Mobile/Email at the end of the day.</p>

			<a target="_blank" href="https://play.google.com/store/apps/details?id=com.rdccbank.mobilebanking" class="hidden-xs">
				<img src="<?php echo base_url().'public/front_assets/images/ps.png'; ?>" class="img-responsive" style="width: 120px;margin-top: 20px"></a>
		</div>
		<div class="col-md-8 col-sm-8 col-xs-12 suppor-thours">
			<div class="row footer-links">
				<?php if(!empty($footer_menu_list)) { 
					foreach ($footer_menu_list as $key => $main_menu) { ?>
						<div class="col-md-3 col-xs-6">
							<h4><?php echo $main_menu['link']['link_title']; ?></h4>
							<ul>
							<?php foreach ($main_menu['below'] as $key => $sub_menu) { ?>
								<li><a href="<?php echo base_url().$sub_menu['link']['link_path']; ?>" title="<?php echo $sub_menu['link']['link_title']; ?>"><?php echo $sub_menu['link']['link_title']; ?></a></li>
							<?php } ?>
							</ul>
						</div>
				<?php } } ?>
			</div>
			<div class="row">
				<div class="col-md-12 col-xs-12 all-links">
					<ul>
						<?php foreach ($footer_bottom_menu_list as $key => $menu) { ?>
						<li><a href="<?php echo base_url().$menu['link']['link_path']; ?>" title="<?php echo $menu['link']['link_title']; ?>"><?php echo $menu['link']['link_title']; ?></a></li>
						<?php } ?>
						
					</ul>
				</div>
			</div>
			
		</div>
		<div class="row text-center note-para">
				<p>Kindly note the content of this website is for general information of the readers and it is not a legal document.
				</p>
			</div>
	</div>
	<div class="container-fluid footer-bottom">
		<div class="container">
			<div class="col-md-4 col-sm-3 col-xs-12">
				<div class="img-container">
					<!-- <img src="<?php echo base_url() ?>public/front_assets/images/footer-logo.png" alt="RDDC Bank" class="img-responsive"> -->
					<span class="icon-mian-logo"><span class="path1"></span><span class="path2"></span><span class="path3"></span><span class="path4"></span><span class="path5"></span><span class="path6"></span><span class="path7"></span><span class="path8"></span><span class="path9"></span><span class="path10"></span><span class="path11"></span><span class="path12"></span><span class="path13"></span><span class="path14"></span><span class="path15"></span><span class="path16"></span><span class="path17"></span><span class="path18"></span><span class="path19"></span><span class="path20"></span><span class="path21"></span><span class="path22"></span><span class="path23"></span><span class="path24"></span><span class="path25"></span><span class="path26"></span><span class="path27"></span><span class="path28"></span><span class="path29"></span><span class="path30"></span><span class="path31"></span><span class="path32"></span><span class="path33"></span><span class="path34"></span><span class="path35"></span><span class="path36"></span><span class="path37"></span><span class="path38"></span><span class="path39"></span><span class="path40"></span><span class="path41"></span><span class="path42"></span><span class="path43"></span><span class="path44"></span><span class="path45"></span><span class="path46"></span><span class="path47"></span><span class="path48"></span><span class="path49"></span><span class="path50"></span></span>

					<a class="hidden-lg" style="display: inline-block; margin-left: 20px;" target="_blank" href="https://play.google.com/store/apps/details?id=com.rdccbank.mobilebanking" ><img src="<?php echo base_url().'public/front_assets/images/ps.png'; ?>" style="width: 120px" class="img-responsive"></a>
				</div>
				
				<p class="site-development">Site Developed & Maintained By <a href="http://gordinateur.com/" title="G-Ordinateur">G-Ordinateur Pvt. Ltd.</a></p>
			</div>
			<div class="col-md-8 col-sm-9 col-xs-12">
				<ul class="pull-right imp-link-top">
					<li><a href="<?php echo base_url().'contact'; ?>" class="df" title="Get our location"><i class="icon-map-point green-color"></i><span> Get our location</span></a>


					</li>
					<li><a href="<?php echo base_url().'downloads'; ?>" class="df" title="Download forms"><i class="icon-download-form green-color"></i><span> Download forms</span></a></li>
					<li><a href="<?php echo base_url().'feedback'; ?>" class="df" title="Feedback"><i class="icon-chat green-color"><i class="path1"></i><i class="path2"></i><i class="path3"></i><i class="path4"></i></i> 
						<span>Feedback</span></span></span></a></li>
					<li><a href="<?php echo base_url().'locator#Atm'; ?>" class="df" title="Find Near ATM"><i class="icon-map-point green-color"></i><span> Find Near ATM</span></a></li>
				</ul>
				<ul class="pull-right imp-link">
					<li>Copyright © 2017 RDCC . All Right Reserved</li>
					<!-- <li><a href="#" title="Career">Career</a></li> -->   
					<li><a href="<?php echo base_url().'disclaimer'; ?>" title="Disclaimer">Disclaimer</a></li>
					<li><a href="<?php echo base_url().'privacy-policy'; ?>" title="Privacy Policy">Privacy Policy</a></li>
					<li><a href="<?php echo base_url().'sitemap'; ?>" title="Sitemap">Sitemap</a></li>
					
				</ul>
				<div class="clearfix"></div>
				<ul class="pull-right imp-link">
					
					<li>Best viewed in IE8+, Firefox 3.5+, Chrome 3+, Safari 5.0+ at resolution 1024 x 768+</li>
				
				</ul>
				<ul class="web-update">
					<li><p>This website is updated on<br class=" visible-lg visible-md visible-sm"> 17 Jan 2018.</p></li>	
				</ul>
			</div>
		</div>
	</div>
</footer>