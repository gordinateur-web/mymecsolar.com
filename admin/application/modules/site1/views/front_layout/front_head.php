<?php
	$base_url=base_url();
?>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<link rel="icon" type="image/x-icon" href="<?php echo base_url() ?>public/front_assets/images/favicon.png">
	<meta name="keyword" content="rdccbank.com, | Term Deposit | Saving Deposit |  Societies Reserve Fund Saving | Society Saving | Personal Saving Accounts | Loans">
<title>
<?php
	// $title = APP_NAME;;
	// if(isset($page_title)) {
	// 	$title = $page_title.' | '.APP_NAME;
	// } 
	// elseif(isset($title)) {
	// 	$title=strip_tags($title).' | '.APP_NAME;
	// }
	// echo $title;

	// $page_meta_description='';
	// if(!empty($meta_description)) {
	// 	$page_meta_description = $meta_description;
	// }

	// $page_meta_keywords='';
	// if(!empty($meta_keywords)) {
	// 	$page_meta_keywords = $meta_keywords;
	// }

	// $page_og_image='';
	// if(!empty($og_image)) {
	// 	$page_og_image=$og_image;
	// }

?>
</title>
 
  <!-- <meta content="<?php //echo $page_meta_keywords; ?>" name="keywords">
  <meta content="<?php //echo $page_meta_description; ?>" name="description">

  <meta property="og:title" content="<?php //echo $title; ?>">
  <meta property="og:image" content="<?php //echo $page_og_image; ?>">
  <meta property="og:description" content="<?php //echo $page_meta_description; ?>">	 -->

<?php
	echo generate_style_link([ 
		$base_url.'public/front_assets/css/bootstrap.min.css',
		$base_url.'public/front_assets/css/font-awesome.min.css',
		$base_url.'public/front_assets/css/bootstrap-custom.css',
		$base_url.'public/front_assets/css/genric.css',
		$base_url.'public/front_assets/css/svg_icon.css',
		
		$base_url.'public/front_assets/css/media.css',
		$base_url.'public/front_assets/css/animate.css',
		$base_url.'public/front_assets/css/slick.css',
		$base_url.'public/front_assets/css/slick-theme.css',
		$base_url.'public/front_assets/css/jquery-ui.css',
		$base_url.'public/front_assets/css/lightgallery.min.css'
	]);
?>

<?php
	echo generate_script_link([
		$base_url.'public/front_assets/js/jquery.min.js',
		$base_url.'public/front_assets/js/bootstrap.min.js',
		$base_url.'public/front_assets/js/jquery.easeScroll.js',
		$base_url.'public/front_assets/js/jquery-ui.js',
		$base_url.'public/front_assets/js/slick.js',
		$base_url.'public/front_assets/js/custom.js',
		$base_url.'public/front_assets/js/aos.js',
		$base_url.'public/front_assets/js/parallax.min.js',
		$base_url.'public/front_assets/js/lightgallery.min.js',
		$base_url.'public/front_assets/js/lightgallery-all.min.js',
		$base_url.'public/front_assets/js/jquery.touchSwipe.min.js',
	]);
?>
<script type="text/javascript">
	var base_url='<?php echo base_url(); ?>';
</script>

<!-- Global site tag (gtag.js) - Google Analytics -->
<script async src="https://www.googletagmanager.com/gtag/js?id=UA-112589480-1"></script>
<script>
  window.dataLayer = window.dataLayer || [];
  function gtag(){dataLayer.push(arguments);}
  gtag('js', new Date());

  gtag('config', 'UA-112589480-1');
</script>




