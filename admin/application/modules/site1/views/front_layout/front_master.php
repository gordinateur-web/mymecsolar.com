<!DOCTYPE html>
<html lang="en">
<head>
    <?php 
        $this->load->view("front_head"); 
        //$page_block=$this->block_model->get_page_blocks(); 
    ?>
</head>
<body>
    <!--Header-->
    <?php
        $this->load->view("front_header"); 
    ?>
    <!--/Header--> 

    <?php $this->load->view('site/sweet_alert_show_msg'); ?>
    
    <?php
    /* Main content view */
    if(!empty($content_view)) {
        if($view_type=='file') {
            $this->load->view($content_view);
        }
        else {
            echo $content_view; 
        }
    }

    /* For right section RHS */
    /*if(!empty($this->uri->segment(1))) {
        if(!empty($page_block['bottom_sidebar'])) {
            echo '<div class="row"><div class="col-sm-12">';
            foreach ($page_block['bottom_sidebar'] as $block) {
                echo $this->block_model->render_block($block);
            }
            echo '</div></div>';
        }
    }*/
    ?>

    <!--Footer-->
    <?php 
        $this->load->view("front_footer.php"); 
    ?>
    <!--/Footer--> 

</body>
</html>
