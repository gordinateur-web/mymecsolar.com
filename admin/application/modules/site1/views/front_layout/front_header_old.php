<header class="main-header">
<?php $base_url=base_url(); ?>
    <!-- desktop header start-->
    <div class="container-fluid hidden-xs">
        <div class="row">
            <div class="col-lg-3 col-md-3 col-sm-3 col-xs-12 header-lhs">
                <a href="<?php echo base_url()."/front"; ?>"><img src="<?php echo base_url()."public/front_assets/images/fintranet.png"; ?>" class="img-responsive"></a>
                <a href="<?php echo base_url()."/front"; ?>"><img src="<?php echo base_url()."public/front_assets/images/fintranet-white.png"; ?>" class="img-responsive"></a>
            </div>
            <div class="col-lg-9 col-md-9 col-sm-9 col-xs-12 header-rhs">
                <ul class="list-unstyled navbar-nav header-listing">
                    <li class="input-group">
                       
                        <?php $form_model=array();
                             echo $this->form->form_model($form_model,$base_url.'front/search_result/',array('name'=>'','id'=>'', 'class'=>'global_search', 'method'=>'get')); ?>
                            <input name="keyword" id="" class="form-control" type="text">
                            <span class="input-group-addon">
                            <button id="btn-search" type="submit">
                            <i class="fa fa-search" aria-hidden="true"></i>
                            </button>
                            </span>
                        <?php echo $this->form->form_close(); ?>
                    </li>
                    <li>
                        <a href="">
                            <?php
                                $user_pic=$this->session->userdata('profile_pic');
                                $user_Name=$this->session->userdata('name');
                                if(!empty($user_pic)) {
                                    $profile_pic= base_url().$user_pic;
                                }
                                else {
                                    $profile_pic= base_url().USER_PROFILE_PIC;
                                }
                            ?>
                            <img src="<?php echo $profile_pic; ?>" class="img-responsive"> 
                            <p>Welcome <span><?php echo $user_Name; ?></span></p><i class="caret"></i>
                        </a>
                    </li>
                    <li class="dropdown dropdown-notification">
                        <a href="" class="dropdown-toggle" data-toggle="dropdown"><img src="<?php echo base_url()."public/front_assets/images/notification.png"; ?>" class="img-responsive img-center"><span class="badge pull-right">3</span></a>
                        <ul class="dropdown-menu dropdown-notification-menu">
                            <div id="notification-scroll" class="">
                                <li><a href="">Aparna Deb has sent you an E-Greeting</a></li>
                                <li><a href="">New Poll has been updated</a></li>
                                <li><a href="">New Fun-Facts has been updated</a></li>
                            </div>
                        </ul>
                    </li>
                    <!-- <li class="dropdown dropdown-notification">
                        <a href="" class="dropdown-toggle" data-toggle="dropdown"><img src="<?php echo base_url()."public/front_assets/images/setting.png"; ?>" class="img-responsive"><span class="caret"></span></a>
                        <ul class="dropdown-menu dropdown-notification-menu">
                            <li><a href="">Edit Profile</a></li>
                            <li><a href="">Logout</a></li>
                            <li><a href="">Appearance</a></li>
                        </ul>
                    </li> -->
                    <li>
                        <a href="">
                            <img src="<?php echo base_url()."public/front_assets/images/group.png"; ?>" class="img-responsive img-center">
                            <p>Sampark</p>
                        </a>
                    </li>
                   <!--  <li>
                        <a href="">
                            <img src="<?php echo base_url()."public/front_assets/images/help.png"; ?>" class="img-responsive img-center">
                            <p>Sahyog</p>
                        </a>
                    </li> -->
                    <li><a href="<?php echo base_url()."/front"; ?>"><img src="<?php echo base_url()."public/front_assets/images/fino-logo.png"; ?>" class="img-responsive img-center"></a></li>
                    <li><a href="<?php echo base_url()."/front"; ?>"><img src="<?php echo base_url()."public/front_assets/images/fino-white-logo.png";?>" class="img-responsive img-center hidden"></a></li>
                </ul>
            </div>
        </div>
    </div>
    <!-- desktop header end-->
    <!-- mobile header start-->
    <div class="container-fluid mobile-header-container visible-xs">
        <div class="row">
            <div class="col-xs-12 header-logos">
                <a href=""><img src="<?php echo base_url()."public/front_assets/images/fintranet.png"; ?>" class="img-responsive"></a>
                <a href=""><img src="<?php echo base_url()."public/front_assets/images/fintranet-white.png"; ?>" class="img-responsive"></a>
                <a href=""><img src="<?php echo base_url()."public/front_assets/images/fino-logo.png"; ?>" class="img-responsive pull-right"></a>
                <a href=""><img src="<?php echo base_url()."public/front_assets/images/fino-white-logo.png"; ?>" class="img-responsive pull-right"></a>
            </div>
            <div class="col-xs-12 header-rhs">
                <ul class="list-unstyled header-profile pull-left">
                    <li>
                        <a href="">
                            <img src="<?php echo base_url()."public/front_assets/images/robin.png"; ?>" class="img-responsive"> 
                            <p>Welcome <span>Robin</span></p>
                        </a>
                    </li>
                </ul>
                <ul class="list-unstyled navbar-nav header-listing pull-right">
                    <li class="dropdown dropdown-notification">
                        <a href="" class="dropdown-toggle" data-toggle="dropdown"><img src="<?php echo base_url()."public/front_assets/images/notification.png"; ?>" class="img-responsive img-center"><span class="badge pull-right">3</span></a>
                        <ul class="dropdown-menu dropdown-notification-menu">
                            <div id="notification-scroll" class="">
                                <li><a href="">Aparna Deb has sent you an E-Greeting</a></li>
                                <li><a href="">New Poll has been updated</a></li>
                                <li><a href="">New Fun-Facts has been updated</a></li>
                            </div>
                            <!-- <li><a href="">Aparna Deb has sent you an E-Greeting</a></li>
                                <li><a href="">New Fun-Facts has been updated</a></li>
                                <li><a href="">New Poll has been updated</a></li> -->
                        </ul>
                    </li>
                    <li class="dropdown dropdown-notification">
                        <a href="" class="dropdown-toggle" data-toggle="dropdown"><img src="<?php echo base_url()."public/front_assets/images/setting.png"; ?>" class="img-responsive"><span class="caret"></span></a>
                        <ul class="dropdown-menu dropdown-notification-menu setting-width">
                            <li><a href="">Edit Profile</a></li>
                            <li><a href="">Logout</a></li>
                            <li><a href="">Appearance</a></li>
                        </ul>
                    </li>
                    <li class="dropdown dropdown-notification">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown"><img src="<?php echo base_url()."public/front_assets/images/group.png"; ?>" class="img-responsive img-center"></a>
                        <ul class="dropdown-menu dropdown-notification-menu menu-dropdown">
                            <li><img src="<?php echo base_url()."public/front_assets/images/group.png"; ?>" class="img-responsive img-center pull-left"><a href="#">Sampark</a></li>
                            <li><img src="<?php echo base_url()."public/front_assets/images/help.png"; ?>" class="img-responsive img-center pull-left"><a href="#">Sahyog</a></li>
                        </ul>
                    </li>
                    <li><span class="glyphicon glyphicon-search search-icon" aria-hidden="true"></span><i class="fa fa-times" aria-hidden="true"></i></li>
                </ul>
                <div class="input-group toggle-search-box">
                    <input id="" class="form-control" type="text">
                    <span class="input-group-addon">
                    <button id="btn-search">
                    <i class="fa fa-search" aria-hidden="true"></i>
                    </button>
                    </span>
                </div>
            </div>
        </div>
    </div>
    <!-- mobile header end-->
</header>

<!-- navbar placed left side fixed -->
<?php 
    /* load menu model */
    $this->load->model('menu/menu_model');
    
    /* get front main menu */
    $menu_list=$this->menu_model->get_menu_tree(1,0);

 ?>
<div class="navbar-header">
    <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#mobile-sidebar" aria-expanded="false">
    <span class="sr-only">Toggle navigation</span>
    <span class="icon-bar one"></span>
    <span class="icon-bar two"></span>
    <span class="icon-bar three"></span>
    </button>
</div>
<aside class="main-sidebar collapse navbar-collapse" id="mobile-sidebar">
    <section class="sidebar">
        <?php
            $this->load->model('menu/menu_model');
            $this->load->model('front/front_model');
            $menu_list=$this->menu_model->get_menu_tree(1,0);
            
            echo '<ul class="sidebar-menu">';
            foreach ($menu_list as  $menu_item) {
                    echo $this->front_model->render_front_menu($menu_item);
            }
            echo '</ul>';
        ?>
    </section>
    <!-- /.sidebar -->
</aside>