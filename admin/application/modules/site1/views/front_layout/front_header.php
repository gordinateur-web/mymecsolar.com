<?php 
    /* load menu model */
    $this->load->model('menu/menu_model');
    $menu_list=$this->menu_model->get_menu_tree(1,0);
    $check_rates=$this->menu_model->get_menu_tree(2,0);
    $main_navigation=$this->menu_model->get_menu_tree(3,0);
    //dsm($main_navigation);die;
    ?>
<header class="">
    <div class="container-fluid white-bg top-menu">
        <div class="container">
            <nav class="">
                <div class="navbar-header">
                    <button class="navbar-toggle top-first" type="button" data-toggle="collapse" data-target=".first">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    </button>
                    <!-- <a class="navbar-brand" href="#" title="">My Store</a> -->
                    <a href="<?php echo base_url() ?>" title="Home" class="visible-xs mob-logo">
                        <span class="icon-mian-logo"><span class="path1"></span><span class="path2"></span><span class="path3"></span><span class="path4"></span><span class="path5"></span><span class="path6"></span><span class="path7"></span><span class="path8"></span><span class="path9"></span><span class="path10"></span><span class="path11"></span><span class="path12"></span><span class="path13"></span><span class="path14"></span><span class="path15"></span><span class="path16"></span><span class="path17"></span><span class="path18"></span><span class="path19"></span><span class="path20"></span><span class="path21"></span><span class="path22"></span><span class="path23"></span><span class="path24"></span><span class="path25"></span><span class="path26"></span><span class="path27"></span><span class="path28"></span><span class="path29"></span><span class="path30"></span><span class="path31"></span><span class="path32"></span><span class="path33"></span><span class="path34"></span><span class="path35"></span><span class="path36"></span><span class="path37"></span><span class="path38"></span><span class="path39"></span><span class="path40"></span><span class="path41"></span><span class="path42"></span><span class="path43"></span><span class="path44"></span><span class="path45"></span><span class="path46"></span><span class="path47"></span><span class="path48"></span><span class="path49"></span><span class="path50"></span></span>
                    </a>
                </div>
                <div class="navbar-collapse header-top df no-pad">
                    <div class="col-md-3 col-sm-3 col-xs-12 hidden-xs">
                        <div class="img-container">
                            <a href="<?php echo base_url() ?>" title="Home">
                                <span class="icon-mian-logo"><span class="path1"></span><span class="path2"></span><span class="path3"></span><span class="path4"></span><span class="path5"></span><span class="path6"></span><span class="path7"></span><span class="path8"></span><span class="path9"></span><span class="path10"></span><span class="path11"></span><span class="path12"></span><span class="path13"></span><span class="path14"></span><span class="path15"></span><span class="path16"></span><span class="path17"></span><span class="path18"></span><span class="path19"></span><span class="path20"></span><span class="path21"></span><span class="path22"></span><span class="path23"></span><span class="path24"></span><span class="path25"></span><span class="path26"></span><span class="path27"></span><span class="path28"></span><span class="path29"></span><span class="path30"></span><span class="path31"></span><span class="path32"></span><span class="path33"></span><span class="path34"></span><span class="path35"></span><span class="path36"></span><span class="path37"></span><span class="path38"></span><span class="path39"></span><span class="path40"></span><span class="path41"></span><span class="path42"></span><span class="path43"></span><span class="path44"></span><span class="path45"></span><span class="path46"></span><span class="path47"></span><span class="path48"></span><span class="path49"></span><span class="path50"></span></span>
                            </a>
                        </div>
                    </div>
                    <div class="col-md-9 col-sm-9 col-xs-12">
                        <div class="first navbar-collapse dark-green-bg no-pad">
                            <ul class="nav navbar-nav">
                                <!-- Top menu -->
                                <?php foreach ($menu_list as $key_main_menu => $menu_val) {  ?>
                                <li class="dropdown">
                                    <?php $below=$menu_val['below'];

                                    if(empty($below)){ ?>
                                        <a href="<?php echo base_url().$menu_val['link']['link_path']; ?>" title="<?php echo $menu_val['link']['link_title']; ?>">
                                        <?php echo $menu_val['link']['link_title']; ?></a>
                                        <?php } else{  ?>
                                        
                                        <a href="<?php echo base_url().$menu_val['link']['link_path']; ?>" title="<?php echo $menu_val['link']['link_title']; ?>" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false">
                                        <?php echo $menu_val['link']['link_title']; ?> <span class="caret"></span></a>
                                        <?php } ?>
                                    <?php if(!empty($menu_val['below'])) { ?>
                                        <ul class="dropdown-menu" role="menu">
                                            <!-- Sub Menu -->
                                            <?php foreach ($menu_val['below'] as $key_below_menu => $sub_menu_val) { ?>
                                            <li><a href="<?php echo base_url().$sub_menu_val['link']['link_path']; ?>" title="<?php echo $sub_menu_val['link']['link_title']; ?>"><?php echo $sub_menu_val['link']['link_title']; ?></a></li>
                                            <?php } ?>  
                                        </ul>
                                    <?php } ?>

                                </li>
                                <?php } ?>
                            </ul>
                            <ul class="nav navbar-nav navbar-right">
                                <li><a href="<?php echo base_url().$check_rates[0]['link']['link_path']; ?>" title="<?php echo $check_rates[0]['link']['link_title']; ?>" class="active"><i class="icon-972899"></i> Check Rates</a></li>
                                <li><a href="<?php echo base_url().'faqs'; ?>" title="Faqs" class="faq"><i class="icon-question"></i></a></li>
                                <li class="dropdown">
                                    <a href="#" title="English" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false">
                                        English <!-- <i class="icon-down-arrow"></i> -->
                                    </a>
                                </li>
                            </ul>
                        </div>
                        <div class="second navbar-collapse light-green-bg">
                            <ul class="nav navbar-nav navbar-right df">
                                <li class="hidden-xs">
                                    <p>Welcome!</p>
                                </li>
                                <li>
                                    <form class="navbar-form navbar-left" method="get" action="<?php echo base_url().'search'; ?>">
                                        <div class="input-group">
                                            <input type="text" class="form-control" placeholder="How Can We Help You?" name="keyword" required="required">
                                            <div class="input-group-btn">
                                                <button class="btn btn-default" type="submit">
                                                <i class="icon-search"></i>
                                                </button>
                                            </div>
                                        </div>
                                    </form>
                                </li>
                                <li class="hidden-xs"><a href="#" title="" class="btn-cm red-btn">Login <i class="icon-padlock"></i></a></li>
                                <li class="visible-xs"><a href="#" title="" class="btn-cm red-btn"><i class="icon-padlock"></i></a></li>
                                <button class="navbar-toggle top-main" type="button" data-toggle="collapse" data-target=".js-navbar-collapse">
                                <span class="sr-only">Toggle navigation</span>
                                <span class="icon-bar"></span>
                                <span class="icon-bar"></span>
                                <span class="icon-bar"></span>
                                </button>                     
                            </ul>
                        </div>
                    </div>
                </div>
            </nav>
        </div>
    </div>
    <div class="container-fluid white-bg main-menu animated">
        <div class="container">
            <nav class="navbar">
                <div class="collapse navbar-collapse js-navbar-collapse no-pad">
                    <ul class="nav navbar-nav">
                        <li>
                            <a href="<?php echo base_url() ?>" title="Home" class="dark-green-color home-ico logo-active">
                                <i class="icon-home hidden-sm"></i><span class="sr-only">Home</span>
                               <span class="icon-rdcc-logo icon-mian-logo"><span class="path1"></span><span class="path2"></span><span class="path3"></span><span class="path4"></span><span class="path5"></span><span class="path6"></span><span class="path7"></span><span class="path8"></span><span class="path9"></span><span class="path10"></span><span class="path11"></span><span class="path12"></span><span class="path13"></span><span class="path14"></span><span class="path15"></span><span class="path16"></span></span>
                            </a>
                        </li>

                        <?php 
                        if(!empty($main_navigation)) {
                        foreach ($main_navigation as $key_main_nav => $main_navigation_val) { ?>

                        <!-- Menu if display image not available -->
                        <?php if(empty($main_navigation_val['link']['media_path'])){ ?>

                                <li class="dropdown mega-dropdown">
                                    <a href="<?php echo base_url().$main_navigation_val['link']['link_path']; ?>" 
                                    title="<?php echo $main_navigation_val['link']['link_title']; ?>" 
                                    class="dropdown-toggle dark-green-color" 
                                    <?php if(!empty($main_navigation_val['below'])) { echo 'data-toggle="dropdown"'; } ?> >
                                    <i class="<?php echo $main_navigation_val['link']['icon_class']; ?>"></i> <?php echo $main_navigation_val['link']['link_title']; ?> 

                                        <?php if(!empty($main_navigation_val['below'])) { ?>
                                            <span class="caret"></span>
                                        <?php } ?> 
                                        
                                        <small><?php echo $main_navigation_val['link']['link_desc']; ?></small></a>    
                                        
                                        <!-- Dont show dropdown if sumenus not available -->
                                        <?php if(empty($main_navigation_val['below'])){ } else { ?>  

                                            <!-- Show dropdown if sumenus available -->
                                            <ul class="dropdown-menu mega-dropdown-menu Banking-menu">

                                                <li class="col-sm-12">
                                                    <ul>
                                                        <?php foreach ($main_navigation_val['below'] as $key => $below_val) { 
                                                            //dsm($below_val);die;
                                                            ?>
                                                                <li class="dropdown-header hidden-xs <?php echo $below_val['link']['link_class']; ?>"><?php echo $below_val['link']['link_title'] ?></li>

                                                                <?php foreach ($below_val['below'] as $key => $multiple_menu) { ?>
                                                                    <li><a href="<?php echo base_url().$multiple_menu['link']['link_path']; ?>" title="<?php echo $multiple_menu['link']['link_title']; ?>" class="<?php echo $below_val['link']['link_class']; ?>"><?php echo $multiple_menu['link']['link_title']; ?></a></li>
                                                                
                                                                <?php } ?>
                                                        <?php } ?>
                                                    </ul>
                                                </li>
                                            </ul> 
                                        <?php } ?>              
                                </li>
                        <?php } else{ ?>

                        <!-- Menu if Display image available -->
                        <li class="dropdown mega-dropdown">
                           <a href="<?php echo $main_navigation_val['link']['link_path']; ?>" title="<?php echo $main_navigation_val['link']['link_title']; ?>" class="dropdown-toggle dark-green-color" data-toggle="dropdown">
                                <i class="<?php echo $main_navigation_val['link']['icon_class']; ?>"></i>  <?php echo $main_navigation_val['link']['link_title']; ?> <span class="caret"></span>
                            
                                
                                <small><?php echo $main_navigation_val['link']['link_desc']; ?></small> 
                            </a>
                                
                                <ul class="dropdown-menu mega-dropdown-menu">

                                    <li class="col-sm-4 hidden-xs">
                                        <img src="<?php echo base_url().$main_navigation_val['link']['media_path']; ?>" alt="<?php echo $main_navigation_val['link']['link_title']; ?>" class="img-responsive">
                                    </li>
                                    <li class="col-sm-4">
                                    <?php foreach ($main_navigation_val['below'] as $key => $main_navigation_below) { 
                                    if(empty($main_navigation_below['below'])) {
                                    ?>
                                   
                                        <ul>
                                            <li>
                                                <a href="<?php echo base_url().$main_navigation_below['link']['link_path']; ?>" class="dropdown-header " title="Salary Account"><?php echo $main_navigation_below['link']['link_title']; ?>
                                                </a>
                                            </li>
                                            <li class="divider visible-xs"></li>
                                        </ul>
                                   
                                    <?php } ?>
                                   
                                    <?php  if(!empty($main_navigation_below['below'])) { ?>
                                    <li class="col-sm-4">
                                        <ul>
                                            <li >
                                                <a href="<?php echo base_url().$main_navigation_below['link']['link_path']; ?>" class="dropdown-header <?php echo $main_navigation_below['link']['icon_class']; ?>" 
                                                    title="<?php echo $main_navigation_below['link']['link_title']; ?>">
                                                    <?php echo $main_navigation_below['link']['link_title']; ?>
                                                </a>  
                                            
                                                <?php foreach ($main_navigation_below['below'] as $key => $below_menu) { ?>
                                                     <li>
                                                        <a href="<?php echo base_url().$below_menu['link']['link_path']; ?>" title="<?php echo $below_menu['link']['link_title']; ?>" class="<?php echo $below_menu['link']['icon_class']; ?>"><?php echo $below_menu['link']['link_title']; ?></a>
                                                     </li>
                                                <?php  } ?>
                                            </li>
                                        </ul>
                                    </li>
                                    <?php }  ?>
                                    <?php }  ?>
                                    
                            </ul>  
                        </li>
                        <?php } } }?>
                       
                    </ul>
                </div>
                <!-- /.nav-collapse -->
            </nav>
        </div>
    </div>
</header>