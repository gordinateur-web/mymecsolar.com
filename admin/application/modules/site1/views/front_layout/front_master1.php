<?php
	/* Return current page blocks */
	
	$page_block=$this->block_model->get_page_blocks();

?>
<!DOCTYPE html>
<html lang="en">
<head>
	<?php include("front_head.php"); ?>
</head>
<body>
	<div class="header-border">
		<?php $this->load->view("front_header.php"); ?>
	</div>
	<div class="container content-wrapper push-footer">

		<?php 
			/* Success or fail message */
			$this->load->view("admin_layout/show_msg");
		?>
		<div class="row">
			<?php 
				/* Define main container class width by RHS block */
				$content_class="col-lg-12 col-md-12 col-sm-12 col-xs-12";
				if(!empty($page_block['right_side'])) {
					$content_class="col-lg-9 col-md-9 col-sm-8 col-xs-12";
				}
			?>
			<div class="<?php echo $content_class; ?>">
				<?php 
					/*
						page_breadcrumb_links format
						array(
							'0'=>array('<a href="/">Home</a>')
							'2'=>array('<a href="/blog.php">Blog</a>')
							'active'=>array('<a href="/about.php">ABout</a>')
						)
						pass active as a key for active link
					*/
					if(!empty($page_breadcrumb_links)) { 
				?>
						<!-- Bredcrumbe section start -->
						<div class="breadcrumb-wrapper">
							<ol class="breadcrumb">
								<?php 
									foreach ($page_breadcrumb_links as $link_key => $link) {
										echo '<li class="breadcrumb-link link-'.$link_key.'">'.$link.'</li>';
									} 
								?>
							</ol>
						</div>
				<?php } /* Bredcrumbe condition end */ ?>

				<div class="wrapper">
					<?php

						/* Before title blocks */
						if(!empty($page_block['before_title'])) {
							echo '<div class="row"><div class="col-sm-12">';
							foreach ($page_block['before_title'] as $block) {
								echo $this->block_model->render_block($block);
							}
							echo '</div></div>';
						}

						/* For page title */ 
						if(!empty($title)) {
							echo '<h1 class="red-text">'.$title.'</h1>';
						}

						/* After title blocks */
						if(!empty($page_block['after_title'])) {
							echo '<div class="row"><div class="col-sm-12">';
							foreach ($page_block['after_title'] as $block) {
								echo $this->block_model->render_block($block);
							}
							echo '</div></div>';
						}

						/* Main content view */
						if(!empty($content_view)) {
							//$this->load->view($content_view);
							if($view_type=='file'){
								$this->load->view($content_view);
							}
							else {
								echo $content_view;	
							}
						}

						/* Content blocks */
						if(!empty($page_block['content'])) {
							echo '<div class="row"><div class="col-sm-12">';
							foreach ($page_block['content'] as $block) {
								echo $this->block_model->render_block($block);
							}
							echo '</div></div>';
						}
					?>
				</div>
			</div>

			<?php
				/* For right section RHS */
				if(!empty($page_block['right_side'])) {
					echo '<div class="col-lg-3 col-md-3 col-sm-4 col-xs-12 blog-rhs-section">';
					foreach ($page_block['right_side'] as $block) {
						echo $this->block_model->render_block($block);
					}
					echo '</div>';
				}

				/* For right section RHS */
				if(!empty($page_block['after_content'])) {
					echo '<div class="row"><div class="col-sm-12">';
					foreach ($page_block['after_content'] as $block) {
						echo $this->block_model->render_block($block);
					}
					echo '</div></div>';
				}
			?>

		</div>
	</div>
	<?php $this->load->view("front_footer.php"); ?>
	<script>
		$(document).ready(function(){
			$(".rhs-sidemenu-accordion .heading").click(function(){
			$(this).toggleClass("border-bottom");
			$
		});
	});
	</script>
</body>
</html>