  <?php
    $error = $this->session->userdata('error');
    $success = $this->session->userdata('success');
    $alert_message=false;
    $sweetalert_css='<link href="https://cdnjs.cloudflare.com/ajax/libs/sweetalert/1.1.3/sweetalert.min.css" rel="stylesheet">';
    $sweetalert_js='<script src="https://cdnjs.cloudflare.com/ajax/libs/sweetalert/1.1.3/sweetalert.min.js"></script>';
    if(!empty($error)) {
      $alert_message='swal({
        title:"Error",
        text:"'.trim(preg_replace('/\s+/', ' ', $error)).'",
        type:"error",
        html:true
      });';

    }
    if(isset($success)) {
       $alert_message='swal({
        title:"Success",
        text:"'.trim(preg_replace('/\s+/', ' ', $success)).'",
        type:"success",
        html:true
      });';
    }

    if($alert_message) {
      echo $sweetalert_js.$sweetalert_css;
      echo '<script type="text/javascript">
        $(document).ready(function() { 
          '.$alert_message.'
        });
        </script>';
    }
    
    $this->session->unset_userdata('success');
    $this->session->unset_userdata('error');
  ?>