<div class="box box-primary">
  	<div class="box-header with-border">
  		<?php 
  		/* Permission check for content type  */
  		if(permission_check(array(2),'and',0)) { //dsm($content_list);die; ?>
  		<!-- <a href="<?php echo base_url()."subscription/subscription/export"; ?>" class="btn btn-primary btn-sm"><i class="fa fa-fw fa-download"></i> Export</a> -->
  		 <a href="<?php echo base_url()."subscription/subscription/export"; ?>" id="export" class="btn btn-primary btn-sm " data-toggle="tooltip" data-placement="top" title="Export to Excel"><i class="fa fa-fw fa-download"></i> Export</a>
  		<?php } ?>
  	</div>
  	<div class="box-body">
		<div class="table-responsive">
	  		<table class="table table-striped table-bordered table-hover full-height-datatable">
				<thead>
		  			<tr>
						<th width="10%">Sr no</th>
						<th>Email</th>
						<th>Date</th>
		  			</tr>
				</thead>
				<tbody>
					<form method="get">
				<?php  $count=0;
					foreach($get_email_listing as $row) { $count++;
				?>
						<tr>
						<td><?php echo $count; ?></td>
						<td><?php echo $row['email']; ?></td>
						<td><?php echo $row['added_date']; ?></td>
					</tr>
				<?php } ?>
				</form>
				</tbody>
	  		</table>
	  	</div>
  	</div>
</div>
<script type="text/javascript">
	$(document).ready(function() {
		$('.delete-content-type').click(function() {
			var id=$(this).attr('data-id');
			swal({
				title: "Are you sure?",
				text: "Content type to delete?",
				type: "warning",
				showCancelButton: true,
				confirmButtonColor: "#DD6B55",
				confirmButtonText: "Yes, delete it!",
				closeOnConfirm: false
			},
			function(){
				window.location = base_url+'custom_content/content_type/delete_content_type/'+id;
			});
		});
	})

</script>