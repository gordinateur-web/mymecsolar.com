<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');
 
 
class Subscription_model extends CI_Model
{
	public function subscription($email){
		$data['email']=$email;
		$data['added_date']=date('Y-m-d');
		return $this->db->insert('subscription',$data);
	}

	public function get_subscription_listing(){
		return $this->db->get('subscription')->result_array();
	}
}