<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Subscription extends CI_Controller {
	
	public function __construct() {
		parent::__construct();
		$this->load->model('subscription_model');
	}

	public function subscription_email_listing(){
		$data['get_email_listing']=$this->subscription_model->get_subscription_listing();
		//dsm($data['get_email_listing']);die;
		view_with_master('subscription/subscription_email_listing',$data);
	}

	public function export(){
		/* Export to csv */
		 	$quer = $this->db->get('subscription')->result_array();
			$options['firstRow']=array(
				'Sr No',
				'Date',
				'Email',
			);
			$options['columns']=array(
				'id',
				'added_date',
				'email',
			);
			$this->load->helper('download');
			convertToCSV($quer,$options,'email_list.csv');
	}
}
?>
