<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class User_model extends CI_model {

	public function __construct() {
		
	}

	/* 
		-user functions
	*/
	public function add_user($data) {
		$this->db->insert('users',$data);
		return $this->db->insert_id();
	}

	public function check_email_unique($value) {
		$edit_id=$this->input->post('user_id');
		return check_unique($value, 'email','users','user_id', $edit_id);
	}

	public function check_empid_unique($value) {
		$edit_id=$this->input->post('user_id');
		return check_unique($value, 'employee_id','users','user_id', $edit_id);
	}

	public function update_user($data, $edit_id) {
		$this->db->where('user_id',$edit_id);
		return $this->db->update('users',$data);
	} 

	public function get_user($filter=false) {

		/* Check if super admin, than only allow system admin permission */
		$role_filter=false;
		if(!permission_check(10,'and',0)) {
			$role_ids = $this->get_role_permission(array(
				'role_permission.permission_id'=>'10',
				'SELECT'=>'role.role_id'
			));

			if(!empty($role_ids)) {
				$role_ids=array_column($role_ids, 'role_id');
				$this->db->where_not_in('role.role_id',$role_ids);
			}
		}

		if(empty($filter['SELECT'])) {
			$this->db->select('role.*, users.*');
		}

		if($filter){
			apply_filter($filter);	
		}

		/* Ignore deleted content types and content */
		$this->db->where('users.deleted_at is null',null,false);
		$this->db->join('role','role.role_id=users.role_id','left');
		//$this->db->join('branch','branch.branch_id=users.branch_id','left');
		$rs=$this->db->get('users');
		return $rs->result_array();
	}

	public function delete_user($filter=false) {
		if($filter){
			apply_filter($filter);
		}
		$this->db->delete('users');
	}


	/* 
		- user save comman function backend or frontend  
	*/

	public function user_save($front_site=false) {
		$this->load->library('form_validation');
		
		$validate_rules=array(
			array(
				'field'=>'role_id',
				'label'=>'Role',
				'rules'=>'required'
			),
			array(
				'field'=>'email',
				'label'=>'Email',
				'rules'=>array(
 					'required',
 					array(
 						'check_email',
 						array($this->user_model, 'check_email_unique')
 					)
 				)
			),
			
			array(
				'field'=>'enable',
				'label'=>'Status',
				'rules'=>'required'
			),
		);

		$post_array= $this->input->post();

		$user_id=$this->input->post('user_id');
		$deleted_pic= $this->input->post('deleted_pic');
		//$profile_pic_path=false;

		if(empty($user_id)) {
			$validate_rules[]=array(
				'field'=>'password',
				'label'=>'Password',
				'rules'=>'required',
			);
			$validate_rules[] = array(
				'field'=>'cpassword',
				'label'=>'Password',
				'rules'=>'required|matches[password]',
			);
		}

		/* userinfo data array */
		$user_data=array(
			'contact_no'=>$post_array['contact_no'],
			'personal_no'=>$post_array['personal_no'],
			'user_about'=>$post_array['user_about'],
			'name'=>$post_array['name'],
			'dob'=>mysql_dateformat($post_array['dob']),
			'email'=>$post_array['email'],
			'role_id'=>$post_array['role_id'],
			'enable'=>$post_array['enable'],
		);

		/* validation only back end */
		if(!empty($validate_rules)) {
			$this->form_validation->set_message('check_email','%s is already Exist');
			$this->form_validation->set_rules($validate_rules);

			if ($this->form_validation->run() == FALSE) {
				set_message(validation_errors());
				$this->session->set_flashdata('old_data',$this->input->post());
				redirect_back();
				return 0;
			}
		}
		/* menu icon upload folder path  */
		$profile_pic = UPLOAD_DIR.'profile_pic/';

		/* logo Upload file */
		$upload_config['upload_path']= $profile_pic;
		$upload_config['allowed_types']= 'gif|jpg|jpeg|png';
		$upload_config['file_ext_tolower']= true;
		$upload_config['max_filename']= 50;

		/* Check if upload folder available */
		if(!file_exists($upload_config['upload_path'])) {
			mkdir($upload_config['upload_path']);
		}

		/* Upload profile pic file */
		$this->load->library('upload', $upload_config);
		$this->upload->initialize($upload_config);
		if(!empty($_FILES['profile_pic']['name'])) {
			if (!$this->upload->do_upload('profile_pic')) {
				set_message($this->upload->display_errors());
				redirect_back();
			}
			else {
				$upload_data=$this->upload->data();
				/* get uploaded profile pic path */
				$profile_pic_path=$upload_config['upload_path'].$upload_data['orig_name'];
				$user_data['profile_pic']=$profile_pic_path;
			}
		}

		$password=$this->input->post('password');

		if(!empty($password) && !$front_site) {
			$user_data['password']=md5($post_array['password']);
			$user_data['temp_password']=$post_array['password'];
		}

		/* user data array */
		

		if(!empty($user_id)) {
			$user_data['updated_at']=date('Y-m-d H:i:s');
			
			if(!empty($profile_pic_path)) {
				$user_data['profile_pic']=$profile_pic_path;
			}
			if(!empty($deleted_pic)) {
				$user_data['profile_pic']='0';
			}
			$this->update_user($user_data, $user_id);
			$this->user_session_update();
		}
		else {
			$user_data['created_at']=date('Y-m-d H:i:s');
			$user_data['user_id']=$user_id;
			$user_id=$this->user_model->add_user($user_data);
		}
		
		set_message("User data saved","success");
		if($front_site==true) {
			redirect_back();
		}
		else {
			redirect(base_url().'user/');
		}
		
	}

	/* 
		-user role functions 
	*/
	public function add_role($data) {
		$this->db->insert('role',$data);
		return $this->db->insert_id();
	}

	public function check_role_title_unique($value) {
		$edit_id=$this->input->post('role_id');
		return check_unique($value, 'role_title','role','role_id', $edit_id);
	}

	public function update_role($data, $edit_id) {
		$this->db->where('role_id',$edit_id);
		return $this->db->update('role',$data);
	} 

	public function get_role($filter=false) {

		/* Check if super admin, than only allow system admin permission */
		$role_filter=false;
		if(!permission_check(10,'and',0)) {
			$role_ids = $this->user_model->get_role_permission(array(
				'role_permission.permission_id'=>'10',
				'SELECT'=>'role.role_id'
			));

			if(!empty($role_ids)) {
				$role_ids=array_column($role_ids, 'role_id');
				$this->db->where_not_in('role.role_id',$role_ids);
			}
		}

		if($filter){
			apply_filter($filter);	
		}

		/* Ignore deleted content types and content */
		$this->db->where('deleted_at is null',null,false);
		$rs=$this->db->get('role');
		return $rs->result_array();
	}

	public function get_designation($filter=false) {
		if($filter){
			apply_filter($filter);	
		}
		$rs=$this->db->get('designation_master');
		return $rs->result_array();
	}

	public function get_grade($filter=false) {
		if($filter){
			apply_filter($filter);	
		}
		$rs=$this->db->get('grade_master');
		return $rs->result_array();
	}

	public function delete_role($filter=false) {
		if($filter){
			apply_filter($filter);
		}
		$this->db->delete('role');
	}

	/* 
		- user role permission functions
	*/
	public function get_permission_master($filter=false) {
		if($filter){
			apply_filter($filter);	
		}
		
		$rs=$this->db->get('permission_master');
		return $rs->result_array();
	}

	/*public function get_role_permission($filter=false) {
		if($filter){
			apply_filter($filter);	
		}
		$rs=$this->db->get('role_permission');
		return $rs->result_array();
	}*/

	public function set_role_permission($role_permission_id, $post_array) {
		
		if(!empty($role_permission_id)) {
			/* Delete all permission of content and add new */
			$this->db->where_in('role_permission_id',$role_permission_id);
			$this->db->delete('role_permission');
			print_last_query();
		}

		$insert_data=array();

		/* Get master permission */
		$permission_master=$this->get_permission_master();

		foreach ($permission_master as $permission_row) {
			if(empty($post_array[$permission_row['permission_id']])) {
				continue;
			}
			foreach ($post_array[$permission_row['permission_id']] as $role_id) {
				$insert_data[]=array(
					//'role_permission_id'=>$role_permission_id,
					'role_id'=>$role_id,
					'permission_id'=>$permission_row['permission_id']
				);
			}
		}
		/* Insert batch */
		if(!empty($insert_data)) {
			$this->db->insert_batch('role_permission',$insert_data);
		}
		return true;
	}

	/* user session update */
	public function user_session_update() {
		$user_id=$this->session->userdata('user_id');
		$user_data=$this->get_user(array('users.user_id'=>$user_id));
		$user_data=$user_data[0];
		$this->session->set_userdata($user_data);
	}

	/*
		Return permission list with user details, role details 
	*/
	public function get_permisson($filter=false) {
		if(!empty($filter)) {
			apply_filter($filter);
		}

		$this->db->select('users.name, users.email, users.role_id, role.role_title, permission_master.permission_id, permission_master.permission_title, permission_master.permission_des,');
		$this->db->join('permission_master','permission_master.permission_id=role_permission.permission_id');
		$this->db->join('role','role.role_id=role_permission.role_id');
		$this->db->join('users','users.role_id=role.role_id');
		$rs=$this->db->get('role_permission');
		return $rs->result_array();
	}

	/*
		Get permission with user id
	*/
	public function get_user_permission($user_id) {
		return $this->get_permisson(array(
			'users.user_id'=>$user_id
		));
	}

	/*
		return permission for specific role
	*/
	public function get_role_permission($filter=false) {
		if(!empty($filter)) {
			apply_filter($filter);
		}
		if(empty($filter['SELECT'])) {
			$this->db->select('role.role_id,role.role_title, permission_master.permission_id, permission_master.permission_title, permission_master.permission_des,  role_permission.role_permission_id');
		}
		$this->db->join('role','role.role_id=role_permission.role_id');
		$this->db->join('permission_master','permission_master.permission_id=role_permission.permission_id');
		$rs=$this->db->get('role_permission');
		return $rs->result_array();
	}

	public function get_branch($filter=false) {

		if($filter){
			apply_filter($filter);	
		}
		/* Ignore deleted content types and content */
		$this->db->where('branch.deleted_at is null',null,false);
		$rs=$this->db->get('branch');
		return $rs->result_array();
	}

	public function get_login_history($filter=false) {

		if($filter){
			apply_filter($filter);	
		}
		$this->db->select('users.*, login_history.*');
		$this->db->join('users','users.user_id=login_history.user_id');
		$this->db->order_by('login_history.login_history_id DESC');
		$rs=$this->db->get('login_history');
		return $rs->result_array();
	}


	/*
		Return designation list from designation_master 
	*/
	public function get_designation_master($filter=false) {

		if($filter){
			apply_filter($filter);	
		}

		$rs=$this->db->get('designation_master');
		return $rs->result_array();
	}


	/*
		Return employee grade list from grade_master 
	*/
	public function get_grade_master($filter=false) {
		if($filter){
			apply_filter($filter);	
		}
		$rs=$this->db->get('grade_master');
		return $rs->result_array();
	}

	/*
		Return employee grade list from grade_master 
	*/
	public function get_branch_region($filter=false) {
		if($filter){
			apply_filter($filter);	
		}
		$rs=$this->db->get('branch_region');
		return $rs->result_array();
	}

	/*
		@announcement_detail - Formatted array retrieve using get_content_with_value or get_value_based_content
		@user_id - Int	
	*/
	public function check_announcement_access($announcements, $user_id=false) {
		if(!$user_id) {
			$user_id=$this->session->userdata('user_id');
		}

		$announcements_fields=$this->config->item('announcements_fields');
		$visibility_type=$announcements[$announcements_fields['announcement_unit']][0]['content_value'];
		
		if($visibility_type=='All') {
			return true;
		}
		elseif($visibility_type=='Branch') {
			$condition_value=array_column($announcements[$announcements_fields['branch']], 'content_value');
			$condition_column='users.branch_id';
		}

		elseif($visibility_type=='Employee grade') {
			$condition_value=array_column($announcements[$announcements_fields['employee_grade']], 'content_value');
			$condition_column='users.employee_grade';
		}
		/* Take region from  */
		elseif($visibility_type=='Region') {
			$region=array_column($announcements[$announcements_fields['region']], 'content_value');

			if(count($region) < 1) {
				return false;
			}

			/* Get list of branch of selected regions */
			$branch_id=$this->get_branch(array(
				'WHERE_IN'=>array(
					'branch_region_id'=>$region
				)
			));

			if(empty($branch_id)) {
				return false;
			}

			$condition_value=array_column($branch_id, 'branch_id');
			$condition_column='users.branch_id';
		}

		$user = $this->get_user(array(
			'WHERE_IN'=>array(
				$condition_column=>$condition_value
			),
			'user_id'=>$user_id
		));
		

		return !empty($user);
	}


}