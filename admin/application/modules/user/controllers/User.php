<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class User extends CI_Controller {

	public function __construct() {
		parent::__construct();
		/* Permission for adminpanel access, User module access */
		permission_check(array(1,5));
		
		$this->load->model('user_model');
	}

	public function index() {
		$data['title']="User List";

		/* get role list */
		$filter_role=array();
		if(!permission_check(array(16),'and',0)) {
			$filter_role['WHERE'][]="role.role_id !='1' ";
		}
		$data['role_list']=$this->user_model->get_role($filter_role);

		/* search comment */
		$filter=array();
		$role_id=$this->input->get('role_id');
		$keyword=$this->input->get('keyword');
		$enable=$this->input->get('enable');
		$data['old_data']=$this->input->get();

		if(!empty($role_id)) {
			$filter['users.role_id']=$role_id;
			$data['clearfilter']=true;
		}
		if(!empty($enable)) {
			$filter['users.enable']=$enable;
			$data['clearfilter']=true;
		}
		if(!empty($keyword)) {
			$filter['WHERE']="users.email LIKE '%".$keyword."%' OR 
			users.name LIKE '%".$keyword."%' OR users.designation LIKE '%".$keyword."%'";
			$data['clearfilter']=true;
		}

		if(!permission_check(array(16),'and',0)) {
			$filter['WHERE'][]="users.role_id !='1' ";
		}

		/* comment count */
		$count_filter=$filter;
		$count_filter['SELECT']='count(*) as cnt';
		$user_cnt=$this->user_model->get_user($count_filter);

		/* list pagination config  */
		$config['per_page'] = '100';
		$get_data=$data['old_data'];
		unset($get_data['per_page']);
		$config['base_url'] = base_url().uri_string()."?".http_build_query($get_data);
		$data['pagination_links']=get_pagination_links($user_cnt[0]['cnt'], $config, false);
		$page=$this->input->get('per_page');
		if(!$page) {
			$page=0;
		}
		$filter_user=$filter;
		$filter_user['LIMIT']=array($config['per_page'],$page);
		$data['users']=$this->user_model->get_user($filter_user);
		view_with_master('user/user_list',$data);
	}

	public function user_add($user_id=false) {
		$data['title']="Add User";

		if(!empty($user_id)) {
			$edit_data=$this->user_model->get_user(array('users.user_id'=>$user_id));
			$data['edit_data']=$edit_data[0];
		}

		/* get role list */
		$filter_role=array();
		if(!permission_check(array(16),'and',0)) {
			$filter_role['WHERE'][]="role.role_id !='1' ";
		}
		$data['role_list']=$this->user_model->get_role($filter_role);
		view_with_master('user/user_add',$data);
	}

	public function user_save() {
		/* user save function model */
		$this->user_model->user_save();
	}

	public function user_delete($user_id=false) {
		$id=$user_id;
		$user_id=$this->session->userdata('user_id');
		if($user_id==$id) {
			set_message('User can not be delete himself user account');
			redirect_back();
			die;
		}
		if($id) {
			$user_data['enable']='0';
			//$user_data['deleted_at']=date('Y-m-d H:i:s');
			//$user_data['deleted_by']=$user_id;
			$this->user_model->update_user($user_data, $id);
			set_message('user deleted successfully','success');
			redirect_back();
		}
		else {
			set_message('Please select valid user');
			redirect_back();
		}
		return 0;
	}
	
	public function role_list() { 
		$data['title']="Role List";

		/* get role list */
		$data['role_list']=$this->user_model->get_role();

		view_with_master('user/role_list',$data);
	}

	public function role_add($role_id=false) { 
		$data=array();
		if(!empty($role_id)) {
			/* get role for edit */
			$edit_data=$this->user_model->get_role(array('role_id'=>$role_id));
			$data['edit_data']=$edit_data[0];
		}
		$this->load->view('user/role_add',$data);
	}

	public function role_save() {
		$this->load->library('form_validation');
		$validate_rules=array(
			array(
				'field'=>'role_title',
				'label'=>'Role Title',
				'rules'=>array(
 					'required',
 					array(
 						'check_role_title',
 						array($this->user_model, 'check_role_title_unique')
 					)
 				)
			),
			
		);

		$this->form_validation->set_rules($validate_rules);
		$this->form_validation->set_message('check_role_title','%s is already Exist');

		if ($this->form_validation->run() == FALSE) {
			set_message(validation_errors());
			$this->session->set_flashdata('old_data',$this->input->post());
			redirect_back();
			return 0;
		}

		/* get post array */
		$post_array=$this->input->post();
		$edit_id=$this->input->post('role_id');
		$user_id=$this->session->userdata('user_id');

		$role_data=array(
			'role_title'=>$post_array['role_title'],
		);
		if(!empty($edit_id)) {
			$role_data['updated_at']=date('Y-m-d H:i:s');
			$role_data['updated_by']=$user_id;
			$this->user_model->update_role($role_data, $edit_id);
		}
		else {
			$role_data['created_at']=date('Y-m-d H:i:s');
			$role_data['created_by']=$user_id;
			$this->user_model->add_role($role_data);
		}

		set_message('Role data saved','success');
		redirect_back();
	}

	public function role_delete($user_id=false) {
		$id=$user_id;
		$user_id=$this->session->userdata('user_id');
		
		if($id) {
			$user_id=$this->user_model->get_user(array(
				'users.role_id'=>$id,
				'users.enable'=>'1'
			));
			
			if(!empty($user_id)) {
				set_message('You can not delete role which assigned to active users.');
				redirect_back();
			}
			else {
				$this->user_model->delete_role(array('role_id'=>$id));
				set_message('Role deleted successfully','success');
				redirect_back();
			}
			
		}
		else {
			set_message('Please select valid user');
			redirect_back();
		}
		return 0;
	}

	/* get permission for role */
	public function permission_list() {
		$data['title']="Permission";
		
		/* Check if super admin, than only allow system admin permission */
		$permission_master_filter=false;
		if(!permission_check(10,'and',0)) {
			$permission_master_filter=array(
				'permission_id !='=>'10'
			);
		}

		/* get permission list */
		$data['permission_list']=$this->user_model->get_permission_master($permission_master_filter);

		/* get role list */
		$data['role_list']=$this->user_model->get_role();

		/* get role permission list */
		$role_permission=$this->user_model->get_role_permission();

		$data['role_permission']=array();

		foreach ($role_permission as $role_permission_row) {
			$data['role_permission'][$role_permission_row['permission_id']][$role_permission_row['role_id']]=$role_permission_row['role_id'];
		}
		view_with_master('user/permission_list',$data);
	}

	public function permission_save() {
		/* post array */
		$post_array=$this->input->post();

		/* get role permission list */
		$role_permission_data=$this->user_model->get_role_permission();
		$role_permission_id=array_column($role_permission_data, 'role_permission_id');
		
		/* set role permission */
		$this->user_model->set_role_permission($role_permission_id, $post_array);

		set_message('Permission data saved','success');
		redirect_back();
	}

}