<div class="row">
	<?php $base_url=base_url();?>
		<div class="col-md-12">
			<div class="box box-primary">
				<div class="box-header with-border">
					<div class="box-title">
						 <a href="#" class="btn btn-sm btn-primary" onclick="get_modaldata('Add Role',base_url+'user/role_add/')" title="Add New"><i class="fa fa-plus"></i> Add</a>
					</div>
				</div>

				<div class="panel-body">
					<div class="box-body table-responsive">
						<table class='table table-bordered table-striped dataTable' id="full-height-datatable">
							<thead>
								<tr>
									<th>Name</th>
									<th>Create At</th>
									<th width="5%">Action</th>
								</tr>
							</thead>
                    
							<tbody> 
							  <?php foreach($role_list as $role_row) { ?>
								<tr>
									
									<td><?php echo $role_row['role_title']; ?></td>
									<td><?php echo dateformat($role_row['created_at']); ?></td>
									<td>
										<div class="btn-group options">
											<button type="button" class="btn btn-default btn-sm" dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
												<i class="fa fa-cog"></i> Options
											</button>
											 
											<ul class="dropdown-menu">
												<li>
													<a class="table-list-edit" data-editurl="user/role_add" data-editid="<?php echo $role_row['role_id']; ?>" data-modeltitle="Edit Role"> 
														<i class="fa fa-edit fa-margin table-list-edit"></i> Edit
													</a>
												</li>
												<li>
													<a data-id="<?php echo $role_row['role_id']; ?>" class="pointer delete-role">
														<i class="fa fa-trash-o fa-margin"></i> Delete
													</a>
												</li>

											</ul>
										</div>
									</td>
									
								</tr>
							<?php }?>  
							</tbody>
						</table>
					</div>
				</div>
			</div>
	</div>
</div>

<script type="text/javascript">
	$(document).ready(function() {
		$('.delete-role').click(function() {
			var id=$(this).attr('data-id');
			swal({
				title: "Are you sure?",
				text: "Role to delete?",
				type: "warning",
				showCancelButton: true,
				confirmButtonColor: "#DD6B55",
				confirmButtonText: "Yes, delete it!",
				closeOnConfirm: false
			},
			function(){
				window.location = base_url+'user/role_delete/'+id;
			});
		});
	})

</script>
