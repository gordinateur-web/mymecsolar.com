<?php
$base_url = base_url();
$form_model=array();
if(!empty($edit_data)) {
	$form_model=$edit_data;
}
/* Fill old data */
$old_data=$this->session->flashdata('old_data');
if(!empty($old_data)) {
	$form_model=$old_data;  
}
	
//dsm($form_model);die;

echo $this->form->form_model($form_model, $base_url.'user/user_save/',array('name'=>'save_user','id'=>'user_form', 'class'=>'validate-form'));
if(!empty($edit_data)) {
	echo $this->form->form_hidden('user_id');
}
?>

<div class="row">

	<div class="col-md-12">
		<div class="box box-primary">
			<div class="box-header with-border">
			</div>
			<div class="box-body">
				<div id="model_errors"></div>
				<div class="col-md-12">

				<?php //if(!empty($edit_data)) { ?>
				<h4>USER </h4>
				<div class="col-md-4 col-sm-6">
					<div class="form-group">
						<label>Email<span class="text-danger">*</span></label>
						<?php 
						$other_option=array(
							'class'=>'form-control',
							'placeholder'=>'Email',
							'data-validation'=>'required,email',
							'title'=>'Email',
							);
						echo $this->form->form_input('email', $other_option); 
						?>
					</div>
				</div>

				<?php //} ?>

				<div class="col-md-4 col-sm-6">
					<div class="form-group">
						<label>Role <span class="text-danger">*</span></label>
						<?php 
						$other_option=array(
							'class'=>'form-control',
							'placeholder'=>'Role',
							'data-validation'=>'required',
							'title'=>'Role',
						);
						echo $this->form->form_dropdown_fromdatabase('role_id',$role_list ,'role_id','role_title','',$other_option); 
						?>
					</div>
				</div> 

				
				<div class="clearfix"></div>

				<div class="col-md-4 col-sm-6">
					<div class="form-group">
						<label>Status <span class="text-danger">*</span></label>
						<?php 
						$other_option=array(
							'class'=>'form-control',
							'placeholder'=>'Status',
							'data-validation'=>'required',
							'title'=>'Status',
						);
						$option= array('1'=>'Enable','0'=>'Disable');
						echo $this->form->form_dropdown('enable',$option,'','', $other_option); 
						?>
					</div>
				</div>
				

				<div class="col-md-4 col-sm-6">
					<div class="form-group">
						<label>Password <span class="text-danger">*</span></label>
						<?php 
						$other_option=array(
							'class'=>'form-control',
							'placeholder'=>'Password',
							'data-validation'=>'required,length',
							'data-validation-length'=>'min7',
							'title'=>'Password',
						);
						if(!empty($edit_data)) {
							$other_option['data-validation-optional']='true';
						}
						echo $this->form->form_password('password', $other_option); 
						?>
					</div>
				</div>

				<div class="col-md-4 col-sm-6">
					<div class="form-group">
						<label>Confirm Password <span class="text-danger">*</span></label>
						<?php 
						$other_option=array(
							'class'=>'form-control',
							'placeholder'=>'Confirm Password',
							'data-validation'=>'required,confirmation',
							'data-validation-confirm'=>'password',
							'title'=>'Your Password and Confirm Password',
							);
						if(!empty($edit_data)) {
							$other_option['data-validation-optional']='true';
						}
						echo $this->form->form_password('cpassword', $other_option); 
						?>
					</div>
				</div>
				<div class="clearfix"></div>

				<h4>PERSONAL INFORMATION</h4>
				<?php //if(!empty($edit_data)) { ?>
				<div class="col-md-4 col-sm-6">
					<div class="form-group">
						<label>Name<span class="text-danger">*</span></label>
						<?php 
						$other_option=array(
							'class'=>'form-control',
							'placeholder'=>'Name',
							'data-validation'=>'required,custom',
							'data-validation-regexp'=>'^([a-zA-Z ]+)$',
							'title'=>'Name',
							);
						echo $this->form->form_input('name', $other_option); 
						?>
					</div>
				</div>
				<?php //} ?>

				<div class="col-md-4 col-sm-6">
					<div class="form-group">
						<label>Contact No.</label>
						<?php 
						$other_option=array(
							'class'=>'form-control number-field',
							'placeholder'=>'Contact No.',
							'data-validation'=>'length',
							'data-validation-length'=>'10-15',
							'data-validation-optional'=>'true',
							);
						echo $this->form->form_input('contact_no', $other_option); 
						?>
					</div>
				</div>

				<div class="col-md-4 col-sm-6">
					<div class="form-group">
						<label>Personal No.</label>
						<?php 
						$other_option=array(
							'class'=>'form-control number-field',
							'placeholder'=>'Personal No.',
							'data-validation'=>'length',
							'data-validation-length'=>'10-15',
							'data-validation-optional'=>'true',
							);
						echo $this->form->form_input('personal_no', $other_option); 
						?>
					</div>
				</div>
				<div class="clearfix"></div>
					
				<?php //if(!empty($edit_data)) { ?>
				<div class="col-md-4 col-sm-6">
					<div class="form-group">
						<label>Date of birth </label>
						<?php 
						$other_option=array(
							'class'=>'form-control datepicker',
							'placeholder'=>'Date of birth',
							"data-date-end-date"=>date('d-m-Y',strtotime('-15 years'))
						);
						echo $this->form->form_input('dob', $other_option); 
						?>
					</div>
				</div>
				<?php //} ?>
				
				<div class="clearfix"></div>

				<div class="col-md-12 col-sm-12">
					<div class="form-group">
						<label>About Me </label>
						<?php 
						$other_option=array(
							'class'=>'form-control',
							'placeholder'=>'About Me',
							);
						echo $this->form->form_textarea('user_about','','', $other_option); 
						?>
					</div>
				</div>

				<div class="col-md-4 col-sm-6">
					<div class="form-group">
						<label>Profile Picture </label>
						<?php 
						$other_option=array(
							'class'=>'form-control number-field',
							'placeholder'=>'Profile Pic',
							'data-validation'=>'mime size', 
		 					'data-validation-allowing'=>'jpg, png, gif', 
		 					'data-validation-max-size'=>'10M',
						);
						echo $this->form->form_upload('profile_pic', $other_option); 
						?>
					</div>
				</div>

				<div class="col-md-4 col-sm-6">
					<div class="form-group">
						<?php if(!empty($edit_data['profile_pic'])) { ?>
						<div class="thumbnail" style="width: 100px;">
							<a href="#" onclick="delete_pic(this)" class="btn btn-xs btn-danger pull-right"><i class="fa fa-trash-o"></i></a>
							<img src="<?php echo $base_url.$edit_data['profile_pic']; ?>" height="50">
						</div>
						<?php } ?>
					</div>
				</div>
				</div>
			</div>

			<div class="box-footer with-border">
				<div class="box-tools pull-right">
					<input type="reset" class="btn btn-default" value="Reset">
					<input type="submit" class="btn btn-primary" value="Submit">
				</div>
			</div>
		</div>
	</div>
</div>

<?php echo $this->form->form_close(); ?>

<script type="text/javascript">
	function delete_pic(ele) {
		var id=$(ele);
		$('#user_form').append('<input type="hidden" name="deleted_pic" value="1"/>');
		$(id).parent().remove();
	}
</script>