<div class="row">
	<?php $base_url=base_url();?>
		<div class="col-md-12">
			<div class="box box-primary">
				<div class="box-header with-border">
					<div class="box-title">
						 <a href="<?php echo $base_url.'user/user_add' ?>" class="btn btn-sm btn-primary"><i class="fa fa-plus"></i> Add</a>
						<a data-toggle="modal" data-target="#search_content" class="btn btn-primary btn-sm"><i class="fa fa-search"></i> Search</a>
						<?php if(!empty($clearfilter)) { ?>
							<a href="<?php echo $base_url.'user/'; ?>" class="btn btn-primary btn-sm">
								<i class="fa fa-search-minus"></i> Clear Search
							</a>
						<?php } ?>
					</div>
					<div class="pull-right">
						<?php if (isset($pagination_links)){ echo $pagination_links; } ?>
					</div>
				</div>

				<div class="panel-body">
					<div class="box-body table-responsive">
						<table class='table table-bordered table-striped dataTable' id="full-height-datatable">
							<thead>
								<tr>
									<th>Name</th>
									<th>Email ID</th>
									<th>Role</th>
									<th>Status</th>
									<th>Action</th>
								</tr>
							</thead>
                    
							<tbody> 
							  <?php foreach($users as $user_row) { ?>
								<tr>
									
									<td><?php echo $user_row['name']; ?></td>
									<td><?php echo $user_row['email']; ?></td>
									<td><?php echo $user_row['role_title']; ?></td>
									<td>
									<?php
									if($user_row['enable']=='1') {
										echo '<label class="label label-success">Enable</label>';
									}
									if($user_row['enable']=='0') {
										echo '<label class="label label-danger">Disable</label>';
									}
									?>	
									</td>
									<td>
										<div class="btn-group options">
											<button type="button" class="btn btn-default btn-sm" dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
												<i class="fa fa-cog"></i> Options
											</button>
											 
											<ul class="dropdown-menu">
												<li>
													<a href="<?php echo base_url()."user/user_add/".$user_row['user_id']; ?>">
														<i class="fa fa-edit fa-margin"></i> Edit
													</a>
												</li>
												<!-- <li>
													<a data-id="<?php echo $user_row['user_id']; ?>" class="pointer delete-user">
														<i class="fa fa-trash-o fa-margin"></i> Delete
													</a>
												</li> -->

											</ul>
										</div>
									</td>
									
								</tr>
							<?php }?>  
							</tbody>
						</table>
					</div>
				</div>
			</div>
	</div>
</div>

<!-- Modal -->
<div class="modal fade" id="search_content" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" data-backdrop="static">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
				<h4 class="modal-title" id="myModalLabel">Search User</h4>
			</div>
			<div class="modal-body">
				<?php 
					$form_model=array();
					/* Fill old data */
					if(!empty($old_data)) {
						$form_model=$old_data;  
					}
					echo $this->form->form_model($form_model, $base_url.'user/index/', array('name'=>'search_content','id'=>'search_comment_form', 'method'=>'get')); 
				?>
					<div class="row">
						<div class="col-md-12">
							<div id="error_div"></div>
						</div>
						<div class="col-md-12">
							<div class="col-md-6">
								<label>Role</label>
								<?php 
								$other_option=array(
									'class'=>'form-control',
									'placeholder'=>'Role',
								);
								echo $this->form->form_dropdown_fromdatabase('role_id',$role_list,'role_id','role_title','', $other_option); 
								?>
							</div>
							<div class="clearfix"></div>

							<div class="col-md-12">
								<label>Keyword</label>
								<?php 
								$other_option=array(
									'class'=>'form-control',
									'placeholder'=>'Keyword',
									);
								echo $this->form->form_input('keyword',$other_option); 
								?>
							</div>
							<div class="clearfix"></div>

							<div class="col-md-6">
								<label>Status</label>
								<?php 
								$other_option=array(
									'class'=>'form-control',
									'placeholder'=>'Status',
								);
								$option= array('1'=>'Enable','0'=>'Disable');
								echo $this->form->form_dropdown('enable',$option,'','', $other_option); 
								?>
							</div>

						</div>
					</div>
					<div class="row"><br/>
						<div class="modal-footer">
							<input type="submit" name="submit"  class="btn btn-primary" value="Search">
							<input type="reset" name="reset"  class="btn btn-default" value="Reset">
							<button type="button" class="btn btn-danger" data-dismiss="modal">Close</button>
						</div>							      			
					</div>							      			
				</form>
			</div>
		</div>
	</div>
</div>	

<script type="text/javascript">
	$(document).ready(function() {
		$('.delete-user').click(function() {
			var id=$(this).attr('data-id');
			swal({
				title: "Are you sure?",
				text: "User to delete?",
				type: "warning",
				showCancelButton: true,
				confirmButtonColor: "#DD6B55",
				confirmButtonText: "Yes, delete it!",
				closeOnConfirm: false
			},
			function(){
				window.location = base_url+'user/user_delete/'+id;
			});
		});
	})

</script>
