<?php
$base_url = base_url();
$form_model=array();
if(!empty($edit_data)) {
	$form_model=$edit_data;
}
/* Fill old data */
$old_data=$this->session->flashdata('old_data');
if(!empty($old_data)) {
	$form_model=$old_data;  
}
echo $this->form->form_model($form_model, $base_url.'user/role_save/',array('name'=>'save_role','id'=>'role_form', 'class'=>'validate-form'));
if(!empty($edit_data)) {
	echo $this->form->form_hidden('role_id');
}
?>

<div class="row">
	<div class="box-body">
		<div id="model_errors"></div>
		<div class="col-md-6 col-sm-6">
			<div class="form-group">
				<label>Role Title <span class="text-danger">*</span></label>
				<?php 
				$other_option=array(
					'class'=>'form-control',
					'placeholder'=>'Role Title',
					'data-validation'=>'required',
					'title'=>'Role Title',
				);
				echo $this->form->form_input('role_title', $other_option); 
				?>
			</div>
		</div>
		
	</div>
	
</div>

<div class="box-footer with-border">
	<div class="box-tools pull-right">
		<input type="reset" class="btn btn-default" value="Reset">
		<input type="submit" class="btn btn-primary" value="Submit">
	</div>
</div>
<?php echo $this->form->form_close(); ?>


<script type="text/javascript">
	$(document).ready(function() {

		jQuery.validate({
			
			form : '#role_form',
		});
	});

</script>