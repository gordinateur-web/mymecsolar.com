<div class="box box-primary">
	<?php
	$form_model=$role_permission;
	if(!empty($edit_data)) {
		$form_model=$edit_data;
	}	

	echo $this->form->form_model($form_model, base_url()."user/permission_save",array('name'=>'permission_save','method'=>'post','id'=>'permission_form'));
	?>
	<div class="box-body">
		<div class="form-group row">

			<div class="form-group col-md-12">
				<div class="alert alert-warning" role="alert"> 
					<strong>Warning!</strong> Permission update will only affect after session logout.
				</div>
				<table class="table table-bordered">
					<thead>
						<tr>
							<th>Permission</th>
							<?php
								foreach ($role_list as $role) {
									echo '<th>'.$role['role_title'].'&nbsp;&nbsp;&nbsp;<input type="checkbox" class="check_all" data-role="'.$role['role_id'].'"/></th>';
								}
							?>
						</tr>
					</thead>
					<tbody>
						<?php
						foreach ($permission_list as $permission) {
							echo '<tr>';
							echo '<td>'.$permission['permission_title'].'</td>';
							foreach ($role_list as $role) {
								echo '<td>';
								echo $this->form->form_checkbox($permission['permission_id'].'['.$role['role_id'].']',$role['role_id']);
								echo '</td>';
							}
							echo '</tr>';
						}
						?>
					</tbody>
				</table>
			</div>

			
		</div>
		<div class="modal-footer">
			<div class="pull-right">
				<button type="reset" class="btn btn-default">Clear</button>
				<button type="submit" class="btn btn-primary">Submit</button>
				<button onclick="window.history.back();" class="btn btn-danger">Close</button>
			</div>
		</div><!-- /.box-footer -->
		<?php echo $this->form->form_close(); ?>
	</div>

	<script type="text/javascript">

		$(document).ready(function() {
			/* Check all permission for role */
			$('.check_all').change(function() {
				var role_id=$(this).attr('data-role');
				if($(this).is(":checked")) {
					$('input[name*="['+role_id+']"]').prop('checked',true);
				}
				else {
					$('input[name*="['+role_id+']"]').prop('checked',false);
				}
			});

		});

	</script>