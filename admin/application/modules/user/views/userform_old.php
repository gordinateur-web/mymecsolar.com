<div class="row">
	<div id="error_div"></div>
</div>
<div class="box box-danger">
	<div class="box-body">
		<div class="row">
			<form name="user" id="user_add_form" action="<?php echo base_url().'user/save_user';?>" method="POST" enctype="multipart/form-data" onsubmit="return chk_module_selected()">	
				<?php 
					if(isset($edit_user)) {
						$user=$edit_user[0];
						echo "<input type='hidden' name='user_id' value='".$user['user_id']."'>";
					}			
					$old_data=$this->session->flashdata('old_data');
					if(isset($old_data) && is_array($old_data)) {
						$user=$old_data;
					}				 
				?>				

				<div class="col-md-12">
					<div class="box-header">
						<h3 class="box-title"><strong>Personal Details</strong></h3>
					</div>
				</div>	
				<div class="col-md-12">
					<?php if($this->session->userdata('role')=="superadmin") {?>
						<div class="col-md-4">
							<label>User's Company <span class="text-danger">*</span></label>
							<?php 
							if(isset($user)) { 
								$option=$user['companyid'];
								$disable="disabled";
							}
							else {
								$option='';
								$disable="";
							}
							echo generate_combobox('companyid',$company,'companyid','company_name',$option,'class="form-control select2" style="width:100%;" id="companyid"'.$disable.' required');
							?>
						</div>
					<?php } else { echo "<input type='hidden' name='companyid' id='companyid' value='".$this->session->userdata('companyid')."'>"; } ?>
					<div class="col-md-3">
						<label>Name <span class="text-danger">*</span></label>
						<input type="text" class="form-control" id="name" name="name" placeholder="Enter Name" data-validation="required" value="<?php if(isset($user['name'])) { echo $user['name']; }?>"/> 
					</div>
					<div class="col-md-3">
						<label>Role<span class="text-danger">*</span></label>
						<?php 
						$other_option=array(
							'class'=>'select2',
							'placeholder'=>'Select Role',
							'style'=>'width:100%;',
							'data-validation'=>'required',
							'multiple'=>'multiple'
						);
						$edit_data='';
						if(!empty($user_role)) {
							$edit_data=$user_role;
						}
						echo $this->form->form_dropdown_fromdatabase('role_id[]', $role_list,'role_id','role_title',$edit_data,$other_option); 	
						?>
					</div>

					<div class="col-md-3">
						<label>User Type <span class="text-danger">*</span></label>
						<?php 
						$other_option=array(
							'class'=>'select2',
							'placeholder'=>'Select user type',
							'style'=>'width:100%;',
							'data-validation'=>'required',
						);
						$edit_data='';
						if(!empty($user['user_type'])) { 
							$edit_data=$user['user_type'];
						}
						$user_type=user_type();
						echo $this->form->form_dropdown_fromdatabase('user_type', $user_type,'key','value',$edit_data,$other_option); 	
						?>
					</div>
					
					<div class="col-md-3 client_field" style="display: block">
						<label>Client Name <span class="text-danger">*</span></label>
						<?php 
							if(isset($user)) {
								$option=$user['client_id'];
							}
							else {
								$option='';	
							}
							echo generate_combobox('client_id',$client_list,'client_id','client_company',$option,'class="form-control select2" style="width:100%;" ');
						?>
					</div>
					</div>	
					<div class="clearfix"></div>

					<div class="col-md-12">
					<div class="col-md-3">
						<label>DOB</label>
						<input type="text" class="form-control number-field" id="dob" name="dob" placeholder="<?php echo dateformat_PHP_to_jQueryUI($dformat); ?>" data-date-format="<?php echo dateformat_PHP_to_jQueryUI($dformat); ?>" value="<?php if(isset($user) && !empty($user['dob'])) { echo date($dformat,strtotime($user['dob'])); } ?>">	
					</div>				
				
				
				
					<div class="col-md-3">
						<label>Mobile </label>
						<input type="text" class="form-control" id="mobile" name="mobile" placeholder="Enter Mobile" data-validation="required,length,number" data-validation-length="min10" data-validation-optional="true" value="<?php if(isset($user['mobile'])) { echo $user['mobile']; }?>"/> 
					</div>
					<div class="col-md-3">	
						<label>Email <span class="text-danger">*</span></label>
						<input type="email" class="form-control" id="email" name="email" data-validation="required,email" placeholder="Enter Email" value="<?php if(isset($user['email'])) { echo $user['email']; }?>"/>
						<span class="help-block">Email id will be used as login id</span>	
					</div>
					<div class="col-md-3">
						<label>Profile Pic <span class="text-danger">(Recommended 250X250)</span>
						<input type="file" name="file" 
						data-validation="mime size" 
                        data-validation-allowing="jpg, png, gif" 
                        data-validation-max-size="2M"
						/><br/>
						<?php if(isset($edit_user) && $user['file_path']!='') { ?>
						<div class="col-md-4">	
							<img src="<?php echo get_thumb(base_url().$user['file_path'],'thumb_150'); ?>" height="50">
						</div>
					<?php } ?>	
					</div>
					
				</div>
				<div class="clearfix"></div>

				<?php //if(isset($user['role']) && $user['role']!='client') { ?>
				<div class="col-md-12">
					<div class="col-md-4 user_field" >
						<label>Department</label>
						<?php 
							if(isset($user)) { 
								$option=$user['department_id'];
							}
							else {
								$option='';
							}
							echo generate_combobox('department_id',$department,'department_id','department',$option,'class="form-control select2" style="width:100%;"');
						?>
					</div>
					<div class="col-md-4 user_field" >
						<label>Joining Date</label>
						<input type="text" class="form-control" id="joining_date" name="joining_date" placeholder="<?php echo dateformat_PHP_to_jQueryUI($dformat); ?>" data-date-format="<?php echo dateformat_PHP_to_jQueryUI($dformat); ?>" value="<?php if(isset($user) && !empty($user['joining_date'])) { echo date($dformat,strtotime($user['joining_date'])); } ?>">	
					</div>	
					<div class="col-md-3">
						<label>Status <span class="text-danger">*</span></label>
						<?php 
							$status="1";
							if(isset($user)) { 
								if($user['enable']=="0") { 
									$status="0"; 
								} 
								else { 
									$status="1";
								} 
							} 
						?>
						<select name="enable" class="form-control" data-validation="required">
							<option value="1" <?php if($status=='1') { echo "selected"; }?>>Enable</option>
							<option value="0" <?php if($status=='0') { echo "selected"; }?>>Disable</option>
						</select>
					</div>			
				</div>	
				<?php //} ?>	

				<div class="col-md-12">
					<div class="col-md-12">
						<label>Bio</label>
						<textarea class="form-control" id="note" name="note" placeholder="Enter Bio"><?php if(isset($user)) { echo $user['note']; } ?></textarea>	
					</div>							
				</div>					
				<div class="col-md-12">
					<div class="box-header">
						<h3 class="box-title"><strong>User Details</strong></h3>
					</div>
				</div>		
				<div class="col-md-6">
					<div class="col-md-6">
						<label>Password <?php if(!isset($edit_user)) { ?><span class="text-danger">*</span><?php } ?></label>
						<input type="password" class="form-control" id="password" name="password" placeholder="Enter Password" data-validation="length" data-validation-length="min6" data-validation-optional="true" <?php if(!isset($edit_user)) { ?> data-validation="required,length" data-validation-length="min6" <?php } ?> value="" /> 
					</div>
					<div class="col-md-6">
						<label>Confirm Password <?php if(!isset($edit_user)) { ?><span class="text-danger">*</span><?php } ?></label>
						<input type="password" class="form-control" id="cfpassword" name="cfpassword" placeholder="Enter confirm Password" data-validation="confirmation"  data-validation-confirm="password"  <?php if(!isset($edit_user)) { ?> data-validation="confirmation"  data-validation-confirm="password" data-validation="required" <?php } ?>  /> 
					</div>	
										
				</div>	

				
				<!-- custom field form -->
				<?php echo $custom_fields_form; ?>
				<div class="clearfix"></div>

				<div class="col-md-12"><br/>			
					<div class="modal-footer">
						<input type="submit" name="submit"  class="btn btn-danger" value="Submit">
						<input type="reset" name="reset"  class="btn btn-default" value="Reset">
						<input type="button" name="close" id="close"  class="btn btn-danger" value="Close">
					</div>
				</div>
				
			</form>
		</div>
	</div>
</div>

<link href="<?php echo base_url().'public/plugins/datepicker/datepicker3.css'; ?>" rel="stylesheet" type="text/css" />
<script src="<?php echo base_url().'public/plugins/datepicker/bootstrap-datepicker.js'; ?>" type="text/javascript"></script>	
<script src="<?php echo base_url().'public/js/validate.cus.js'; ?>" type="text/javascript"></script>
<style type="text/css">
	.datepicker {
		top:420px;
	}
</style>
<script type="text/javascript">
	$(document).ready(function() {

		jQuery.validate({
			modules : 'security, file',
			form : '#user_add_form',
		});

		$('.select2').select2();
		$('#dob').datepicker({
			autoclose:true
		});
		
		$('#dob').click(function() {
			$('.datepicker').attr("style","top: 420px; left: 150.156px; z-index: 10; display: block;")
		});

		$('#joining_date').datepicker({
			autoclose:true
		});
		$('#joining_date').click(function() {
			$('.datepicker').attr("style","top: 420px; left: 495px; z-index: 10; display: block;")
		});

		$('#close').click(function () {
			window.location.href=base_url+'user/view_user';
		});		

		/* if select client hide users field */
		/*$('select[name="role"]').on('change', function() {
			change_role();
		});
		change_role();*/
	});


	/*function change_role(argument) {
		var $select=$('select[name="role"]');
		var role=$select.val();

		if(typeof role=='undefined') {
			role=$('input[name="role"]').attr('value');
		}


		if(role=='client') {
			$('.user_field').hide();
			$('.client_field').show();
		}
		else {
			$('.user_field').show();
			$('.client_field').hide();
		}
	}*/

	
</script>