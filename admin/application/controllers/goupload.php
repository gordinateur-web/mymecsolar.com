<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Goupload extends CI_Controller {
	public $image_table;
	public $allowedTypes;
	public $fileSize;
	public function __construct() {
		parent::__construct();
		$this->load->helper('text');
		$this->load->model('custom_content/media_model');
		$this->image_table='media';
		$this->allowedTypes='jpg,jpeg,png,gif,pdf,doc,docx,xls,xlsx,csv,ppt,pptx,txt,mp4,avi,wms,WebM,Ogv';
		$this->fileSize=1048576*55;
	}
	
	public function index($value='') {

		$media_filter=false;
		if(!permission_check(array(1,8),'and', 0)) {
			$media_filter=array(
				'created_by'=>$this->session->userdata('user_id')
			);
		}

		$data['file']=$this->media_model->get_media($media_filter);

		$this->load->view('goupload_start',$data);
	}

	public function upload() {
		$file_data = array();
		
		$user_email=clean_name($this->session->userdata('email'));

		if (!is_dir(UPLOAD_DIR.'cleanupload/'.$user_email)) {
			mkdir(UPLOAD_DIR.'cleanupload/'.$user_email, 0777, TRUE);
		}

		/* menu icon upload folder path  */
		$cleanupload = UPLOAD_DIR.'cleanupload/'.$user_email.'/';

		/* logo Upload file */
		$upload_config['upload_path']= $cleanupload;
		$upload_config['allowed_types']= 'gif|jpg|jpeg|png|pdf|doc|docx|xls|xlsx|csv|ppt|pptx|txt|mp4|avi|wms|WebM|Ogv';
		$upload_config['file_ext_tolower']= true;
		$upload_config['max_filename']= 100;

		/* Check if upload folder available */
		if(!file_exists($upload_config['upload_path'])) {
			mkdir($upload_config['upload_path']);
		}

		/* Upload profile pic file */
		$this->load->library('upload', $upload_config);
		$this->upload->initialize($upload_config);
		if(!empty($_FILES['myfile']['name'])) {
			if (!$this->upload->do_upload('myfile')) {
				set_message($this->upload->display_errors());
				redirect_back();
			}
			else {
				$upload_data=$this->upload->data();

				$file_data=array(
					'media_title'=>$upload_data['orig_name'],
					'media_alt'=>$upload_data['orig_name'],
					'media_path'=>$upload_config['upload_path'].$upload_data['orig_name'],
					'media_type'=>$upload_data['file_type'],
					'media_size'=>$upload_data['file_size'],
					'created_at'=>date('Y-m-d H:i:s'),
				);
				$file_id=$this->media_model->add_media($file_data);
				$file_data['media_id']=$file_id;
				$thumb_name=$upload_data['raw_name'].$upload_data['file_ext'];
			
				if(!file_exists($cleanupload.'150')) {
					mkdir($cleanupload.'150/');
				}
				if(!file_exists($cleanupload.'300')) {
					mkdir($cleanupload.'300/');
				}
				if(!file_exists($cleanupload.'500')) {
					mkdir($cleanupload.'500/');
				}
				
				/* Create thumb */
				image_thumb($file_data['media_path'],'150','150',$thumb_name,$cleanupload.'150/');
				image_thumb($file_data['media_path'],'300','300',$thumb_name,$cleanupload.'300/');
				image_thumb($file_data['media_path'],'500','500',$thumb_name,$cleanupload.'500/');
				
				
			}
		}
		echo json_encode($file_data);
	}

	public function delete() {
		$del_id=$this->input->post('id');
		if(empty($del_id)) {
			echo json_encode(array('status'=>0,'messgae'=>'Select atleast one file'));
			die;
		}

		$this->db->where('file_id',$del_id);
		$content_value=$this->db->get('content_value')->result_array();
		
		if(!empty($content_value)) {
			echo json_encode(array('status'=>0,'message'=>'You can not delete files which are uploaded from content forms'));
			die;
		}
		
		$this->db->where('fid',$del_id);
		$rs=$this->db->get($this->image_table)->result_array();
		if(empty($rs)) {
			echo json_encode(array('status'=>0,'message'=>'Invalid file selected'));
			die;
		}

		/*remove image*/
		unlink($rs[0]['imagepath']);
		unlink(get_thumb($rs[0]['imagepath'],'thumb_150'));
		unlink(get_thumb($rs[0]['imagepath'],'thumb_300'));

		$this->db->where('fid',$del_id);
		$res=$this->db->delete($this->image_table);
		echo json_encode(array('status'=>1,'messgae'=>'File deleted successfully'));
			die;
	}
}