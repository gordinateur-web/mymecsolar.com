<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed'); 
class Pagenotfound extends CI_Controller {
    public function __construct() {
        parent::__construct(); 
    } 
 
    public function index() { 
    	//$data['page_title']="404";
        $this->output->set_status_header('404');
       // view_with_master('404',$data, FRONT_MASTER_VIEW);
        $this->load->view('site/404.php');
    } 
} 
?> 