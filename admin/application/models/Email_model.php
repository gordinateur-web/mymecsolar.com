<?php

class Email_model extends CI_Model {
	public $from;
	public $bcc;
	public $cc;
	public $to;
	public $compapny_name;
	public $template_view;
	public $receipt_path;

	public function __construct() {
		parent::__construct();
		//$this->from="no-reply@".$_SERVER['HTTP_HOST'];
		$this->to="";
		$this->bcc="";
		$this->cc="";
		$this->company_name="";
		$this->template_view='emailer';
	}

	function send_forgotpwd_token($token_data) {
		$to=$token_data['email'];
		$subject="Reset Password request for ".APP_NAME;
		$data['body']="<h4 style=\"color:#00a89c\">Dear ".APP_NAME." User,</h4>
		<p style=\"color:#333333\">As per your request to reset password. Please click below link .</p><p>Click here <a href='".base_url()."login/verify_password_token/".$token_data['forgot_token']."'>Password link</a></p><p>Thanks and Regards</p><p>".APP_NAME."</p>";

		$body=$this->load->view($this->template_view,$data,true);
		return $this->send_email($to,$subject,$body);		
	}

	public function send_email($to,$subject,$body,$attach=false, $from=false) {

		$config['charset'] = 'iso-8859-1';

		$this->load->library('email');

		//SMTP & mail configuration
		  $config = array(
		   	'protocol'  => 'smtp',
			'smtp_host' => 'md-91.webhostbox.net',
			'smtp_port' => 587,
			'smtp_user' => 'no-reply@gordinateur.net',
			'smtp_pass' => '#Xpg[0R2eIAc',
			'mailtype'  => 'html',
			'charset'   => 'utf-8'
		  );
		//$this->email->clear(TRUE);

		//$to='ramjeet@gordinateur.com';

		$this->email->initialize($config);
		$this->email->from('no-reply@gordinateur.net','Mymecsolar');
		/*if(!empty($from)) {
			$this->email->from($this->from, $this->company_name);	
		}
		else {
			$this->email->from($from);
		}*/
		
		$this->email->to($to);
		if($this->bcc!='') {
			$this->email->bcc($this->bcc);	
		}
		if($this->cc!='') {
			$this->email->cc($this->cc);	
		}		
		$this->email->subject($subject);
		$this->email->message($body);
		if($attach!='') {
			$this->email->attach($attach);
		}
		
		$send=$this->email->send();
		
 	// 	echo $this->email->print_debugger();
		// echo $body;
		// die;
		
		return $send;
	}
} 
?>