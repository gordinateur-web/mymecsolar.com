<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');
  
class User_model extends CI_Model {
	/*
		Return permission list with user details, role details 
	*/
	public function get_permisson($filter=false) {
		if(!empty($filter)) {
			apply_filter($filter);
		}

		$this->db->select('users.name, users.email, users.role_id, role.role_title, permission_master.permission_id, permission_master.permission_title, permission_master.permission_des');
		$this->db->join('permission_master','permission_master.permission_id=role_permission.permission_id');
		$this->db->join('role','role.role_id=role_permission.role_id');
		$this->db->join('users','users.role_id=role.role_id');
		$rs=$this->db->get('role_permission');
		return $rs->result_array();
	}

	/*
		Get permission with user id
	*/
	public function get_user_permission($user_id) {
		return $this->get_permisson(array(
			'users.user_id'=>$user_id
		));
	}

	/*
		return permission for specific role
	*/
	public function get_role_permission($role_id) {
		$this->db->select('role.role_id,role.role_title, permission_master.permission_id, permission_master.permission_title, permission_master.permission_des');
		$this->db->join('role','role.role_id=role_permission.role_id');
		$this->db->join('permission_master','permission_master.permission_id=role_permission.permission_id');
		$rs=$this->db->get('role_permission');
		return $rs->result_array();
	}
}