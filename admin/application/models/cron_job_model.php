<?php

class Cron_job_model extends CI_Model {

	/* Validate get parameter of cron call */
	function check_cron_key($key=false) {
		/* Set default value */
		if($key==false) {
			$key=$this->input->get('cron_key');
		}

		/* Check not empty */
		if(empty($key)) {
			return false;
		}

		$query="select `variable_value` from `variable` where `variable_key`='CRON_JOB' and `variable_value`=?";
		$res=$this->db->query($query,array($key));
		if($res->num_rows() > 0) {
			return true;
		}
		return false;
	}

	/* Insert entry for cron job run */
	public function log_cron_job() {
		/* Add cron_log entry */
		$insert_array=array(
			'method_name'=>$this->uri->segment(2),
			'created_at'=>date('Y-m-d H:i:s'),
		);
		$this->db->insert('cronjob_log',$insert_array);
		return $this->db->insert_id();
	}

}