<!DOCTYPE html>
<html lang="en">
	<head>
		<?php $this->load->view('admin_layout/head'); ?>	
	</head>

	<body class="hold-transition skin-blue sidebar-mini">
		<div class="wrapper">
			<?php $this->load->view('admin_layout/header'); ?>
			<?php $this->load->view('admin_layout/sidebar'); ?>

			<!-- Content Wrapper. Contains page content -->
			<div class="content-wrapper">
				<!-- Content Header (Page header) -->
				<section class="content-header">
					<h1>
						<?php 
							if(isset($title)) {
								echo $title;
							}
							$content_type_section_id=$this->input->get('content_type_section_id');
							$content_type_id=$this->input->get('section_content_parent');

							if(!empty($content_type_section_id)){
							$get_content_type_section=$this->content_model->get_content_type_section_data($content_type_section_id);
							$content_id=$get_content_type_section[0]['content_type_id'];
							$get_content_type_data=$this->content_model->content_type_data($content_id);
						}
							
						?>
						<small>
							<?php 
							if(!empty($content_type_section_id) || !empty($content_type_id)){
								echo 'For '.$get_content_type_data[0]['content_type_title'].' in '.$get_content_type_section[0]['section_title'];
							}
								if(isset($pageDescription)) {
									echo $pageDescription;
								}
							?>
						</small>
					</h1>
					<!-- beradcrum -->
					<!-- <ol class="breadcrumb">
						<li><a href="#"><i class="fa fa-dashboard"></i> Level</a></li>
						<li class="active">Here</li>
					</ol> -->
				</section>
				<?php $this->load->view("admin_layout/show_msg"); ?>

				<!-- Main content -->
				<section class="content">
					<!-- Your Page Content Here -->
					<?php 
						if(!empty($content_view)) {
							if($view_type=='file'){
								$this->load->view($content_view);
							}
							else {
								echo $content_view;	
							}
						}
					?>
				</section><!-- /.content -->
			</div><!-- /.content-wrapper -->
		</div>
	</body>
</html>