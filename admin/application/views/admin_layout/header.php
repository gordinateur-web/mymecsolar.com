    <header class="main-header">
        <!-- Logo -->
        <a href="<?php echo base_url(); ?>" class="logo">
            <!-- mini logo for sidebar mini 50x50 pixels -->
            <span class="logo-mini"><?php echo APP_SHORT_NAME; ?></span>
            <!-- logo for regular state and mobile devices -->
            <span class="logo-lg"><?php echo APP_NAME; ?></span>
        </a>
        <!-- Header Navbar -->
        <nav class="navbar navbar-static-top" role="navigation">
            <!-- Sidebar toggle button-->
            <a href="#" class="sidebar-toggle" data-toggle="offcanvas" role="button">
                <span class="sr-only">Toggle navigation</span>
            </a>
            <!-- Navbar Right Menu -->
            <div class="navbar-custom-menu">
                <ul class="nav navbar-nav">
                    <li>
                        <?php define("BASE_URL", "http://localhost/mymecsolar.com/"); ?>
                        <a href="<?php echo BASE_URL; ?>" target="_blank">
                            Front End
                        </a>
                    </li>

                    <li class="dropdown">                       
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                            View Content
                        </a>
                         <ul class="dropdown-menu">
                             <?php 
                                $this->load->model('custom_content/content_type_model');
                                $content_list=$this->content_type_model->get_content_type(array(
                                    'ORDER_BY'=>array('content_type.content_type_title'=>'asc')
                                ));

                                foreach ($content_list as $content_type_row) {
                                    $href='href="'.base_url().'custom_content/content/view_content/'.$content_type_row['content_type_id'].'"';
                                    if($content_type_row['is_singular']=='1') {
                                        $href='';
                                    }
                                ?>
                            <li>
                                <a <?php echo $href; ?>> <?php echo $content_type_row['rank_prefix'].$content_type_row['content_type_title']; ?> </a>
                            </li>
                            <?php } ?>
                        </ul>
                    </li>
                    
                    <!-- add content -->
                    <li class="dropdown">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                            Add Template
                        </a>
                        <ul class="dropdown-menu">
                            <?php
                            $this->load->model('custom_content/content_type_model');
                            $content_list=$this->content_type_model->get_content_type(array(
                                'ORDER_BY'=>array('content_type.list_rank'=>'asc')
                            ));

                            foreach ($content_list as $content_type_row) {  
                            if(!$this->content_type_model->check_content_permission($content_type_row['content_type_id'], array(1), false, 0) || $content_type_row['content_type_category']=='webform') {
                            continue;
                            }
                        ?>
                            <li>
                                <?php 
                                    $href='href="'.base_url().'custom_content/content/add_content_data/'.$content_type_row['content_type_id'].'"';
                                ?>
                                <a <?php echo $href; ?>><?php echo $content_type_row['rank_prefix'].$content_type_row['content_type_title']; ?> </a>
                            </li>
                            <?php } ?>
                        </ul>
                    </li>
                   
                    
                    <!-- User Account Menu -->
                    <li class="dropdown user user-menu">
                        <!-- Menu Toggle Button -->
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                            <!-- The user image in the navbar-->
                            <?php
                                $user_pic=$this->session->userdata('profile_pic');
                                //dsm($user_pic);die;
                                if(!empty($user_pic)) {
                                    $profile_pic= base_url().$user_pic;
                                }
                                else {
                                    $profile_pic= base_url().USER_PROFILE_PIC;
                                }
                            ?>
                            <img src="<?php echo $profile_pic; ?>" class="user-image" alt="User Image">
                            <!-- hidden-xs hides the username on small devices so only the image appears. -->
                            <span class="hidden-xs"><?php echo $this->session->userdata('name'); ?></span>
                        </a>
                        <ul class="dropdown-menu">
                            <!-- The user image in the menu -->
                            <li class="user-header">
                                
                                <img src="<?php echo $profile_pic; ?>" class="img-circle" alt="User Image">
                                <p>
                                    <?php echo $this->session->userdata('name'); ?> - <?php echo ucfirst($this->session->userdata('designation'));?>
                                    <small></small>
                                </p>
                            </li>
                            <!-- Menu Footer-->
                            <li class="user-footer">
                                <div class="pull-left">
                                    <a href="#" class="btn btn-default btn-flat" data-toggle="modal" data-target="#cngpwd">Change Password</a>
                                </div>
                                <div class="pull-right">
                                    <a href="<?php echo base_url().'admin_login/login/logout'; ?>" class="btn btn-default btn-flat">Sign out</a>
                                </div>
                            </li>
                        </ul>
                    </li>
                </ul>
            </div>
        </nav>
    </header>
<!-- Modal -->
<div class="modal fade" id="cngpwd" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" data-backdrop="static">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
                <h4 class="modal-title" id="myModalLabel">Change Password</h4>
            </div>
            <?php $this->load->helper('form');
            echo form_open('admin_login/login/save_change_password', array('name' => 'passwordform','id'=>'passwordform')); ?>
            <div class="modal-body">
                <div class="form-group">
                    <label>Old Password</label>
                    <input type="password" class="form-control" id="opasssword" name="password" placeholder="Enter Old Passsword " data-validation="required"/>
                </div>
                <div class="form-group">
                    <label>New Password</label>
                    <input type="password" class="form-control" id="npasssword" name="new_passsword" placeholder="Enter New Passsword " data-validation="length,required" data-validation-length="min7"/>
                </div>
                <div class="form-group">
                    <label>New Confirm Password</label>
                    <input type="password" class="form-control" id="ncpasssword" name="confirm_passsword" placeholder="Enter New Confirm Passsword " data-validation="confirmation" data-validation-confirm="new_passsword"/>
                </div>
                 <div class="form-group">
                    <input type="submit" name="submit"  class="btn btn-primary" value="Submit">
                </div>
            </div>                      
            <?php echo form_close(); ?>
        </div>
    </div>
</div>
<script type="text/javascript">
    $(document).ready(function() {
        jQuery.validate({
            form : '#passwordform',
            modules : 'security'
        });
    });
</script>