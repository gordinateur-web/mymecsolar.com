
<!-- Left side column. contains the logo and sidebar -->
<aside class="main-sidebar">
	<section class="sidebar">

		<!-- Sidebar Menu -->
		<ul class="sidebar-menu">
			<li><a href="<?php echo base_url().'dashboard'; ?>"><i class="fa fa-dashboard"></i> <span>Dashboard</span></a></li>
			</li>
			<?php if(permission_check(array(1,6),'and',0)) { ?>
				<!-- <li>
					<a href="<?php echo base_url().'comment/' ?>"><i class="fa fa-unlock-alt" aria-hidden="true"></i> <span>Comment</span></a>
				</li> -->
			<?php } ?>
			<?php if(permission_check(array(1,12),'and',0)) { ?>
				<!-- <li>
					<a href="<?php echo base_url().'quiz/' ?>"><i class="fa fa-question-circle-o" aria-hidden="true"></i> <span>Quiz/Poll</span></a>
				</li> -->
			<?php } ?>

			<!-- <li>
				<a href="<?php echo base_url().'custom_content/content/view_content/30' ?>"><i class="fa fa-rss" aria-hidden="true"></i> <span> Forum</span></a>
			</li> -->

			<li class="treeview">
				<a href="<?php echo base_url()."custom_content/Content/" ?>"><i class="fa fa-file-text"></i> <span>Contents</span> </a>
			</li>

			<li class="treeview">
				<a href="#"><i class="fa fa-anchor" aria-hidden="true"></i> <span> Structure </span> <i class="fa fa-angle-left pull-right"></i></a>
				<ul class="treeview-menu">
				<!-- <?php if(permission_check(array(1,4),'and',0)) { ?>
					<li><a href="<?php echo base_url()."block/" ?>"><i class="fa fa-circle-o"></i> Block</a></li>
					<li><a href="<?php echo base_url()."block/block_visibility_list/" ?>"><i class="fa fa-circle-o"></i> Block Visibility</a></li>
				<?php } ?> -->
				<!-- <?php if(permission_check(array(1,3),'and',0)) { ?>
					<li><a href="<?php echo base_url().'menu/' ?>"><i class="fa fa-circle-o"></i> <span>Menu</span></a></li>
				<?php } ?> -->
					<li><a href="<?php echo base_url()."custom_content/content/content_type_list/" ?>"><i class="fa fa-circle-o"></i> Content type</a></li>
				</ul>
			</li>

			<?php if(permission_check(array(1,5),'and',0)) { ?>
			<li class="treeview">
				<a href="#"><i class="fa fa-users" aria-hidden="true"></i> <span> People </span> <i class="fa fa-angle-left pull-right"></i></a>
				<ul class="treeview-menu">
					<li><a href="<?php echo base_url().'user/' ?>"><i class="fa fa-circle-o"></i> <span>User</span></a></li>
					<?php if(permission_check(array(1,15),'and',0)) { ?>
					<li><a href="<?php echo base_url().'user/role_list' ?>"><i class="fa fa-circle-o" aria-hidden="true"></i> <span>Role</span></a></li>
					<li><a href="<?php echo base_url().'user/permission_list' ?>"><i class="fa fa-circle-o" aria-hidden="true"></i> <span>Permission</span></a></li>
					<?php } ?>
				</ul>
			</li>
			<?php } ?>

			<?php if(permission_check(array(1,9),'and',0)) { ?>
			<li>
				<a href="<?php echo base_url().'dashboard/login_history/' ?>"><i class="fa fa-unlock-alt" aria-hidden="true"></i> <span>Login History</span>
				</a>
			</li>
			<!-- <li>
				<a href="<?php echo base_url().'subscription/subscription/subscription_email_listing/' ?>"><i class="fa fa-envelope" aria-hidden="true"></i> <span>Subscribed User</span>
				</a>
			</li> -->
			<?php } ?>

			<!-- <?php if(permission_check(array(1,7),'and',0)) { ?>
			<li>
				<a href="<?php echo base_url().'custom_content/content/import_json/'; ?>" ><i class="fa fa-upload"></i><span> Import Content</span></a>
			</li>
			<?php } ?> -->
		</ul><!-- /.sidebar-menu -->
	</section>
	<!-- /.sidebar -->
</aside>

<!-- Modal -->
<div class="modal fade" id="commanModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" data-backdrop="static">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
				<h4 class="modal-title" id="myModalLabel"></h4>
			</div>
			<div class="modal-body row"></div>
		</div>
	</div>
</div>					