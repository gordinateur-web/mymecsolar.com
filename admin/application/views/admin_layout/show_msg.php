<?php
	$alert_message='';
 	if($this->session->userdata('success') != '') {
		$alert_message.='<div class="alert alert-success alert-dismissable">
			<button aria-hidden="true" data-dismiss="alert" class="close" type="button">x</button> 
				<ul>'.$this->session->userdata('success').'</ul>
		</div>';
		$this->session->unset_userdata('success');
	}

	if($this->session->userdata('error') != '') {
		$alert_message.='<div class="alert alert-danger alert-dismissable">
			<button aria-hidden="true" data-dismiss="alert" class="close" type="button">x</button> 
				<ul>'.$this->session->userdata('error').'</ul>
		</div>';
		$this->session->unset_userdata('error');
	}

	if($this->session->userdata('warning') != '') {
		$alert_message.='<div class="alert alert-warning alert-dismissable">
			<button aria-hidden="true" data-dismiss="alert" class="close" type="button">x</button> 
				<ul>'.$this->session->userdata('warning').'</ul>
		</div>';
		$this->session->unset_userdata('warning');
	}

	/* show alerts */
	if(!empty($alert_message)) {
		echo '<div class="row"><div class="col-md-12">';
		echo $alert_message;
		echo '</div></div>';
	}
