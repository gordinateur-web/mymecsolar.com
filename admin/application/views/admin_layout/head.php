	<meta charset="UTF-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<title>
		<?php 
			if(isset($page_title)) {
				echo $page_title.' | '.APP_NAME;
			} 
			elseif(isset($title)) {
				echo strip_tags($title).' | '.APP_NAME;
			}
			else {
				echo APP_NAME;
			}
		?>
	</title>
	<link rel="icon" href="<?php echo base_url().'public/front_assets/images/fav.png'; ?>" type="image/x-icon">
	<!-- Tell the browser to be responsive to screen width -->
    <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
    
    <?php
    	$global_css=array(
    		base_url().'public/bootstrap/css/bootstrap.min.css',
    		base_url().'public/plugins/font-awesome-4.7.0/css/font-awesome.min.css',
    		base_url().'public/AdminLte/css/skins/_all-skins.min.css',
    		base_url()."public/plugins/datatables/dataTables.bootstrap.css",
    		base_url()."public/plugins/sweetalert/sweetalert.css",
    		base_url()."public/plugins/datepicker/datepicker3.css",
    		base_url()."public/plugins/select2/select2.min.css", 
    		base_url().'public/AdminLte/css/Admin.min.css',
    	);

    	$global_js=array(
			base_url().'public/plugins/jquery/jquery-3.1.1.min.js',
			base_url().'public/bootstrap/js/bootstrap.min.js',
			base_url().'public/AdminLte/js/app.min.js',
			base_url().'public/plugins/jquery-validation/jquery.form-validator.min.js',
			base_url()."public/plugins/datatables/jquery.dataTables.js",
			base_url()."public/plugins/sweetalert/sweetalert.min.js",
			base_url()."public/plugins/select2/select2.full.min.js",
			base_url()."public/plugins/datepicker/bootstrap-datepicker.js",
			base_url()."public/plugins/slimScroll/jquery.slimscroll.min.js",
			base_url()."public/js/script.js",
	    );

		/* to load css in header pass url of css in $loadCss variable as array*/
		if(isset($loadCss)) {
			if(is_array($loadCss)) {
				foreach($loadCss as $css) {
					$global_css[]=$css;
				}
			}
		}

		/* Custom admin css at last */
		$global_css[]=base_url()."public/css/custom_admin.css";
		echo generate_style_link($global_css);

	?>
	<!--[if lt IE 9]>
		<script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
		<script src="https://oss.maxcdn.com/libs/respond.js/1.3.0/respond.min.js"></script>
	<![endif]-->
	
	<script type="text/javascript">
		var base_url = "<?php echo base_url(); ?>";	
	</script>

	<?php 
		/* to load javascript file pass url of css in $loadJs variable as array */
		if(isset($loadJs)) {
			if(is_array($loadJs)) {
				foreach($loadJs as $js) {
					$global_js[]=$js;
				}
			}
		}
		echo generate_script_link($global_js);
	?>