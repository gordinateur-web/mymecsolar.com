<html>
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <link rel="stylesheet" href="<?php echo base_url().'public/AdminLte/css/AdminLTE.css'; ?>">
        <link rel="stylesheet" href="<?php echo base_url().'public/bootstrap/css/bootstrap.min.css'; ?>">
        <link href="<?php echo base_url().'public/plugins/ckeditor/plugins/cleanuploader/assets/css/uploadfile.css'; ?>" rel="stylesheet">
        <title>GoUpload</title>
        <style type="text/css">
            .navbar-default {
                background-color: #e1e1e1;
                border-color: #a4a3a3;
            }
        </style>
    </head>
    <body>
        <nav class="navbar navbar-default navbar-fixed-top navbar">
            <div class="container">
                <div class="navbar-brand text-left"> GoUpload </div>
            </div>
        </nav>
        <div class="container-fluid text-center">
            <div class="row">
                <div class="col-xs-12">
                    <div class="upload" id="multipleupload"></div>
                </div>
            </div>
            <div class="row" id="filelist"> </div>
            <div class="row">
                <div class="col-xs-12 text-center">
                    <div class="uploadButtonContainer" id="uploadButtonContainer">
                        <button type="button" class="btn btn-primary" onClick="startUpload();"><i class="glyphicon glyphicon-upload"></i> Upload</button>
                    </div>
                </div>
            </div>
        </div>
        <div class="container currentFiles">
            <!-- <table class="table table-striped" id="list">
                <tbody>
                    <?php
                        foreach ($file as $row) {
                            $ext=trim(strtolower(pathinfo($row['media_path'],PATHINFO_EXTENSION)));
                        ?>
                    <tr id="<?php echo $row['media_id'] ?>">
                        <td>
                            <?php if($ext=='jpg' || $ext=='jpeg' || $ext=='png' || $ext=='gif') { ?>
                                    <img src="<?php echo get_thumb(base_url().$row["media_path"],'150'); ?>" onClick="useImage('<?php echo base_url().$row["media_path"]; ?>');" id="addImg" data-toggle="tooltip" data-placement="right" title="Add image"/>
                            <?php } else { ?>
                                    <center><a href="javascript:void(0)" onClick="useImage('<?php echo base_url().$row["media_path"]; ?>');" id="addImg" data-toggle="tooltip" data-placement="right" title="Click to Select"><i class="glyphicon glyphicon-file" style="font-size:34px;"></i></a></center>
                            <?php } ?>
                        </td>
                        <td>
                            <p><?php echo $row["media_title"] ?></p>
                        </td>
                        
                        <td><button type="button" class="btn btn-danger pull-right" onClick="deleteImage('<?php echo $row['media_id'] ?>');"><i class="glyphicon glyphicon-trash"> </i> Delete</button></td>
                    </tr>
                    <?php } ?>
                </tbody>
            </table> -->



            <div class="grid-section" id="list">
               <?php $i=1;
                foreach ($file as $row) {
                $ext=trim(strtolower(pathinfo($row['media_path'],PATHINFO_EXTENSION)));
                ?>
                    <div class="grid" id="<?php echo $row['media_id'] ?>">
                        <?php if($ext=='jpg' || $ext=='jpeg' || $ext=='png' || $ext=='gif') { ?>
                            <img src="<?php echo get_thumb(base_url().$row["media_path"],'150'); ?>" onClick="useImage('<?php echo base_url().$row["media_path"]; ?>');" id="addImg" data-toggle="tooltip" data-placement="right" title="Add image"/>
                        <?php } else { ?>
                            <center><a href="javascript:void(0)" onClick="useImage('<?php echo base_url().$row["media_path"]; ?>');" id="addImg" data-toggle="tooltip" data-placement="right" title="Click to Select"><i class="glyphicon glyphicon-file" style="font-size:34px;"></i></a></center>
                        <?php  } ?>
                        <p><?php echo $row["media_title"] ?></p>
                        <button type="button" class="btn btn-primary" onClick="deleteImage('<?php echo $row['media_id'] ?>');"><i class="glyphicon glyphicon-trash"> </i> Delete</button>
                    </div>
                <?php } ?>
                 
            </div>






        </div>
        <!--  Scripts   --> 
        <script type="text/javascript" src="<?php echo base_url().'public/plugins/jquery/jquery-3.1.1.min.js'; ?>"></script>
        <script type="text/javascript" src="<?php echo base_url().'public/bootstrap/js/bootstrap.min.js'; ?>"></script>
        <script src="<?php echo base_url().'public/plugins/ckeditor/plugins/cleanuploader/assets/js/jquery.uploadfile.js'; ?>"></script> 
        <script src="<?php echo base_url().'public/plugins/ckeditor/plugins/cleanuploader/assets/js/functions.js'; ?>"></script>
        <link href="<?php echo base_url().'public/plugins/sweetalert/sweetalert.css'; ?>" rel="stylesheet">
        <script src="<?php echo base_url().'public/plugins/sweetalert/sweetalert.min.js'; ?>"></script>
        <script>
            var downloadObj = $("#multipleupload").uploadFile({
                url: '<?php echo base_url(); ?>'+"goupload/upload",
                multiple: true,
                dragDrop: true,
                fileName: "myfile",
                allowedTypes: "<?php echo $this->allowedTypes; ?>",
                maxFileSize: <?php echo $this->fileSize; ?>,
                autoSubmit: false,
                showPreview: true,
                previewHeight: 120,
                previewWidth: 120,
                onLoad: function (obj) {
                    $("#filelist").hide()
                },
                afterUploadAll: function (obj)
                {
            
                },
                onSuccess: function (files, data, xhr, pd)
                {
                    $("#filelist").fadeOut(750, "linear"),
                    jObj = JSON.parse(data),
                    addNewImage(jObj.media_path, jObj.media_title, jObj.media_size, jObj.media_type, jObj.id),
                    $("#uploadButtonContainer").hide()
            
                },
                onSelect: function (files)
                {
                    $("#filelist").fadeIn(250, "linear"),
                    controlOfFileExtension(files)
                },
                onCancel: function (files, pd)
                {
                    numberOfFilesSelected--,
                    checkNumberOfFiles()
                },
                onError: function (files, status, errMsg, pd) {
            
                }
            });
        </script>
    </body>
</html>