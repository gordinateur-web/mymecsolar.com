<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/*
| -------------------------------------------------------------------------
| URI ROUTING
| -------------------------------------------------------------------------
| This file lets you re-map URI requests to specific controller functions.
|
| Typically there is a one-to-one relationship between a URL string
| and its corresponding controller class/method. The segments in a
| URL normally follow this pattern:
|
|	example.com/class/method/id/
|
| In some instances, however, you may want to remap this relationship
| so that a different class/function is called than the one
| corresponding to the URL.
|
| Please see the user guide for complete details:
|
|	https://codeigniter.com/user_guide/general/routing.html
|
| -------------------------------------------------------------------------
| RESERVED ROUTES
| -------------------------------------------------------------------------
|
| There are three reserved routes:
|
|	$route['default_controller'] = 'welcome';
|
| This route indicates which controller class should be loaded if the
| URI contains no data. In the above example, the "welcome" class
| would be loaded.
|
|	$route['404_override'] = 'errors/page_missing';
|
| This route will tell the Router which controller/method to use if those
| provided in the URL cannot be matched to a valid route.
|
|	$route['translate_uri_dashes'] = FALSE;
|
| This is not exactly a route, but allows you to automatically route
| controller and method names that contain dashes. '-' isn't a valid
| class or method name character, so it requires translation.
| When you set this option to TRUE, it will replace ALL dashes in the
| controller and method URI segments.
|
| Examples:	my-controller/index	-> my_controller/index
|		my-controller/my-method	-> my_controller/my_method
*/

if(defined('EXTERNAL_CI')) {
	$route['(.+)'] = 'site/index/$1';
	$route['default_controller'] = 'site/index';
}
else {
	$route['default_controller'] = 'admin_login/login';
}

$route['admin'] = 'admin_login/login';
$route['404_override'] = 'pagenotfound';
$route['translate_uri_dashes'] = true;

$route['admin_login'] = 'admin_login';
$route['admin_login/(.+)'] = 'admin_login/$1';

$route['block'] = 'block';
$route['block/(.+)'] = 'block/$1';

$route['comment'] = 'comment';
$route['comment/(.+)'] = 'comment/$1';

$route['custom_content'] = 'custom_content';
$route['custom_content/(.+)'] = 'custom_content/$1';

$route['dashboard'] = 'dashboard/dashboard';
$route['dashboard/(.+)'] = 'dashboard/dashboard/$1';

$route['menu'] = 'menu';
$route['menu/(.+)'] = 'menu/$1';

$route['user'] = 'user';
$route['user/(.+)'] = 'user/$1';

$route['subscription'] = 'subscription';
$route['subscription/(.+)'] = 'subscription/$1';

$route['goupload'] = 'goupload';
$route['goupload/(.+)'] = 'goupload/$1';

$route['Pagenotfound'] = 'Pagenotfound';

/* All url will first hit on this controller */
$route['(.+)'] = 'site/index/$1';
$route['site/get_apply_category/'] = 'site/site/get_apply_category/';



/* front url route */
/*$route['home'] = 'front/index/';
$route['program/(:any)'] = 'front/program/$1';
$route['sub_program/(:any)'] = 'front/sub_program/$1';
$route['sub_program/(:any)/(:any)'] = 'front/sub_program/$1/$2';
$route['partner'] = 'front/partner/';
$route['media'] = 'front/media/';
$route['our_people'] = 'front/our_people/';
$route['search_result'] = 'front/search_result/';
$route['contact-us'] = 'front/contact_us/';
$route['about-us'] = 'front/about_us/';
$route['case-studies'] = 'front/case_studies/';
$route['case-studies-filter'] = 'front/case_studies_filter/';
$route['case_for_support'] = 'front/case_for_support/';
$route['subscription'] = 'front/subscription_email/';
$route['sitemap'] = 'front/sitemap/';
$route['privacy_policy'] = 'front/privacy_policy/';
$route['terms_and_conditions'] = 'front/terms_and_conditions/';
$route['latest_acf'] = 'front/latest_acf/';
$route['geographics'] = 'front/geographics/';
$route['geographics/(:any)'] = 'front/geographics/$1';*/
//$route['greeting/greeting_view/(:any)'] = 'front/greeting/greeting_view/$1';

//Site route