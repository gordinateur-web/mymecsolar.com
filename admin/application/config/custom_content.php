<?php

$config=array(
	'user_table'=>'user',
	'user_table_primary'=>'user_id',
	'user_id_insession'=>'user_id',
	'role_id_insession'=>'role_id',
	'role_table'=>'role',
	'role_table_primary'=>'role_id',
	'role_table_title'=>'role_title',

	'product_fields'=>array(
		'content_type_id'=>7,
		'overview_image'=>'overview_image_7',
		'overview_description'=>'overview_description_7',
		'features_description'=>'features_description_7',
		'download'=>'download_7',
		'specifications_description'=>'specifications_description_7',
		'related_image'=>'related_image_7',
	),

	'blog_fields'=>array(
		'content_type_id'=>6,
		'image'=>'image_6',
		'date'=>'date_6',
		'facebook_link'=>'facebook_link__6',
		'linkedin_link'=>'linkedin_link_6',
		'twitter_link'=>'twitter_link_6',
		'description'=>'description_6',
	),

	'career_fields'=>array(
		'content_type_id'=>2,
		'description'=>'description_2'
	),

	'news_media_fields'=>array(
		'content_type_id'=>1,
		'image'=>'image_1',
		'date'=>'date_1',
		'description'=>'description_1',
	),

	'content_type_id_map'=>array(
		'7'=>'product_fields',
		'6'=>'blog_fields',
		'2'=>'career_fields',
		'1'=>'news_media_fields'
	)
);