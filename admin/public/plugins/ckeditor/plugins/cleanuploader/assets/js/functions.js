/*!
 * jQuery Upload File Plugin
 * version: 4.0.10
 * @requires jQuery v1.5 or later & form plugin
 * Copyright (c) 2013 Ravishanker Kusuma
 * http://hayageek.com/
 */
 
/*!
 * CleanUploader CKEditor plugin
 * version: 1.0
 * @requires Jquery Latest version, Bootstrap Latest version, MySql, PHP capable server.
 * Copyright (c) 2016 Anders Larsson
 * Website
 */

$("#uploadButtonContainer").hide();

//current selected number of files
var numberOfFilesSelected = 0;

function addNewImage(path,image,size,extension,id) {
	$("#list").prepend($('<div class="grid" id="'+id+'"><img width="150" src="'+path+'" onClick="useImage(this.src);" id="addImg" data-toggle="tooltip" data-placement="right" data-original-title="Add image"/><p>'+image+'</p><button type="button" class="btn btn-primary" onClick="deleteImage(\''+id+'\');"><i class="glyphicon glyphicon-trash"> </i> Delete</button>').hide().fadeIn(500));
};

function deleteImage(id) {
	swal({
		title: "Are you sure to delete image?",
		type: "warning",
		showCancelButton: true,
		confirmButtonColor: "#DD6B55",
		confirmButtonText: "Yes, delete it!",
		closeOnConfirm: true
	}, function () {
		$.post("goupload/delete", { id: id})
		.done(function( data ) {
			data=JSON.parse(data);
			if(data.status=='1') {
				$("#"+id).remove();
				swal("Deleted!", data.message, "success");
			}
			else {
				swal("Error!", data.message, "error");	
			}
		});
	});
};

function controlOfFileExtension(files){
	var fileExtensions = "jpg,jpeg,png,gif,pdf,doc,docx,xls,xlsx,csv,ppt,pptx,txt,mp4,avi,wms,WebM,Ogv".toLowerCase().split(",");
	var filesLength = files.length;
	for (var i = 0; i < filesLength; i++) {
		var fileName = files[i].name;
		var ext = fileName.split('.').pop().toLowerCase();
		if(jQuery.inArray(ext, fileExtensions) >= 0) {
			numberOfFilesSelected++;
		}
	}
	if(numberOfFilesSelected > 0){
		$("#uploadButtonContainer").show();
	}
};

function checkNumberOfFiles(){
	if(numberOfFilesSelected <= 0){
		$("#uploadButtonContainer").hide();
	}	
};
function startUpload() {
	downloadObj.startUpload();
};
function useImage(src) {
	newsrc = src.replace('thumb/','');
	function getUrlParam( paramName ) {
		var reParam = new RegExp( '(?:[\?&]|&)' + paramName + '=([^&]+)', 'i' ) ;
		var match = window.location.search.match(reParam) ;
		return ( match && match.length > 1 ) ? match[ 1 ] : null ;
	}
	var funcNum = getUrlParam( 'CKEditorFuncNum' );
	var imgSrc = newsrc;
	var funcNum = getUrlParam( 'CKEditorFuncNum' );
	var fileUrl = src;
	window.opener.CKEDITOR.tools.callFunction( funcNum, fileUrl );
	window.close();
};
function getUrlParam( paramName ) {
	var reParam = new RegExp( '(?:[\?&]|&)' + paramName + '=([^&]+)', 'i' ) ;
	var match = window.location.search.match(reParam) ;
	return ( match && match.length > 1 ) ? match[ 1 ] : null ;
}
//Get config data, Not used
/*function getConfigData(type) {
$.post( "config/config.php", { requestType: type })
.done(function( data ) {
return JSON.parse(data);
});
};*/
//Login Modal
$(window).load(function(){
	$('#loginModal').modal('show');
});
// Functions for Install
$(function () {
	$('[data-toggle="tooltip"]').tooltip()
});
function checkPassword() {
	if($('#admin_password').val() != $('#admin_password_again').val()){
		$("#admin_password_again").attr('class', 'form-control notEqualTextfield');
		$("#admin_password").attr('class', 'form-control notEqualTextfield');
	} else {
		$("#admin_password_again").attr('class', 'form-control equalTextField');
		$("#admin_password").attr('class', 'form-control equalTextField');
	}
}