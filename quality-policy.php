<?php require_once("config.php"); ?>
<!DOCTYPE html>
<html lang="en">
<head>
   <?php include("head.php"); ?>
 <title>Quality Policy | Mymecsolar</title>
  <meta  name="keywords" content="Pump & Motor, Controller , Remote Monitoring System">
  <meta  name="description" content="MEC has pioneered, innovated and excelled in the engineering and manufacturing of solar powered water pumping. We design, develop and manufacture the widest range of solar pumps for any company. Mechanical, electronic and software design is all in-house with a specialized team that have been working in solar pumping for 10 years.">
</head>
<body>
   <?php include("header.php"); ?>
   <section class="breadcum">
      <img src="images/privacy-policy.jpg" class="img-fluid w-100 lazy-image" alt="privacy-policy-breadcum">
      <div class="container breadcum_container">
         <h1 class="white f_light">Quality Policy</h1>
         <a href="index.php" class="white f_light" title="Home">Home</a>
       <span class="icon-1256495 white"></span>
        <span class="white f_medium">Quality Policy</span>
      </div>        
   </section>
  <section class="common-pages">
    <div class="container">
       <img src="images/quality-policy.png" class="img-fluid quality" alt="quality-policy" data-aos="zoom-in" data-aos-delay="100">
    </div>
  </section>
 <?php include("footer.php"); ?>
</body>
</html>