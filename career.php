<?php 
    require_once("config.php"); 
    
  $form=new Form();
  $form_model=array();

  $old_data=$CI->session->flashdata('old_data',$_POST);

  if(!empty($old_data)) {
    $form_model=$old_data;  
  }  

  $career_fields=$CI->config->item('career_fields');
    
  $filter_career_media['content.content_type_id']=$career_fields['content_type_id'];
  $career_list=$CI->content_model->get_published_content($filter_career_media);
?>

<?php require_once("config.php"); ?>
<!DOCTYPE html>
<html lang="en">
<head>
   <?php include("head.php"); ?>
 <title>Career | Mymecsolar</title>
  <meta  name="keywords" content="Pump & Motor, Controller , Remote Monitoring System">
  <meta  name="description" content="MEC has pioneered, innovated and excelled in the engineering and manufacturing of solar powered water pumping. We design, develop and manufacture the widest range of solar pumps for any company. Mechanical, electronic and software design is all in-house with a specialized team that have been working in solar pumping for 10 years.">
</head>
<body>
   <?php include("header.php"); ?>
   <section class="breadcum">
      <img src="images/about.jpg" class="img-fluid w-100" alt="about-breadcum">
      <div class="container breadcum_container">
         <h1 class="white f_light">Career</h1>
         <a href="index.php" class="white f_light" title="Home">Home</a>
       <span class="icon-1256495 white"></span>
        <span class="white f_medium">Career</span>
      </div>        
   </section>
   <section class="openings">
    <div class="container">
      <h1 class="text-center f_light heading">Current Openings</h1>
      <div class="row">

      <?php foreach ($career_list as $key => $career_row) { ?>
        <div class="col-md-6">
          <div class="career-wrapp">
            <h1 class="f_medium"><?php echo $career_row['content']['content_title']; ?></h1>
            <p class="f_book"><?php echo $career_row[$career_fields['description']][0]['content_value']; ?> </p>
            <a href="#career-form" class="common-btn f_medium">Apply Now <span class="icon-go-back-left-arrow"></span></a>
          </div>
        </div>
      <?php } ?>

      </div>
    </div>
  </section>
  <section class="creer-form" id="career-form">
    <div class="container">
      <div class="form-wrapp">
        <h1 class="f_light text-center heading">Join Us</h1>
       <!--  <form action="form_send.php"  class="contact-validation form_label_stly" method="post" enctype="multipart/form-data">
          <input type="hidden" name="token" value="<?php //echo $token; ?>"> -->
          <?php 
          echo $CI->form->form_model($form_model,BASE_URL_ADMIN.'custom_content/content/save_content_data',array('name'=>'contact_us_mymecsolar','class'=>'form-validation contact-validation form_label_stly', 'id'=>'form-captch', 'method'=>'post','enctype'=>'multipart/form-data'));
          ?>
          <input type="hidden" name="content_type_section_id" value=""/>
          <input type="hidden" name="section_content_parent" value=""/>
          <input type="hidden" name="content_type_id" value="3"/>
          <div class="row">
            <div class="col-lg-6 col-md-6">
              <div class="form-group">
                <label class="hide_label">Name</label>
                <input type="text" name="name_3[]" class="form-control" data-validation="required">
              </div>
            </div>
            <div class="col-lg-6 col-md-6">
              <div class="form-group phone">
                <label class="hide_label">Contact</label>
                 <input type="text" name="phone_3[]" class="form-control number-field"data-validation="required" maxlength="10">
              </div>
            </div>
            <div class="col-lg-6 col-md-6">
              <div class="form-group email">
                <label class="hide_label">Email</label>
                <input type="email" name="email_3[]" class="form-control" data-validation="required">
              </div>
            </div>
            <div class="col-lg-6 col-md-6">
              <div class="form-group email">
                <label >Choose File</label>
                <input type="file" name="file_3[]" class="form-control" data-validation="required">
              </div>
            </div>

            <div class="col-lg-6 col-md-6">
              <div class="form-group">
                <label class="hide_label"> Message</label>
                <textarea type="text" name="message_3[]" class="form-control" rows="3" data-validation="required"></textarea>
              </div>
            </div>

            <div class="col-lg-6 col-md-6 col-sm-7 col-12">
              <div class="captcha">
                <div class="g-recaptcha" data-sitekey="<?php echo RECAPTCHA_KEY; ?>"></div>  
              </div>
            </div>
            <div class="col-lg-6 col-md-6 col-sm-5 col-12">
              <button type="submit" class="contact-btn common-btn f_medium">Submit <span class="icon-go-back-left-arrow"></span></button>
            </div>
          </div>
        </form> 
      </div>
    </div>
  </section>

  <?php include("footer.php"); ?>
    <?php include("show_msg.php"); ?>
    <script src="https://www.google.com/recaptcha/api.js"></script>
    <script type="text/javascript" src="js/jquery.form-validator.min.js"></script>

    <script type="text/javascript">
      $(document).ready(function(){
//on hash click smooth scroll
$(".career-wrapp a").on('click', function(event) {
  if (this.hash !== "") {
    event.preventDefault();
    var hash = this.hash;
    $('html, body').animate({ scrollTop: $(hash).offset().top - ($('header').height() + 10)
  }, 800); 
  }  
});
        $.validate({
          form: ".contact-validation",
        });

        $(document).on("focus",".form_label_stly .form-control", function(){
          $(this).parent().find('.hide_label').css("top", "-12px");
          $(this).parent().find('.hide_label').css("font-size", "13px");
        });

        $(document).on("blur",".form_label_stly .form-control",function(){
          $(this).parent().find('.hide_label').css("top", "-12px");
          $(this).parent().find('.hide_label').css("font-size", "13px");
        });

        $(".form_label_stly .form-control").blur(function() {
          if($(this).val() =='' && !$(this).is("select")) {
            $(this).parent().find('.hide_label').css("top", "5px");
            $(this).parent().find('.hide_label').css("font-size", "16px");
          }
        });   
      });

      $('#form-captch').on('submit', function(e) {
        if(grecaptcha.getResponse() == "") {
          e.preventDefault();
          alert("Please check the captcha");
        } 
});

    </script>

</body>
</html>