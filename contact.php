<?php 
    require_once("config.php");

    $form=new Form();
    $form_model=array();

    $old_data=$CI->session->flashdata('old_data',$_POST);

    if(!empty($old_data)) {
        $form_model=$old_data;  
    }  
?>
    <!DOCTYPE html>
    <html lang="en">

    <head>
        <?php include("head.php"); ?>
        <title>Contact | Mymecsolar </title>
       <meta  name="keywords" content="Pump & Motor, Controller , Remote Monitoring System">
  <meta  name="description" content="MEC has pioneered, innovated and excelled in the engineering and manufacturing of solar powered water pumping. We design, develop and manufacture the widest range of solar pumps for any company. Mechanical, electronic and software design is all in-house with a specialized team that have been working in solar pumping for 10 years.">
    </head>
    <body>
    <?php include("header.php"); ?>
        <div class="contact-main">
            <section class="breadcum">
                <img src="images/contact-breadcum.jpg" class="img-fluid w-100" alt="about-breadcum">
                <div class="container breadcum_container">
                    <h1 class="white f_light">Let's have a talk</h1>
                    <a href="index.php" class="white f_light" title="Home">Home</a>
                    <span class="icon-1256495 white"></span>
                    <span class="white f_medium"> Contact</span>
                </div>        
            </section>

            <section class="contact-form">
                <div class="container">
                    <h1 class="blue f_light text-center heading">Get in Touch</h1>
                    <p class="f_light gray text-center para">If you have any questions about the services we provide simply use the form below</p>
                    <div class="form-div" >

                       <!--  <form action="form_send.php"  class="contact-validation form_label_stly" method="post">
                            <input type="hidden" name="token" value="<?php //echo $token; ?>"> -->
                            <?php 
                            echo $CI->form->form_model($form_model,BASE_URL_ADMIN.'custom_content/content/save_content_data',array('name'=>'contact_us_mymecsolar','class'=>'form-validation contact-validation form_label_stly', 'id'=>'form-captch', 'method'=>'post','enctype'=>'multipart/form-data'));
                            ?>
                            <input type="hidden" name="content_type_section_id" value=""/>
                            <input type="hidden" name="section_content_parent" value=""/>
                            <input type="hidden" name="content_type_id" value="5"/>
                            <div class="row">
                                <div class="col-lg-6 col-md-6">
                                    <div class="form-group">
                                        <label class="hide_label">Name</label>
                                        <input type="text" name="name_5[]" class="form-control char-field" data-validation="required">
                                    </div>
                                </div>
                                <div class="col-lg-6 col-md-6">
                                    <div class="form-group email">
                                        <label class="hide_label">Email</label>
                                        <input type="email" name="email_5[]" class="form-control" data-validation="required">
                                    </div>
                                </div>
                                <div class="col-lg-6 col-md-6">
                                    <div class="form-group phone ">
                                        <label class="hide_label">Contact</label>
                                        <input type="text" name="phone_5[]" class="form-control number-field"data-validation="required" maxlength="10">
                                    </div>
                                </div>

                                <div class="col-lg-6 col-md-6">
                                    <div class="form-group">
                                        <label class="hide_label"> Message</label>
                                        <textarea type="text" name="message_5[]" class="form-control" rows="3" data-validation="required"></textarea>
                                    </div>
                                </div>

                                <div class="col-lg-6 col-md-6 col-sm-7 col-12">
                                    <div class="captcha">
                                        <div class="g-recaptcha" data-sitekey="<?php echo RECAPTCHA_KEY; ?>"></div></div>
                                </div>
                                <div class="col-lg-6 col-md-6 col-sm-5 col-12">
                                    <button type="submit" class="contact-btn common-btn f_medium">Submit <span class="icon-go-back-left-arrow"></span></button>
                                </div>
                            </div>
                        </form> 
                    </div> 
                </div> 
            </section>
            <section class="customer-support">
                <div class="container">
                    <h1 class="heading white f_light text-center">Customer Support</h1>
                    <div class="support-wrapp">
                        <div class="support-single">
                             <span class="icon-placeholder"></span>
                            <h1 class="f_bold">Find Us</h1>
                            <h2 class="f_medium">Mercury Electronic Corporation</h2>
                            <p class="f_light">Survey No. 91/1, Kanakadasa Layout, Seegehalli, Magadi Road, Bangalore-560091</p>
                        </div>
                        <div class="support-single">
                             <span class="icon-placeholder"></span>
                            <h1 class="f_bold">Find Us</h1>
                            <h2 class="f_medium">Mecwin Technologies India Pvt.Ltd.</h2>
                            <p class="f_light">Survey No.91/1,Kanakadasa Layout, Seegehalli, Magadi road, Bangalore 560091.</p>
                        </div>
                        <div class="support-single">
                            <span class="icon-phone-call1"></span>
                            <h1 class="f_bold">Call Us</h1>
                            <span class="call-us">
                            <a href="tel:+919731760263" title="Call">+919731760263</a>
                        </span>
                        </div>
                        <div class="support-single">
                            <span class="icon-213381 email"></span>
                            <h1 class="f_bold">Email Us</h1>
                           <a href="mailto:rajeev@mymecsolar.com" title="Email">rajeev@mymecsolar.com </a>
                        </div>
                    </div>
                    <!-- Stay Connected -->
                    <div class="stay-connected">
                        <span class="f_bold connect">Stay Connected</span>
                        <a href="https://www.facebook.com/mymecindia" target="_blank"><span class="icon-facebook-logo-outline"></span></a>
                        <a href="https://www.linkedin.com/in/mercury-electronic-corporation-b76280176" target="_blank"><span class="icon-linkedin-social-outline-logotype"></span></a>
                      
                    </div>
                </div>
            </section>
            <section class="channel-partner">
                <h1 class="heading blue text-center f_light">Our Channel Partners Across Globe</h1>
                <div class="container">
                <img src="images/channel-partner.png" alt="Our Channel Partners Across Globe" class="img-fluid">
                </div>
            </section>
            <div class="map-div">
       <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d248837.60401109204!2d77.26098485881101!3d12.96624855424915!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x3bae3a187e201d51%3A0xd0c1cef20efc5f86!2sMercury%20Electronic%20Corporation!5e0!3m2!1sen!2sin!4v1595386018980!5m2!1sen!2sin" width="100%" height="450" frameborder="0" style="border:0;" allowfullscreen="" aria-hidden="false" tabindex="0"></iframe>
            </div>
        </div>

    <?php include("footer.php"); ?>
    <?php include("show_msg.php"); ?>
    <script src="https://www.google.com/recaptcha/api.js"></script>
    <script type="text/javascript" src="js/jquery.form-validator.min.js"></script>

    <script type="text/javascript">
        $(document).ready(function(){

            $.validate({
                form: ".contact-validation",
            });

            $(document).on("focus",".form_label_stly .form-control", function(){
                $(this).parent().find('.hide_label').css("top", "-12px");
                $(this).parent().find('.hide_label').css("font-size", "13px");
            });

            $(document).on("blur",".form_label_stly .form-control",function(){
                $(this).parent().find('.hide_label').css("top", "-12px");
                $(this).parent().find('.hide_label').css("font-size", "13px");
            });

            $(".form_label_stly .form-control").blur(function() {
                if($(this).val() =='' && !$(this).is("select")) {
                    $(this).parent().find('.hide_label').css("top", "5px");
                    $(this).parent().find('.hide_label').css("font-size", "16px");
                }
            });   
        });

        $('#form-captch').on('submit', function(e) {
            if(grecaptcha.getResponse() == "") {
                e.preventDefault();
                alert("Please check the captcha");
            } 

        });
    </script>
    </body>
    </html>