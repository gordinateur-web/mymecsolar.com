<?php require_once("config.php"); ?>
<!DOCTYPE html>
<html lang="en">
<head>
   <?php include("head.php"); ?>
 <title>Clients | Mymecsolar</title>
 <meta  name="keywords" content="Pump & Motor, Controller , Remote Monitoring System">
  <meta  name="description" content="MEC has pioneered, innovated and excelled in the engineering and manufacturing of solar powered water pumping. We design, develop and manufacture the widest range of solar pumps for any company. Mechanical, electronic and software design is all in-house with a specialized team that have been working in solar pumping for 10 years.">
</head>
<body>
   <?php include("header.php"); ?>
   <section class="breadcum">
      <img src="images/clients.jpg" class="img-fluid w-100" alt="about-breadcum">
      <div class="container breadcum_container">
         <h1 class="white f_light">Our Esteemed Clients</h1>
         <a href="index.php" class="white f_light" title="Home">Home</a>
       <span class="icon-1256495 white"></span>
        <span class="white f_medium">Clients</span>
      </div>        
   </section>
 
   <section class="client-main">
    <h2 class="heading text-center blue f_light" data-aos="fade-up" data-aos-delay="100">Here are just some of the clients we work with..</h1>
      
      <div class="container">
        <div class="client-wrapper">
          <div class="client-single boxes" data-aos="fade-up" data-aos-delay="100">
            <div class="img-wrapp">
              <img class="lazy-image" src="images/client-logo1.png" alt="maxijit">
            </div>
            <div class="overlay">
              <h1 class="f_book white">Rainbow Group is a system integrator of complete solar system required for Irrigation</h1>
            </div>
          </div>
          <div class="client-single boxes" data-aos="fade-up" data-aos-delay="200">
            <div class="img-wrapp">
              <img class="lazy-image" src="images/client-logo2.png" alt="smith">
            </div>
            <div class="overlay">
              <h1 class="f_book white">Gautam Solar is a system integrator of complete solar system required for Irrigation</h1>
            </div>
          </div>
          <div class="client-single boxes" data-aos="fade-up" data-aos-delay="300">
            <div class="img-wrapp">
              <img class="lazy-image" src="images/client-logo3.png" alt="mixergi">
            </div>
            <div class="overlay">
              <h1 class="f_book white">Mahindra Susten working to create sustainable eco system.</h1>
            </div>
          </div>
              <div class="client-single boxes" data-aos="fade-up" data-aos-delay="300">
            <div class="img-wrapp">
              <p class="f-book">Tulsyan Enterprises</p>
            </div>
            <div class="overlay">
              <h1 class="f_book white">Tulsyan Enterprises is a system integrator of complete solar system required for Irrigation</h1>
            </div>
          </div>
              <div class="client-single boxes" data-aos="fade-up" data-aos-delay="300">
            <div class="img-wrapp">
              <p class="f-book">Navikrit</p>

            </div>
            <div class="overlay">
              <h1 class="f_book white">Navikrit is a system integrator of system required for Irrigation</h1>
            </div>
          </div>
              <div class="client-single boxes" data-aos="fade-up" data-aos-delay="300">
            <div class="img-wrapp">
              <p class="f-book">Solar Aqua Solutions</p>
            </div>
            <div class="overlay">
              <h1 class="f_book white">Solar Aqua Solutions is a system integrator of system required for Irrigation</h1>
            </div>
          </div>
        </div>
      </div>
    </section>


 <?php include("footer.php"); ?>
 </body>
</html>