<?php require_once("config.php"); ?>
<!DOCTYPE html>
<html lang="en">
<head>
   <?php include("head.php"); ?>
 <title>FAQ's | Mymecsolar</title>
  <meta  name="keywords" content="Pump & Motor, Controller , Remote Monitoring System">
  <meta  name="description" content="MEC has pioneered, innovated and excelled in the engineering and manufacturing of solar powered water pumping. We design, develop and manufacture the widest range of solar pumps for any company. Mechanical, electronic and software design is all in-house with a specialized team that have been working in solar pumping for 10 years.">
</head>
<body>
   <?php include("header.php"); ?>
   <section class="breadcum">
      <img src="images/faq-breadcum.jpg" class="img-fluid w-100" alt="about-breadcum">
      <div class="container breadcum_container">
         <h1 class="white f_light">Frequently Asked Questions</h1>
         <a href="index.php" class="white f_light" title="Home">Home</a>
       <span class="icon-1256495 white"></span>
        <span class="white f_medium">FAQ's</span>
      </div>        
   </section>
 
   <section class="faq-main">
    <div class="container">
      <!-- <ul class="nav nav-tabs" id="myTab" role="tablist">
        <li class="nav-item">
          <a class="nav-link active f_book" id="home-tab" data-toggle="tab" href="#home" role="tab" aria-controls="home" aria-selected="true">Download <br> Manual</a>
        </li>
        <li class="nav-item">
          <a class="nav-link f_book" id="profile-tab" data-toggle="tab" href="#profile" role="tab" aria-controls="profile" aria-selected="false">Download <br> Manual</a>
        </li>
      </ul> -->
      <div class="tab-content" id="myTabContent">
        <!-- faq 1 -->
        <div class="tab-pane fade show active" id="home" role="tabpanel" aria-labelledby="home-tab">
          <div class="row">
            <div class="col-lg-4 col-md-5">
              <div class="img-wrapp">
                <img src="images/faq.jpg" alt="FAQ" class="img-fluid">
              </div>
            </div>
            <div class="col-lg-8 col-md-7">
              <div class="faq-wrapp">
                <div class="accordion" id="accordion-faq">
                  <div class="card">
                    <div class="card-header" id="faq_1">
                      <h2 class="mb-0">
                        <button class="btn btn-link f_medium" type="button" data-toggle="collapse" data-target="#faq-1" aria-expanded="true" aria-controls="faq-1">Why DC Motor are suitable for solar pumping application?<span class="icon-1256495"></span></button>
                      </h2>
                    </div>

                    <div id="faq-1" class="collapse show" aria-labelledby="faq_1" data-parent="#accordion-faq">
                      <div class="card-body f_book">
                        BLDC motor for pumping system technique along with solar PV source, both combination increases its utilization and reliability.
                      </div>
                    </div>
                  </div>
                  <div class="card">
                    <div class="card-header" id="faq_2">
                      <h2 class="mb-0">
                        <button class="btn btn-link collapsed f_medium" type="button" data-toggle="collapse" data-target="#faq-2" aria-expanded="false" aria-controls="faq-2"> How does a solar water pump work? <span class="icon-1256495"></span></button>
                      </h2>
                    </div>
                    <div id="faq-2" class="collapse" aria-labelledby="faq_2" data-parent="#accordion-faq">
                      <div class="card-body f_book">
                        The system operates on power generated using solar PV (photovoltaic) system. The photovoltaic array converts the solar energy into electricity, which is used for running the motor pump set. The pumping system draws water from the open well, bore well, stream, pond, canal etc..
                      </div>
                    </div>
                  </div>
                  <div class="card">
                    <div class="card-header" id="faq_3">
                      <h2 class="mb-0">
                        <button class="btn btn-link collapsed f_medium" type="button" data-toggle="collapse" data-target="#faq-3" aria-expanded="false" aria-controls="faq-3"> Why to choose MEC?
                          <span class="icon-1256495"></span></button>
                      </h2>
                    </div>
                    <div id="faq-3" class="collapse" aria-labelledby="faq_3" data-parent="#accordion-faq">
                      <div class="card-body f_book">
                       MEC is having a sophisticated team and each products quality inspected for requirements and approved.
                      </div>
                    </div>
                  </div>
                  <div class="card">
                    <div class="card-header" id="faq_4">
                      <h2 class="mb-0">
                        <button class="btn btn-link collapsed f_medium" type="button" data-toggle="collapse" data-target="#faq-4" aria-expanded="false" aria-controls="faq-4"> Why to choose MEC Pumps and Controllers? <span class="icon-1256495"></span></button>
                      </h2>
                    </div>
                    <div id="faq-4" class="collapse" aria-labelledby="faq_4" data-parent="#accordion-faq">
                      <div class="card-body f_book">
                        MEC Pump and Controllers are designed and manufactured which meets required performance per MNRE Standards.
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
        <!-- faq 2 -->
        <div class="tab-pane fade" id="profile" role="tabpanel" aria-labelledby="profile-tab">
          <div class="row">
            <div class="col-lg-4 col-md-5">
              <div class="img-wrapp">
                <img src="images/faq.jpg" alt="FAQ" class="img-fluid">
              </div>
            </div>
            <div class="col-lg-8 col-md-7">
              <div class="faq-wrapp">
                <div class="accordion" id="accordion-faq2">
                  <div class="card">
                    <div class="card-header" id="faqnew_1">
                      <h2 class="mb-0">
                        <button class="btn btn-link f_medium" type="button" data-toggle="collapse" data-target="#faqnew-1" aria-expanded="true" aria-controls="faqnew-1">Consectetur adipisicing elit? <span class="icon-1256495"></span></button>
                      </h2>
                    </div>

                    <div id="faqnew-1" class="collapse show" aria-labelledby="faqnew_1" data-parent="#accordion-faq2">
                      <div class="card-body f_book">
                        Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod
                        tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam,
                        quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea 
                      </div>
                    </div>
                  </div>
                  <div class="card">
                    <div class="card-header" id="faqnew_2">
                      <h2 class="mb-0">
                        <button class="btn btn-link collapsed f_medium" type="button" data-toggle="collapse" data-target="#faqnew-2" aria-expanded="false" aria-controls="faqnew-2"> Lorem ipsum dolor sit amet, consectetur? <span class="icon-1256495"></span></button>
                      </h2>
                    </div>
                    <div id="faqnew-2" class="collapse" aria-labelledby="faqnew_2" data-parent="#accordion-faq2">
                      <div class="card-body f_book">
                        Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod
                        tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam,
                        quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo
                      </div>
                    </div>
                  </div>
                  <div class="card">
                    <div class="card-header" id="faqnew_3">
                      <h2 class="mb-0">
                        <button class="btn btn-link collapsed f_medium" type="button" data-toggle="collapse" data-target="#faqnew-3" aria-expanded="false" aria-controls="faqnew-3"> Lorem ipsum dolor sit amet, consectetur? <span class="icon-1256495"></span></button>
                      </h2>
                    </div>
                    <div id="faqnew-3" class="collapse" aria-labelledby="faqnew_3" data-parent="#accordion-faq2">
                      <div class="card-body f_book">
                        Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod
                        tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam,
                        quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo
                      </div>
                    </div>
                  </div>
                  <div class="card">
                    <div class="card-header" id="faqnew_4">
                      <h2 class="mb-0">
                        <button class="btn btn-link collapsed f_medium" type="button" data-toggle="collapse" data-target="#faqnew-4" aria-expanded="false" aria-controls="faqnew-4"> Lorem ipsum dolor sit amet, consectetur? <span class="icon-1256495"></span></button>
                      </h2>
                    </div>
                    <div id="faqnew-4" class="collapse" aria-labelledby="faqnew_4" data-parent="#accordion-faq2">
                      <div class="card-body f_book">
                        Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod
                        tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam,
                        quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo
                      </div>
                    </div>
                  </div>
                  <div class="card">
                    <div class="card-header" id="faqnew_5">
                      <h2 class="mb-0">
                        <button class="btn btn-link collapsed f_medium" type="button" data-toggle="collapse" data-target="#faqnew-5" aria-expanded="false" aria-controls="faqnew-5"> Lorem ipsum dolor sit amet, consectetur? <span class="icon-1256495"></span></button>
                      </h2>
                    </div>
                    <div id="faqnew-5" class="collapse" aria-labelledby="faqnew_5" data-parent="#accordion-faq2">
                      <div class="card-body f_book">
                        Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod
                        tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam,
                        quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo
                      </div>
                    </div>
                  </div>
                  <div class="card">
                    <div class="card-header" id="faqnew_6">
                      <h2 class="mb-0">
                        <button class="btn btn-link collapsed f_medium" type="button" data-toggle="collapse" data-target="#faqnew-6" aria-expanded="false" aria-controls="faqnew-6"> Lorem ipsum dolor sit amet, consectetur? <span class="icon-1256495"></span></button>
                      </h2>
                    </div>
                    <div id="faqnew-6" class="collapse" aria-labelledby="faqnew_6" data-parent="#accordion-faq2">
                      <div class="card-body f_book">
                        Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod
                        tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam,
                        quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </section>
     <?php include("footer.php"); ?>

</body>
</html>