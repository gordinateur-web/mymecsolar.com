<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1">
<link rel="icon" type="image/x-icon" href="images/favicon.ico">
<link href="https://fonts.googleapis.com/css?family=Open+Sans:300,400,600,700,800" rel="stylesheet">
<script src="https://cdn.polyfill.io/v2/polyfill.js?features=IntersectionObserver"></script>

<?php
	echo generate_style_link([
		BASE_URL.'./css/bootstrap.min.css',
		BASE_URL.'./css/font-awesome-new.css',
		BASE_URL.'./css/animate.min.css',
		BASE_URL.'./css/aos.css',
		BASE_URL.'./css/svg-icon.css',
		BASE_URL.'./css/bootstrap-custom.css',
		BASE_URL.'./css/media.css',
		BASE_URL.'./css/owl.theme.default.min.css',
		BASE_URL.'./css/owl.carousel.min.css',
	]);
?>
<!--[if lt IE 9]>
  <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
  <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
<![endif]-->

<!-- Global site tag (gtag.js) - Google Analytics -->
<script async src="https://www.googletagmanager.com/gtag/js?id=UA-173441529-1"></script>
<script>
  window.dataLayer = window.dataLayer || [];
  function gtag(){dataLayer.push(arguments);}
  gtag('js', new Date());

  gtag('config', 'UA-173441529-1');
</script>    