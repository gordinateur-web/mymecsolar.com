<?php require_once("config.php"); ?>
<!DOCTYPE html>
<html lang="en">
<head>
   <?php include("head.php"); ?>
 <title>Terms and Conditions | Mymecsolar</title>
  <meta  name="keywords" content="Pump & Motor, Controller , Remote Monitoring System">
  <meta  name="description" content="MEC has pioneered, innovated and excelled in the engineering and manufacturing of solar powered water pumping. We design, develop and manufacture the widest range of solar pumps for any company. Mechanical, electronic and software design is all in-house with a specialized team that have been working in solar pumping for 10 years.">
</head>
<body>
   <?php include("header.php"); ?>
   <section class="breadcum">
      <img src="images/terms-condition.jpg" class="img-fluid w-100" alt="terms-condition-breadcum">
      <div class="container breadcum_container">
         <h1 class="white f_light">Terms and Conditions</h1>
         <a href="index.php" class="white f_light" title="Home">Home</a>
       <span class="icon-1256495 white"></span>
        <span class="white f_medium">Terms and Conditions</span>
      </div>        
   </section>
  <section class="common-pages" data-aos="fade-up" data-aos-delay="100">
            <div class="container">
                
                <p class="f-light">This web site contains material including text, photographs and other images and sound, which is protected by copyright and/or other intellectual property rights. All copyright and other intellectual property rights in this material are either owned by mymecsolar or have been licensed to it by the owner(s) of those rights so that it can use this material as part of this web site. This web site also contains trade marks, including mymecsolar logo. All trade marks included on this web site belong to mymecsolar or have been licensed to it by the owner(s) of those trade marks for use on this web site.</p>
                 <h1 class="f-book">You may : </h1>
                <p class="f-light">Access any part of the web site; Print off one copy of any or all of the pages for your own personal reference.</p>
                 <h1 class="f-book">You may not :</h1>
                <p class="f-light">Copy (whether by printing off onto paper, storing on disk, downloading or in any other way), distribute (including distributing copies), broadcast, alter or tamper with in any way or otherwise use any material contained in the web site except as set out under "you may". These restrictions apply in relation to all or part of the material on the web site; Remove any copyright, trade mark or other intellectual property notices contained in the original material from any material copied or printed off from the web site; Link to this web site.</p>
                <h1 class="f-book">Without our express written consent :</h1>
                <p class="f-light">If you wish to provide a hypertext or other link to this web site, please email the webmaster with details of: The url(s) of the web page(s) from which you are proposing to link to this web site. The url(s) of the web page(s) on this web site to which you are proposing to link And we will consider your request. It is our decision as to whether we agree to your request and we do not have to do so.</p>
                <h1 class="f-book">Changes to terms and conditions :</h1>
                <p class="f-light">mymecsolar may change the terms and conditions and disclaimer set out above from time to time. By browsing this web site you are accepting that you are bound by the current terms and conditions and disclaimer and so you should check these each time you revisit the site.</p>
                <h1 class="f-book">Changes to/operation of web site :</h1>
                <p class="f-light">Mymecsolar may change the format and content of this web site at any time. Mymecsolar may suspend the operation of this web site for support or maintenance work, in order to update the content or for any other reason. Mymecsolar reserves the right to terminate access to this web site at any time and without notice.</p>
                 <h1 class="f-book">Data protection :</h1>
                <p class="f-light">Personal details provided to mymecsolar through this web site will only be used in accordance with our privacy policy. Please read this carefully before going on. By providing your personal details to us you are consenting to its use in accordance with our privacy policy.</p>
            </div>
        </section>
 <?php include("footer.php"); ?>
</body>
</html>