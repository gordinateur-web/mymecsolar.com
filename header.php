<?php require_once("config.php"); ?>

<header class="animated">
	<div class="nav-head">
		<div class="container">				
			<nav class="navbar navbar-expand-md ">
				<a class="logo-wrapp" href="<?php echo BASE_URL;?>"><img src="<?php echo BASE_URL.'images/header-logo.png';?>" class="img-fluid" alt="logo"></a>
				<div class="search-box d-block d-md-none">
					<a href="" data-toggle="modal" data-target=".search-modal"  class="search-btn"><span class="icon-1379714"></span></a>
				</div>
				<button class="navbar-toggler" type="button"  id="mobile-menu-action" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
					<span class="icon-bar one"></span>
					<span class="icon-bar two"></span>
					<span class="icon-bar three"></span>
				</button>
				<div class=" navbar-mob" id="mobile-navbar">
					<ul class=" navbar-nav" id="navbar">
						<li class="nav-item">
							<a class="nav-link f_medium" href="<?php echo BASE_URL.'index.php';?>">Home</a>
						</li>
						<li class="nav-item">
							<a class="nav-link f_medium" href="<?php echo BASE_URL.'about.php';?>">About</a>
						</li>
						<li class="nav-item dropdown">
							<a class="nav-link dropdown-toggle" href="<?php echo BASE_URL.'remote_monitoring_system.php';?>" id="product-dropdown" data-toggle="dropdown" role="button">Product</a>
							<ul class="dropdown-menu" aria-labelledby="product-dropdown">
								<a class="dropdown-item f_medium" href="<?php echo BASE_URL.'product/pump-motor';?>">Pump & Motor</a>
								<a class="dropdown-item f_medium" href="<?php echo BASE_URL.'product/solar-pump-mppt-controller';?>">Controller</a>
								<a class="dropdown-item f_medium" href="<?php echo BASE_URL.'product/remote-monitoring-system-rms';?>">Remote Monitoring System</a>
							</ul>
						</li> 
						<li class="nav-item">
							<a class="nav-link f_medium" href="<?php echo BASE_URL.'expertise.php';?>"> Expertise </a>
						</li>
						<li class="nav-item">
							<a class="nav-link f_medium" href="<?php echo BASE_URL.'gallery.php';?>"> Gallery </a>
						</li>
						<li class="nav-item">
							<a class="nav-link f_medium" href="<?php echo BASE_URL.'contact.php';?>"> Contact </a>
						</li>
						<li class="nav-item search-box d-none d-md-block"><!-- <a href="" data-toggle="modal" data-target=".search-modal"  class="nav-link search-btn f_medium"><span class="icon-1379714"></span>Search</a> --></li>
						<li class="nav-item d-none d-lg-block"><a href="" class="nav-link f_medium" data-toggle="modal" data-target="#request-quote" id="request_header">Get A Quote<span class="icon-go-back-left-arrow"></span></a></li>
					</ul>  
				</div> 			  	   
			</nav>
		</div>
	</div>
</header>

