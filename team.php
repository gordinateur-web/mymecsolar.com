<?php require_once("config.php"); ?>
<!DOCTYPE html>
<html lang="en">
<head>
   <?php include("head.php"); ?>
 <title>Team | Mymecsolar</title>
  <meta  name="keywords" content="Pump & Motor, Controller , Remote Monitoring System">
  <meta  name="description" content="MEC has pioneered, innovated and excelled in the engineering and manufacturing of solar powered water pumping. We design, develop and manufacture the widest range of solar pumps for any company. Mechanical, electronic and software design is all in-house with a specialized team that have been working in solar pumping for 10 years.">
</head>
<body>
   <?php include("header.php"); ?>
   <section class="breadcum">
      <img src="images/team-breadcum.jpg" class="img-fluid w-100" alt="about-breadcum">
      <div class="container breadcum_container">
         <h1 class="white f_light">Meet Our Team</h1>
         <a href="index.php" class="white f_light" title="Home">Home</a>
       <span class="icon-1256495 white"></span>
        <span class="white f_medium">Team</span>
      </div>        
   </section>
   <section class="team-main">
    <h1 class="heading blue f_light text-center" data-aos="fade-up" data-aos-delay="100">Top Management is a bunch of inventive & passionate minds </h1>
    <div class="container">
      <div class="team-wrapp">
        <div class="team-single line" data-aos="fade-up" data-aos-delay="500">
            <div class="img-wrapp">
              <img src="images/testi4.png" alt="Team" class="lazy-image">
            </div>
            <div class="text-wrapp">
            <h1 class="f_medium">Mrs. Asha Rani</h1>
            <p class="f_book gray">Having a great vision to set up an industry in Electronics and Renewable energy, Result is she founded MEC in 2015 as a trading Industry in Electronics products. Later in 2016 started “Designing and manufacturing of Solar Motors” along with Dr. Shiva Kumar.</p>
          </div>
          </div>
        <div class="team-single line" data-aos="fade-up" data-aos-delay="100">
          <div class="img-wrapp">
            <img src="images/testi3.png" alt="Team">
          </div>
          <div class="text-wrapp">
          <h1 class="f_medium">Dr. Shiva kumar HM</h1>
          <p class="f_book gray">Having a vision to grow the organisation with 14+ Years of Experience in Product development like Pumps, Solar Panels, Grid and Sync Panels, Inerter and Controllers and Business set up in different Organisations. Under his Guidance Organisation is having a high growth year on year.</p>
          </div>
        </div>
          <div class="team-single line" data-aos="fade-up" data-aos-delay="300">
            <div class="img-wrapp">
              <img src="images/testi1.png" alt="Team" class="lazy-image">
            </div>
            <div class="text-wrapp">
            <h1 class="f_medium">Mr. A. N. Vishwanath</h1>
            <p class="f_book gray">Top Notch Technology Leader with a long and proven track record of successful business delivery with high performing teams and wide experience of 25+ years in emerging technologies like  Aerospace & Defence, Precision Segment, Start-up specialist, risk taker, team/ enterprise builder, consummate, result oriented and highly experienced leader with established credentials.</p>
          </div>
          </div>
          
          <div class="team-single" data-aos="fade-up" data-aos-delay="700">
             <div class="img-wrapp">
              <img src="images/testi2.png" alt="Team" class="lazy-image">
            </div>
            <div class="text-wrapp">
            <h1 class="f_medium">Dr. P Venkateshwalu</h1>
            <p class="f_book gray">Dr. P. Venkateswarlu is M. Sc. in Engineering Physics & Ph. D in Material Science from Indian Institute of Science. He has worked 23 years in ISRO in the area of Development of solar cells, Solar Modules for Satellite Applications. He is having more than 30 years’ experience with Design, production, R&D, process, quality management, of solar cell, Solar Module manufacturing & PV Power plants design installation and technology experience with ISRO, Moser Baer, Reliance Industries and different other  industries. 
			 </p>
      </div>
        </div>
      </div>
    </section>
    <section>
      <div class="container">
        <img src="images/md.png" class="img-fluid d-block m-auto">
      </div>
    </section>
<!-- Different teams for specific expertise & experience -->
<section class="expertise">
  <h1 class="f_light text-center heading" data-aos="fade-up" data-aos-delay="100">Different teams for specific expertise & experience</h1>
  <div class="container">
    <div class="expertise-wrapp">
      <div class="expertise-single" data-aos="zoom-in" data-aos-delay="100">
        <div class="icon-wrap">
          <span class="icon-jhdfjv7"></span>
        </div>
        <p><span class="blue f_medium">2</span><span class="f_book">Engineers</span></p>
        <h2 class="f_medium">Mechanical <br> Design Team</h2>
      </div>
      <div class="expertise-single" data-aos="zoom-in" data-aos-delay="200">
        <div class="icon-wrap">
         <span class="icon-mnmndf98"></span>
        </div>
        <p><span class="blue f_medium">3</span><span class="f_book">Engineers</span></p>
        <h2 class="f_medium">R&D and <br> Testing Team</h2>
      </div>
      <div class="expertise-single" data-aos="zoom-in" data-aos-delay="300">
        <div class="icon-wrap">
         <span class="icon-hghdfvdfverg6"></span>
        </div>
        <p><span class="blue f_medium">1</span><span class="f_book">Engineers</span></p>
        <h2 class="f_medium">Motor Design <br> Team</h2>
      </div>
      <div class="expertise-single" data-aos="zoom-in" data-aos-delay="400">
        <div class="icon-wrap">
          <span class="icon-hghdfvsacadfverg6"></span>
        </div>
        <p><span class="blue f_medium">3</span><span class="f_book">Engineers</span></p>
        <h2 class="f_medium">Installation and <br> Commissioning Team</h2>
      </div>
      <div class="expertise-single" data-aos="zoom-in" data-aos-delay="500">
        <div class="icon-wrap">
         <span class="icon-sjcd7"></span>
        </div>
        <p><span class="blue f_medium">3</span><span class="f_book">Engineers</span></p>
        <h2 class="f_medium">PCB & Embedded <br> Design Team</h2>
      </div>
       <div class="expertise-single" data-aos="zoom-in" data-aos-delay="600">
        <div class="icon-wrap">
         <span class="icon-jhdsdf87"></span>
        </div>
        <p><span class="blue f_medium">4</span><span class="f_book">Engineers</span></p>
        <h2 class="f_medium">Service <br> Engineers</h2>
      </div>
       <div class="expertise-single" data-aos="zoom-in" data-aos-delay="700">
        <div class="icon-wrap">
         <span class="icon-jx87"></span>
        </div>
        <p><span class="blue f_medium">1</span><span>Engineers</span></p>
        <h2 class="f_medium">GUI <br> Development</h2>
      </div>
    </div>
  </div>
</section>

 <?php include("footer.php"); ?>
 </body>
</html>