<?php
	define("EXTERNAL_CI", true);
	define("BASE_URL", "http://localhost/mymecsolar.com/");
	define("BASE_URL_ADMIN", "http://localhost/mymecsolar.com/admin/");
	define("ADMIN_PATH", "./admin/");
	define("RECAPTCHA_KEY", "6LeGetgUAAAAAEL2dGY5A5X7DOuZ6m29-MLpifyL");
	
	ob_start();
	include(ADMIN_PATH.'index.php');
	ob_end_clean();

	date_default_timezone_set("Asia/Kolkata");

    $CI->load->model('custom_content/content_model');
    $CI->load->config('custom_content');
?>
