<?php require_once("config.php"); ?>
<!DOCTYPE html>
<html lang="en">
<head>
   <?php include("head.php"); ?>
 <title>About | Mymecsolar</title>
  <meta  name="keywords" content="Pump & Motor, Controller , Remote Monitoring System">
  <meta  name="description" content="MEC has pioneered, innovated and excelled in the engineering and manufacturing of solar powered water pumping. We design, develop and manufacture the widest range of solar pumps for any company. Mechanical, electronic and software design is all in-house with a specialized team that have been working in solar pumping for 10 years.">
</head>
<body>
   <?php include("header.php"); ?>
   <section class="breadcum">
      <img src="images/about.jpg" class="img-fluid w-100 lazy-image" alt="about-breadcum">
      <div class="container breadcum_container">
         <h1 class="white f_light">Facilitating Investment In Renewable Energy</h1>
         <a href="index.php" class="white f_light" title="Home">Home</a>
       <span class="icon-1256495 white"></span>
        <span class="white f_medium">About</span>
      </div>        
   </section>
 
<section class="about-main" id="overview">
  <img src="images/blue-net.png" alt="blue-net" class="blue-net lazy-image">
  <div class="container">
    <div class="row">
      <div class="col-lg-6 col-md-7 order-md-1 order-2">
        <div class="about-left">
          <h1 class="f_light black" >MEC Solar is a division of <span class="blue f_light">Mercury Electronic Corporation</span> founded in 2016 headquartered in Bangalore, India.</h1>
          <p class="f_book">To cater to the needs of the new and emerging Indian solar market, MEC has invested in world class facilities and technologies to cater to the demands of Solar pumps and industrial pumps across the Indian sub-continent. Our world class facilities in India are fully equipped to advise customers, design, test, fabricate and quality-assure on all aspects of fluid mechanics in India. Our manufacturing plant in Bangalore, is a showcase of immaculate manufacturing and engineering practices!</p>
        </div>
      </div>
      <div class="col-lg-6 col-md-5 order-md-2 order-1">
        <div class="img-wrapp" data-aos="zoom-in" data-aos-delay="300">
          <img src="images/about-main.png" alt="About Image lazy-image">
        </div>
      </div>
       <div class="col-md-12 order-md-3 order-3">
      <div class="make-in-india">
      <div class="img-wrapper" data-aos="zoom-in" data-aos-delay="400">
        <img src="images/make-in-india.png" alt="make-in-india" class="img-fluid w-100">
      </div>
      <h2 class="f_light blue" data-aos="fade-up" data-aos-delay="500"><img src="images/quote-top.png" alt="Quote-top" class="quote-top">Global Sourcing <span class="f_bold">Re-Defined<img src="images/quote-bottom.png" alt="Quote-bottom" class="quote-bottom"></span></h2>
      </div>
    </div>
    </div>
  </div>
</section>
<!-- vision mission -->
<section class="vision" id="vision-mission">
  <div style="background: url(images/vision-bg.jpg); background-repeat: no-repeat; background-size: cover; background-position: center center;" class="background-changer"> 
    <a href="#" class="lazy-image" data-background="images/vision-bg1.jpg" data-aos="fade-up" data-aos-delay="100">
      <div class="caption">
       <span class="new-icon-vision-h"></span>
        <h1 class="white f_book">Vision</h1>
        <span class="icon-go-back-left-arrow"></span>
        <p class="f_light white">To be within Best Two company in India’s DC Pump Manufacturer and within top 5 company in the world by innovation and best solutions to our customers.</p>
      </div>
    </a>
    <a href="#" class="lazy-image" data-background="images/mission-bg.jpg" data-aos="fade-up" data-aos-delay="200">
      <div class="caption">
        <span class="new-icon-mission-h"></span>
        <h1 class="white f_book">Mission</h1>
        <span class="icon-go-back-left-arrow"></span>
        <p class="f_light white">We’re on a mission to leverage our core technology-expertise to address unmet national and international needs, including critical sustainable development challenges such as water, energy & climate.</p>
      </div>
    </a>
    <a href="#" class="lazy-image" data-background="images/values-bg.jpg" data-aos="fade-up" data-aos-delay="300">
      <div class="caption">
        <span class="new-icon-values2"></span>
        <h1 class="white f_book">Values</h1>
        <span class="icon-go-back-left-arrow"></span>
        <p class="f_light white">To be Open, Trust worthy and Loyal…We do Quality work with great team work</p>
      </div>
    </a> 
    <div class="clearfix"></div>
  </div>
</section>
<!-- out certification -->
<section class="certification" id="certification">
  <div class="parallax-container">
    <div class="parallax">
      <img src="images/certification.jpg" class="parallax-img img-fluid lazy-image" alt="Parallax Image">
      <div class="container" data-aos="fade-up" data-aos-delay="100">
        <div class="row">
          <div class="col-lg-3 col-md-12 ">
            <h1 class="f_light white">Our <br> Certifications</h1>
          </div>
          <div class="col-lg-9 col-md-12">
          <div class="owl-carousel owl-theme certificate-slider lightgallery">
            <div class="item">
              <div class="certificate-single">
                
                <img src="images/iso.png" alt="" class="lazy-image">
            

              </div>
               <a href="images/ISO-Certificate.pdf" class="download" target="_blank"> Download</a>
            </div>
             <div class="item">
              <div class="certificate-single">
                 
                <img src="images/ul.png" alt="" class="lazy-image">
             
              </div>
              <a href="images/Certificates.pdf" class="download" target="_blank"> Download</a>
            </div>
            <div class="item">
              <div class="certificate-single">
                 
                <img src="images/tul.png" alt="" class="lazy-image">
             
              </div>
            </div>
            <div class="item">
              <div class="certificate-single">
                
                <img src="images/mnre.png" alt="" class="lazy-image">
              
              </div>
            </div>
           
          </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</section>
<!-- Testimonials -->

<section class="leadership" id="leadership">
<h1 class="heading text-center blue f_light">Leadership</h1>
  <div class="owl-carousel owl-theme testimonials-slider">
    <div class="item">
      <div class="item-wrapp">
        <div class="img-wrapp">
          <img src="images/testi4.png" alt="Testi" class="lazy-image">
        </div>
        <div class="right-text">
          <h1 class="f_medium">Mrs. Asha Rani</h1>
          <h2 class="f_book">Founder and Director of MEC</h2>
          <p class="f_book">Having a great vision to set up an industry in Electronics and Renewable energy, Result is she founded MEC in 2015 as a trading Industry in Electronics products. Later in 2016 started “Designing and manufacturing of Solar Motors” along with Dr. Shiva Kumar.</p>
          <p class="f_book">She is having more than 12 years rich experience in Electronics industry with expertise with Sourcing and Supply chain management and Business development Electronic products.</p>
          <p class="f_book">She holds her MBA in Material Management from ICFAI.</p>
          <p class="f_book">Her experience in leadership roles was in “Digital Circuits, Kaynes Technology, and SM Electronics”.</p>
        </div>
        <img src="images/quoteblue-top.png" alt="quoteblue-top" class="quote-top">
        <img src="images/quoteblue-bottom.png" alt="quoteblue-top" class="quote-bottom">
      </div>
    </div>
    <div class="item">
      <div class="item-wrapp">
        <div class="img-wrapp">
          <img src="images/testi3.png" alt="Testi" class="lazy-image">
        </div>
        <div class="right-text">
          <h1 class="f_medium">Dr. Shiva Kumar HM</h1>
          <h2 class="f_book">Co-Founder</h2>
          <p class="f_book">Having a vision to grow the organisation with 14+ Years of Experience in Product development like Pumps, Solar Panels, Grid and Sync Panels, Inerter and Controllers and Business set up in different Organisations. Under his Guidance Organisation is having a high growth year on year.</p>
          <p class="f_book">His Strategy and Operational excellence are extraordinary. He is an Expert Lean Manufacturing and a certified Six Sigma Black Belt expert. His potentials are very high in Solar Industry in Establishing Solar Power Projects.</p>
          <p class="f_book">Dr. Shiva Kumar was a MBA Graduate and acquired his PhD.in Operations from Mumbai University in the year 2014.</p>
          <p class="f_book">His experience was in leadership roles with “Videocon Industries, Wipro Limited, Reliance Industries and Relyon Solar”.</p>
        </div>
        <img src="images/quoteblue-top.png" alt="quoteblue-top" class="quote-top">
        <img src="images/quoteblue-bottom.png" alt="quoteblue-top" class="quote-bottom">
      </div>
    </div>
     <div class="item">
      <div class="item-wrapp">
        <div class="img-wrapp">
          <img src="images/testi1.png" alt="Testi" class="lazy-image">
        </div>
        <div class="right-text">
          <h1 class="f_medium">Mr. A. N. Vishwanath</h1>
          <h2 class="f_book">Chief Operations Officer</h2>
          <p class="f_book">Top Notch Technology Leader with a long and proven track record of successful business delivery with high performing teams and wide experience of 25+ years in emerging technologies like  Aerospace & Defence, Precision Segment, Start-up specialist, risk taker, team/ enterprise builder, consummate, result oriented and highly experienced leader with established credentials.</p>
          <p class="f_book">Worked in diverse environments of “Manufacturing, Quality Assurance, Business Management, Product Management, Project/Program/Release Management, Operations Management, etc..” in different levels as “Team member, Team leader, Manager, Executive” in areas of “Aerospace & Defence, Medical Device, Light Engineering, Tool room etc..” and geographies “India, US, MEAPAC, etc..”.</p>
          <p class="f_book">He was associated with organisations such “SASMOS, Fokker Elmo SASMOS, Bio-Rad Medisys, SIKA, GTTC, Timex”.</p>
          <p class="f_book">He is a Tooling Engineer from GTTC and he has reached to his current state with Hard Work, Dedication, Team build and Quality of work with more than 25+ years of experience.</p>
        </div>
        <img src="images/quoteblue-top.png" alt="quoteblue-top" class="quote-top">
        <img src="images/quoteblue-bottom.png" alt="quoteblue-top" class="quote-bottom">
      </div>
    </div>
    <div class="item">
      <div class="item-wrapp">
        <div class="img-wrapp">
          <img src="images/testi2.png" alt="Testi" class="lazy-image">
        </div>
        <div class="right-text">
          <h1 class="f_medium">Dr. P. Venkateshwarulu</h1>
          <h2 class="f_book">Consultant</h2>
          <p class="f_book">Dr. P. Venkateswarlu is M. Sc. in Engineering Physics & Ph. D in Material Science from Indian Institute of Science. He has worked 23 years in ISRO in the area of Development of solar cells, Solar Modules for Satellite Applications. He is having more than 30 years’ experience with Design, production, R&D, process, quality management, of solar cell, Solar Module manufacturing & PV Power plants design installation and technology experience with ISRO, Moser Baer, Reliance Industries and different other  industries. He is an expert in Designing of rooftop solar plants, he has designed and installed 60MW of rooftop as well as ground based solar PV systems.</p>
          <p class="f_book">“He has Published 19 papers in national and international journals”.</p>
        </div>
        <img src="images/quoteblue-top.png" alt="quoteblue-top" class="quote-top">
        <img src="images/quoteblue-bottom.png" alt="quoteblue-top" class="quote-bottom">
      </div>
    </div>
   
  </div>
</section>
     <?php include("footer.php"); ?>
     <script type="text/javascript">
       $(document).ready(function(){
 
    $('.certificate-slider').owlCarousel({
        loop:false,
        margin:40,
        dots:false,
        nav:true,
        smartSpeed:800,
        navText: ['<span class="icon-1256495"></span>','<span class="icon-1256495"></span>'], 
        autoplay:false,
        autoplayTimeout:5000,
        responsive:{
          0:{
            items:2,
             margin:0,
          },
          768:{
            items:4,
            margin:10,
          },
          992:{
            items:4,
            margin:10,
          },
          1200:{
            items:4
          }
        }
      });
      // testimonials-slider
       $('.testimonials-slider').owlCarousel({
        loop:false,
        dots:false,
        nav:true,
         items:3,
        center:true,
        navText: ['<span class="icon-1256495"></span>','<span class="icon-1256495"></span>'], 
         autoplay: false,
        smartSpeed: 900,
        responsive:{
           0:{
            items:1,
            center:false,
          },
          600:{
            items:1,
            center:false,
          },
          768:{
            items:2,
          },
          992:{
            items:3,
          },
          1200:{
            items:3
          }
        }
      });
    });
       
    // background change on mouse hover 
      $('.background-changer').on('mouseover', 'a', function () {
        var background = "url('" + $(this).attr('data-background') + "')";
        setTimeout(function () {
          $('.background-changer').css('background-image', background);
        }, 500);
      });

        // hash change scroll top on same page
        var hash = window.location.hash;
        if(hash!='') {
          $('html, body').animate({
            scrollTop: $(hash).offset().top - ($('header').height() - 30)
          }, 1000);
        }

        // scroll smooth on same page
        $( window ).on( 'hashchange', function( e ) { 
          var hash = window.location.hash;
          if(hash!='') {
            $('html, body').animate({
              scrollTop: $(hash).offset().top - ($('header').height() - 0)
            }, 800);
          }
        });
     </script>
<link rel="stylesheet" type="text/css" href="css/lightgallery.css">
<script type="text/javascript" src="js/lightgallery.js"></script>
     <script type="text/javascript">
    $(document).ready(function(){
      $('.lightgallery').lightGallery({
        selector:".img-box",
        zoom:false,
        thumbnail:false,
        autoplay:false,
        fullScreen:false,
        autoplayFirstVideo:false,
        progressBar:false,
        share:false,
        download: false,
        autoplayControls:false,
        loop:false,
      });
    });
  </script>
</body>
</html>